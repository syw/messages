USE `messagesapplogs`;

CREATE TABLE `log` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `Date` datetime NOT NULL,
  `Thread` varchar(4) NOT NULL,
  `Level` varchar(20) NOT NULL,
  `Logger` varchar(100) NOT NULL,
  `Message` varchar(2000) NOT NULL,
  `UserName` varchar(100) DEFAULT NULL,
  `Domain` varchar(100) DEFAULT NULL,
  `MachineName` varchar(45) DEFAULT NULL,
  `FileName` varchar(100) DEFAULT NULL,
  `Class` varchar(100) DEFAULT NULL,
  `Method` varchar(100) DEFAULT NULL,
  `LineNumber` varchar(10) DEFAULT NULL,
  `Exception` varchar(4000) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;


DELIMITER $$
CREATE PROCEDURE `sp_log_data`(logDate DATETIME, thread NVARCHAR(4), errorLevel NVARCHAR(20), logger NVARCHAR(100), message NVARCHAR(2000), userName NVARCHAR(100), domain NVARCHAR(100), machineName NVARCHAR(45), fileName NVARCHAR(100), class NVARCHAR(100), method NVARCHAR(100), lineNumber NVARCHAR(10), exception NVARCHAR(4000))
BEGIN

	INSERT INTO `messagesapplogs`.`log` (`Date`,`Thread`,`Level`,`Logger`,`Message`,`UserName`,`Domain`,`MachineName`,`FileName`,`Class`,`Method`,`LineNumber`,`Exception`)
	VALUES (logDate, thread, errorLevel, logger, message, username, domain, machinename, filename, class, method, linenumber, exception);

END$$
DELIMITER ;
