﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;

namespace Mongo
{
	public class BaseEntity<T>
	{
		[BsonId]
		public T _id { get; set; }
	}
}
