using System.Configuration;
using MongoDB.Driver;

namespace Mongo
{
	public class MongoAppHarborDatabaseProvider : IMongoDatabaseProvider
	{
		private IMongoDatabase _database;
		private object _lock = new object();

		public string ConnectionString
		{
			get { return ConfigurationManager.AppSettings.Get("MONGOLAB_URI"); }
		}

		public MongoUrlBuilder Connection
		{
			get
			{
				return new MongoUrlBuilder
					(
						ConfigurationManager.AppSettings.Get("MONGOLAB_URI")
					);
			}
		}

		public static bool IsViaAppHarbor
		{
			get { return !string.IsNullOrEmpty(ConfigurationManager.AppSettings.Get("MONGOLAB_URI")); }
		}

		public IMongoDatabase Database
		{
			get
			{
				if (_database == null)
				{
					lock (_lock)
					{
						if (_database == null)
						{
							var client = new MongoClient(ConnectionString);
							_database = client.GetDatabase(Connection.DatabaseName);
						}
					}
				}
				return _database;
			}
		}
	}
}