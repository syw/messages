using System.Configuration;
using MongoDB.Driver;

namespace Mongo
{
	public interface IMongoDatabaseProvider
	{
		IMongoDatabase Database { get; }
		MongoUrlBuilder Connection { get; }
		string ConnectionString { get; }
	}

	public class MongoDatabaseProvider : IMongoDatabaseProvider
	{
		private volatile IMongoDatabase _database;
		private string _conntectionString = ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString;
		private readonly object _lock = new object();


		public MongoUrlBuilder Connection
		{
			get { return new MongoUrlBuilder(_conntectionString); }
		}

		public string ConnectionString
		{
			get { return _conntectionString ?? (_conntectionString = ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString); }
		}
		
		public IMongoDatabase Database
		{
			get
			{
				if (_database == null)
				{
					lock (_lock)
					{
						if (_database == null)
						{
							var client = new MongoClient(ConnectionString);
							_database = client.GetDatabase(Connection.DatabaseName);
						}
					}
				}
				return _database;
			}
		}
	}
}