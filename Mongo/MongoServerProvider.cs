using System.Configuration;
using MongoDB.Driver;

namespace Mongo
{
	public interface IMongoServerProvider
	{
		MongoUrlBuilder Connection { get; }
		string ConnectionString { get; }
	}

	public class MongoServerProvider : IMongoServerProvider
	{
		public MongoUrlBuilder Connection
		{
			get { return new MongoUrlBuilder(ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString); }
		}

		public string ConnectionString
		{
			get { return ConfigurationManager.ConnectionStrings["MongoDB"].ConnectionString; }
		}
	}
}