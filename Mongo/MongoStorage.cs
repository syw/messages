using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using log4net;

namespace Mongo
{
	public interface IMongoStorage<T, TY> where T : BaseEntity<TY>
	{
		void Upsert(T instance);
		bool Update(TY id, UpdateDefinition<T> updateDefinition);
		void UpdateBulk(FilterDefinition<T> filterDefinition, UpdateDefinition<T> updateDefinition);
		T Insert(T instance);
		void InsertBulk(List<T> list);
		void Remove(FilterDefinition<T> filterDefinition);
		IList<T> GetAll();
		List<BsonDocument> SelectAs(FilterDefinition<T> filterDefinition, ProjectionDefinition<T> projection, SortDefinition<T> sort);
		List<BsonDocument> SelectAs(FilterDefinition<T> filterDefinition, ProjectionDefinition<T> projection);
		T GetFirstOrDefault(FilterDefinition<T> filterDefinition, SortDefinition<T> sort);
		T GetFirstOrDefault(FilterDefinition<T> filterDefinition);
		BsonDocument GetFirstOrDefault(FilterDefinition<T> filterDefinition, ProjectionDefinition<T> projection);
		List<T> GetList(FilterDefinition<T> filterDefinition, SortDefinition<T> sort);
		List<T> GetList(FilterDefinition<T> filterDefinition);
		IMongoCollection<T> GetMongoCollection();
		void DropCollection();
		string TableName();
		void UpdateBulk(List<UpdateOneModel<T>> filterDefinition, BulkWriteOptions updateDefinition);
	}

	public class MongoStorage<T, TY> : IMongoStorage<T, TY>
		where T : BaseEntity<TY>
	{
		private readonly IMongoDatabaseProvider _mongoDatabaseProvider;
		private readonly IMongoCollection<T> _collection;
		private readonly ILog _logger;

		public MongoStorage(IMongoDatabaseProvider mongoDatabaseProvider, ILog logger)
		{
			_mongoDatabaseProvider = mongoDatabaseProvider;
			_logger = logger;
			_collection = _mongoDatabaseProvider.Database.GetCollection<T>(TableName());
		}

		public void DropCollection()
		{
			RetryOnFailure(() => _mongoDatabaseProvider.Database.DropCollection(TableName()), "DropCollection");
		}

		public IMongoCollection<T> GetMongoCollection()
		{
			return RetryOnFailure(() => _collection, "GetMongoCollection");
		}

		public void Upsert(T instance)
		{
			RetryOnFailure(() =>
				               {
					               _collection.ReplaceOneAsync(x => x._id.Equals(instance._id), instance, new UpdateOptions
						                                                                                      {
							                                                                                      IsUpsert = true
						                                                                                      });
				               }, "Upsert");
		}

		public List<T> GetList(FilterDefinition<T> filterDefinition, SortDefinition<T> sort)
		{
			return RetryOnFailure(() =>
				                      {
					                      var result = _collection.Find(filterDefinition).Sort(sort);
					                      return result.ToListAsync().Result;
				                      }, "GetList");
		}

		public List<T> GetList(FilterDefinition<T> filterDefinition)
		{
			return RetryOnFailure(() =>
				                      {
					                      var collection = _collection.Find(filterDefinition);

					                      return collection == null ? null : collection.ToListAsync().Result;
				                      }, "GetList");
		}

		public void UpdateBulk(FilterDefinition<T> filterDefinition, UpdateDefinition<T> updateDefinition)
		{
			RetryOnFailure(() => _collection.UpdateManyAsync(filterDefinition, updateDefinition).Wait(), "UpdateBulk");
		}

		public bool Update(TY id, UpdateDefinition<T> updateDefinition)
		{
			return RetryOnFailure(() => _collection.UpdateOneAsync(x => x._id.Equals(id), updateDefinition).Result.ModifiedCount > 0, "Update");
		}

		public T Insert(T instance)
		{
			return RetryOnFailure(() =>
				                      {
					                      _collection.InsertOneAsync(instance).Wait();
					                      return instance;
				                      }, "Insert");
		}

		public void InsertBulk(List<T> list)
		{
			RetryOnFailure(() => _collection.InsertManyAsync(list).Wait(), "InsertBulk");
		}

		public void Remove(FilterDefinition<T> filterDefinition)
		{
			RetryOnFailure(() => { _collection.DeleteManyAsync(filterDefinition); }, "Remove");
		}

		public IList<T> GetAll()
		{
			var filter = Builders<T>.Filter.Empty;
			return RetryOnFailure(() => GetList(filter), "GetAll");
		}

		public string TableName()
		{
			return RetryOnFailure(() => typeof (T).Name.ToLower(), "TableName");
		}

		public void UpdateBulk(List<UpdateOneModel<T>> updateModels, BulkWriteOptions bulkWriteOptions = null)
		{
			_collection.BulkWriteAsync(updateModels, bulkWriteOptions);
		}

		public List<BsonDocument> SelectAs(FilterDefinition<T> filterDefinition, ProjectionDefinition<T> projection)
		{
			return RetryOnFailure(() => _collection.Find(filterDefinition).Project(projection).ToListAsync().Result,
			                      "SelectAs");
		}

		public List<BsonDocument> SelectAs(FilterDefinition<T> filterDefinition, ProjectionDefinition<T> projection, SortDefinition<T> sort)
		{
			return RetryOnFailure(() =>
				                      {
					                      var result = _collection.Find(filterDefinition).Sort(sort);
					                      return result.Project(projection).ToListAsync().Result;
				                      }, "SelectAs");
		}

		public T GetFirstOrDefault(FilterDefinition<T> filterDefinition)
		{
			return RetryOnFailure(() => _collection.Find(filterDefinition).FirstOrDefaultAsync().Result, "GetFirstOrDefault");
		}

		public T GetFirstOrDefault(FilterDefinition<T> filterDefinition, SortDefinition<T> sort)
		{
			return RetryOnFailure(() =>
				                      {
					                      var result = _collection.Find(filterDefinition).Sort(sort);
					                      return result.FirstOrDefaultAsync().Result;
				                      }, "GetFirstOrDefault");
		}

		public BsonDocument GetFirstOrDefault(FilterDefinition<T> filterDefinition, ProjectionDefinition<T> projection)
		{
			return RetryOnFailure(() => _collection.Find(filterDefinition).Project(projection).FirstOrDefaultAsync().Result, "GetFirstOrDefault");
		}

		private TResult RetryOnFailure<TResult>(Func<TResult> action, string actionNameForLog)
		{
			var exception = new Exception("MongoStorage");
			for (var counter = 1; counter < 2; counter++)
			{
				try
				{
					return action();
				}
				catch (Exception ex)
				{
					if (_logger != null)
						_logger.WarnFormat("MongoStorage<{0}> exception on {1}, attempt #{2}: {3}",
						                   typeof (T).Name, actionNameForLog, counter, ex.Message);

					exception = ex;
				}
			}

			throw exception;
		}

		private void RetryOnFailure(Action action, string actionNameForLog)
		{
			RetryOnFailure(() =>
				               {
					               action();
					               return 0; //needed for the overload to work
				               }, actionNameForLog);
		}


	}
}