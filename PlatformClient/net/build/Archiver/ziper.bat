@echo off

set SCRIPT_PATH=%~dp0

set PACKAGE=%1
set NETAPP_FOLDER=%2

set PACKAGE_NAME=


for %%i in (%PACKAGE:\= %) do set PACKAGE_NAME=%%i

echo.
echo Ready to prepare package %NETAPP_FOLDER%\%PACKAGE_NAME%.zip
pause

%SCRIPT_PATH%\7z.exe a %NETAPP_FOLDER%\%PACKAGE_NAME%.zip %PACKAGE% > nul
%SCRIPT_PATH%\md5sums.exe -u %NETAPP_FOLDER%\%PACKAGE_NAME%.zip > %NETAPP_FOLDER%\%PACKAGE_NAME%.md5
