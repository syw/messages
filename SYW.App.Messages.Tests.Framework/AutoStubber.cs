﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Rhino.Mocks;
using SYW.App.Messages.Web.Services;

namespace SYW.App.Messages.Tests.Framework
{
	public class AutoStubber<T> where T : class
	{
		static readonly Type TypeofT;
		static readonly ConstructorInfo Constructor;
		static readonly Type[] ParameterTypes;
		static readonly Dictionary<object, AutoStubber<T>> Instances = new Dictionary<object, AutoStubber<T>>();

		static AutoStubber()
		{
			TypeofT = typeof (T);
			Constructor = TypeofT.GetConstructors().OrderByDescending(ci => ci.GetParameters().Length).First();
			ParameterTypes = Constructor.GetParameters().Select(pi => pi.ParameterType).ToArray();
		}

		public static AutoStubber<T> GetStubberFor(T obj)
		{
			return Instances[obj];
		}

		bool _created;

		/// <summary>
		/// Instructs the stubber to use the specified instance as an implementor for the specified dependency type.
		/// </summary>
		/// <typeparam name="TDependency">The type of dependency that this instance implements.</typeparam>
		/// <param name="instance">The instance to use for the dependency.</param>
		/// <returns><c>this</c>, to allow fluent usage.</returns>
		/// <remarks>You must explicitly supply the type of the dependency; there is no type inference support.</remarks>
		public AutoStubber<T> Use<TDependency>(object instance)
		{
			var dependencyType = typeof (TDependency);
			if (_dependencies.ContainsKey(dependencyType)) throw new InvalidOperationException(string.Format("A stub for type {0} has already been created; To use an explicit instance, call Use() before calling Create().", dependencyType.Name));
			_dependencies[dependencyType] = instance;
			return this;
		}

		/// <summary>
		/// Creates a new instance of type <see cref="T"/> with automatically-stubbed dependencies.
		/// </summary>
		/// <returns>The newly created instance.</returns>
		public T Create()
		{
			if (_created)
				throw new InvalidOperationException("Create can only be called once per AutoStubber");

			Instance = CreateInstance();
			_created = true;
			Instances[Instance] = this;

			return Instance;
		}

		readonly Dictionary<Type, object> _dependencies = new Dictionary<Type, object>();
		private T Instance { get; set; }

		private T CreateInstance()
		{
			var parameters = new List<object>(ParameterTypes.Length);
			foreach (var parameterType in ParameterTypes)
			{
				if (!_dependencies.ContainsKey(parameterType))
				{
					_dependencies[parameterType] = CreateStub(parameterType);
				}
				var parameter = _dependencies[parameterType];
				parameters.Add(parameter);
			}
			return (T)Constructor.Invoke(parameters.ToArray());
		}

		private object CreateStub(Type parameterType)
		{
			// Array dependencies become empty
			if (parameterType.IsArray)
				return Array.CreateInstance(parameterType.GetElementType(), 0);

			return MockRepository.GenerateStub(parameterType);
		}

		public TDependency Get<TDependency>()
		{
			object dependency;
			if (_dependencies.TryGetValue(typeof (TDependency), out dependency) == false)
				throw new ArgumentException(
					string.Format("You cannot ask for {0}, because it is not a constructor dependency of {1}",
								typeof (TDependency), typeof (T)));
			return (TDependency)_dependencies[typeof (TDependency)];
		}
	}

	public static class AutoStubberExtensions
	{
		public static AutoStubber<T> Stubs<T>(this T obj)
			where T : class
		{
			return AutoStubber<T>.GetStubberFor(obj);
		}

		public static void ImpersonateUser<T>(this T obj, long entityId)
			where T : class
		{
			obj.ImpersonateEntity(entityId, EntityType.User);
		}

		public static void ImpersonateEntity<T>(this T obj, long entityId, EntityType entityType)
			where T : class
		{
			obj.Stubs().Get<IEntityContextProvider>().Stub(x => x.CurrentEntity())
				.Return(new EntityContext {EntityType = entityType, OriginalId = entityId});
		}
	}
}