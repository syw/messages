﻿using System;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.IntegrationTests.Conversations
{
	[TestFixture]
	public class ConversationStatusServiceTests : IntegrationTestBase
	{
		private IConversationStatusService _target;

		private IConversationRepository _conversationRepository;
		private IMessageRepository _messageRepository;
		private IUserConversationStatusRepository _userConversationStatusRepository;
		private IUserMessageStatusRepository _userMessageStatusRepository;
		private IMongoCleaner _mongoCleaner;

		private IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private IMongoStorage<UserConversationStatus, ObjectId> _userConversationStatusStorage;
		private IMongoStorage<MessageDto, ObjectId> _messagesStorage;
		private IMongoStorage<UserMessageStatus, ObjectId> _userMessageStatusStorage;
		private IConversationService _conversationService;

		private Entity _user1;
		private Entity _user2;
		private Entity _systemMesagesSource;

		[SetUp]
		public void SetUp()
		{
			_target = Resolve<IConversationStatusService>();

			_messageRepository = Resolve<IMessageRepository>();
			_conversationRepository = Resolve<IConversationRepository>();
			_userConversationStatusRepository = Resolve<IUserConversationStatusRepository>();
			_userMessageStatusRepository = Resolve<IUserMessageStatusRepository>();
			_mongoCleaner = Resolve<IMongoCleaner>();

			_conversationStorage = Resolve<IMongoStorage<ConversationDto,ObjectId>>();
			_userConversationStatusStorage = Resolve<IMongoStorage<UserConversationStatus, ObjectId>>();
			_messagesStorage = Resolve<IMongoStorage<MessageDto, ObjectId>>();
			_userMessageStatusStorage = Resolve<IMongoStorage<UserMessageStatus, ObjectId>>();
			_conversationService = Resolve<IConversationService>();

			_mongoCleaner.Clean();
			_user1 = IntegrationHelper.CreateEntity(1, "gilly");
			_user2 = IntegrationHelper.CreateEntity(2, "doron");
			_systemMesagesSource = IntegrationHelper.CreateEntity(0, "Lands' End");
		}

		[Test]
		public void MarkAllAsRead_WhenHasNewConversation_MarkItAsRead()
		{
			var newConversation = CreateConversation();

			_target.MarkAllAsRead(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(1));
			var conversation = conversations.Single();
			Assert.That(conversation.Id, Is.EqualTo(newConversation.Id));
			Assert.That(conversation.Status, Is.EqualTo(ConversationStatus.Read));
		}

		[Test]
		public void MarkAllAsRead_WhenHasUnreadConversation_MarkItAsRead()
		{
			var newConversation = CreateConversation(ConversationStatus.Unread);

			_target.MarkAllAsRead(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(1));
			var conversation = conversations.Single();
			Assert.That(conversation.Id, Is.EqualTo(newConversation.Id));
			Assert.That(conversation.Status, Is.EqualTo(ConversationStatus.Read));
		}

		[Test]
		public void MarkAllAsRead_WhenHasNewSystemConversation_MarkItAsRead()
		{
			var newConversation = CreateSystemConversation();

			_target.MarkAllAsRead(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(1));
			var conversation = conversations.Single();
			Assert.That(conversation.Id, Is.EqualTo(newConversation.Id));
			Assert.That(conversation.Status, Is.EqualTo(ConversationStatus.Read));
		}

		[Test]
		public void MarkAllAsRead_WhenHasUnreadSystemConversation_MarkItAsRead()
		{
			var newConversation = CreateSystemConversation(ConversationStatus.Unread);

			_target.MarkAllAsRead(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(1));
			var conversation = conversations.Single();
			Assert.That(conversation.Id, Is.EqualTo(newConversation.Id));
			Assert.That(conversation.Status, Is.EqualTo(ConversationStatus.Read));
		}

		[Test]
		public void ArchiveAll_WhenHasNewConversation_ArchivesIt()
		{
			CreateConversation();

			_target.ArchiveAll(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(0));
		}

		[Test]
		public void ArchiveAll_WhenHasUnreadConversation_ArchivesIt()
		{
			CreateConversation(ConversationStatus.Unread);

			_target.ArchiveAll(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(0));
		}

		[Test]
		public void ArchiveAll_WhenHasNewSystemConversation_ArchivesIt()
		{
			CreateSystemConversation();

			_target.ArchiveAll(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(0));
		}

		[Test]
		public void ArchiveAll_WhenHasUnreadSystemConversation_ArchivesIt()
		{
			CreateSystemConversation(ConversationStatus.Unread);

			_target.ArchiveAll(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(0));
		}

		[Test]
		public void MarkAllAsRead_SystemConversationWithStatusDateLessThanLastMessageDate_ConversationIsMarkedAsRead()
		{
			var conversationId = CreateSystemConversation(_systemMesagesSource, GetUtcDate(2013, 5, 8), GetUtcDate(2013, 9, 24));
			CreateConversationStatus(conversationId, _user1._id, GetUtcDate(2013, 9, 25), ConversationStatus.Read);
			var messageId = CreateMessage(conversationId, _systemMesagesSource._id, GetUtcDate(2013, 10, 31), GetUtcDate(2013, 11, 7));
			CreateMessageStatus(messageId, conversationId, _user1._id, ConversationStatus.Read, GetUtcDate(2012, 11, 3));

			_target.MarkAllAsRead(_user1._id);

			var conversations = _conversationService.GetAll(_user1._id, GetUtcDate(2013, 11, 3), 1, 20);
			Assert.That(conversations, Has.Count.EqualTo(1));
			var conversation = conversations.Single();
			Assert.That(conversation.Id, Is.EqualTo(conversationId));
			Assert.That(conversation.Status, Is.EqualTo(ConversationStatus.Read));
		}

		private ConversationDescriptor CreateConversation(ConversationStatus conversationStatus = ConversationStatus.New)
		{
			var conversation = _conversationRepository.Create(ObjectId.GenerateNewId(), new[] { _user1._id, _user2._id });
			_messageRepository.Create(conversation.Id, _user1._id, "bla bla bla");

			_userConversationStatusRepository.UpdateStatus(conversation.Id, _user1._id, conversationStatus);

			return conversation;
		}

		private ConversationDescriptor CreateSystemConversation(ConversationStatus conversationStatus = ConversationStatus.New)
		{
			var conversation = _conversationRepository.CreateSystemConversation(ObjectId.GenerateNewId());
			var systemMessage = new SystemMessage {AuthorId = _systemMesagesSource._id, Content = "testing", IsHtml = false, PreviewText = "testing", endDate = DateTime.MaxValue};
			var message = _messageRepository.CreateSystemMessage(conversation.Id, systemMessage);

			_userMessageStatusRepository.UpdateStatus(conversation.Id, message.Id, _user1._id, (int)conversationStatus);

			return conversation;
		}

		private DateTime GetUtcDate(int year, int month, int day)
		{
			return DateTime.SpecifyKind(new DateTime(year, month, day), DateTimeKind.Utc);
		}

		private void CreateMessageStatus(ObjectId messageId, ObjectId conversationId, ObjectId userId, ConversationStatus status, DateTime date)
		{
			var userMessageStatus = new UserMessageStatus
										{
											_id = ObjectId.GenerateNewId(),
											ConversationId = conversationId,
											MessageId = messageId,
											UserId = userId,
											Status = (int)status,
											Date = date,
                                            CreatedDateTime = date
										};

			_userMessageStatusStorage.Insert(userMessageStatus);
		}

		private ObjectId CreateMessage(ObjectId conversationId, ObjectId authorId, DateTime date, DateTime endDate)
		{
			var messageDto = new MessageDto
								{
									ConversationId = conversationId,
									AuthorId = authorId,
									Date = date,
									EndDate = endDate,
									Content = "<html><body>hihihi</body></html>",
									ContentType = 1,
									PreviewText = "30% off sweaters",
									ContentHeight = 2086,
									ContentWidth = 1024,
									FeedMessageId = "tag:sears.com,2013-10-31:8f9cb1c5-cbbb-4e7c-ace6-dcae90cade36"
								};

			_messagesStorage.Insert(messageDto);

			return messageDto._id;
		}

		private void CreateConversationStatus(ObjectId converationId, ObjectId userId, DateTime date, ConversationStatus status)
		{
			var userConversationStatus = new UserConversationStatus
											{
												ConversationId = converationId,
												UserId = userId,
												Date = date,
                                                CreatedDateTime = date,
												Status = (int)status
											};

			_userConversationStatusStorage.Insert(userConversationStatus);
		}

		private ObjectId CreateSystemConversation(Entity creator, DateTime startDate, DateTime timestamp)
		{
			var conversationDto = new ConversationDto
									{
										CreatorId = creator._id,
										ParticipantsIds = new[] { creator._id },
										StartDate = startDate,
										//Timestamp = timestamp,
										IsSystemMessage = true
									};

			_conversationStorage.Insert(conversationDto);

			return conversationDto._id;
		}
	}
}