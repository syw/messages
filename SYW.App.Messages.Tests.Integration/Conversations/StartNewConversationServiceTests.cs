﻿using System.Collections.Generic;
using System.Linq;
using NSubstitute;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services.Platform;

namespace SYW.App.Messages.IntegrationTests.Conversations
{
	[TestFixture]
	public class StartNewConversationServiceTests : IntegrationTestBase
	{
		private IUserService _userService;
		private IStartNewConversationService _target;
		private IConversationRepository _conversationRepository;
		private IEntitiesInitializer _entitiesInitializer;
		private IEntitiesResolver _entitiesResolver;
		private IEntitiesRepository _entitiesRepository;
		private IConversationService _conversationService;
		private IMessagesService _messagesService;

		[SetUp]
		public void SetUp()
		{
			_messagesService = Resolve<IMessagesService>();
			_userService = Substitute.For<IUserService>();
			_entitiesRepository = Resolve<IEntitiesRepository>();
			_entitiesInitializer = new EntitiesInitializer(_userService, _entitiesRepository);
			_entitiesResolver = Resolve<IEntitiesResolver>();
			_conversationRepository = Resolve<IConversationRepository>();

			_target = new StartNewConversationService(
				_conversationRepository, _entitiesResolver,
				_entitiesInitializer, _entitiesRepository, _messagesService);

			_conversationService = Resolve<IConversationService>();
		}

		[Test]
		public void Start_StandardMessages_NewConversationAndFirstMessageExist()
		{
			var entity1 = IntegrationHelper.CreateEntity(1, "gilly");
			var entity2 = IntegrationHelper.CreateEntity(2, "nir");
			SetupEntities(new long[] {1, 2});

			var conversationDescriptor = _target.Start(1, new[] {1L, 2}, "It's tricky to rock around");

			var newConversation = _conversationRepository.Get(conversationDescriptor.Id);
			Assert.That(newConversation.Id, Is.EqualTo(conversationDescriptor.Id));
			Assert.That(newConversation.Participants, Has.Count.EqualTo(2));
			Assert.That(newConversation.Participants, Has.Some.Matches<Entity>(x => x._id == entity1._id));
			Assert.That(newConversation.Participants, Has.Some.Matches<Entity>(x => x._id == entity2._id));
		}

		[Test(Description = "6188 - Sending a message to 1 friend is seen at the friend's messages page")]
		public void Start_SendMessageToFriend_FriendHasMessage()
		{
			const long messageCreatorOriginalId = 3;
			const long messageRecipientOriginalId = 4;
			var entity1 = IntegrationHelper.CreateEntity(messageCreatorOriginalId, "gilly");
			var entity2 = IntegrationHelper.CreateEntity(messageRecipientOriginalId, "doron");
			SetupEntities(new long[] {3, 4});

			var conversationDescriptor = _target.Start(messageCreatorOriginalId, new[] {messageCreatorOriginalId, messageRecipientOriginalId}, "to rock around it's right on time, it's tricky");
			Assert.That(conversationDescriptor, Is.Not.Null);
			Assert.That(conversationDescriptor.Id, Is.Not.Null);

			var doronsConversations = _conversationService.GetAll(entity2._id, entity2.MemberSince.Value, 1, 20);
			Assert.That(doronsConversations, Is.Not.Null);
			Assert.That(doronsConversations, Has.Count.EqualTo(1));
			Assert.That(doronsConversations, Has.Some.Matches<ConversationDescriptor>(x => x.Id.Pid == conversationDescriptor.Id.Pid));
		}

		[Test(Description = "6189 - Sending a message to several friends is seen at all the friends' messages pages")]
		public void Start_SendMessageToMultipleFriends_AllOfThemHaveTheSentConversation()
		{
			var entity1 = IntegrationHelper.CreateEntity(5, "gilly");
			var entity2 = IntegrationHelper.CreateEntity(6, "doron");
			var entity3 = IntegrationHelper.CreateEntity(7, "niro");
			SetupEntities(new long[] { 5, 6, 7 });

			var conversationDescriptor = _target.Start(5, new[] { 5L, 6, 7 }, "how is it D ??");

			var entity2Conversations = _conversationService.GetAll(entity2._id, entity2.MemberSince.Value, 1, 20);
			var entity3Conversations = _conversationService.GetAll(entity3._id, entity2.MemberSince.Value, 1, 20);
			Assert.That(entity2Conversations.Any(x => x.Id == conversationDescriptor.Id));
			Assert.That(entity3Conversations.Any(x => x.Id == conversationDescriptor.Id));
		}

		[Test]
		public void Start_SendMessageWithAttachmentToAFriend_FriendHasMessageWithAttachment()
		{
			const long messageCreatorOriginalId = 8;
			const long messageRecipientOriginalId = 9;
			var entity1 = IntegrationHelper.CreateEntity(messageCreatorOriginalId, "gilly");
			var entity2 = IntegrationHelper.CreateEntity(messageRecipientOriginalId, "doron");
			var attachment = new Attachment("Super Dooper Entity", "http://www.google.com", "http://img.spikedmath.com/math-games/games/images/Nyan-Cat-FLY!.png");

			SetupEntities(new [] { messageCreatorOriginalId, messageRecipientOriginalId });

			var conversationDescriptor = _target.Start(messageCreatorOriginalId, new[] { messageCreatorOriginalId, messageRecipientOriginalId }, "This is my electronic press kit", new [] {attachment});
			Assert.That(conversationDescriptor, Is.Not.Null);
			Assert.That(conversationDescriptor.Id, Is.Not.Null);

			var doronsConversations = _conversationService.GetAll(entity2._id, entity2.MemberSince.Value, 1, 20);
			Assert.That(doronsConversations, Is.Not.Null);
			Assert.That(doronsConversations, Has.Count.EqualTo(1));
			Assert.That(doronsConversations, Has.Some.Matches<ConversationDescriptor>(x => x.Id.Pid == conversationDescriptor.Id.Pid));
			Assert.That(doronsConversations.First().Messages, Has.Count.EqualTo(1));
			Assert.That(doronsConversations.First().Messages.First().Attachments, Has.Count.EqualTo(1));
			Assert.That(doronsConversations.First().Messages.First().Attachments.First(), Is.Not.Null);
		}

		[Test]
		public void Start_SendMessageWithoutAttachmentsToAFriend_AttachmentsInFriendMessageIsNull()
		{
			const long messageCreatorOriginalId = 10;
			const long messageRecipientOriginalId = 11;
			var entity1 = IntegrationHelper.CreateEntity(messageCreatorOriginalId, "gilly");
			var entity2 = IntegrationHelper.CreateEntity(messageRecipientOriginalId, "doron");
			SetupEntities(new [] { messageCreatorOriginalId, messageRecipientOriginalId });

			var conversationDescriptor = _target.Start(messageCreatorOriginalId, new[] { messageCreatorOriginalId, messageRecipientOriginalId }, "to rock around it's right on time, it's tricky");
			Assert.That(conversationDescriptor, Is.Not.Null);
			Assert.That(conversationDescriptor.Id, Is.Not.Null);

			var doronsConversations = _conversationService.GetAll(entity2._id, entity2.MemberSince.Value, 1, 20);
			Assert.That(doronsConversations, Is.Not.Null);
			Assert.That(doronsConversations, Has.Count.EqualTo(1));
			Assert.That(doronsConversations, Has.Some.Matches<ConversationDescriptor>(x => x.Id.Pid == conversationDescriptor.Id.Pid));
			Assert.That(doronsConversations.First().Messages, Has.Count.EqualTo(1));
			Assert.That(doronsConversations.First().Messages.First().Attachments, Is.Null);
		}

		private void SetupEntities(IEnumerable<long> ids)
		{
			var fakeUsers = ids.Select(entityId => new SocialUserInfo
														{
															id = entityId,
															name = "gilly" + entityId
														}).ToList();
			_userService.GetUsers(Arg.Any<IList<long>>())
						.Returns(fakeUsers);
		}
	}
}