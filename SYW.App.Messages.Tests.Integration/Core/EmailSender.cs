﻿using System.Net.Mail;

namespace SYW.App.Messages.IntegrationTests.Core
{
    public interface IEmailSender
    {
        bool SendEmail(MailMessage message);
    }

    public class EmailSender : IEmailSender
    {
        private readonly IEmailSenderSettings _emailSenderSettings;

        public EmailSender(IEmailSenderSettings emailSenderSettings)
        {
            _emailSenderSettings = emailSenderSettings;
        }

        public bool SendEmail(MailMessage message)
        {
            var smtpclient = new SmtpClient { Host = _emailSenderSettings.EmailHost, Port = _emailSenderSettings.EmailPort };
            try
            {
                smtpclient.Send(message);
                return true;
            }
            finally
            {
                smtpclient.Dispose();
            }
        }
    }
}
