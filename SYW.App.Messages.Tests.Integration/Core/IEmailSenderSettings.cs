﻿using CommonGround.Settings;

namespace SYW.App.Messages.IntegrationTests.Core
{
    public interface IEmailSenderSettings
    {
        [Default("127.0.0.1")]
        string EmailHost { get; set; }
        
        [Default(25)]
        int EmailPort { get; set; }
    }
}
