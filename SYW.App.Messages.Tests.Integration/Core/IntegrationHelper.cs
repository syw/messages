using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.IntegrationTests.Core
{
	public interface IIntegrationHelper
	{
		Entity CreateEntity(long originalId, string name);
	}

	public class IntegrationHelper : IIntegrationHelper
	{
		private readonly IEntitiesRepository _entitiesRepository;

		public IntegrationHelper(IEntitiesRepository entitiesRepository)
		{
			_entitiesRepository = entitiesRepository;
		}

		public Entity CreateEntity(long originalId, string name)
		{
			var newEntityId = ObjectId.GenerateNewId();
			var newEntity = new Entity
								{
									_id = newEntityId, Name = name, OriginalId = originalId
								};
			_entitiesRepository.Add(newEntity);

			return newEntity;
		}
	}
}