﻿using System;
using System.Reflection;
using CommonGround;
using NUnit.Framework;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.Web.Config;
using SYW.App.Messsages.Domain.DataAccess;
using SYW.App.Messsages.Domain.Services;
using UtilRunner.Utils;

namespace SYW.App.Messages.IntegrationTests.Core
{
	public class IntegrationTestBase
	{
		protected IIntegrationHelper IntegrationHelper;

		private MongoServerForTesting _mongoServerForTesting;

		private ICommonContainer _container;

		[TestFixtureSetUp]
		public virtual void TestFixtureSetUp()
		{
			_container = new CommonContainer();
			var bootstrapper = new Bootstrapper(_container);
			bootstrapper.AddAssemblies(Assembly.GetExecutingAssembly());

			// Need to register the mocked httpcontext
			_container.RegisterInstance(typeof(IHttpContextProvider), new MockedHttpContextProvider());

			var logger = bootstrapper.InitializeLogger();

			logger.Debug("Bootstrapper: registering MongoDb");
			bootstrapper.RegisterMongoDb();
			logger.Debug("Bootstrapper: registering default interfaces");
			bootstrapper.RegisterDefaultInterfaces();
			logger.Debug("Bootstrapper: registering settings Manager");
			bootstrapper.RegisterSettingsManager();
			logger.Debug("Bootstrapper: registering events");
			bootstrapper.RegisterEvents();
			logger.Debug("Bootstrapper: initializing mappers");
			bootstrapper.InitializeMappers();
			logger.Debug("Bootstrapper: initializing integration Helper");
			SetUpIntegrationHelper();

			logger.Debug("Integration TestFixtureSetUp: Starting MongoDb..");
			_mongoServerForTesting = new MongoServerForTesting();
			_mongoServerForTesting.Start();
			logger.Debug("Integration TestFixtureSetUp: Cleaning MongoDb..");
			Resolve<IMongoCleaner>().Clean();

			
		}

		private void SetUpIntegrationHelper()
		{
			try
			{
				IntegrationHelper = Resolve<IIntegrationHelper>();
			}
			catch (Exception ex)
			{
				throw new Exception("initializing integration helper failed.", ex);
			}
		}


		public T Resolve<T>()
		{
			return _container.Resolve<T>();
		}

		public void Register<T>(T instance)
		{
			_container.RegisterInstance(instance);
		}
	}
}