using System.IO;
using System.Web;
using SYW.App.Messsages.Domain.Services;

namespace SYW.App.Messages.IntegrationTests.Core
{
	public class MockedHttpContextProvider : IHttpContextProvider
	{
		public HttpContext GetContext()
		{
			var context = new HttpContext(new HttpRequest("", "http://www.shopyourway.com", ""),
			                              new HttpResponse(new StringWriter()));
			return context;
		}
	}
}