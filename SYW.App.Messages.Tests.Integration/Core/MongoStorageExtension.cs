﻿using Mongo;

namespace SYW.App.Messages.IntegrationTests.Core
{
	public static class MongoStorageExtension
	{
		public static void Clear<T, TY>(this IMongoStorage<T, TY> mongoStorage) where T : BaseEntity<TY>
		{
			mongoStorage.DropCollection();
		}
	}
}