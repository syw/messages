﻿using System.Linq;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.Audits;
using SYW.App.Messsages.Domain.DataAccess.Audits;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Audits
{
	public class AuditRepositoryTests : IntegrationTestBase
	{
		private IAuditRepository _target;
		private IMongoStorage<AuditDto,ObjectId> _storage;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_storage = Resolve<IMongoStorage<AuditDto, ObjectId>>();
			_target = Resolve<IAuditRepository>();
		}

		[Test]
		public void Save_WhenSavingAnAudit_ShouldBeSaved()
		{
			_target.Save(new Audit { ActionPerformed = "Test" });
			var result = _storage.GetAll();

			Assert.That(result.Count, Is.EqualTo(1));
			Assert.That(result.First().ActionPerformed, Is.EqualTo("Test"));
		}
	}
}