﻿using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Conversations
{
	[TestFixture]
	public class ConversationRepositoryTests : IntegrationTestBase
	{
		private IConversationRepository _target;

		private Entity _user1;
		private Entity _user2;
		private Entity _user3;
		private IMongoStorage<ConversationDto, ObjectId> _storage;

		private IMessageRepository _messageRepository;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_storage = Resolve<IMongoStorage<ConversationDto, ObjectId>>();
			_target = Resolve<IConversationRepository>();
			_messageRepository = Resolve<IMessageRepository>();

			_user1 = IntegrationHelper.CreateEntity(1, "gilly");
			_user2 = IntegrationHelper.CreateEntity(2, "nir");
			_user3 = IntegrationHelper.CreateEntity(3, "doron");
		}

		[SetUp]
		public void SetUp()
		{
			_storage.Clear();
		}

		[Test]
		public void Create_NewConversation_IsInMongoDb()
		{
			var newConversation = _target.Create(_user1._id, new[] { _user1._id, _user2._id });

			var conversation = _target.Get(newConversation.Id);

			Assert.That(conversation, Is.Not.Null);
			Assert.That(conversation.Id, Is.EqualTo(newConversation.Id));
		}

		[Test]
		public void GetAll_ManyConversations_ReturnsOnlyRequestedAuthors()
		{
			_target.Create(_user1._id, new[] { _user1._id, _user2._id });
			_target.Create(_user2._id, new[] { _user1._id, _user2._id });
			_target.Create(_user3._id, new[] { _user3._id, _user1._id });
			_target.Create(_user2._id, new[] { _user2._id, _user3._id });

			var usersConversations = _target.GetAll(_user1._id, SystemTime.Now());

			Assert.That(usersConversations, Has.Count.EqualTo(3));
			Assert.That(usersConversations.All(x => x.Participants.Select(s => s._id).Contains(_user1._id)), Is.True);
		}

		[Test]
		public void GetSystemConversationIds_WhenThereAreNoSystemConversations_ShouldReturnEmptyList()
		{
			var result = _target.GetSystemConversationIds();

			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetSystemConversationIds_WhenThereIsASystemConversations_ShouldReturnConversationId()
		{
			var conversation = _target.CreateSystemConversation(_user1._id);

			var result = _target.GetSystemConversationIds().First();

			Assert.That(result, Is.EqualTo(conversation.Id));
		}

		[Test]
		public void GetSystemConversationIds_WhenThereIsASystemConversationsWithMultipleMessages_ShouldReturnSingleConversationId()
		{
			var conversation = _target.CreateSystemConversation(_user1._id);
			_messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage(), _user1._id);
			_messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage(), _user1._id);
			_messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage(), _user1._id);

			var result = _target.GetSystemConversationIds().Single();

			Assert.That(result, Is.EqualTo(conversation.Id));
		}

		[Test]
		public void GetByAuthor_AuthorIdDoesNotExist_ReturnsNull()
		{
			var result = _target.GetByAuthor(ObjectId.GenerateNewId());

			Assert.That(result, Is.Null);
		}

		[Test]
		public void GetPersonalConversationsByParticipantsIds_WhenNoConversationsMatchingParticipantIds_ShouldReturnEmptyList()
		{
			_target.Create(_user1._id, new[] { _user1._id, _user2._id });

			ObjectId[] participantsIds = new[] { _user1._id, _user2._id, _user3._id };

			var result = _target.GetPersonalConversationsByParticipantsIds(participantsIds);

			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetPersonalConversationsByParticipantsIds_WhenConversationsMatchingParticipantIdsExists_ShouldReturnMatchingConversations()
		{
			var newConversation = _target.Create(_user1._id, new[] { _user1._id, _user2._id, _user3._id });

			var result = _target.GetPersonalConversationsByParticipantsIds(new[] { _user1._id, _user2._id, _user3._id });

			Assert.That(result.Any(x => x.Id == newConversation.Id));
		}
	}
}