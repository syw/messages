﻿using System;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Conversations
{
    [TestFixture]
    public class UserConversationStatusRepositoryTests : IntegrationTestBase
    {
        private IConversationRepository _conversationRepository;
        private IUserConversationStatusRepository _userConversationStatusRepository;
        private IMessageRepository _messageRepository;
        private IConversationStatusService _conversationStatusService;
        private IConversationCounterService _conversationCounterService;
        private IConversationService _conversationService;
        private IUserMessageStatusRepository _userMessageStatusRepository;
        private IMongoStorage<ConversationDto, ObjectId> _conversationStorage;

        [SetUp]
        public void SetUp()
        {
            _conversationRepository = Resolve<IConversationRepository>();
            _userConversationStatusRepository = Resolve<IUserConversationStatusRepository>();
            _messageRepository = Resolve<IMessageRepository>();
            _conversationStatusService = Resolve<IConversationStatusService>();
            _conversationCounterService = Resolve<IConversationCounterService>();
            _conversationService = Resolve<IConversationService>();
            _userMessageStatusRepository = Resolve<IUserMessageStatusRepository>();
            _conversationStorage = Resolve<IMongoStorage<ConversationDto, ObjectId>>();
        }

        [Test]
        public void UpdateStatus_ConversationGetsNewStatus()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");
            var conversationId = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversationId, new[] { user1._id, user2._id });

            _userConversationStatusRepository.UpdateStatus(conversationId, user1._id, ConversationStatus.Read);

            var updatedConversationStatus = _userConversationStatusRepository.Get(conversationId, user1._id);
            Assert.That(updatedConversationStatus.Status, Is.EqualTo((int)ConversationStatus.Read));
        }

        [Test]
        public void GetConversationsStatuses_WhenThereAreTwoStatuses_ShouldGetTheTwo()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");

            var conversationId = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversationId, new[] { user1._id, user2._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(conversationId, user2._id, ConversationStatus.Unread);

            var conversationsStatuses = _userConversationStatusRepository.GetConversationsStatuses(new[] { conversationId });

            Assert.That(conversationsStatuses.Count, Is.EqualTo(2));
        }

        [Test]
        public void GetConversationsStatuses_WhenThereAreTwoConversations_ShouldGetTheTwo()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");

            var conversation1 = ObjectId.GenerateNewId();
            var conversation2 = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversation1, new[] { user1._id, user2._id });
            _userConversationStatusRepository.UpdateStatus(conversation1, user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(conversation2, user1._id, ConversationStatus.Unread);

            var conversationsStatuses = _userConversationStatusRepository.GetConversationsStatuses(new[] { conversation1, conversation2 });

            Assert.That(conversationsStatuses.Count, Is.EqualTo(2));
        }

        [Test]
        public void CountConversations_WhenThereAreNoConversations_ReturnZero()
        {
            var user = IntegrationHelper.CreateEntity(1, "gilly");

            var countPersonal = _conversationCounterService.CountPersonalConversations(user._id, new[] { ConversationStatus.Unread });
            var countUnread = _conversationCounterService.CountUnread(user._id);

            Assert.That(countPersonal, Is.EqualTo(0));
            Assert.That(countUnread, Is.EqualTo(0));
        }

        [Test]
        public void CountConversations_WhenThereAreUnreadConversations_ReturnOne()
        {
            var user = IntegrationHelper.CreateEntity(1, "gilly");
            var conversationId = ObjectId.GenerateNewId();

            _conversationRepository.Create(conversationId, new[] { user._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user._id, ConversationStatus.Unread);

            var countPersonal = _conversationCounterService.CountPersonalConversations(user._id, new[] { ConversationStatus.Unread });
            var countUnread = _conversationCounterService.CountUnread(user._id);

            Assert.That(countPersonal, Is.EqualTo(1));
            Assert.That(countUnread, Is.EqualTo(1));
        }

        [Test]
        public void CountConversations_WhenThereAreReadConversations_ReturnZero()
        {
            var user = IntegrationHelper.CreateEntity(1, "gilly");
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user._id, ConversationStatus.Read);

            var countPersonal = _conversationCounterService.CountPersonalConversations(user._id, new[] { ConversationStatus.Unread });
            var countUnread = _conversationCounterService.CountUnread(user._id);

            Assert.That(countPersonal, Is.EqualTo(0));
            Assert.That(countUnread, Is.EqualTo(0));
        }

        [Test]
        public void CountConversations_WhenThereAreUnresolvedConversations_ReturnZero()
        {
            var user = IntegrationHelper.CreateEntity(1, "gilly");
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user._id, ConversationStatus.Unresolved);

            var countPersonal = _conversationCounterService.CountPersonalConversations(user._id, new[] { ConversationStatus.Unread });
            var countUnread = _conversationCounterService.CountUnread(user._id);

            Assert.That(countPersonal, Is.EqualTo(0));
            Assert.That(countUnread, Is.EqualTo(0));
        }

        [Test]
        public void CountConversations_WhenThereAreUnresolvedAndUnreadConversations_ReturnUnreadCount()
        {
            var user = IntegrationHelper.CreateEntity(1, "gilly");
            var conversationId = ObjectId.GenerateNewId();

            _conversationRepository.Create(conversationId, new[] { user._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user._id, ConversationStatus.Unresolved);
            _userConversationStatusRepository.UpdateStatus(conversationId, user._id, ConversationStatus.Unread);

            var countPersonal = _conversationCounterService.CountPersonalConversations(user._id, new[] { ConversationStatus.Unread });
            var countUnread = _conversationCounterService.CountUnread(user._id);

            Assert.That(countPersonal, Is.EqualTo(1));
            Assert.That(countUnread, Is.EqualTo(1));
        }

        [Test]
        public void CountConversations_WhenAllAreArchived_ReturnsAllArchived()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");

            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Archive);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Archive);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Read);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Unresolved);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Unread);

            var c = _conversationRepository.Create(ObjectId.GenerateNewId(), new[] { user1._id, user2._id });
            _messageRepository.Create(c.Id, user1._id, "bla bla bla");
            c = _conversationRepository.Create(ObjectId.GenerateNewId(), new[] { user1._id, user2._id });
            _messageRepository.Create(c.Id, user1._id, "bla bla bla");

            _conversationStatusService.ArchiveAll(user1._id);

            var conversations = _conversationService.GetAll(user1._id, user1.MemberSince.Value, 1, 20);
            Assert.That(conversations.All(x => x.Status == ConversationStatus.Archive));
        }

        [Test]
        public void GetAllConversations_WhenSystemMessageNewerThenConversationIsInConversationAndAllAreArchived_ShouldReturnAllArchived()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var searsUser = IntegrationHelper.CreateEntity(2, "doron");

            var systemConversation = _conversationRepository.CreateSystemConversation(searsUser._id);
            _userConversationStatusRepository.UpdateStatus(systemConversation.Id, user1._id, ConversationStatus.Archive);

            System.Threading.Thread.Sleep(1000);

            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));
            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));
            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));
            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));

            _conversationStatusService.ArchiveAll(user1._id);

            var conversations = _conversationService.GetAll(user1._id, user1.MemberSince.Value, 1, 20);
            Assert.That(conversations.All(x => x.Status == ConversationStatus.Archive));
        }

        [Test]
        public void CountConversations_WhenAllAreArchived_ReturnZeroArchivedForOtherUser()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");

            ObjectId conversationId = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversationId, new[] { user1._id, user2._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(conversationId, user2._id, ConversationStatus.Unread);

            conversationId = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversationId, new[] { user1._id, user2._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(conversationId, user2._id, ConversationStatus.Unread);

            _conversationStatusService.ArchiveAll(user1._id);
            var count = _conversationCounterService.CountPersonalConversations(user2._id, new[] { ConversationStatus.Archive });

            Assert.That(count, Is.EqualTo(0));
        }

        [Test]
        public void CountConversations_WhenMarkAllAsRead_ReturnsAllRead()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");

            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Archive);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Read);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Unresolved);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), user1._id, ConversationStatus.Unread);

            var c = _conversationRepository.Create(ObjectId.GenerateNewId(), new[] { user1._id, user2._id });
            _messageRepository.Create(c.Id, user1._id, "bla bla bla");
            c = _conversationRepository.Create(ObjectId.GenerateNewId(), new[] { user1._id, user2._id });
            _messageRepository.Create(c.Id, user1._id, "bla bla bla");

            _conversationStatusService.MarkAllAsRead(user1._id);

            var conversations = _conversationService.GetAll(user1._id, user1.MemberSince.Value, 1, 20);
            Assert.That(conversations.All(x => x.Status == ConversationStatus.Read));
        }

        [Test]
        public void GetAllConversations_WhenSystemMessageNewerThenConversationIsInConversationAndAllAreRead_ShouldReturnAllRead()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var searsUser = IntegrationHelper.CreateEntity(2, "doron");

            var systemConversation = _conversationRepository.CreateSystemConversation(searsUser._id);
            _userConversationStatusRepository.UpdateStatus(systemConversation.Id, user1._id, ConversationStatus.Archive);

            System.Threading.Thread.Sleep(1000);

            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));
            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));
            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));
            _messageRepository.CreateSystemMessage(systemConversation.Id, CreateSystemMessage(searsUser._id));

            _conversationStatusService.MarkAllAsRead(user1._id);

            var conversations = _conversationService.GetAll(user1._id, user1.MemberSince.Value, 1, 20);
            Assert.That(conversations.All(x => x.Status == ConversationStatus.Read));
        }

        [Test]
        public void CountConversations_WhenMarkAllAsRead_ReturnZeroReadForOtherUser()
        {
            var user1 = IntegrationHelper.CreateEntity(1, "gilly");
            var user2 = IntegrationHelper.CreateEntity(2, "doron");

            ObjectId conversationId = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversationId, new[] { user1._id, user2._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(conversationId, user2._id, ConversationStatus.Unread);

            conversationId = ObjectId.GenerateNewId();
            _conversationRepository.Create(conversationId, new[] { user1._id, user2._id });
            _userConversationStatusRepository.UpdateStatus(conversationId, user1._id, ConversationStatus.Unread);
            _userConversationStatusRepository.UpdateStatus(conversationId, user2._id, ConversationStatus.Unread);

            _conversationStatusService.MarkAllAsRead(user1._id);
            var count = _conversationCounterService.CountPersonalConversations(user2._id, new[] { ConversationStatus.Read });

            Assert.That(count, Is.EqualTo(0));
        }


        [Test]
        public void CountMessages_WhenThereAreUnreadSystemConversationsAndPersonalConversations_ReturnsUnreadPersonalCount()
        {
            var user = IntegrationHelper.CreateEntity(1, "gilly");
            var conversation = _conversationRepository.CreateSystemConversation(user._id);
            CreateSystemMessages(conversation, user._id);

            var personalConversationId = ObjectId.GenerateNewId();

            _conversationRepository.Create(personalConversationId, new[] { user._id });
            _userConversationStatusRepository.UpdateStatus(personalConversationId, user._id, ConversationStatus.Unread);

            var countUnread = _conversationCounterService.CountUnread(user._id);

            var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation.Id);
            _conversationStorage.Remove(filter);

            Assert.That(countUnread, Is.EqualTo(1));
        }

        private SystemMessage CreateSystemMessage(ObjectId authorId)
        {
            return new SystemMessage
                        {
                            AuthorId = authorId,
                            Content = "bla bla bla",
                            Height = 1200,
                            Width = 1200,
                            IsHtml = false,
                            PreviewText = "Bla",
                            endDate = DateTime.MaxValue
                        };
        }

        private void CreateSystemMessages(ConversationDescriptor conversation, ObjectId userId)
        {
            var message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
            _userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Archive);

            message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
            _userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Unread);

            message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
            _userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Unread);

            message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
            _userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Read);
        }
    }
}