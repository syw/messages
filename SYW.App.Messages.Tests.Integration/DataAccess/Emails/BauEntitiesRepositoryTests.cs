using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Emails
{
	[TestFixture]
	public class BauEntitiesRepositoryTests : IntegrationTestBase
	{
		private IBauEntitiesRepository _target;
		private IMongoStorage<BauEntity, ObjectId> _storage;
		
		[SetUp]
		public void Setup()
		{
			_storage = Resolve<IMongoStorage<BauEntity, ObjectId>>();
			
			_target = new BauEntitiesRepository(_storage);

			_storage.Clear();
		}

		[Test]
		public void GetEntityByBauType_WhenBauTypeExist_ShouldReturnEntityId()
		{
			var bauEntity = InsertBauEntity("any-bau-type");

			var result = _target.GetEntityByBrandName(bauEntity.BauType);

			Assert.That(result, Is.EqualTo(bauEntity.EntityId));
		}

		[Test]
		public void GetEntityByBauType_WhenBauTypeDoesntExist_ShouldReturnNull()
		{
			var result = _target.GetEntityByBrandName("unexisting-bau-type");

			Assert.That(result, Is.Null);
		}

		[Test]
		public void GetEntityByBauType_WhenBauTypeExistWithDifferentCase_ShouldReturnEntityId()
		{
			var bauEntity = InsertBauEntity("any-bau-type");

			var result = _target.GetEntityByBrandName(bauEntity.BauType.ToUpper());

			Assert.That(result, Is.EqualTo(bauEntity.EntityId));
		}

		[Test]
		public void GetEntityByBauType_WhenArgumentHasLeadingOrTrailingSpaces_ShouldSearchForTrimmedEntityInDb()
		{
			var bauEntity = InsertBauEntity("any-bau-type");

			var result = _target.GetEntityByBrandName(string.Format("   {0}   ", bauEntity.BauType));

			Assert.That(result, Is.EqualTo(bauEntity.EntityId));
		}
		
		private BauEntity InsertBauEntity(string entityName)
		{
			var bauEntity = new BauEntity { BauType = entityName, EntityId = ObjectId.GenerateNewId() };
			
			_storage.Insert(bauEntity);

			return bauEntity;
		}
	}
}