using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Emails
{
	[TestFixture]
	public class BauEntitiesServiceTests : IntegrationTestBase
	{
		private IBauToEntitiesService _target;
		private IMongoStorage<BauEntity, ObjectId> _bauEntitiesStorage;
		private IMongoStorage<Entity, ObjectId> _entitiesStorage;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_entitiesStorage = Resolve<IMongoStorage<Entity, ObjectId>>();
			_bauEntitiesStorage = Resolve<IMongoStorage<BauEntity, ObjectId>>();
			_target = Resolve<IBauToEntitiesService>();
		}

		[SetUp]
		public void Setup()
		{
			_bauEntitiesStorage.Clear();
			_entitiesStorage.Clear();
		}

		[Test]
		public void GetOrCreateEntityByName_WhenNameExists_ShouldJustReturnExpectedIdentity()
		{
			const string imgUrl = "imageUrl.com";
			const string bauName = "specific-brand-name";

			var bauEntity = new BauEntity {BauType = bauName, EntityId = ObjectId.GenerateNewId()};
			var originalEntity = new Entity() {_id = bauEntity.EntityId, ImageUrl = imgUrl, Name = bauName};

			_bauEntitiesStorage.Insert(bauEntity);
			_entitiesStorage.Insert(originalEntity);

			var result = _target.GetOrCreateEntityByName(bauName, imgUrl);

			Assert.AreEqual(result._id, originalEntity._id);
			Assert.AreEqual(result.Name, originalEntity.Name);
			Assert.AreEqual(result.ImageUrl, originalEntity.ImageUrl);
		}

		[Test]
		public void GetOrCreateEntityByName_WhenNameDoesntExist_ShouldCreateNewEntityInDb()
		{
			const string imgUrl = "imageUrl.com";
			const string bauName = "specific-brand-name";

			var entity = _target.GetOrCreateEntityByName(bauName, imgUrl);
			Assert.IsNotNull(entity);

			Assert.AreEqual(entity.ImageUrl, imgUrl);
			Assert.AreEqual(entity.Name, bauName);
		}

		[Test]
		public void GetOrCreateEntityByName_WhenNameExistsButImageUrlIsDifferent_ShouldReturnTheSameIdAndUpdateUrl()
		{
			const string imgUrl = "imageUrl.com";
			const string newImgUrl = "newUrl.com";
			const string bauName = "specific-brand-name";
			Assert.AreNotEqual(imgUrl, newImgUrl, "Test constants are wrong.");
			var bauEntity = new BauEntity {BauType = bauName, EntityId = ObjectId.GenerateNewId()};
			var originalEntity = new Entity() {_id = bauEntity.EntityId, ImageUrl = imgUrl, Name = bauName};

			_bauEntitiesStorage.Insert(bauEntity);
			_entitiesStorage.Insert(originalEntity);
			var resultedEntity = _target.GetOrCreateEntityByName(bauName, newImgUrl);

			Assert.IsNotNull(resultedEntity);
			Assert.AreEqual(resultedEntity._id, bauEntity.EntityId, "When calling method, should return existing Entity, not a new one");
			Assert.AreEqual(resultedEntity.ImageUrl, newImgUrl, "image should have been updated");
			Assert.AreEqual(resultedEntity.Name, originalEntity.Name, "entity.name shouldn't change");
		}
	}
}