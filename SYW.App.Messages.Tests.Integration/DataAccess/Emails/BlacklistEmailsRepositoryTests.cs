using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Entities;
using Mongo;
using MongoDB.Bson;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Emails
{
	[TestFixture]
	public class BlacklistEmailsRepositoryTests : IntegrationTestBase
	{
		private IBlacklistEmailsRepository _target;
		private IMongoStorage<BlacklistEmail, ObjectId> _storage;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_storage = Resolve<IMongoStorage<BlacklistEmail, ObjectId>>();
			_target = Resolve<IBlacklistEmailsRepository>();
		}

		[SetUp]
		public void Setup()
		{
			_storage.Clear();
		}

		[Test]
		public void IsEmailInBlackList_WhenEmailExists_ShouldGetBackTrue()
		{
			var blacklistEmail = new BlacklistEmail {Email = "test@delver.com"};
			_storage.Insert(blacklistEmail);

			var result = _target.IsEmailInBlackList(blacklistEmail.Email);

			Assert.That(result, Is.True);
		}

		[Test]
		public void IsEmailInBlackList_WhenEmailNotExists_ShouldGetBackFalse()
		{
			var result = _target.IsEmailInBlackList("other@delver.com");

			Assert.That(result, Is.False);
		}

		[Test]
		public void IsEmailInBlackList_WhenEmailWithDifferentCaseExists_ShouldGetBackTrue()
		{
			var blacklistEmail = new BlacklistEmail {Email = "test@delver.com"};
			_storage.Insert(blacklistEmail);

			var result = _target.IsEmailInBlackList("TesT@DelVer.com");

			Assert.That(result, Is.True);
		}
	}
}