﻿using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Entities
{
	[TestFixture]
	public class EntityNetworkRepositoryTests : IntegrationTestBase
	{
		private IEntityNetworkRepository _target;

		private Entity user;

		[SetUp]
		public void SetUp()
		{
			_target = Resolve<IEntityNetworkRepository>();

			user = IntegrationHelper.CreateEntity(1, "gilly");
		}

		[Test]
		public void UpdateNetwork_WhenThereIsTwoNetwork_ShouldGetBackTwoFriends()
		{
			_target.UpdateNetwork(user._id,
								new[]
									{
										new Entity {Name = "Nir", ImageUrl = "Image", OriginalId = 2},
										new Entity {Name = "Gadi", ImageUrl = "Image", OriginalId = 3}
									});

			var result = _target.GetNetwork(user._id);

			Assert.That(result.Friends.Length, Is.EqualTo(2));
		}

		[Test]
		public void UpdateNetwork_WhenThereIsAlreadyNetwork_ShouldUpdateTheNetwork()
		{
			_target.UpdateNetwork(user._id,
								new[]
									{
										new Entity {Name = "Nir", ImageUrl = "Image", OriginalId = 2},
										new Entity {Name = "Gadi", ImageUrl = "Image", OriginalId = 3}
									});

			var result = _target.GetNetwork(user._id);

			_target.UpdateNetwork(user._id,
								new[]
									{
										new Entity {Name = "Nir", ImageUrl = "Image", OriginalId = 2},
									});

			var newWesult = _target.GetNetwork(user._id);

			Assert.That(newWesult._id, Is.EqualTo(result._id));
			Assert.That(newWesult.Friends.Length, Is.EqualTo(1));
		}
	}
}