﻿using System;
using System.Collections.Generic;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.IntegrationTests.DataAccess
{
	[TestFixture]
	public class EntitiesRepositoryTests : IntegrationTestBase
	{
		private IEntitiesRepository _entitiesRepository;
		private IMongoStorage<Entity, ObjectId> _storage;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_storage = Resolve<IMongoStorage<Entity, ObjectId>>();
			_entitiesRepository = Resolve<IEntitiesRepository>();
		}


		[SetUp]
		public void SetUp()
		{
			_storage.Clear();
		}

		[Test]
		public void GetEntitiesIdsByEmail_WhenGettingByEmail_ShouldReturnEntityId()
		{
			var objectId = ObjectId.GenerateNewId();

			AddEntityByEmail(objectId, "test@test.test");

			var result = _entitiesRepository.GetEntitiesIdsByEmail(new List<string> { "test@test.test" });

			Assert.That(result, Has.Count.EqualTo(1));
			Assert.That(result, Has.All.Matches<ObjectId>(x => x.Equals(objectId)));
		}


		[Test]
		public void GetEntitiesIdsByEmail_WhenGettingByEmailWithADifferentCase_ShouldReturnEntityId()
		{
			var objectId = ObjectId.GenerateNewId();
			AddEntityByEmail(objectId, "test@test.test");

			var result = _entitiesRepository.GetEntitiesIdsByEmail(new List<string> { "tEst@Test.teSt" });

			Assert.That(result, Has.Count.EqualTo(1));
			Assert.That(result, Has.All.Matches<ObjectId>(x => x.Equals(objectId)));
		}

		[Test]
		public void GetEntityIdByOriginalEmail_WhenGettingByEmail_ShouldReturnEntityId()
		{
			var objectId = ObjectId.GenerateNewId();

			AddEntityByOriginalEmail(objectId, "test@test.test");

			var result = _entitiesRepository.GetEntityIdByOriginalEmail("test@test.test");

			Assert.That(result, Is.EqualTo(objectId));
		}


		[Test]
		public void GetEntityIdByOriginalEmail_WhenGettingByEmailWithADifferentCase_ShouldReturnEntityId()
		{
			var objectId = ObjectId.GenerateNewId();
			AddEntityByOriginalEmail(objectId, "test@test.test");

			var result = _entitiesRepository.GetEntityIdByOriginalEmail("tEst@Test.teSt");

			Assert.That(result, Is.EqualTo(objectId));
		}

		private void AddEntityByEmail(ObjectId generateNewId, string email)
		{
			_entitiesRepository.Add(new Entity
										{
											_id = generateNewId,
											OriginalId = DateTime.Now.Ticks,
											Name = "Our lovely user",
											ImageUrl = "image",
											Email = email
										});
		}

		private void AddEntityByOriginalEmail(ObjectId generateNewId, string email)
		{
			_entitiesRepository.Add(new Entity
										{
											_id = generateNewId,
											OriginalId = DateTime.Now.Ticks,
											Name = "Our lovely user",
											ImageUrl = "image",
											OriginalEmail = email
										});
		}


		[Test]
		public void GetEntitiesByIds_WhenAddingEntity_ShouldReturnEntityOriginalId()
		{
			var originalId = DateTime.Now.Ticks;

			AddEntityByOriginalId(originalId);

			var result = _entitiesRepository.Get(originalId);

			Assert.That(result.OriginalId, Is.EqualTo(originalId));
		}

		private void AddEntityByOriginalId(long originalId)
		{
			_entitiesRepository.Add(new Entity
										{
											_id = ObjectId.GenerateNewId(),
											OriginalId = originalId,
											Name = "Our lovely user",
											ImageUrl = "image",
											OriginalEmail = "email@email.com"
										});
		}
	}
}