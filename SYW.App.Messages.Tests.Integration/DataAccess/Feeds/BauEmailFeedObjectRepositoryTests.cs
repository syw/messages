﻿using System.Linq;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Feeds
{
	[TestFixture]
	class BauEmailFeedObjectRepositoryTests : IntegrationTestBase
	{
		private IBauEmailFeedObjectRepository _bauEmailFeedObjectRepository;
		private IMongoStorage<BauEmailFeedObjectDto, ObjectId> _storage;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_storage = Resolve<IMongoStorage<BauEmailFeedObjectDto, ObjectId>>();
			_bauEmailFeedObjectRepository = Resolve<IBauEmailFeedObjectRepository>();
		}

		[SetUp]
		public void SetUp()
		{
			_storage.Clear();
		}

		[Test]
		public void Add_WhenTryingToAddBauEmailFeedObjectDto_ShouldSaveIt()
		{
			_bauEmailFeedObjectRepository.Add(new BauEmailFeedObject { BrandName = "Test" });

			var bau = _storage.GetAll();

			Assert.That(bau.First().BrandName, Is.EqualTo("Test"));
			Assert.That(bau.Count,Is.EqualTo(1));
		}
	}
}
