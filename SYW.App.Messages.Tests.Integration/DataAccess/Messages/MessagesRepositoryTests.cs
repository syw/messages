﻿using System;
using System.Linq;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Messages
{
	[TestFixture]
	public class MessagesRepositoryTests : IntegrationTestBase
	{
		private IMessageRepository _messageRepository;
		private IConversationRepository _conversationRepository;
		private IMongoCleaner _mongoCleaner;

		[SetUp]
		public void SetUp()
		{
			_messageRepository = Resolve<IMessageRepository>();
			_conversationRepository = Resolve<IConversationRepository>();
			_mongoCleaner = Resolve<IMongoCleaner>();

			_mongoCleaner.Clean();
		}

		[Test(Description = "6190 - Replying in a single message thread, replies to all the friends in the thread")]
		public void Create_ReplyToThread_AppearsForAllParticipantsOfThread()
		{
			var user1 = IntegrationHelper.CreateEntity(2, "gilly");
			var user2 = IntegrationHelper.CreateEntity(3, "doron");
			var user3 = IntegrationHelper.CreateEntity(4, "niro");
			var conversation = _conversationRepository.Create(user1._id, new[] { user1._id, user2._id, user3._id });

			var message = _messageRepository.Create(conversation.Id, user1._id, "I'm fly cause i'm hot, you ain't cause you not");
			var users2Conversations = _conversationRepository.GetAll(user2._id, user2.MemberSince.Value);
			var users3Conversations = _conversationRepository.GetAll(user3._id, user3.MemberSince.Value);

			Assert.That(users2Conversations.Any(x => x.Id == conversation.Id));
			Assert.That(users3Conversations.Any(x => x.Id == conversation.Id));
		}

		[Test]
		public void Create_ThereAreInvalidAttachments_SavedMessageHasOnlyValidMessages()
		{
			var user1 = IntegrationHelper.CreateEntity(13, "gilly");
			var user2 = IntegrationHelper.CreateEntity(14, "doron");
			var attachments = new[] { new Attachment("name", "link", "url"), new Attachment(null, null, null) };

			var conversation = _conversationRepository.Create(user1._id, new[] { user1._id, user2._id });
			var message = _messageRepository.Create(conversation.Id, user1._id, "I'm fly cause i'm hot, you ain't cause you not", null, attachments);

			Assert.That(message.Attachments, Has.All.Matches<Attachment>(a => a.IsValid()));
		}

		[Test]
		public void CreateSystemMessage_CreateValidMessage_UserReceivesSystemMessageInFeed()
		{
			var user2 = IntegrationHelper.CreateEntity(6, "Rotschild");
			var user1 = IntegrationHelper.CreateEntity(5, "shlomo");
			var conversation = _conversationRepository.Create(user1._id, new[] { user1._id });

			var message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage
																					{
																						FeedId = "fakeFeedId",
																						AuthorId = user1._id,
																						Content = "This is just a system message",
																						PreviewText = "some preview text...",
																						endDate = DateTime.Now.AddDays(5),
																						IsHtml = true,
																						Width = 800,
																						Height = 1300
																					});

			var retrievedMessage = _messageRepository.GetMessageById(message.Id.ToString());

			Assert.That(retrievedMessage.Id, Is.EqualTo(message.Id));
		}

		[Test]
		public void GetAllDescriptors_WhenCalled_ShouldRetrieveAllDescriptorsForGivenConversations()
		{
			var conversationsIds = new[] { GenerateSystemConversation(), GenerateSystemConversation() };
			var messagesDescriptors = conversationsIds.Select(GenerateSystemMessage).Select(m => new MessageDescriptor
																									{
																										Id = m.Id,
																										ConversationId = m.ConversationId,
																										AuthorId = m.Author._id,
																										Date = m.Date,
																										Status = ConversationStatus.Unread,
																										PreviewText = m.PreviewText,
																										ContentType = (int)m.ContentType,
																										ContentHeight = m.ContentHeight,
																										ContentWidth = m.ContentWidth,
																									}).ToList();

			var retrievedMessageDescriptors = _messageRepository.GetAllDescriptors(conversationsIds).ToList();

			Assert.That(messagesDescriptors.Select(x => x.Id).ToList(), Is.EquivalentTo(retrievedMessageDescriptors.Select(r => r.Id).ToList()));
		}

		[Test]
		public void CreateFormattedMessage_WhenCalled_CreatesMessageWithTypeFormatted()
		{
			var user1 = IntegrationHelper.CreateEntity(7, "Uri");
			var user2 = IntegrationHelper.CreateEntity(8, "gillo");
			var user3 = IntegrationHelper.CreateEntity(9, "niram");

			var conversation = _conversationRepository.Create(user1._id, new[] { user1._id, user2._id, user3._id });

			var createdMessage = _messageRepository.CreateFormattedMessage(conversation.Id, user1._id, "I'm <b>hot</b>");

			var retrievedMessage = _messageRepository.GetMessageById(createdMessage.Id.ToString());
			Assert.That(retrievedMessage.ContentType, Is.EqualTo(MessageContentType.FormattedText));
		}

		[Test]
		public void CreateFormattedMessage_WhenCalled_ContentIsSavedAsIs()
		{
			var user1 = IntegrationHelper.CreateEntity(10, "Uro");
			var user2 = IntegrationHelper.CreateEntity(11, "galileo");
			var user3 = IntegrationHelper.CreateEntity(12, "nirem");

			var conversation = _conversationRepository.Create(user1._id, new[] { user1._id, user2._id, user3._id });

			var createdMessage = _messageRepository.CreateFormattedMessage(conversation.Id, user1._id, "I'm <b>hot</b>");

			var retrievedMessage = _messageRepository.GetMessageById(createdMessage.Id.ToString());
			Assert.That(retrievedMessage.Content, Is.EqualTo("I'm <b>hot</b>"));
		}

		[Test]
		public void Delete_WhenCallingDeleteWithExistingMessageId_MessageShouldBeDeleted()
		{
			var conversationId = ObjectId.GenerateNewId();
			var message = GenerateSystemMessage(conversationId);

			_messageRepository.Delete(message.Id);
			var deletedMessage = _messageRepository.GetMessageById(message.Id.ToString());

			Assert.That(deletedMessage, Is.Null);
		}

		private Message GenerateSystemMessage(ObjectId conversationId)
		{
			return _messageRepository.CreateSystemMessage(conversationId, new SystemMessage
																			{
																				FeedId = "fakeFeedId",
																				AuthorId = ObjectId.GenerateNewId(),
																				Content = "This is just a system message",
																				PreviewText = "some preview text...",
																				endDate = DateTime.Now.AddDays(5),
																				IsHtml = true,
																				Width = 800,
																				Height = 1300
																			});
		}

		private ObjectId GenerateSystemConversation()
		{
			return _conversationRepository.CreateSystemConversation(ObjectId.GenerateNewId()).Id;
		}
	}
}