﻿using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.IntegrationTests.DataAccess.Messages
{
	[TestFixture]
	public class UserMessageStatusRepositoryTests : IntegrationTestBase
	{
		private IUserMessageStatusRepository _userMessageStatusRepository;
		private IConversationRepository _conversationRepository;
		private IMessageRepository _messageRepository;
		private IConversationStatusService _conversationStatusService;
		private IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private IConversationCounterService _conversationCounterService;

		[SetUp]
		public void SetUp()
		{
			_userMessageStatusRepository = Resolve<IUserMessageStatusRepository>();
			_conversationRepository = Resolve<IConversationRepository>();
			_messageRepository = Resolve<IMessageRepository>();
			_conversationStatusService = Resolve<IConversationStatusService>();
			_conversationStorage = Resolve<IMongoStorage<ConversationDto, ObjectId>>();
			_conversationCounterService = Resolve<IConversationCounterService>();
		}

		[Test]
		public void UpdateStatus_MessageGetsNewStatus()
		{
			var user1Id = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();

			var conversationId = ObjectId.GenerateNewId();
			_userMessageStatusRepository.UpdateStatus(conversationId, messageId, user1Id, (int)ConversationStatus.Archive);

			var updatedStatus = _userMessageStatusRepository.Get(messageId, user1Id);
			Assert.That(updatedStatus.Status, Is.EqualTo((int)ConversationStatus.Archive));
		}

		[Test]
		public void GetMessagesStatuses_WhenThereAreTwoStatusesForSameConversation_ShouldGetTwo()
		{
			var user1Id = ObjectId.GenerateNewId();
			var conversationId = ObjectId.GenerateNewId();
			_userMessageStatusRepository.UpdateStatus(conversationId, ObjectId.GenerateNewId(), user1Id, (int)ConversationStatus.Archive);
			_userMessageStatusRepository.UpdateStatus(conversationId, ObjectId.GenerateNewId(), user1Id, (int)ConversationStatus.Unread);

			var updatedStatus = _userMessageStatusRepository.GetAll(conversationId, user1Id);
			Assert.That(updatedStatus.Count(), Is.EqualTo(2));
		}

		[Test]
		public void GetMessagesStatuses_WhenSameMessagesBeingUpdated_GetTheLastStatusOnly()
		{
			var user1Id = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();

			var conversationId = ObjectId.GenerateNewId();
			_userMessageStatusRepository.UpdateStatus(conversationId, messageId, user1Id, (int)ConversationStatus.Archive);
			_userMessageStatusRepository.UpdateStatus(conversationId, messageId, user1Id, (int)ConversationStatus.Unread);
			_userMessageStatusRepository.UpdateStatus(conversationId, messageId, user1Id, (int)ConversationStatus.Read);

			var updatedStatus = _userMessageStatusRepository.GetAll(conversationId, user1Id);
			Assert.That(updatedStatus.Count(), Is.EqualTo(1));
			Assert.That(updatedStatus[0].Status, Is.EqualTo((int)ConversationStatus.Read));
		}

		[Test]
		public void GetMessagesStatuses_WhenMultipleConversationsHasStatus_ReturnAllStatuses()
		{
			var userId = ObjectId.GenerateNewId();
			var conversationId1 = ObjectId.GenerateNewId();
			var conversationId2 = ObjectId.GenerateNewId();
			_userMessageStatusRepository.UpdateStatus(conversationId1, ObjectId.GenerateNewId(), userId, (int)ConversationStatus.Archive);
			_userMessageStatusRepository.UpdateStatus(conversationId2, ObjectId.GenerateNewId(), userId, (int)ConversationStatus.Unread);

			var updatedStatus = _userMessageStatusRepository.GetAll(new[] {conversationId1, conversationId2}, userId);
			Assert.That(updatedStatus.Count(), Is.EqualTo(2));
		}

		[Test]
		public void GetMessagesStatuses_WhenUserHasMultipleMessageStatus_ReturnAllStatuses()
		{
			var userId = ObjectId.GenerateNewId();
			_userMessageStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), ObjectId.GenerateNewId(), userId, (int)ConversationStatus.Archive);
			_userMessageStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), ObjectId.GenerateNewId(), userId, (int)ConversationStatus.Unread);
			_userMessageStatusRepository.UpdateStatus(ObjectId.GenerateNewId(), ObjectId.GenerateNewId(), userId, (int)ConversationStatus.Read);

			var updatedStatus = _userMessageStatusRepository.GetAll(userId);
			Assert.That(updatedStatus.Count(), Is.EqualTo(3));
		}

		[Test]
		public void CountMessages_WhenThereAreNoMessages_ReturnsZero()
		{
			var count = _conversationCounterService.CountSystemMessages(ObjectId.GenerateNewId(), new[] {ConversationStatus.Read});
			Assert.That(count, Is.EqualTo(0));
		}

		[Test]
		public void CountMessages_WhenThereAreUnreadConversations_ReturnsUnreadCount()
		{
			var userId = ObjectId.GenerateNewId();
			var conversation = _conversationRepository.CreateSystemConversation(userId);
			CreateMessages(conversation, userId);

			var count = _conversationCounterService.CountSystemMessages(userId, new[] {ConversationStatus.Unread});
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation.Id);
			_conversationStorage.Remove(filter);

			Assert.That(count, Is.EqualTo(2));
		}


        [Test]
        public void CountMessages_WhenThereAreUnreadSystemConversations_ReturnsZeroUnreadCount()
        {
            var userId = ObjectId.GenerateNewId();
            var conversation = _conversationRepository.CreateSystemConversation(userId);
            CreateMessages(conversation, userId);

            var count = _conversationCounterService.CountUnread(userId);
            var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation.Id);
            _conversationStorage.Remove(filter);

            Assert.That(count, Is.EqualTo(0));
        }

		[Test]
		public void CountMessages_WhenAllAreMarkedAsRead_ReturnsReadCount()
		{
			var userId = ObjectId.GenerateNewId();
			var conversation = _conversationRepository.CreateSystemConversation(userId);
			CreateMessages(conversation, userId);

			_conversationStatusService.MarkAllAsRead(userId);

			var count = _conversationCounterService.CountSystemMessages(userId, new[] {ConversationStatus.Read});
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation.Id);
			_conversationStorage.Remove(filter);

			// Will be 3 because the MarkAllAsRead skips archived conversations...
			Assert.That(count, Is.EqualTo(3));
		}

		[Test]
		public void CountMessages_WhenAllAreArchived_ReturnsArchiveCount()
		{
			var userId = ObjectId.GenerateNewId();
			var conversation = _conversationRepository.CreateSystemConversation(userId);
			CreateMessages(conversation, userId);

			_conversationStatusService.ArchiveAll(userId);

			var count = _conversationCounterService.CountSystemMessages(userId, new[] {ConversationStatus.Archive});
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation.Id);
			_conversationStorage.Remove(filter);

			Assert.That(count, Is.EqualTo(4));
		}

		[Test]
		public void CountMessages_WhenAllAreArchived_ReturnsZeroForOtherStatus()
		{
			var userId = ObjectId.GenerateNewId();
			var conversation = _conversationRepository.CreateSystemConversation(userId);
			CreateMessages(conversation, userId);

			_conversationStatusService.ArchiveAll(userId);

			var count = _conversationCounterService.CountSystemMessages(userId, new[] {ConversationStatus.Read});
			
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation.Id);
			_conversationStorage.Remove(filter);

			Assert.That(count, Is.EqualTo(0));
		}

		private void CreateMessages(ConversationDescriptor conversation, ObjectId userId)
		{
			var message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
			_userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Archive);

			message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
			_userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Unread);

			message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
			_userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Unread);

			message = _messageRepository.CreateSystemMessage(conversation.Id, new SystemMessage());
			_userMessageStatusRepository.UpdateStatus(message.ConversationId, message.Id, userId, (int)ConversationStatus.Read);
		}
	}
}