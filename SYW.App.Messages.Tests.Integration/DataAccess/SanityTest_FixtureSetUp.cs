﻿using Mongo;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;

namespace SYW.App.Messages.IntegrationTests.DataAccess
{
	[TestFixture]
	public class SanityTest_FixtureSetUp : IntegrationTestBase
	{
		private IMongoDatabaseProvider _mongoProvider;

		[Test]
		public void MongoCleaner_AfterFixtureSetUp_MongoDatabaseShouldBeCompletelyEmpty()
		{
			_mongoProvider = Resolve<IMongoDatabaseProvider>();

			var collectionNames = _mongoProvider.Create().GetCollectionNames();
			Assert.That(collectionNames, Is.Empty);
		}
	}
}