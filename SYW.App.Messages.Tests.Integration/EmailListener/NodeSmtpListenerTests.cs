﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web;
using Microsoft.Security.Application;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messages.IntegrationTests.EmailListener
{
	[Ignore]
    public class NodeSmtpListenerTests : IntegrationTestBase
    {
        private IConversationRepository _conversationRepository;
        private IMessageRepository _messageRepository;
        private IApplicationSettings _applicationSettings;
        private IEntitiesRepository _entitiesRepository;
        private IEmailSender _emailSender;

        [SetUp]
        public void SetUp()
        {
            _conversationRepository = Resolve<IConversationRepository>();
            _messageRepository = Resolve<IMessageRepository>();
            _applicationSettings = Resolve<IApplicationSettings>();
            _entitiesRepository = Resolve<IEntitiesRepository>();
            _emailSender = Resolve<IEmailSender>();
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasNoAttachmentsAndSenderEntityDoesntExist_ShouldCreateSenderEntityAndSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test1", 1, 0, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var senderId = _entitiesRepository.GetEntityIdByOriginalEmail(emailMessage.From.Address);

            Assert.NotNull(senderId);

            var conversation = _conversationRepository.GetByAuthor(senderId.Value);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == senderId), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            Assert.True(string.IsNullOrEmpty(messages.First().AttachedImagesUrl));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasNoAttachmentsAndRecipiantEntityDoesntExist_ShouldNotCreateOrUpdateConversation()
        {
            var emailMessage = CreateEmailMessage("test2", 1, 0, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.Null(conversation);
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWithNoResizeNecessaryAndSenderDoesntExist_ShouldCreateSenderEntityAndSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test3", 1, 1, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var senderId = _entitiesRepository.GetEntityIdByOriginalEmail(emailMessage.From.Address);

            Assert.NotNull(senderId);

            var conversation = _conversationRepository.GetByAuthor(senderId.Value);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == senderId), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasNoAttachmentsSenderAndRecipiantExists_ShouldSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test4", 1, 0, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            Assert.True(string.IsNullOrEmpty(messages.First().AttachedImagesUrl));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWithNoResizeNecessarySenderAndRecipiantExists_ShouldSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test5", 1, 1, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasMultipleValidAttachmentWithNoResizeNecessarySenderAndRecipiantExists_ShouldSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test6", 1, 4, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(emailMessage.Attachments.Count()));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneInValidAttachment_ShouldSaveMessageWithoutAttachments()
        {
            var emailMessage = CreateEmailMessage("test7", 1, 0, 1);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            Assert.True(string.IsNullOrEmpty(messages.First().AttachedImagesUrl));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasMultipleInValidAttachment_ShouldSaveMessageWithoutAttachments()
        {
            var emailMessage = CreateEmailMessage("test8", 1, 0, 3);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            Assert.True(string.IsNullOrEmpty(messages.First().AttachedImagesUrl));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasMultipleAttachmentsAndOneIsValid_ShouldSaveMessageWithoutInvalidAttachments()
        {
            var emailMessage = CreateEmailMessage("test9", 1, 1, 2);
            var createdEntities = CreateEntities(new[] { emailMessage.To.First().Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.First().Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWithNoResizeNecessaryMultipleExistingRecipiants_ShouldSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test10", 2, 1, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWithNoResizeNecessaryMultipleRecipiantsNotAllExist_ShouldSaveMessageWithValidParticipants()
        {
            var emailMessage = CreateEmailMessage("test11", 2, 1, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(2));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWithNoResizeNecessaryMultipleRecipiants_ShouldSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test12", 2, 1, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasMultipleValidAttachmentWithNoResizeNecessaryMultipleRecipiants_ShouldSaveMessage()
        {
            var emailMessage = CreateEmailMessage("test13", 2, 3, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(3));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasAttachmentAndConversationBetweenSenderAndRecipiantsExists_ShouldSaveMessageToExistingConversation()
        {
            var emailMessage = CreateEmailMessage("test14", 2, 3, 0);
            var secondMail = CreateEmailMessage("test14", 2, 3, 0);
            secondMail.Subject = "SecondMail";
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            _emailSender.SendEmail(secondMail);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 2);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));
            Assert.True(CheckMessageContent(secondMail.Subject, secondMail.Body, messages.ElementAt(1).Content));


            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(3));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneLargeInValidAttachmentMultipleRecipiants_ShouldSaveMessageWithoutAttachment()
        {
            var emailMessage = CreateEmailMessage("test15", 2, 0, 0,0,1);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            Assert.True(string.IsNullOrEmpty(messages.First().AttachedImagesUrl));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWhichNeedsToResizeMultipleRecipiants_ShouldSaveMessageWithResizedAttachment()
        {
            var emailMessage = CreateEmailMessage("test16", 2, 0, 0, 1, 0);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(5000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath,1));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidAttachmentWhichNeedToResizeAndOneLargeAttachmentMultipleRecipiants_ShouldSaveMessageWithResizedAttachmentOnly()
        {
            var emailMessage = CreateEmailMessage("test17", 2, 0, 0, 1, 1);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(9000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(1));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath, 1));
        }

        [Test]
        public void TestNodeSmtpListener_WhenEmailHasOneValidSmallAttachmentOneValidAttachmentWhichNeedToResizeAndOneLargeAttachmentMultipleRecipiants_ShouldSaveMessageWithoutLargeAttachment()
        {
            var emailMessage = CreateEmailMessage("test18", 2, 1, 0, 1, 1);
            var createdEntities = CreateEntities(new[] { emailMessage.To.ElementAt(0).Address, emailMessage.To.ElementAt(1).Address, emailMessage.From.Address });

            _emailSender.SendEmail(emailMessage);

            System.Threading.Thread.Sleep(10000);
            var conversation = _conversationRepository.GetByAuthor(createdEntities[emailMessage.From.Address]);

            Assert.NotNull(conversation);
            Assert.That(conversation.Participants.Count, Is.EqualTo(3));
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(0).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.To.ElementAt(1).Address]), Is.True);
			Assert.That(conversation.Participants.Any(x => x._id == createdEntities[emailMessage.From.Address]), Is.True);

            var messages = _messageRepository.GetAll(conversation.Id).ToList();

            Assert.NotNull(messages);
            Assert.That(messages.Count == 1);
            Assert.True(CheckMessageContent(emailMessage.Subject, emailMessage.Body, messages.First().Content));

            var attachmentsPath = messages.First().AttachedImagesUrl.Split(';');

            Assert.That(attachmentsPath.Count(), Is.EqualTo(2));
            Assert.True(CheckEmailAttachmentsExist(attachmentsPath, 1));
        }

        private MailMessage CreateEmailMessage(string mailAddressSuffix, int recipiantsNumber, int validAttachmentsNum, int inValidSuffixAttachmentsNum,int validAttachmentsToResize = 0,int largeAttachmentsNum = 0, bool isBodyHtml = true)
        {
            var message = new MailMessage
            {
                From = new MailAddress("test@" + mailAddressSuffix + ".com"),
                Subject = "Test Email",
                Body = "<div>Test Boddy</div><br/>",
                IsBodyHtml = isBodyHtml
            };
            for (var i = 0; i < recipiantsNumber; i++)
                message.To.Add(new MailAddress("tomer2rioi11dk3b2udo@" + mailAddressSuffix + "_" + i + ".com"));

            for (var i = 0; i < validAttachmentsNum; i++)
                message.Attachments.Add(new Attachment("..\\..\\EmailListener\\EmailAttachments\\ValidAttachment.jpg", MediaTypeNames.Application.Octet));
            for (var i = 0; i < validAttachmentsToResize; i++)
                message.Attachments.Add(new Attachment("..\\..\\EmailListener\\EmailAttachments\\AttachmentToResize.jpg", MediaTypeNames.Application.Octet));
            for (var i = 0; i < largeAttachmentsNum; i++)
                message.Attachments.Add(new Attachment("..\\..\\EmailListener\\EmailAttachments\\LargeAttachment.jpg", MediaTypeNames.Application.Octet));
            for (var i = 0; i < inValidSuffixAttachmentsNum; i++)
                message.Attachments.Add(new Attachment("..\\..\\EmailListener\\EmailAttachments\\test.txt", MediaTypeNames.Application.Octet));

            return message;
        }

        private Dictionary<string, ObjectId> CreateEntities(IEnumerable<string> entitiesAddresses)
        {
            var entitiesIds = new Dictionary<string, ObjectId>();
            foreach (var address in entitiesAddresses)
            {
                var entityId = GetOrCreateEntityIdByEmail(address);
                entitiesIds.Add(address, entityId);
            }
            return entitiesIds;
        }

        private ObjectId GetOrCreateEntityIdByEmail(string email)
        {
            return _entitiesRepository.GetEntityIdByOriginalEmail(email)
                ?? _entitiesRepository.GetEntitiesIdsByEmail(email)
                ?? CreateNewEntity(email);
        }

        private ObjectId CreateNewEntity(string sender)
        {
            var newEntity = new Entity
            {
				_id = ObjectId.GenerateNewId(),
                OriginalEmail = sender,
                Email = sender,
                Name = sender,
                ImageUrl = _applicationSettings.StaticContentLocation + "/missing-images/email.png"
            };
            _entitiesRepository.Add(newEntity);

			return newEntity._id;
        }

        private bool CheckEmailAttachmentsExist(IEnumerable<string> attachmentsPath, int resizedAttachmentsNum = 0)
        {
            var resized = 0;
            var total = 0;
            foreach (var attachmentPath in attachmentsPath)
            {
                using (var attachment = LoadAttachments(_applicationSettings.EmailAttachmentsUrl + "/" + attachmentPath))
                {
                    using (var image = Image.FromStream(attachment))
                    {
                        if (image.Width == 1100)
                            resized++;
                        total++;
                    }
                }
            }
            return resized == resizedAttachmentsNum && total == attachmentsPath.Count();
        }

        private string DecodeAndSanitizeContent(string content)
        {
            var decodedContent = HttpUtility.UrlDecode(content);
            if (string.IsNullOrWhiteSpace(decodedContent))
                return null;

            var sb = new StringBuilder();

            using (var writer = new StringWriter(sb))
            {
                using (var reader = new StringReader(decodedContent))
                {
                    Sanitizer.GetSafeHtmlFragment(reader, writer);
                }
            }

            return sb.ToString();
        }

        private bool CheckMessageContent(string messageSubject, string messageBody, string checkedContent)
        {
            var expectedContent = "<div>" + messageSubject + ":</div>" + messageBody;
            expectedContent = DecodeAndSanitizeContent(expectedContent);
            return checkedContent == expectedContent;
        }

        private Stream LoadAttachments(string url)
        {
            var req = WebRequest.Create(url);
            var response = req.GetResponse();
            return response.GetResponseStream();
        }
    }
}
