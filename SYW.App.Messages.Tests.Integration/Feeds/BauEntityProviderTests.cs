﻿using System.Linq;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.IntegrationTests.Feeds
{
	public class BauEntityProviderTests : IntegrationTestBase
	{
		private IBauEntitiesRepository _bauEntitiesRepository;
		private IMongoStorage<BauEntity, ObjectId> _storage;
		private IBauEntityProvider _target;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();
			_storage = Resolve<IMongoStorage<BauEntity, ObjectId>>();
			_bauEntitiesRepository = Resolve<IBauEntitiesRepository>();
			_target = Resolve<IBauEntityProvider>();
		}

		[SetUp]
		public void SetUp()
		{
			_storage.Clear();
		}

		[Test]
		public void GetAll_WhenGetAll_ShouldReturnAllObjectInStorage()
		{
			const string brandNameOne = "test";
			const string brandNameTwo = "test2";

			_bauEntitiesRepository.Add(brandNameOne, ObjectId.GenerateNewId());
			_bauEntitiesRepository.Add(brandNameTwo, ObjectId.GenerateNewId());

			var result = _target.GetAll();

			Assert.That(result,Has.Some.Matches<BauEntity>(x => x.BauType == brandNameOne));
			Assert.That(result, Has.Some.Matches<BauEntity>(x => x.BauType == brandNameTwo));
			Assert.That(result.Count, Is.EqualTo(2));
		}

		[Test]
		public void GetEntityById_WhenGetEntityById_ShouldReturnEntity()
		{
			_bauEntitiesRepository.Add("Test", ObjectId.GenerateNewId());
			var savedItem = _bauEntitiesRepository.GetAll();

			var result = _target.GetEntityById(savedItem.First()._id);

			Assert.That(savedItem.Count, Is.EqualTo(1));
			Assert.That(result.BauType, Is.EqualTo("Test"));
		}
	}
}