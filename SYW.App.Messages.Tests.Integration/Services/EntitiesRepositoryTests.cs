﻿using System.Collections.Generic;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Services.ExternalEmails;

namespace SYW.App.Messages.IntegrationTests.Services
{
	[TestFixture]
	public class ExternalEmailReceptorTests : IntegrationTestBase
	{
		private IExternalEmailReceptor _target;
		private IMongoStorage<Entity, ObjectId> _entitiesStorage;
		private IMongoStorage<MessageDto, ObjectId> _messageStorage;
		private IMongoStorage<ConversationDto, ObjectId> _ConversationStorage;

		[TestFixtureSetUp]
		public override void TestFixtureSetUp()
		{
			base.TestFixtureSetUp();

			_target = Resolve<IExternalEmailReceptor>();
			_entitiesStorage = Resolve<IMongoStorage<Entity, ObjectId>>();
			_messageStorage = Resolve<IMongoStorage<MessageDto, ObjectId>>();
			_ConversationStorage = Resolve<IMongoStorage<ConversationDto, ObjectId>>();
		}


		[SetUp]
		public void SetUp()
		{
			_entitiesStorage.Clear();
			_messageStorage.Clear();
			_ConversationStorage.Clear();
		}

		[Test]
		public void CreateMessage_WhenSenderIsNotFound_CreatesAnEntityForIt()
		{
			CreateUser("participant@email");
			Assert.That(GetEntityByEmail("unknown@email"), Is.Null); //sanity

			_target.CreateMessage("unknown@email", "participant@email", "content");

			var entity = GetEntityByEmail("unknown@email");
			Assert.That(entity, Is.Not.Null);
		}

		[Test]
		public void CreateMessage_WhenSenderIsNotFound_CreatedEntityIsAddedToParticipants()
		{
			CreateUser("participant@email");

			var result = _target.CreateMessage("unknown@email", "participant@email", "content");

			var authorId = GetEntityByEmail("unknown@email");

			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, result.ConversationId);
			var conversation = _ConversationStorage.GetFirstOrDefault(filter);
			Assert.That(conversation, Is.Not.Null);
			Assert.That(conversation.ParticipantsIds, Has.Exactly(1).Matches<ObjectId>(x=>x == authorId._id));
		}

		[Test]
		public void CreateMessage_WhenSenderIsNotFound_CreatedEntityIsTheAuthorOfTheMessage()
		{
			CreateUser("participant@email");

			_target.CreateMessage("unknown@email", "participant@email", "probably not");

			var createdEntity = GetEntityByEmail("unknown@email");
			var filter = Builders<MessageDto>.Filter.Eq(x => x.Content, "probably not");
			var message = _messageStorage.GetFirstOrDefault(filter);
			Assert.That(message, Is.Not.Null);
			Assert.That(message.AuthorId, Is.EqualTo(createdEntity._id));
		}


		[Test]
		public void CreateMessage_WhenParticipantIsNotFound_ThrowsException()
		{
			Assert.Throws<ParticipantNotFoundException>(() => _target.CreateMessage("sender", "unknownParticipants", "content"));
		}

		[Test]
		public void CreateMessage_WhenParticipantAreFound_TheyAreReturned()
		{
			var participant = CreateUser("participant@email");

			var result = _target.CreateMessage("unknown@email", "participant@email", "content");

			Assert.That(result.ParticipantIds, Is.EquivalentTo(new[] {participant._id}));
		}

		[Test]
		public void CreateMessage_WhenSenderIsFound_HeIsAddedToConversation()
		{
			CreateUser("participant@email");
			var sender = CreateUser("sender");

			var result = _target.CreateMessage("sender", "participant@email", "content");
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, result.ConversationId);
			var conversation = _ConversationStorage.GetFirstOrDefault(filter);
			Assert.That(conversation.ParticipantsIds, Has.Exactly(1).Matches<ObjectId>(x => x == sender._id));
		}

		[Test]
		public void CreateMessage_WhenSenderIsFound_HeIsTheAuthorOfTheMessage()
		{
			CreateUser("participant@email");
			var sender = CreateUser("sender");

			var result = _target.CreateMessage("sender", "participant@email", "hot or not?");
			var filter = Builders<MessageDto>.Filter.Eq(x => x.Content, "hot or not?");
			var message = _messageStorage.GetFirstOrDefault(filter);
			Assert.That(message, Is.Not.Null);
			Assert.That(message.AuthorId, Is.EqualTo(sender._id));
		}

		private Entity GetEntityByEmail(string entityEmail)
		{
			var filterList = new List<FilterDefinition<Entity>>
				                 {
									 Builders<Entity>.Filter.Eq(x=>x.OriginalEmail,entityEmail),
									 Builders<Entity>.Filter.Eq(x=>x.Email,entityEmail)
				                 };
			var filter = Builders<Entity>.Filter.Or(filterList);
			return _entitiesStorage.GetFirstOrDefault(filter);
		}

		private Entity CreateUser(string participantEmail)
		{
			var entity = new Entity {Email = participantEmail, _id = ObjectId.GenerateNewId()};
			_entitiesStorage.Insert(entity);
			return entity;
		}
	}
}