﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using NUnit.Framework;
using SYW.App.Messages.IntegrationTests.Core;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Services.Settings;
using UtilRunner.Utils;

namespace SYW.App.Messages.IntegrationTests.Utils
{
	[TestFixture]
	class ExpiredSystemMessagesCleaningUtilTests
		: IntegrationTestBase
	{
		private IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private IMongoStorage<UserConversationStatus, ObjectId> _userConversationStatusStorage;
		private IMongoStorage<MessageDto, ObjectId> _messagesStorage;
		private IMongoStorage<UserMessageStatus, ObjectId> _userMessageStatusStorage;
		private ISystemMessagesSettings _systemMessagesSettings;
		private IMongoCleaner _mongoCleaner;

		private Entity _testUser;
		private Entity _systemMesagesSource;
		private IExpiredSystemMessagesCleaningUtil _target;

		[SetUp]
		public void SetUp()
		{
			_conversationStorage = Resolve<IMongoStorage<ConversationDto, ObjectId>>();
			_userConversationStatusStorage = Resolve<IMongoStorage<UserConversationStatus, ObjectId>>();
			_messagesStorage = Resolve<IMongoStorage<MessageDto, ObjectId>>();
			_userMessageStatusStorage = Resolve<IMongoStorage<UserMessageStatus, ObjectId>>();
			_systemMessagesSettings = Resolve<ISystemMessagesSettings>();
			_mongoCleaner = Resolve<IMongoCleaner>();

			_target = new ExpiredSystemMessagesCleaningUtil(
				_conversationStorage,
				_messagesStorage,
				_userConversationStatusStorage,
				_userMessageStatusStorage,
				_systemMessagesSettings);
			
			_testUser = IntegrationHelper.CreateEntity(1, "gilly");
			_systemMesagesSource = IntegrationHelper.CreateEntity(0, "Lands' End");
			_mongoCleaner.Clean();
		}

		[Test]
		public void Run_WhenAllMessagesAreOldShouldDeleteAllMessagesAllConversationsAndAllStatuses()
		{
			var conversationsIds = CreateSystemConversations(10);
			CreateSystemMessages(conversationsIds, true);

			_target.Run(new string[1]);

			var projection1 = Builders<ConversationDto>.Projection.Include(x=>x._id);
			var filter1 = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var allConversationsIds = _conversationStorage.SelectAs(filter1, projection1).Select(x => x["_id"].AsObjectId);

			var projection2 = Builders<UserConversationStatus>.Projection.Include(x => x._id);
			var filter2 = Builders<UserConversationStatus>.Filter.In(x => x.ConversationId, allConversationsIds);
			var allConversationsStatusesIds = _userConversationStatusStorage.SelectAs(filter2, projection2).Select(x => x["_id"].AsObjectId);

			var projection3 = Builders<MessageDto>.Projection.Include(x => x._id);
			var filter3 = Builders<MessageDto>.Filter.In(x => x.ConversationId, allConversationsIds);
			var allMessagesIds = _messagesStorage.SelectAs(filter3, projection3).Select(x => x["_id"].AsObjectId);

			var projection4 = Builders<UserMessageStatus>.Projection.Include(x => x._id);
			var filter4 = Builders<UserMessageStatus>.Filter.In(x => x.MessageId, allMessagesIds);
			var allMessagesStatusesIds = _userMessageStatusStorage.SelectAs(filter4, projection4).Select(x => x["_id"].AsObjectId);

			Assert.That(allConversationsIds, Is.Empty);
			Assert.That(allConversationsStatusesIds, Is.Empty);
			Assert.That(allMessagesIds, Is.Empty);
			Assert.That(allMessagesStatusesIds, Is.Empty);
		}

		[Test]
		public void Run_WhenSomeMessagesAreOld_ShouldDeleteOldMessagesEmptyConversationsAndOldStatuses()
		{
			var conversationsIds = CreateSystemConversations(10).ToList();
			CreateSystemMessages(conversationsIds.GetRange(0, 5), true);
			var newMessagesIds = CreateSystemMessages(conversationsIds.GetRange(5, 5), false);

			_target.Run(new string[1]);

			var projection1 = Builders<ConversationDto>.Projection.Include(x => x._id);
			var filter1 = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var allConversationsIds = _conversationStorage.SelectAs(filter1, projection1).Select(x => x["_id"].AsObjectId).ToList();

			var projection2 = Builders<UserConversationStatus>.Projection.Include(x => x._id);
			var filter2 = Builders<UserConversationStatus>.Filter.In("ConversationId", allConversationsIds);
			var allConversationsStatusesIds = _userConversationStatusStorage.SelectAs(filter2, projection2).Select(x => x["_id"].AsObjectId).ToList();

			var projection3 = Builders<MessageDto>.Projection.Include(x => x._id);
			var filter3 = Builders<MessageDto>.Filter.In("ConversationId", allConversationsIds);
			var allMessagesIds = _messagesStorage.SelectAs(filter3, projection3).Select(x => x["_id"].AsObjectId).ToList();

			var projection4 = Builders<UserMessageStatus>.Projection.Include(x => x._id);
			var filter4 = Builders<UserMessageStatus>.Filter.In("MessageId", allMessagesIds);
			var allMessagesStatusesIds = _userMessageStatusStorage.SelectAs(filter4, projection4).Select(x => x["_id"].AsObjectId).ToList();

			Assert.That(allConversationsIds.Count == 5);
			Assert.That(allConversationsStatusesIds.Count == 5);
			Assert.That(allMessagesIds, Is.EquivalentTo(newMessagesIds));
			Assert.That(allMessagesStatusesIds.Count == 5);
		}

		[Test]
		public void Run_WhenAllMessagesAreNew_ShouldNotDeleteAnything()
		{
			var conversationsIds = CreateSystemConversations(10).ToList();
			var newMessagesIds = CreateSystemMessages(conversationsIds, false);

			_target.Run(new string[1]);

			var filter1 = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var projection1 = Builders<ConversationDto>.Projection.Include(x => x._id);
			var allConversationsIds = _conversationStorage.SelectAs(filter1,projection1).Select(x=>x["_id"].AsObjectId).ToList();

			var filter2 = Builders<UserConversationStatus>.Filter.In("ConversationId", allConversationsIds);
			var projection2 = Builders<UserConversationStatus>.Projection.Include(x => x._id);
			var allConversationsStatusesIds = _userConversationStatusStorage.SelectAs(filter2, projection2).Select(x => x["_id"].AsObjectId).ToList();

			var filter3 = Builders<MessageDto>.Filter.In("ConversationId", allConversationsIds);
			var projection3 = Builders<MessageDto>.Projection.Include(x => x._id);
			var allMessagesIds = _messagesStorage.SelectAs(filter3, projection3).Select(x => x["_id"].AsObjectId).ToList();

			var filter4 = Builders<UserMessageStatus>.Filter.In("MessageId", allMessagesIds);
			var projection4 = Builders<UserMessageStatus>.Projection.Include(x => x._id);
			var allMessagesStatusesIds = _userMessageStatusStorage.SelectAs(filter4, projection4).Select(x => x["_id"].AsObjectId).ToList();

			Assert.That(allConversationsIds, Is.EquivalentTo(conversationsIds));
			Assert.That(allConversationsStatusesIds.Count == 10);
			Assert.That(allMessagesIds, Is.EquivalentTo(newMessagesIds));
			Assert.That(allMessagesStatusesIds.Count == 10);
		}

		private IEnumerable<ObjectId> CreateSystemConversations(int amount)
		{
			var conversationsIds = new List<ObjectId>();
			for (var i = 0; i < amount; i++)
			{
				var systemConversation = new ConversationDto
											{
												CreatorId = _systemMesagesSource._id,
												IsSystemMessage = true,
												ParticipantsIds = new[] {_systemMesagesSource._id},
												StartDate = DateTime.UtcNow,
												//Timestamp = DateTime.UtcNow
											};
				_conversationStorage.Insert(systemConversation);
				conversationsIds.Add(systemConversation._id);
			}

			foreach (var covnersationId in conversationsIds)
			{
				var userConversationStatus = new UserConversationStatus
												{
													ConversationId = covnersationId,
													UserId = _testUser._id,
													Date = DateTime.UtcNow,
                                                    CreatedDateTime = DateTime.UtcNow,
													Status = (int)ConversationStatus.Read
												};
				_userConversationStatusStorage.Insert(userConversationStatus);
			}

			return conversationsIds;
		}

		private IEnumerable<ObjectId> CreateSystemMessages(IEnumerable<ObjectId> conversationsIds, bool isOld)
		{
			var messagesConversations = new Dictionary<ObjectId, ObjectId>();
			foreach (var conversationId in conversationsIds)
			{
				var message = new MessageDto
				{
					ConversationId = conversationId,
					AuthorId = _systemMesagesSource._id,
					Date = GetDate(isOld),
					EndDate = GetDate(isOld),
					Content = "<html><body>Test System Message</body></html>",
					ContentType = 1,
					PreviewText = "30% off sweaters",
					ContentHeight = 2086,
					ContentWidth = 1024,
					FeedMessageId = "tag:sears.com,2013-10-31:8f9cb1c5-cbbb-4e7c-ace6-dcae90cade36"
				};
				_messagesStorage.Insert(message);
				messagesConversations.Add(message._id, conversationId);
			}

			foreach (var conversationMessagePair in messagesConversations)
			{
				var userMessageStatus = new UserMessageStatus
				{
					ConversationId = conversationMessagePair.Value,
					MessageId = conversationMessagePair.Key,
					UserId = _testUser._id,
					Status = (int)ConversationStatus.Read,
                    Date = DateTime.UtcNow,
                    CreatedDateTime = DateTime.UtcNow
				};
				_userMessageStatusStorage.Insert(userMessageStatus);
			}

			return messagesConversations.Keys;
		}

		private DateTime GetDate(bool isOld)
		{
			return isOld ? DateTime.UtcNow.AddMonths(-7) : DateTime.UtcNow;
		}
	}
}