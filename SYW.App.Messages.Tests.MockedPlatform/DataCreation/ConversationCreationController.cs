﻿using System.Collections.Generic;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.Tests.MockedPlatform.DataCreation
{
	public class ConversationCreationController : Controller
	{
		private readonly IConversationRepository _conversationRepository;
		private readonly IEntitiesRepository _entitiesRepository;

		public ConversationCreationController(IConversationRepository conversationRepository, IEntitiesRepository entitiesRepository)
		{
			_conversationRepository = conversationRepository;
			_entitiesRepository = entitiesRepository;
		}

		[PatternRoute("/-/testing/create-system-conversation")]
		public ActionResult CreateSystemMessage(string creatorId, string content, string previewText)
		{
			//_conversationRepository.CreateSystemConversation(ObjectId.Parse(creatorId));
			return Content("OK");
		}

		[PatternRoute("/-/testing/create-conversation")]
		public ActionResult CreateMessage()
		{
			//ObjectId creator = _entitiesRepository.Get(long.Parse(originalCreatorID)).Id;

			//List<ObjectId> participents = new List<ObjectId>();
			//foreach (var participentID in originalParticipentsIDs.Split(','))
			//    participents.Add(_entitiesRepository.Get(long.Parse(participentID)).Id);

			//_conversationRepository.Create(creator, participents.ToArray());
			return Content("OK");
		}

		[PatternRoute("/-/testing/create-sears-user")]
		public ActionResult CreateSearsUser()
		{
			var newUserId = ObjectId.GenerateNewId();
			_entitiesRepository.Add(new Entity
										{
											Name = "Sears", _id = newUserId, OriginalId = 0
										});
			return Content(newUserId.ToString());
		}
	}
}