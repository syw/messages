﻿using System.Reflection;
using System.Web;
using System.Web.Routing;
using CommonGround.MvcInvocation;

namespace SYW.App.Messages.Tests.MockedPlatform
{
	public class MvcApplication : HttpApplication
	{
		protected void Application_Start()
		{
			var currentAssembly = Assembly.GetExecutingAssembly();
			RoutesRegistrar.RegisterRoutes(RouteTable.Routes, new[] {currentAssembly});
		}
	}
}