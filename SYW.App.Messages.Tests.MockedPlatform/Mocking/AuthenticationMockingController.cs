﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;

namespace SYW.App.Messages.Tests.MockedPlatform.Mocking
{
	public class AuthenticationMockingController : Controller
	{
		[PatternRoute("/auth/get-token")]
		public ActionResult MockOfflineToken()
		{
			return Json("Mocked_Offline_Token", JsonRequestBehavior.AllowGet);
		}
	}
}