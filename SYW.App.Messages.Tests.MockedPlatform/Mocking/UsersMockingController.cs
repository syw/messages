﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Messsages.Domain.Services.Platform;

namespace SYW.App.Messages.Tests.MockedPlatform.Mocking
{
	public class UsersMockingController : Controller
	{
		// TODO: refactor this to some static class, or something we can persist throughout the test
		// TODO: so we will be able to change the values between tests.
		private Dictionary<long, SocialUserInfo> _mockedUserDb;

		public UsersMockingController()
		{
			_mockedUserDb = new Dictionary<long, SocialUserInfo>();
			_mockedUserDb.Add(30, new SocialUserInfo {id = 30, name = "gilly"});
			_mockedUserDb.Add(40, new SocialUserInfo {id = 40, name = "doron"});
			_mockedUserDb.Add(50, new SocialUserInfo {id = 50, name = "nir"});
			_mockedUserDb.Add(55, new SocialUserInfo {id = 55, name = "avihu"});
		}

		[PatternRoute("/users/current")]
		public ActionResult MockCurrentUser()
		{
			return Json(new SocialUserInfo
							{
								id = 30, name = "gilly", profileUrl = "/people/gilly-the-first/" + 30
							}, JsonRequestBehavior.AllowGet);
		}

		[PatternRoute("/users/get")]
		public ActionResult GetUsersList(string ids)
		{
			if (string.IsNullOrEmpty(ids))
				return Json(new long[0], JsonRequestBehavior.AllowGet);

			var idList = ids.Split(',').Select(x => long.Parse(x)).ToArray();

			var userList = idList.Select(id => _mockedUserDb[id]).ToList();
			return Json(userList, JsonRequestBehavior.AllowGet);
		}

		[PatternRoute("/users/followers")]
		public ActionResult MockFollowers(long userId)
		{
			return Json(new[]
							{
								30, 40, 50
							}, JsonRequestBehavior.AllowGet);
		}

		[PatternRoute("/users/followed-by")]
		public ActionResult MockFollowedBy(long userId)
		{
			return Json(new[]
							{
								30, 40, 50, 55
							}, JsonRequestBehavior.AllowGet);
		}
	}
}