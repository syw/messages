﻿using System.IO;
using System.Linq;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using Mongo;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess;
using SYW.App.Messsages.Domain.Settings;
using log4net;
using log4net.Core;

namespace SYW.App.Messages.Tests.MockedPlatform.Platform
{
	public class MockedPlatformController : Controller
	{

		[PatternRoute("/mocking/init-platform-mock-url")]
		public ActionResult SomeEndpoint()
		{
			const string mockedPlatformUrl = "http://localhost:28100";

			var mongoProvider = new MongoDatabaseProvider();
			new MongoCleaner(mongoProvider).Clean();
			ILog log = LogManager.GetLogger(typeof(MongoStorage<SettingValue, ObjectId>));
			log4net.Config.XmlConfigurator.Configure(new FileInfo("log4net.config"));
			var mongoStorage = new MongoStorage<SettingValue, ObjectId>(mongoProvider, log);

			SetPlatformIntegrationSettingValue("PlatformApiBaseUrl", mockedPlatformUrl, mongoStorage);
			SetPlatformIntegrationSettingValue("PlatformSecureApiBaseUrl", mockedPlatformUrl, mongoStorage);

			return Content("OK");
		}

		private static void SetPlatformIntegrationSettingValue(string key, string value, MongoStorage<SettingValue,ObjectId> mongoStorage)
		{
			var mockedSetting = mongoStorage.GetAll()
											.FirstOrDefault(x => x.Section == "PlatformIntegrationSettings" && x.Key == key);

			var mockedSettingId = mockedSetting == null ? ObjectId.GenerateNewId() : mockedSetting._id;
			mongoStorage.Insert(new SettingValue
								{
									_id = mockedSettingId,
									Section = "PlatformIntegrationSettings",
									Key = key,
									Value = value
								});
		}
	}
}