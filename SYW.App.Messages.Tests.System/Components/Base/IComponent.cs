﻿using OpenQA.Selenium;

namespace SYW.App.Messages.SystemTests.Framework
{
	public interface IComponent
	{
		IWebElement Element { get; set; }
	}
}