﻿using OpenQA.Selenium;

namespace SYW.App.Messages.SystemTests.Framework
{
	public interface IPage
	{
		IWebDriver WebDriver { get; set; }

		void Validate();
	}
}