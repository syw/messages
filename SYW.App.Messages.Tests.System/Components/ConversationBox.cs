using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Components
{
	public class ConversationBox : IComponent
	{
		public IWebElement Element { get; set; }
		public IWebDriver WebDriver { get; set; }

		public ConversationBox(IWebElement element)
		{
			Element = element;
		}

		public IList<string> GetParticipants()
		{
			return Element.FindElement(By.CssSelector(".message .entity-name.link"))
						.Text.Split(',').Select(x => x.Trim()).ToList();
		}

		public string GetPreviewContent()
		{
			var content = Element.FindElement(By.CssSelector(".message .content"));
			return content != null ? content.Text : null;
		}

		public string GetConversationId()
		{
			return Element.GetAttribute("data-conversation-id");
		}

		public void OpenConversation()
		{
			Element.Click();
		}

		public void Archive()
		{
			WebDriver.WaitForAjax(() => Element.FindElement(By.CssSelector(".archive")).Click());
		}
	}
}