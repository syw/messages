using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework;

namespace SYW.App.Messages.SystemTests.Components
{
	public class ConversationPage : IPage
	{
		public IWebDriver WebDriver { get; set; }

		public void Validate()
		{
			WebDriver.FindElement(By.ClassName("conversation-container"));
		}

		public bool CanReply()
		{
			return WebDriver.FindElements(By.ClassName("new-message-container")).Any();
		}

		public IEnumerable<MessageInConversation> GetRegularMessages()
		{
			var messages = WebDriver.FindElements(By.CssSelector(".message-container"));
			return messages.Select(messageContainer => new MessageInConversation(messageContainer));
		}
	}
}