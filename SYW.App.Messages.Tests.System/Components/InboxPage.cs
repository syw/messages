﻿using System.Linq;
using System.Collections.Generic;
using NUnit.Framework;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Components
{
	public class InboxPage : IPage
	{
		public IWebDriver WebDriver { get; set; }

		public static string Url
		{
			get { return "/mailbox?token=12345"; }
		}

		public void Validate()
		{
			this.ValidateBodyId("conversations-page");
		}

		public IList<ConversationBox> GetConversations()
		{
			return WebDriver.FindElements(By.CssSelector(".conversations .conversation.box"))
							.Select(x => new ConversationBox(x) {WebDriver = WebDriver})
							.ToList();
		}

		public void OpenActionsPopup()
		{
			WebDriver.FindElement(By.Id("more")).Click();
			WebDriver.FindElement(By.Id("archiveAll")).WaitUntil(x => x.Displayed, 2, "archiveAll wasnt visible after clicking more button");
			WebDriver.FindElement(By.Id("markAllAsRead")).WaitUntil(x => x.Displayed, 2, "markAllAsRead wasnt visible after clicking more button");
		}

		public void ArchiveAll()
		{
			WebDriver.FindElement(By.Id("archiveAll")).Click();
			WebDriver.FindElement(By.Id("btnArchiveAll")).Click();
		}

		public void MarkAllAsRead()
		{
			WebDriver.FindElement(By.Id("markAllAsRead")).Click();
		}

		public void AssertAllConversationsAreRead()
		{
			try
			{
				WebDriver.WaitForElement(By.ClassName("unread"), 2);
			}
			catch
			{
				return;
			}

			throw new AssertionException("Found messages that are not read");
		}

		public void AssertInboxIsEmpty()
		{
			try
			{
				WaitForAnyConversation();
			}
			catch
			{
				return;
			}
			throw new AssertionException("Expected inbox to be empty, but was not");
		}

		public void WaitForAnyConversation()
		{
			WebDriver.WaitForElement(By.CssSelector(".conversations .conversation.box"), 10);
		}

		public bool HasMessageWith(string userName, string content)
		{
			WaitForAnyConversation();

			var conversations = GetConversations();
			return conversations.Any(x => x.GetParticipants().SingleOrDefault() == userName &&
										x.GetPreviewContent() == content);
		}
	}
}