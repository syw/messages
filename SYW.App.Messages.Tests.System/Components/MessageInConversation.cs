using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework;

namespace SYW.App.Messages.SystemTests.Components
{
	public class MessageInConversation : IComponent
	{
		public IWebElement Element { get; set; }
		public IWebDriver WebDriver { get; set; }

		public MessageInConversation(IWebElement element)
		{
			Element = element;
		}

		public string GetAuthor()
		{
			var content = Element.FindElement(By.CssSelector(".message .entity-name"));
			return content != null ? content.Text : null;
		}

		public string GetContent()
		{
			var content = Element.FindElement(By.CssSelector(".message .content"));
			return content != null ? content.Text : null;
		}
	}
}