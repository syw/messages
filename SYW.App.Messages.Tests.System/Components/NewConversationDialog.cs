﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Components
{
	public class NewConversationDialog : IPage
	{
		public IWebDriver WebDriver { get; set; }

		public static string Url
		{
			get { return "/mailbox/dialog/messages/new-conversation-box?token=12345"; }
		}

		public void SendNewMessage(string name, long userOriginalId, string content)
		{
			AddRecipient(name, userOriginalId);

			WebDriver.FindElement(By.Id("content")).SendKeys(content);
			EmulateClickOk();
		}

		/// <summary>
		/// Sends a message with unique content.
		/// </summary>
		public string SendNewMessage()
		{
			var content = GenerateUniqueMessage();
			SendNewMessage(name: "nir", userOriginalId: 50, content: content);

			return content;
		}

		private string GenerateUniqueMessage()
		{
			var g = Guid.NewGuid();
			var guidString = Convert.ToBase64String(g.ToByteArray());
			guidString = guidString.Replace("=", "");
			guidString = guidString.Replace("+", "");
			return guidString;
		}

		public void EmulateClickOk()
		{
			WebDriver.ExecuteScript("buttonsCallback('SEND');");
		}

		public void Validate() {}

		public void ManualAddRecipient(string recipientsName)
		{
			var participantBar = WebDriver.FindElement(By.Id("participants"));
			participantBar.Click();
			participantBar.SendKeys(recipientsName);

			WaitForAutoComplete();
			var firstAutoCompleteSuggestion = WebDriver.FindElements(By.CssSelector("ul.ui-autocomplete li a")).First();

			firstAutoCompleteSuggestion.Click();
		}

		public void AddRecipient(string recipientsName, long userOriginalId)
		{
			const string recipientsInputBoxSelector = ".recipients";
			const string participantClass = "participant";

			WebDriver.ExecuteScript(
				string.Format("$('{0}').prepend($('<span/>').data('id', {1}).addClass('{2}').text('{3}'))",
							recipientsInputBoxSelector, userOriginalId, participantClass, recipientsName));
		}

		private void WaitForAutoComplete()
		{
			// TODO: change this css to something more friendly and unique to the autocomplete
			WebDriver.WaitUntil(x => x.FindElements(By.CssSelector("ul.ui-autocomplete li")).Count > 0, 10,
								"Waiting for friends autocomplete list to open");
		}

		public IList<string> GetRecipients()
		{
			var participants = WebDriver.FindElements(By.CssSelector(".input-container.recipients .participant"));
			return participants.Select(x => x.Text).ToList();
		}
	}
}