﻿using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework;

namespace SYW.App.Messages.SystemTests.Components
{
	public class SettingsPage : IPage
	{
		public IWebDriver WebDriver { get; set; }

		public void Validate()
		{
			WebDriver.FindElement(By.ClassName("settings-container"));
		}
	}
}