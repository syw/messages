﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace SYW.App.Messages.SystemTests.Framework
{
	public static class EndPointInvoker
	{
		public static string Invoke(string url, NameValueCollection urlParams = null)
		{
			Console.WriteLine("Invoking url : " + url);
			if (urlParams == null) urlParams = new NameValueCollection();
			var webClient = new WebClient();
			var invokeUrl = new Uri(new Uri(ConfigurationManager.AppSettings["MockPlatformUrl"]), url);

			try
			{
				var response = webClient.UploadValues(invokeUrl, "POST", urlParams);
				return Encoding.UTF8.GetString(response);
			}
			catch (WebException ex)
			{
				var exceptionResponse = "undefined";
				try
				{
					exceptionResponse = GetExceptionResponse(ex);
				}
					// ReSharper disable EmptyGeneralCatchClause
				catch
					// ReSharper restore EmptyGeneralCatchClause
				{}
				var exceptionMessage = new StringBuilder().AppendFormat("Error when invoking server testing endpoint '{0}'.", invokeUrl).AppendLine();
				exceptionMessage.AppendFormat("WebException Status: {0}", ex.Status).AppendLine();
				if (exceptionResponse != null) exceptionMessage.AppendFormat("Server response: ").AppendLine(exceptionResponse);
				throw new Exception(exceptionMessage.ToString(), ex);
			}
		}

		private static string GetExceptionResponse(WebException ex)
		{
			if (ex.Status != WebExceptionStatus.ProtocolError) return null;
			if (ex.Response == null) return null;
			var rs = ex.Response.GetResponseStream();

			if (rs == null) return null;
			return new StreamReader(rs, Encoding.UTF8).ReadToEnd();
		}
	}
}