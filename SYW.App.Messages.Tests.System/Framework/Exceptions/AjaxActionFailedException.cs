﻿using System;

namespace SYW.App.Messages.SystemTests.Framework.Exceptions
{
	public class AjaxActionFailedException : Exception
	{
		public AjaxActionFailedException()
			: base("Ajax action failed") {}
	}
}