﻿using System;

namespace SYW.App.Messages.SystemTests.Framework.Exceptions
{
	public class ExpectationException : Exception
	{
		public ExpectationException(string message)
			: base(message) {}

		public ExpectationException(string message, Exception inner)
			: base(message, inner) {}
	}

	public class PageBodyIdExpectationException : ExpectationException
	{
		public PageBodyIdExpectationException(string expectedBodyId, string actual)
			: base("Expected body id of: \"" + expectedBodyId + "\" but was: \"" + actual + "\"") {}
	}
}