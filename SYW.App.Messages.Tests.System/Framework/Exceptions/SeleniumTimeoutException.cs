﻿using System;

namespace SYW.App.Messages.SystemTests.Framework.Exceptions
{
	public class SeleniumTimeoutException : Exception
	{
		public SeleniumTimeoutException(string operation, Exception exception)
			: base("Operation timed out: " + operation, exception) {}
	}
}