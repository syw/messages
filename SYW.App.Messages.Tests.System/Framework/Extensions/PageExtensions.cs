﻿using System;
using System.Linq;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework.Exceptions;

namespace SYW.App.Messages.SystemTests.Framework.Extensions
{
	public static class PageExtensions
	{
		public static void ValidateBodyId(this IPage page, params string[] expectedBodyIds)
		{
			var actual = page.GetBodyId();
			if (actual == null || expectedBodyIds.All(x => x != actual))
				throw new PageBodyIdExpectationException(expectedBodyIds[0], actual);
		}

		public static string GetBodyId(this IPage page)
		{
			var element = page.WebDriver.FindElement(By.TagName("body"));

			if (element == null)
				throw new Exception("Cannot find body Id in page");

			var actual = element.GetAttribute("id");
			return actual;
		}
	}
}