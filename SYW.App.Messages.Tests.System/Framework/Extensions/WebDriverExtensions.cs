﻿using System;
using System.Configuration;
using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SYW.App.Messages.SystemTests.Framework.Exceptions;

namespace SYW.App.Messages.SystemTests.Framework.Extensions
{
	public static class WebDriverExtensions
	{
		public static string BaseWebsiteUrl = ConfigurationManager.AppSettings["BaseWebsiteUrl"];

		public static T GoToPage<T>(this IWebDriver webDriver, string url)
			where T : IPage, new()
		{
			webDriver.Navigate().GoToUrl(BaseWebsiteUrl + url);

			var page = webDriver.As<T>();

			page.WebDriver = webDriver;

			return page;
		}

		public static T WaitUntil<T>(this IWebDriver webDriver, Func<IWebDriver, T> condition, int seconds, string operation)
		{
			var clock = new SystemClock();
			var wait = new WebDriverWait(clock, webDriver, TimeSpan.FromSeconds(seconds), TimeSpan.FromMilliseconds(200));

			try
			{
				return wait.Until(condition);
			}
			catch (TimeoutException ex)
			{
				throw new SeleniumTimeoutException(operation, ex);
			}
		}

		public static void WaitUntilElementDisappears(this IWebDriver webDriver, By locator, int seconds)
		{
			webDriver.WaitUntil(_ => !webDriver.IsElementExist(locator), seconds, string.Format("Waiting for element '{0}' to disappear", locator));
		}

		/// <summary>
		/// Waits until the specified component can be obtained and is valid.
		/// </summary>
		public static TComponent WaitFor<TComponent>(this IWebDriver target, Func<IWebDriver, TComponent> componentSelector, int seconds)
			where TComponent : class, IComponent
		{
			return target.WaitUntil(_ =>
										{
											var component = componentSelector(target);
											if (component.Element == null) return null;

											return component;
										}, seconds, string.Format("Obtain component '{0}'", typeof (TComponent).Name));
		}

		/// <summary>
		/// Waits until the specified element can be obtained and is valid.
		/// </summary>
		public static IWebElement WaitForElement(this IWebDriver target, By locator, int seconds, string errorMessage)
		{
			var clock = new SystemClock();
			var wait = new WebDriverWait(clock, target, TimeSpan.FromSeconds(seconds), TimeSpan.FromMilliseconds(200));

			try
			{
				return wait.Until(t => t.FindElement(locator));
			}
			catch (TimeoutException ex)
			{
				throw new SeleniumTimeoutException(errorMessage, ex);
			}
		}

		public static IWebElement WaitForElement(this IWebDriver target, By locator, int seconds)
		{
			return target.WaitForElement(locator, seconds, string.Format("Obtain element '{0}'", locator));
		}

		public static void WaitForAjax(this IWebDriver webDriver, Action action)
		{
			webDriver.WaitForAjax(action, 5);
		}

		public static void WaitForAjax(this IWebDriver webDriver, Action action, int seconds)
		{
			var currentActiveRequests = webDriver.GetActiveAjaxRequests();
			action();
			webDriver.WaitUntil(wd => wd.GetActiveAjaxRequests() <= currentActiveRequests, seconds, "Waiting for an AJAX request to complete");
		}

		public static int GetActiveAjaxRequests(this IWebDriver webDriver)
		{
			var result = webDriver.ExecuteScript("return jQuery.active");
			var num = Int32.Parse(result.ToString());
			return num;
		}

		public static void DeleteAllCookies(this IWebDriver webDriver)
		{
			webDriver.Manage().Cookies.DeleteAllCookies();
		}

		public static void MaximizeWindow(this IWebDriver webDriver)
		{
			if (!webDriver.GetBrowserName().ToLower().StartsWith("chrome"))
			{
				webDriver.ExecuteScript(@"if (window.screen) 
				{window.moveTo(0, 0);window.resizeTo(window.screen.availWidth, 
				window.screen.availHeight);};");
			}
		}

		public static string GetBrowserName(this IWebDriver webDriver)
		{
			var remoteWebDriver = webDriver as RemoteWebDriver;
			if (remoteWebDriver == null)
				return "";

			return remoteWebDriver.Capabilities.BrowserName;
		}

		private static IJavaScriptExecutor GetJsExecutorOrThrow(IWebDriver webDriver)
		{
			var jsExecutor = webDriver as IJavaScriptExecutor;
			if (jsExecutor == null)
				throw new Exception("expected WebDriver of type IJavaScriptExecutor");
			return jsExecutor;
		}

		public static void WaitForNotification(this IWebDriver webDriver)
		{
			var topMessage = webDriver.WaitForElement(By.Id("top-message-container"), 15);

			if (topMessage.HasClass("error"))
				throw new AjaxActionFailedException();
		}

		public static void WaitForNotificationToDisappear(this IWebDriver webDriver)
		{
			WaitUntilElementDisappears(webDriver, By.Id("top-message-container"), 15);
		}

		public static void Hover(this IWebDriver webDriver, IWebElement element)
		{
			//This will sometime work, when our Selenium version will be compatible with our Firefox version
			var actions = new Actions(webDriver);
			var build = actions.MoveToElement(element).Build();
			build.Perform();
		}

		public static void HoverUsingJquery(this IWebDriver webDriver, IWebElement element)
		{
			var id = element.GetAttribute("id");
			if (id == String.Empty)
			{
				return;
			}
			webDriver.ExecuteScript("$('#" + id + "').trigger('mouseover');");
		}

		public static void HoverUsingJquery(this IWebDriver webDriver, string locator)
		{
			webDriver.ExecuteScript("$('" + locator + "').trigger('mouseover');");
		}

		public static void NewWindow(this IWebDriver webDriver, int currentWindowsCount)
		{
			var newWindow = webDriver.WindowHandles[currentWindowsCount];
			webDriver.WaitUntil(x =>
									{
										try
										{
											webDriver.SwitchTo().Window(newWindow);
										}
										catch
										{
											return false;
										}
										return true;
									}, 20, "Open and switch to new window");
		}

		public static Screenshot GetScreenshot(this IWebDriver webDriver)
		{
			var takeScreenShot = webDriver as ITakesScreenshot;
			if (takeScreenShot == null)
			{
				throw new Exception("webdrive is not of ITakeScreenShot type!");
			}
			return takeScreenShot.GetScreenshot();
		}

		public static object ExecuteScript(this IWebDriver webDriver, string script)
		{
			return GetJsExecutorOrThrow(webDriver).ExecuteScript(script);
		}

		public static bool IsElementExist(this ISearchContext target, By selector)
		{
			return target.FindElements(selector).Any();
		}

		public static void CloseWebDriver(this IWebDriver webDriver)
		{
			if (webDriver == null)
				return;
			try
			{
				webDriver.Close();
				webDriver.Dispose();
			}
			catch (Exception e)
			{
				Console.Error.WriteLine("Error closing Webdriver! {0}", e);
			}
		}

		public static void CloseAlertIfExists(this IWebDriver webDriver)
		{
			try
			{
				webDriver.SwitchTo().Alert().Accept();
			}
			catch (NoAlertPresentException) {}
		}

		public static void AssertGlassWall(this IWebDriver webDriver)
		{
			webDriver.WaitForElement(By.Id("join-teaser-modal"), 20, "GlassWall should appear...");
		}
	}
}