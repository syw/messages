﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using SYW.App.Messages.SystemTests.Framework.Exceptions;

namespace SYW.App.Messages.SystemTests.Framework.Extensions
{
	public static class WebElementExtensions
	{
		private const int BrowserRetryAmount = 5;

		public static T As<T>(this IWebElement element)
			where T : IComponent, new()
		{
			return new T {Element = element};
		}

		public static T[] As<T>(this ReadOnlyCollection<IWebElement> elements)
			where T : IComponent, new()
		{
			return elements.Select(x => x.As<T>()).ToArray();
		}

		public static T As<T>(this IWebDriver driver) where T : IPage, new()
		{
			return As<T>(driver, 4);
		}

		public static T As<T>(this IWebDriver driver, int timeoutInSeconds) where T : IPage, new()
		{
			var page = CreatePage<T>(driver);
			var exception = new ElementNotVisibleException("Validate page as " + typeof (T).Name + " After " + BrowserRetryAmount + " Retries...");
			ExecuteWithRetry(() => driver.SwitchTo().DefaultContent(), exception);
			ExecuteWithRetry(() => ValidatePage(driver, page, timeoutInSeconds), exception);
			ValidationHelper.ValidatePageJsErrors(driver);
			return page;
		}

		private static void ExecuteWithRetry(Action action, Exception toThrow)
		{
			for (var i = 0; i < BrowserRetryAmount; i++)
			{
				try
				{
					action();
					return;
				}
				catch (WebDriverException)
				{
					Thread.Sleep(300);
				}
			}

			throw toThrow;
		}


		private static void ValidatePage<T>(IWebDriver driver, T page, int timeoutInSeconds) where T : IPage
		{
			driver.WaitUntil(x =>
								{
									try
									{
										page.Validate();
									}
									catch
									{
										return false;
									}
									finally
									{
										CheckForErrorPage(driver);
									}
									return true;
								}, timeoutInSeconds, "Validate page as " + typeof (T).Name);
		}

		private static void CheckForErrorPage(IWebDriver driver)
		{
			var serverError = driver.FindElements(By.TagName("H1")).FirstOrDefault(x => x.Text.Contains("Server Error"));
			if (serverError == null)
				return;
			var error = "Error page detected!\n" + driver.FindElement(By.TagName("body")).Text;
			throw new Exception(error);
		}

		public static T AsIframe<T>(this IWebDriver driver, string frameName, int timeoutInSeconds) where T : IPage, new()
		{
			var page = CreatePage<T>(driver);

			driver.SwitchTo().Frame(frameName);

			ValidatePage(driver, page, timeoutInSeconds);

			return page;
		}

		private static T CreatePage<T>(IWebDriver driver) where T : IPage, new()
		{
			return new T {WebDriver = driver};
		}

		public static IWebDriver GetParentWebDriver(this IWebElement webElement)
		{
			var remoteWebDriver = webElement as RemoteWebElement;
			if (remoteWebDriver == null)
				throw new Exception("Can't get parent webdriver for an element which is not of type RemoteWebElement");

			return remoteWebDriver.WrappedDriver;
		}

		public static bool HasClass(this IWebElement webElement, string className)
		{
			var classes = webElement.GetAttribute("class");
			if (string.IsNullOrEmpty(classes))
				return false;

			var classArray = classes.Split(' ');
			return classArray.Contains(className);
		}

		public static bool IsSelectElement(this IWebElement webElement)
		{
			return (webElement.TagName.ToLower() == "select");
		}

		public static SelectElement AsSelectElement(this IWebElement webElement)
		{
			if (webElement.TagName.ToLower() != "select")
				throw new InvalidElementStateException(string.Format("Expected <select> element but found <{0}>", webElement.TagName));

			return new SelectElement(webElement);
		}

		public static IWebElement FindOptionalElement(this ISearchContext target, By selector)
		{
			return target.FindElements(selector).FirstOrDefault();
		}

		/// <summary>
		/// Waits until the specified element can be obtained and is valid.
		/// </summary>
		public static IWebElement WaitForElement(this IWebElement target, By locator, int seconds)
		{
			return target.GetParentWebDriver().WaitForElement(locator, seconds);
		}

		public static T WaitUntil<T>(this IWebElement webDriver, Func<IWebElement, T> condition, int seconds, string operation)
		{
			var clock = new SystemClock();
			var wait = new WebElementWait(clock, webDriver, TimeSpan.FromSeconds(seconds), TimeSpan.FromMilliseconds(200));

			try
			{
				return wait.Until(condition);
			}
			catch (TimeoutException ex)
			{
				throw new SeleniumTimeoutException(operation, ex);
			}
		}
	}
}