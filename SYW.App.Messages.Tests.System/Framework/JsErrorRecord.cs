﻿using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace SYW.App.Messages.SystemTests.Framework
{
	public class JsErrorRecord
	{
		public string ErrorMessage { get; set; }
		public string SourceName { get; set; }
		public string PageUrl { get; set; }
		public int LineNumber { get; set; }

		public string FriendlyDescription
		{
			get { return ErrorMessage + " found at " + SourceName + " in line number " + LineNumber + ". pageUrl:" + PageUrl; }
		}
	}

	public class JsErrorsFoundException : WebDriverException
	{
		private readonly string _exMessage;

		public JsErrorsFoundException(string message)
			: base("")
		{
			_exMessage = message;
		}

		public IEnumerable<JsErrorRecord> JsErrorRecords { get; set; }

		public override string Message
		{
			get { return FormatMessage(); }
		}

		protected virtual string FormatMessage()
		{
			return JsErrorRecords.Aggregate(_exMessage, (current, record) => current + record.FriendlyDescription);
		}
	}
}