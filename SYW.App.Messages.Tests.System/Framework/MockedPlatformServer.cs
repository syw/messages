﻿using System;
using System.Diagnostics;
using System.IO;

namespace SYW.App.Messages.SystemTests.Framework
{
	public class MockedPlatformServer : IDisposable
	{
		private readonly Process _devServer;

		public static int Port = 28100;

		private string _devServerExe
		{
			get { return ProgramFilesHelper.GetProgrameFilesCorrectPath(@"C:\{Program Files}\Common Files\microsoft shared\DevServer\10.0\WebDev.WebServer40.EXE"); }
		}

		private string _mockedPlatformSitePath
		{
			get
			{
				var currentDirectory = Directory.GetCurrentDirectory();
				return Path.GetFullPath(currentDirectory + @"..\..\..\..\SYW.App.Messages.Tests.MockedPlatform\");
			}
		}

		public MockedPlatformServer()
		{
			_devServer = new Process
							{
								StartInfo =
									{
										FileName = _devServerExe,
										Arguments = string.Format("/port:{0} /path:{1}", Port, Path.Combine(Directory.GetCurrentDirectory(), _mockedPlatformSitePath))
									}
							};

			try
			{
				_devServer.Start();
			}
			catch
			{
				Console.WriteLine("Unable to start development server for the immortal test site");
				throw;
			}
		}

		public void Dispose()
		{
			try
			{
				_devServer.Kill();
				_devServer.Dispose();
			}
			catch {}
		}
	}
}