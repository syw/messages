﻿using System;
using System.ServiceProcess;
using MongoDB.Driver;

namespace SYW.App.Messages.SystemTests.Framework
{
	public class MongoServerForTesting
	{
		private ServiceController _mongoService;

		public MongoServerForTesting()
		{
			_mongoService = new ServiceController("MongoDB");
		}

		public void Start()
		{
			try
			{
				if (_mongoService.Status != ServiceControllerStatus.Running)
					_mongoService.Start();
				_mongoService.WaitForStatus(ServiceControllerStatus.Running, TimeSpan.FromSeconds(30));
			}
			catch (Exception ex)
			{
				throw new MongoException("TestMongoServer: could not start MongoDB", ex);
			}
		}
	}
}