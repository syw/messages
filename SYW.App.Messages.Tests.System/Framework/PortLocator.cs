﻿using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;

namespace SYW.App.Messages.SystemTests.Framework
{
	public class PortLocator
	{
		private List<int> _triedPorts;

		public PortLocator()
		{
			_triedPorts = new List<int>();
		}

		public int GetAvailablePort()
		{
			const int portStartIndex = 7000;
			const int portEndIndex = 9999;
			var properties = IPGlobalProperties.GetIPGlobalProperties();
			var tcpEndPoints = properties.GetActiveTcpListeners();

			var usedPorts = tcpEndPoints.Select(p => p.Port).ToList();
			var unusedPort = 0;

			for (var port = portStartIndex; port < portEndIndex; port++)
			{
				if (!usedPorts.Contains(port) && !_triedPorts.Contains(port))
				{
					unusedPort = port;
					break;
				}
			}

			_triedPorts.Add(unusedPort);
			return unusedPort;
		}
	}
}