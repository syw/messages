﻿using System.IO;

namespace SYW.App.Messages.SystemTests.Framework
{
	public static class ProgramFilesHelper
	{
		public static string GetProgrameFilesCorrectPath(string path)
		{
			DirectoryInfo dir = new DirectoryInfo(@"C:\Program Files (x86)");
			string replacemant = dir.Exists ? "Program Files (x86)" : "Program Files";
			return path.Replace("{Program Files}", replacemant);
		}
	}
}