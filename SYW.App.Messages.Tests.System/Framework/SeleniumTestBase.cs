﻿using System;
using System.Configuration;
using NUnit.Framework;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Components;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Framework
{
	[Ignore]
	[TestFixture]
	public class SeleniumTestBase
	{
		protected IWebDriver WebDriver { get; private set; }
		protected bool DeleteAllCookiesBetweenTests { get; set; }

		private MongoServerForTesting _mongoServerForTesting;
		private MockedPlatformServer _mockedPlatformServer;

		protected SeleniumTestBase()
		{
			DeleteAllCookiesBetweenTests = true;
			_mongoServerForTesting = new MongoServerForTesting();
		}

		[TestFixtureSetUp]
		protected void TestFixtureSetUp()
		{
			Console.Error.WriteLine(" == Starting TestFixture == ");
			Console.WriteLine(" == Starting TestFixture == ");
			WebDriver = WebDriverInitializer.InitWebDriver(ConfigurationManager.AppSettings);
			_mongoServerForTesting.Start();

			SetUpMockPlatform(); //also takes care of Mongo cleaning.
		}

		private void SetUpMockPlatform()
		{
			_mockedPlatformServer = new MockedPlatformServer();
			var result = EndPointInvoker.Invoke("/mocking/init-platform-mock-url");
			if (result != "OK")
				throw new Exception("invoking mocking endpoint failed");
		}

		[SetUp]
		protected void TestSetUp()
		{
			RefreshCookies();
		}

		[TearDown]
		protected void TestTearDown()
		{
			if (!DeleteAllCookiesBetweenTests) return;
			try
			{
				WebDriver.DeleteAllCookies();
			}

			catch {}
		}

		[TestFixtureTearDown]
		protected void TestFixtureTearDown()
		{
			WebDriver.CloseWebDriver();
			_mockedPlatformServer.Dispose();
		}

		private void RefreshCookies()
		{
			//This should invalidate any cookies so we can start afresh.
			WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["BaseWebsiteUrl"] + "/-/refresh-settings");
			WebDriver.GoToPage<InboxPage>(InboxPage.Url);
		}
	}
}