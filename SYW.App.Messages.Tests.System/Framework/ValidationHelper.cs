﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Framework
{
	public class ValidationHelper
	{
		private const string WhiteList =
			"(" +
			"window._adr" +
			"|elems is null" +
			"|https://ohio.local/static/js/profile-settings.js" +
			"|Node cannot be inserted at the specified point in the hierarchy" +
			"|NS_ERROR_DOM_SECURITY_ERR" +
			"|swfobject.js" +
			"|data is null" +
			"|http://s.ytimg.com/yt/jsbin/html5player-vfl17mxzL.js" +
			")";

		public static void ValidatePageJsErrors(IWebDriver driver)
		{
			try
			{
				var errors = (driver.ExecuteScript("return window.JSErrorCollector_errors.pump()")) as ReadOnlyCollection<object>;
				if (errors == null || errors.Count == 0)
					return;

				var recordList = errors.Select(err =>
													{
														var record = err as Dictionary<string, object>;
														return record != null ? new JsErrorRecord
																					{
																						ErrorMessage = record["errorMessage"].ToString(),
																						LineNumber = int.Parse(record["lineNumber"].ToString()),
																						SourceName = record["sourceName"].ToString(),
																						PageUrl = driver.Url
																					} : null;
													}).Where(err => !IsInWhiteList(err));

				if (recordList.Any())
				{
					throw new JsErrorsFoundException("The following js errors found on page: ")
							{
								JsErrorRecords = recordList
							};
				}
			}
			catch (InvalidOperationException) {}
		}

		private static bool IsInWhiteList(JsErrorRecord record)
		{
			var pattern = new Regex(WhiteList);

			return pattern.IsMatch(record.ErrorMessage) || pattern.IsMatch(record.SourceName);
		}
	}
}