using System;
using System.Collections.Specialized;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Framework
{
	public static class WebDriverInitializer
	{
		private const int SeleniumTimeOutSeconds = 1;
		private const int WebDriverStartTimeout = 10*1000;

		public enum BrowserType
		{
			firefox,
			explorer,
			chrome
		}

		public static IWebDriver InitWebDriver(NameValueCollection appSettings)
		{
			var webDriver = CreateWebDriver(null, appSettings);
			webDriver.MaximizeWindow();
			webDriver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, SeleniumTimeOutSeconds));
			return webDriver;
		}

		public static IWebDriver InitWebDriver(BrowserType browserType)
		{
			var webDriver = CreateWebDriver(browserType, null);
			webDriver.MaximizeWindow();
			webDriver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 0, 0, SeleniumTimeOutSeconds));
			return webDriver;
		}

		private static IWebDriver CreateWebDriver(BrowserType? browserType, NameValueCollection appSettings)
		{
			if (browserType == null)
			{
				var type = appSettings["BrowserType"];
				if (type == null)
					browserType = BrowserType.firefox;
				else
					browserType = (BrowserType)Enum.Parse(typeof (BrowserType), type.ToLower());
			}

			switch (browserType)
			{
				case BrowserType.explorer:
					{
						return new InternetExplorerDriver(new InternetExplorerOptions {IntroduceInstabilityByIgnoringProtectedModeSettings = true});
					}
				case BrowserType.chrome:
					var options = new ChromeOptions();
					options.AddArgument("--start-maximized");
					return new ChromeDriver(options);
				default:
					return InitializeFireFoxDriver(appSettings);
			}
		}

		private static IWebDriver InitializeFireFoxDriver(NameValueCollection appSettings)
		{
			var nameValueCollection = appSettings ?? new NameValueCollection();
			var profilePath = nameValueCollection["FirefoxProfile"];
			var binaryPath = nameValueCollection["FirefoxPath"];

			var firefoxProfile = new FirefoxProfile();
			var firefoxBinary = new FirefoxBinary {TimeoutInMilliseconds = WebDriverStartTimeout};
			if (profilePath != null && Directory.Exists(profilePath) && binaryPath != null && File.Exists(binaryPath))
			{
				firefoxBinary = new FirefoxBinary(binaryPath);
				firefoxProfile = new FirefoxProfile(profilePath);
			}

			TryAddFirebug(firefoxProfile, appSettings);
			TryAddJsErrorCollector(firefoxProfile, appSettings);

			var tryCount = 0;
			const int retryAmount = 3;
			var portLocator = new PortLocator();
			while (tryCount < retryAmount)
			{
				try
				{
					firefoxProfile.Port = portLocator.GetAvailablePort();
					return new FirefoxDriver(firefoxBinary, firefoxProfile);
				}
				catch (WebDriverException e)
				{
					if (!e.Message.Contains("Unable to bind to locking port"))
						throw;
					tryCount++;
				}
			}
			throw new Exception("Couldn't initiate Webdriver after " + retryAmount + " Retries!");
		}

		private static void TryAddJsErrorCollector(FirefoxProfile firefoxProfile, NameValueCollection appSettings)
		{
			var errorCollectorFile = appSettings.Get("JsCollectorExtension");
			var firebugVersion = appSettings.Get("FirebugVersion");

			if (errorCollectorFile == null)
				return;

			try
			{
				if (File.Exists(errorCollectorFile))
				{
					firefoxProfile.AddExtension(errorCollectorFile);
				}
				firefoxProfile.SetPreference("extensions.jsErrorCollector.currentVersion", firebugVersion);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed to attach jsErrorCollector to the FireFoxDriver.");
				Console.WriteLine("Error : " + ex.Message);
			}
		}

		private static void TryAddFirebug(FirefoxProfile firefoxProfile, NameValueCollection appSettings)
		{
			var firebugFile = appSettings.Get("FirebugExtension");
			var firebugVersion = appSettings.Get("FirebugVersion");

			if (firebugFile == null)
				return;

			firebugFile = ProgramFilesHelper.GetProgrameFilesCorrectPath(firebugFile);

			try
			{
				if (File.Exists(firebugFile))
				{
					firefoxProfile.AddExtension(firebugFile);
				}
				firefoxProfile.SetPreference("extensions.firebug.currentVersion", firebugVersion);
			}
			catch (Exception ex)
			{
				Console.WriteLine("Failed to attach firebug to the FireFoxDriver.");
				Console.WriteLine("Error : " + ex.Message);
			}
		}
	}
}