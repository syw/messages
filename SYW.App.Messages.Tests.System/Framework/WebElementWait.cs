﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace SYW.App.Messages.SystemTests.Framework
{
	public class WebElementWait : DefaultWait<IWebElement>
	{
		private static readonly TimeSpan DefaultSleepTimeout = TimeSpan.FromMilliseconds(500);

		public WebElementWait(IWebElement element, TimeSpan timeout)
			: this(new SystemClock(), element, timeout, DefaultSleepTimeout) {}

		public WebElementWait(IClock clock, IWebElement webelement, TimeSpan timeout, TimeSpan sleepInterval)
			: base(webelement, clock)
		{
			Timeout = timeout;
			PollingInterval = sleepInterval;
			IgnoreExceptionTypes(typeof (NotFoundException));
		}
	}
}