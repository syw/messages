// Render Multiple URLs to file

var RenderUrlsToFile, bauEmailsIds, system, renderingServiceUrl;

system = require("system");

/*
Render given urls
@param array of URLs to render
@param callbackPerUrl Function called after finishing each URL, including the last URL
@param callbackFinal Function called after finishing everything
*/
RenderUrlsToFile = function (ids, callbackPerUrl, callbackFinal) {
	var getFilename, next, page, retrieve, webpage;
	webpage = require("webpage");
	page = null;
	getFilename = function (id) {
		return id + ".jpg";
	};
	next = function (status, url, file) {
		page.close();
		callbackPerUrl(status, url, file);
		return retrieve();
	};
	retrieve = function () {
		var id;
		if (ids.length > 0) {
			id = ids.shift();
			page = webpage.create();
			page.viewportSize = {
				width: 1024,
				height: 768
			};
			page.settings.userAgent = "Phantom.js bot";
			return page.open(renderingServiceUrl + "" + id, function (status) {
				var file;
				file = getFilename(id);
				if (status === "success") {
					return window.setTimeout((function () {
						page.render(file);
						return next(status, id, file);
					}), 200);
				} else {
					return next(status, id, file);
				}
			});
		} else {
			return callbackFinal();
		}
	};
	return retrieve();
};

bauEmailsIds = Array.prototype.slice.call(system.args, 1);

renderingServiceUrl = bauEmailsIds.shift();

RenderUrlsToFile(bauEmailsIds, (function (status, url, file) {
	if (status !== "success") {
		return system.stderr.writeLine("Unable to render '" + url + "'");
	} else {
		return console.log("Rendered '" + url + "' at '" + file + "'");
	}
}), function () {
	return phantom.exit();
});