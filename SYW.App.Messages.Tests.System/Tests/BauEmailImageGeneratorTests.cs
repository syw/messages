﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Feeds;
using log4net;
using MongoDB.Driver;

namespace SYW.App.Messages.SystemTests.Tests
{
	[TestFixture]
	public class BauEmailImageGeneratorTests
	{
		private IMongoStorage<BauEmailFeedObjectDto,ObjectId> _mongoStorage;
		private ILog _log;
		private IBauEmailImageGeneratorSettings _bauEmailImageGeneratorSettings;
		private IBauScreenshotsFilesService _bauScreenshotsFilesService;
		private ITumbnailCreatorService _tumbnailCreatorService;

		private IBauEmailImageGenerator _target;

		[SetUp]
		public void SetUp()
		{
			_mongoStorage = MockRepository.GenerateStub<IMongoStorage<BauEmailFeedObjectDto, ObjectId>>();
			_log = MockRepository.GenerateStub<ILog>();
			_bauEmailImageGeneratorSettings = MockRepository.GenerateStub<IBauEmailImageGeneratorSettings>();
			_bauScreenshotsFilesService = MockRepository.GenerateStub<IBauScreenshotsFilesService>();
			_tumbnailCreatorService = new TumbnailCreatorService();

			_target = new BauEmailImageGenerator(_mongoStorage, _log, _bauEmailImageGeneratorSettings, _bauScreenshotsFilesService, _tumbnailCreatorService);
		}

		[Test]
		public void Run_WhenAllBauEmailsDoesntHaveScreenShotsInPhantomJsFolder_ShouldCreateScreenShotsToPhantomJsFolderAndCopyResizedScreenShotToStaticContentPathAndUpdateDb()
		{
			var testedBauEmailImageId = ObjectId.GenerateNewId();
			var secondTestedBauEmailImageId = ObjectId.GenerateNewId();
			SetBauImageGeneratorSettings();
			var bauEmailFeedObjects = SetBauEmailFeedObjectDto(new ObjectId[] { testedBauEmailImageId, secondTestedBauEmailImageId });
			SetMongoStorage(bauEmailFeedObjects);
			SetBauScreenshotsFilesService();

			_target.GenerateImage();
			Assert.That(CheckIfScreenShotExistInTargetFolderAndSourceFolder(testedBauEmailImageId.ToString()), Is.True);
			Assert.That(CheckIfScreenShotHasResized(testedBauEmailImageId.ToString()), Is.True);

			Assert.That(CheckIfScreenShotExistInTargetFolderAndSourceFolder(secondTestedBauEmailImageId.ToString()), Is.True);
			Assert.That(CheckIfScreenShotHasResized(secondTestedBauEmailImageId.ToString()), Is.True);

			_mongoStorage.AssertWasCalled(x => x.Upsert(Arg<BauEmailFeedObjectDto>.Is.Same(bauEmailFeedObjects.ElementAt(0))));
			_mongoStorage.AssertWasCalled(x => x.Upsert(Arg<BauEmailFeedObjectDto>.Is.Same(bauEmailFeedObjects.ElementAt(1))));

			DeleteCreatedTestImages(new[] { testedBauEmailImageId, secondTestedBauEmailImageId });
		}

		[Test]
		public void Run_WhenOneBauEmailsHaveScreenShotInPhantomJsFolder_ShouldCopyResizedScreenShotToStaticContentPathAndUpdateDb()
		{
			var testedExistsBauEmailImageId = ObjectId.GenerateNewId();
			SetBauImageGeneratorSettings();
			var bauEmailFeedObjects = SetBauEmailFeedObjectDto(new[] { testedExistsBauEmailImageId });
			SetMongoStorage(bauEmailFeedObjects);
			SetBauScreenshotsFilesService();
			CreateImageInPhantomJsFolder(testedExistsBauEmailImageId);

			_target.GenerateImage();

			Assert.That(CheckIfScreenShotExistInTargetFolderAndSourceFolder(testedExistsBauEmailImageId.ToString()), Is.True);
			Assert.That(CheckIfScreenShotHasResized(testedExistsBauEmailImageId.ToString()), Is.True);

			_mongoStorage.AssertWasCalled(x => x.Upsert(Arg<BauEmailFeedObjectDto>.Is.Same(bauEmailFeedObjects.ElementAt(0))));

			DeleteCreatedTestImages(new[] { testedExistsBauEmailImageId });
		}

		[Test]
		public void Run_WhenAllBauEmailsDoesntHaveScreenShotsInPhantomJsFolderAndPhantomJsReturnsErrors_ShouldLogErrorAndExit()
		{
			var testedExistsBauEmailImageId = ObjectId.GenerateNewId();
			var secondTestedBauEmailImageId = ObjectId.GenerateNewId();
			SetBauImageGeneratorSettings(false);
			var bauEmailFeedObjects = SetBauEmailFeedObjectDto(new[] { testedExistsBauEmailImageId, secondTestedBauEmailImageId });
			SetMongoStorage(bauEmailFeedObjects);
			SetBauScreenshotsFilesService();

			_target.GenerateImage();

			_log.AssertWasCalled(x => x.Error(Arg<string>.Is.Anything, Arg<Exception>.Is.Anything));
			_mongoStorage.AssertWasNotCalled(x => x.Upsert(Arg<BauEmailFeedObjectDto>.Is.Same(bauEmailFeedObjects.ElementAt(0))));
			_mongoStorage.AssertWasNotCalled(x => x.Upsert(Arg<BauEmailFeedObjectDto>.Is.Same(bauEmailFeedObjects.ElementAt(1))));
		}

		private void SetBauImageGeneratorSettings(bool isValidSettings = true)
		{
			_bauEmailImageGeneratorSettings.ThumbnailWidth = 208;
			_bauEmailImageGeneratorSettings.BauImageRenderingUrl = ConfigurationManager.AppSettings["BauEndpointUrl"] + "/bau/";
			_bauEmailImageGeneratorSettings.SystemMessageImageRenderingUrl = ConfigurationManager.AppSettings["BauEndpointUrl"] + "/bau/message/";

			_bauEmailImageGeneratorSettings.StaticContentPath = Path.Combine(Path.GetFullPath("../../"), "UtilResizedImages");

			_bauEmailImageGeneratorSettings.PhatomJsPath = (isValidSettings) ? Path.Combine(Path.GetFullPath("../../"), "PhantomJsFolder") : "Invalid Path";
		}

		private void CreateImageInPhantomJsFolder(ObjectId bauEmailImageId)
		{
			using (var createdImage = new Bitmap(1024, 768))
			{
				createdImage.Save(_bauScreenshotsFilesService.GetBauScreenshotPath(bauEmailImageId.ToString()));
			}
		}

		private void DeleteCreatedTestImages(IEnumerable<ObjectId> bauEmailImageId)
		{
			foreach (var imageId in bauEmailImageId)
			{
				var imageToDeletePhantomJsFolder = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(imageId.ToString()));
				if (imageToDeletePhantomJsFolder.Exists)
					imageToDeletePhantomJsFolder.Delete();

				var imageToDeleteSourceFolder = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(imageId.ToString()));
				if (imageToDeleteSourceFolder.Exists)
					imageToDeleteSourceFolder.Delete();
			}
		}

		private List<BauEmailFeedObjectDto> SetBauEmailFeedObjectDto(IEnumerable<ObjectId> testedBauEmailImageIds)
		{
			var images = new List<BauEmailFeedObjectDto>();
			foreach (var bauEmailImageId in testedBauEmailImageIds)
			{
				images.Add(new BauEmailFeedObjectDto { _id = bauEmailImageId, HasScreenshot = false, Content = "" });
			}
			return images;
		}

		private void SetMongoStorage(List<BauEmailFeedObjectDto> testedBauEmailFeedObjects)
		{
			_mongoStorage.Stub(x => x.GetList(Arg<FilterDefinition<BauEmailFeedObjectDto>>.Is.Anything)).Return(testedBauEmailFeedObjects);
		}

		private void SetBauScreenshotsFilesService()
		{
			_bauScreenshotsFilesService.Stub(x => x.GetBauScreenshotPath(Arg<string>.Is.Anything))
				.Do((Func<string, string>)(id => Path.Combine(Path.Combine(Path.GetFullPath("../../"), "PhantomJsFolder"), id + ".jpg")));

			_bauScreenshotsFilesService.Stub(x => x.GetBauStaticContentScreenshotPath(Arg<string>.Is.Anything))
				.Do((Func<string, string>)(id => Path.Combine(_bauEmailImageGeneratorSettings.StaticContentPath, id + ".jpg")));

			_bauScreenshotsFilesService.Stub(x => x.BauScreenShotExists(Arg<string>.Is.Anything))
				.Do((Func<string, bool>)(id => new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(id)).Exists));
		}

		private bool CheckIfScreenShotExistInTargetFolderAndSourceFolder(string screenShotId)
		{
			var resizedScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(screenShotId));
			var originalScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(screenShotId));
			return resizedScreenshot.Exists && originalScreenshot.Exists;
		}

		private bool CheckIfScreenShotHasResized(string screenShotId)
		{
			var originalScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(screenShotId));
			var resizedScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(screenShotId));
			var originalImage = Image.FromFile(originalScreenshot.FullName);
			var resizedImage = Image.FromFile(resizedScreenshot.FullName);
			var percentWidth = ((float)originalImage.Width / (float)_bauEmailImageGeneratorSettings.ThumbnailWidth);

			var isResizedCorrectly = (resizedImage.Width == _bauEmailImageGeneratorSettings.ThumbnailWidth && resizedImage.Height == (int)(originalImage.Height / percentWidth));
			originalImage.Dispose();
			resizedImage.Dispose();
			return isResizedCorrectly;
		}
	}
}