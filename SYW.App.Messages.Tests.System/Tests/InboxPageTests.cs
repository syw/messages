﻿using System;
using System.Collections.Specialized;
using System.Linq;
using NUnit.Framework;
using SYW.App.Messages.SystemTests.Components;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Tests
{
	[Ignore]
	[TestFixture]
	public class InboxPageTests : SeleniumTestBase
	{
		[Test(Description = "6181 - Clicking on an existing messages opens the thread")]
		public void ClickingOnConversationOpensTheThread()
		{
			var conversation = SetUpMessageInInbox();
			var expectedText = conversation.GetPreviewContent();
			conversation.OpenConversation();

			var conversationPage = WebDriver.AsIframe<ConversationPage>("messages-frame", 4);

			var messages = conversationPage.GetRegularMessages();
			Assert.That(messages.Select(m => m.GetContent()), Has.Some.EqualTo(expectedText));
		}

		[Test]
		public void ClickingOnArchiveRemoveTheConversation()
		{
			var conversation = SetUpMessageInInbox();
			var expectedId = conversation.GetConversationId();

			conversation.Archive();

			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			var conversationsInInboxIds = inboxPage.GetConversations().Select(c => c.GetConversationId());
			Assert.That(conversationsInInboxIds, Has.No.EqualTo(expectedId));
		}

		private ConversationBox SetUpMessageInInbox()
		{
			var newConversationsPage = WebDriver.GoToPage<NewConversationDialog>(NewConversationDialog.Url);
			newConversationsPage.SendNewMessage();

			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);

			inboxPage.WaitForAnyConversation();
			return inboxPage.GetConversations().First();
		}

		private void EmulateNewMessage()
		{
			EndPointInvoker.Invoke("/-/testing/create-conversation");
		}

		#region ignored tests

		[Test]
		[Ignore("We don't have a good enough mocking platform to take care of this yet... :(  -Gilly")]
		public void UserCantReplyToSystemConversation()
		{
			var systemUserId = EndPointInvoker.Invoke("/-/testing/create-sears-user");
			EndPointInvoker.Invoke("/-/testing/create-system-conversation", new NameValueCollection
																				{
																					{"creatorId", systemUserId},
																					{"content", "This is just a plain system message"},
																					{"previewText", "Just a system message..."}
																				});
			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url); //should add Assert.That(inboxPage.AnyConversationExistWithWaiting());
			var systemMessage = inboxPage.GetConversations().First(x => x.GetParticipants().Contains("Sears"));
			systemMessage.OpenConversation();

			var systemConversation = WebDriver.As<ConversationPage>();

			Assert.That(systemConversation.CanReply(), Is.False);
		}

		[Test(Description = "Clicking on ArchiveAll removes all conversations")]
		[Ignore("Waiting for upgarde in mocked platform capabilities - Saar")]
		public void ClickingOnArchiveAllRemovesAllConversations()
		{
			EmulateNewMessage();
			EmulateNewMessage();

			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			inboxPage.OpenActionsPopup();
			inboxPage.ArchiveAll();

			inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			inboxPage.AssertInboxIsEmpty();
		}

		[Test(Description = "Clicking on MarkAllAsRead marks all conversations as read")]
		[Ignore("Waiting for upgarde in mocked platform capabilities - Saar")]
		public void ClickingOnMarkAllAsReadMarksAllConversationsAsRead()
		{
			EmulateNewMessage();
			EmulateNewMessage();

			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			inboxPage.OpenActionsPopup();
			inboxPage.MarkAllAsRead();

			inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			inboxPage.AssertAllConversationsAreRead();
		}

		#endregion
	}
}