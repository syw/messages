﻿using System.Linq;
using NUnit.Framework;
using SYW.App.Messages.SystemTests.Components;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Tests
{
	[Ignore]
	[TestFixture]
	public class NewMessageDialogTests : SeleniumTestBase
	{
		[Test]
		public void MessageSentAppearsInInbox()
		{
			var newConversationsPage = WebDriver.GoToPage<NewConversationDialog>(NewConversationDialog.Url);
			var messageContent = newConversationsPage.SendNewMessage();

			var inbox = WebDriver.GoToPage<InboxPage>(InboxPage.Url);

			Assert.That(inbox.HasMessageWith(userName: "nir", content: messageContent));
		}

		[Test(Description = "6187 - possible to add several friends")]
		public void AddRecipient_WhenTryingTooAddMultipleFriends_TwoFriendsAreAdded()
		{
			var newConversationsPage = WebDriver.GoToPage<NewConversationDialog>(NewConversationDialog.Url);
			newConversationsPage.AddRecipient("nir", 50);
			newConversationsPage.AddRecipient("doron", 55);

			var recipients = newConversationsPage.GetRecipients();
			Assert.That(recipients, Has.Count.EqualTo(2));
		}


		[Test(Description = "6193 - Can't send message with no friends")]
		public void CantSendMessageWithNoRecipients()
		{
			var newConversationsPage = WebDriver.GoToPage<NewConversationDialog>(NewConversationDialog.Url);
			newConversationsPage.EmulateClickOk();

			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			inboxPage.AssertInboxIsEmpty();
		}

		[Test(Description = "6194 - Can't send message with no content")]
		public void CantSendMessageWithNoContent()
		{
			var newConversationsPage = WebDriver.GoToPage<NewConversationDialog>(NewConversationDialog.Url);
			newConversationsPage.AddRecipient("nir", 50);

			newConversationsPage.EmulateClickOk();

			var inboxPage = WebDriver.GoToPage<InboxPage>(InboxPage.Url);
			inboxPage.AssertInboxIsEmpty();
		}

		[Test(Description = "cover that the manual autocomplete performs as expected")]
		[Ignore("autocomplete still doesnt work in CI :( ~Uri")]
		public void Autocomplete_WhenInsertingUserName_SuggestFriend()
		{
			var newConversationsPage = WebDriver.GoToPage<NewConversationDialog>(NewConversationDialog.Url);
			newConversationsPage.ManualAddRecipient("nir");

			var result = newConversationsPage.GetRecipients();
			Assert.That(result, Has.Count.EqualTo(1));
			Assert.That(result.First(), Is.EqualTo("nir"));
		}
	}
}