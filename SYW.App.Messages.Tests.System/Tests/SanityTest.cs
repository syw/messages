﻿using System.Configuration;
using NUnit.Framework;
using SYW.App.Messages.SystemTests.Components;
using SYW.App.Messages.SystemTests.Framework;
using SYW.App.Messages.SystemTests.Framework.Extensions;

namespace SYW.App.Messages.SystemTests.Tests
{
	[Ignore]
	[TestFixture]
	public class SanityTest : SeleniumTestBase
	{
		[Test]
		public void CheckConversationsPage()
		{
			WebDriver.Navigate().GoToUrl(ConfigurationManager.AppSettings["BaseWebsiteUrl"] + "/mailbox?token=123");
			WebDriver.As<InboxPage>();
		}
	}
}