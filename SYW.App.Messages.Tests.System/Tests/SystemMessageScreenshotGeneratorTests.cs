﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Feeds;
using UtilRunner.Utils;
using log4net;

namespace SYW.App.Messages.SystemTests.Tests
{
	[TestFixture]
	public class SystemMessageScreenshotGeneratorTests
	{
		private IMongoStorage<MessageDto,ObjectId> _messagesStorage;
		private IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private IBauScreenshotsFilesService _bauScreenshotsFilesService;
		private IBauEmailImageGeneratorSettings _bauEmailImageGeneratorSettings;
		private ILog _log;
		private ITumbnailCreatorService _tumbnailCreatorService;

		private ISystemMessageScreenshotGenerator _target;

		[SetUp]
		public void SetUp()
		{
			_messagesStorage = MockRepository.GenerateStub<IMongoStorage<MessageDto, ObjectId>>();
			_conversationStorage = MockRepository.GenerateStub<IMongoStorage<ConversationDto, ObjectId>>();
			_bauScreenshotsFilesService = MockRepository.GenerateStub<IBauScreenshotsFilesService>();
			_bauEmailImageGeneratorSettings = MockRepository.GenerateStub<IBauEmailImageGeneratorSettings>();
			_log = MockRepository.GenerateStub<ILog>();
			_tumbnailCreatorService = new TumbnailCreatorService();

			_target = new SystemMessageScreenshotGenerator(_messagesStorage, _bauScreenshotsFilesService, _conversationStorage, _log, _bauEmailImageGeneratorSettings, _tumbnailCreatorService);
		}

		[Test]
		public void Run_WhenThereAreNoConversations_ShouldExitWithoutExeutingLogic()
		{
			var conversations = SetConversations(0);
			SetConversationStorage(conversations);

			_target.Run(new string[1]);

			_conversationStorage.AssertWasCalled(x => x.SelectAs(Arg<FilterDefinition<ConversationDto>>.Is.Anything, Arg<ProjectionDefinition<ConversationDto>>.Is.Anything, Arg<SortDefinition<ConversationDto>>.Is.Anything));
			_messagesStorage.AssertWasNotCalled(x => x.GetList(Arg<FilterDefinition<MessageDto>>.Is.Anything));
		}

		[Test]
		public void Run_WhenThereAreSystemConversationsWithoutMessages_ShouldExitWithoutExeutingLogic()
		{
			var conversations = SetConversations(1);
			SetConversationStorage(conversations);

			SetMessageStorage(new Dictionary<ObjectId, int>());

			_target.Run(new string[1]);

			_conversationStorage.AssertWasCalled(x => x.SelectAs(Arg<FilterDefinition<ConversationDto>>.Is.Anything, Arg<ProjectionDefinition<ConversationDto>>.Is.Anything, Arg<SortDefinition<ConversationDto>>.Is.Anything));
			_messagesStorage.AssertWasCalled(x => x.GetList(Arg<FilterDefinition<MessageDto>>.Is.Anything));
			_bauScreenshotsFilesService.AssertWasNotCalled(x => x.MessageScreenShotExists(Arg<string>.Is.Anything));
		}

		[Test]
		public void Run_WhenThereAreSystemConversationsWithMessagesWhoDoesntExistInStaticContentPath_ShouldCreateImagesInStaticContentPath()
		{
			SetBauImageGeneratorSettings();

			var conversations = SetConversations(1);
			SetConversationStorage(conversations);

			var messages = SetMessageStorage(new Dictionary<ObjectId, int> { { conversations.ElementAt(0)._id, 1 } });
			SetBauScreenshotsFilesService();

			_target.Run(new string[1]);

			_conversationStorage.AssertWasCalled(x => x.SelectAs(Arg<FilterDefinition<ConversationDto>>.Is.Anything, Arg<ProjectionDefinition<ConversationDto>>.Is.Anything, Arg<SortDefinition<ConversationDto>>.Is.Anything));
			_messagesStorage.AssertWasCalled(x => x.GetList(Arg<FilterDefinition<MessageDto>>.Is.Anything));
			_bauScreenshotsFilesService.AssertWasCalled(x => x.MessageScreenShotExists(Arg<string>.Is.Equal(messages.ElementAt(0)._id.ToString())));

			Assert.That(CheckIfScreenShotExistInTargetFolderAndPhatomJsFolder(messages.ElementAt(0)._id.ToString()), Is.True);
			Assert.That(CheckIfScreenShotHasResized(messages.ElementAt(0)._id.ToString()), Is.True);
			DeleteCreatedTestImages(new[] { messages.ElementAt(0)._id });
		}

		[Test]
		public void Run_WhenThereAreSystemConversationsWithMessagesWhoExistInStaticContentPath_ShouldNotCreateImagesInStaticContentPath()
		{
			SetBauImageGeneratorSettings();

			var conversations = SetConversations(1);
			SetConversationStorage(conversations);

			var messages = SetMessageStorage(new Dictionary<ObjectId, int> { { conversations.ElementAt(0)._id, 1 } });
			SetBauScreenshotsFilesService();
			CreateImageInStaticContentPath(messages.ElementAt(0)._id);

			_target.Run(new string[1]);

			_conversationStorage.AssertWasCalled(x => x.SelectAs(Arg<FilterDefinition<ConversationDto>>.Is.Anything, Arg<ProjectionDefinition<ConversationDto>>.Is.Anything, Arg<SortDefinition<ConversationDto>>.Is.Anything));
			_messagesStorage.AssertWasCalled(x => x.GetList(Arg<FilterDefinition<MessageDto>>.Is.Anything));
			_bauScreenshotsFilesService.AssertWasCalled(x => x.MessageScreenShotExists(Arg<string>.Is.Equal(messages.ElementAt(0)._id.ToString())));

			Assert.That(CheckIfScreenShotExistInTargetFolderAndPhatomJsFolder(messages.ElementAt(0)._id.ToString()), Is.True);
			_bauEmailImageGeneratorSettings.AssertWasNotCalled(x => x.PhatomJsPath);

			DeleteCreatedTestImages(new[] { messages.ElementAt(0)._id });
		}

		private void DeleteCreatedTestImages(IEnumerable<ObjectId> bauEmailImageId)
		{
			foreach (var imageId in bauEmailImageId)
			{
				var imageToDeletePhantomJsFolder = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(imageId.ToString()));
				if (imageToDeletePhantomJsFolder.Exists)
					imageToDeletePhantomJsFolder.Delete();

				var imageToDeleteSourceFolder = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(imageId.ToString()));
				if (imageToDeleteSourceFolder.Exists)
					imageToDeleteSourceFolder.Delete();
			}
		}

		private bool CheckIfScreenShotExistInTargetFolderAndPhatomJsFolder(string screenShotId)
		{
			var resizedScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(screenShotId));
			var originalScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(screenShotId));
			return resizedScreenshot.Exists && originalScreenshot.Exists;
		}

		private void SetConversationStorage(IEnumerable<ConversationDto> conversations)
		{
			_conversationStorage.Stub(x => x.SelectAs(Arg<FilterDefinition<ConversationDto>>.Is.Anything, Arg<ProjectionDefinition<ConversationDto>>.Is.Anything, Arg<SortDefinition<ConversationDto>>.Is.Anything)).Return(conversations.Select(x => x.ToBsonDocument()).ToList());
		}

		private List<MessageDto> SetMessageStorage(IEnumerable<KeyValuePair<ObjectId, int>> messagesNumberPerConversation)
		{
			var messages = new List<MessageDto>();
			foreach (var item in messagesNumberPerConversation)
			{
				for (var i = 0; i < item.Value; i++)
					messages.Add(new MessageDto { _id = ObjectId.GenerateNewId(), ConversationId = item.Key });
			}

			_messagesStorage.Stub(x => x.GetList(Arg<FilterDefinition<MessageDto>>.Is.Anything)).Return(messages);

			return messages;
		}

		private void SetBauScreenshotsFilesService()
		{
			_bauScreenshotsFilesService.Stub(x => x.GetBauScreenshotPath(Arg<string>.Is.Anything))
				.Do((Func<string, string>)(id => Path.Combine(Path.Combine(Path.GetFullPath("../../"), "PhantomJsFolder"), id + ".jpg")));

			_bauScreenshotsFilesService.Stub(x => x.GetBauStaticContentScreenshotPath(Arg<string>.Is.Anything))
				.Do((Func<string, string>)(id => Path.Combine(_bauEmailImageGeneratorSettings.StaticContentPath, id + ".jpg")));

			_bauScreenshotsFilesService.Stub(x => x.BauScreenShotExists(Arg<string>.Is.Anything))
				.Do((Func<string, bool>)(id => new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(id)).Exists));
		}

		private void SetBauImageGeneratorSettings(bool isValidSettings = true)
		{
			_bauEmailImageGeneratorSettings.ThumbnailWidth = 208;
			_bauEmailImageGeneratorSettings.BauImageRenderingUrl = ConfigurationManager.AppSettings["BauEndpointUrl"] + "/bau/";
			_bauEmailImageGeneratorSettings.SystemMessageImageRenderingUrl = ConfigurationManager.AppSettings["BauEndpointUrl"] + "/bau/message/";

			_bauEmailImageGeneratorSettings.StaticContentPath = Path.Combine(Path.GetFullPath("../../"), "UtilResizedImages");

			_bauEmailImageGeneratorSettings.PhatomJsPath = (isValidSettings) ? Path.Combine(Path.GetFullPath("../../"), "PhantomJsFolder") : "Invalid Path";
		}

		private void CreateImageInStaticContentPath(ObjectId bauEmailImageId)
		{
			using (var createdImage = new Bitmap(1024, 768))
			{
				createdImage.Save(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(bauEmailImageId.ToString()));
			}
		}

		private List<ConversationDto> SetConversations(int conversationsNum, bool isSystemConversation = true)
		{
			var conversations = new List<ConversationDto>();
			for (var i = 0; i < conversationsNum; i++)
			{
				conversations.Add(new ConversationDto { _id = ObjectId.GenerateNewId(), IsSystemMessage = isSystemConversation });
			}
			return conversations;
		}

		private bool CheckIfScreenShotHasResized(string screenShotId)
		{
			var originalScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(screenShotId));
			var resizedScreenshot = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(screenShotId));
			var originalImage = Image.FromFile(originalScreenshot.FullName);
			var resizedImage = Image.FromFile(resizedScreenshot.FullName);
			var percentWidth = ((float)originalImage.Width / (float)_bauEmailImageGeneratorSettings.ThumbnailWidth);

			var isResizedCorrectly = (resizedImage.Width == _bauEmailImageGeneratorSettings.ThumbnailWidth && resizedImage.Height == (int)(originalImage.Height / percentWidth));
			originalImage.Dispose();
			resizedImage.Dispose();
			return isResizedCorrectly;
		}
	}
}