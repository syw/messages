﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messages.Web.Conversations;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messages.UnitTests.Controllers
{
	public class ConversationPageControllerTests
	{
		private ConversationsPageController _target;

		[SetUp]
		public void SetUp()
		{
			_target = new AutoStubber<ConversationsPageController>().Create();
			MockApplicationSettings();
		}

		[Test]
		public void ConversationPage_StandardConversation_UpdateConversationStatus()
		{
			var conversationId = ObjectId.GenerateNewId();
			var currentEntityId = ObjectId.GenerateNewId();
			MockGetConversation(conversationId, currentEntityId, new List<Message>());
			MockCurrentEntity(currentEntityId);

			_target.ConversationPage(conversationId.ToString());

			_target.Stubs().Get<IUpdateConversationStatusService>()
				.AssertWasCalled(x => x.UpdateStatus(Arg<ObjectId>.Is.Equal(conversationId),
				                                     Arg<ObjectId>.Is.Equal(currentEntityId),
				                                     Arg<ConversationStatus>.Is.Equal(ConversationStatus.Read)));
		}

		[Test]
		public void ConversationPage_MoreThanTenMessages_ReturnOnlyTen()
		{
			var conversationId = ObjectId.GenerateNewId();
			var currentEntityId = ObjectId.GenerateNewId();
			MockCurrentEntity(currentEntityId);
			MockGetConversation(conversationId, currentEntityId, new List<Message>
				                                                     {
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()},
					                                                     new Message {Id = ObjectId.GenerateNewId()}
				                                                     });

			var viewResult = _target.ConversationPage(conversationId.ToString());

			Assert.That(((ViewResult)viewResult).Model, Is.TypeOf<ConversationPageModel>()
				                                            .And.Matches<ConversationPageModel>(x => x.Conversation.Messages.Count == 10));
		}

		[Test]
		public void MoreMessages_ConversationWithManyMessages_GetTheRightOnesByDate()
		{
			var conversationId = ObjectId.GenerateNewId();
			var currentEntityId = ObjectId.GenerateNewId();
			MockCurrentEntity(currentEntityId);
			var lastLoadedMessageId = ObjectId.GenerateNewId();
			MockGetConversation(conversationId, currentEntityId, new List<Message>
				                                                     {
					                                                     new Message {Id = ObjectId.GenerateNewId(), Date = new DateTime(2012, 1, 20)},
					                                                     new Message {Id = lastLoadedMessageId, Date = new DateTime(2012, 1, 19)},
					                                                     new Message {Id = ObjectId.GenerateNewId(), Date = new DateTime(2012, 1, 18)},
					                                                     new Message {Id = ObjectId.GenerateNewId(), Date = new DateTime(2012, 1, 17)},
					                                                     new Message {Id = ObjectId.GenerateNewId(), Date = new DateTime(2012, 1, 16)},
				                                                     });

			var viewResult = _target.MoreMessages(lastLoadedMessageId.ToString(), conversationId.ToString());

			Assert.That(((PartialViewResult)viewResult).Model, Is.TypeOf<ConversationPageModel>()
				                                                   .And.Matches<ConversationPageModel>(x => x.Conversation.Messages.Count == 3));
			var conversationMessages = ((ConversationPageModel)((PartialViewResult)viewResult).Model).Conversation.Messages;
			Assert.That(conversationMessages.ToList(), Is.Ordered.Using(new MessageDateComparer()));
		}

		private void MockCurrentEntity(ObjectId currentEntityId)
		{
			_target.Stubs().Get<IEntityContextProvider>().Stub(x => x.CurrentEntity())
				.Return(new EntityContext
					        {
						        EntityType = EntityType.User,
						        Name = "gilly",
						        OriginalId = 1,
						        ObjectId = currentEntityId.ToString()
					        });
		}

		private void MockGetConversation(ObjectId conversationId, ObjectId userId, List<Message> conversationMessagesList)
		{
			_target.Stubs().Get<IConversationService>().Stub(x => x.Get(Arg<string>.Is.Equal(conversationId.ToString()), Arg<ObjectId>.Is.Equal(userId)))
				.Return(new Conversation
					        {
						        Id = conversationId,
						        Messages = conversationMessagesList,
						        Participants = new List<Entity>
							                       {
								                       new Entity {_id = userId, Name = "gilly", OriginalId = 1},
								                       new Entity {_id = ObjectId.GenerateNewId(), Name = "nir", OriginalId = 2}
							                       }
					        });
		}

		private void MockApplicationSettings()
		{
			_target.Stubs().Get<IApplicationSettings>().InvitationUrlFormat = "http://sometesturl.com/{0}/{1}";
		}
	}

	internal class MessageDateComparer : IComparer<Message>
	{
		public int Compare(Message x, Message y)
		{
			unchecked
			{
				return (int)(x.Date.Ticks - y.Date.Ticks);
			}
		}
	}
}