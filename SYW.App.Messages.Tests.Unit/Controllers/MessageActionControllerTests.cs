﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messages.Web.Controllers;
using SYW.App.Messages.Web.Models;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Exceptions;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.UnitTests.Controllers
{
	public class MessageActionControllerTests 
	{
		private MessageActionController _target;

		[SetUp]
		public void SetUp()
		{
			_target = new AutoStubber<MessageActionController>().Create();
		}

		[Test]
		public void GetMessageByIndex_ReturnMessageAndChangesThereStatus()
		{
			var conversationid = ObjectId.GenerateNewId();
			var userId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			MockGetConversation(conversationid, userId, new List<Message> {new Message {Id = messageId, Status = ConversationStatus.Unread}});
			MockCurrentEntity(userId);

			var modelJson = _target.GetMessageByIndex(conversationid.ToString(), 1);
			var model = (SystemMessageModel)modelJson.Data;

			Assert.That(model.MessageId, Is.EqualTo(messageId.ToString()));
			Assert.That(model.TotalUnreadCount, Is.EqualTo(0));
			_target.Stubs().Get<IMessageStatusService>()
					.AssertWasCalled(x => x.UpdateStatus(Arg<ObjectId>.Is.Equal(conversationid),
														Arg<ObjectId>.Is.Equal(messageId),
														Arg<ObjectId>.Is.Equal(userId),
														Arg<ConversationStatus>.Is.Equal(ConversationStatus.Read)));
		}

		[Test]
		public void GetMessageByIndex_ThrowsWhenConversationNotFound()
		{
			var userId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			MockGetConversation(ObjectId.GenerateNewId(), userId, new List<Message> {new Message {Id = messageId, Status = ConversationStatus.Unread}});
			MockCurrentEntity(userId);

			Assert.Throws(typeof (ConversationNotFoundException), () => _target.GetMessageByIndex(ObjectId.GenerateNewId().ToString(), 1));
		}

		[Test]
		public void GetMessageByIndex_ThrowsMessageIndexOutOfBounds()
		{
			var conversationid = ObjectId.GenerateNewId();
			var userId = ObjectId.GenerateNewId();
			MockGetConversation(conversationid, userId, new List<Message>());
			MockCurrentEntity(userId);

			Assert.Throws(typeof (ArgumentException), () => _target.GetMessageByIndex(conversationid.ToString(), 1));
		}

		[Test]
		public void GetMessageByIndex_FilterArchivedMessages()
		{
			var conversationid = ObjectId.GenerateNewId();
			var userId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			var archivedMessageId = ObjectId.GenerateNewId();
			var massages = new List<Message>
								{
									new Message {Id = messageId, Status = ConversationStatus.Unread},
									new Message {Id = archivedMessageId, Status = ConversationStatus.Archive}
								};
			MockGetConversation(conversationid, userId, massages);
			MockCurrentEntity(userId);

			var modelJson = _target.GetMessageByIndex(conversationid.ToString(), 1);
			var model = (SystemMessageModel)modelJson.Data;

			Assert.That(model.TotalMessagesCount, Is.EqualTo(massages.Count(x => x.Status != ConversationStatus.Archive)));
		}

		[Test]
		public void GetMessageByID_ReturnMessageAndChangesThereStatus()
		{
			var conversationid = ObjectId.GenerateNewId();
			var userId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			MockGetConversation(conversationid, userId, new List<Message> { new Message { Id = messageId, Status = ConversationStatus.Unread, Author = new Entity() } });
			MockCurrentEntity(userId);
			MockGetMessageContentById(messageId.ToString(), "Message Content");

			var modelJson = _target.GetMessageByID(conversationid.ToString(), messageId.ToString());
			var model = (SystemMessageModel)modelJson.Data;

			Assert.That(model.MessageId, Is.EqualTo(messageId.ToString()));
			Assert.That(model.TotalUnreadCount, Is.EqualTo(0));
			_target.Stubs().Get<IMessageStatusService>()
					.AssertWasCalled(x => x.UpdateStatus(Arg<ObjectId>.Is.Equal(conversationid),
														Arg<ObjectId>.Is.Equal(messageId),
														Arg<ObjectId>.Is.Equal(userId),
														Arg<ConversationStatus>.Is.Equal(ConversationStatus.Read)));
		}

		[Test]
		public void GetMessageByID_ThrowsWhenConversationNotFound()
		{
			var userId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			MockGetConversation(ObjectId.GenerateNewId(), userId, new List<Message> {new Message {Id = messageId, Status = ConversationStatus.Unread}});
			MockCurrentEntity(userId);

			Assert.Throws(typeof (ConversationNotFoundException), () => _target.GetMessageByID(ObjectId.GenerateNewId().ToString(), messageId.ToString()));
		}

		[Test]
		public void GetMessageByID_ThrowsMessageIndexOutOfBounds()
		{
			var conversationid = ObjectId.GenerateNewId();
			var userId = ObjectId.GenerateNewId();
			MockGetConversation(conversationid, userId, new List<Message>());
			MockCurrentEntity(userId);

			Assert.Throws(typeof (ArgumentException), () => _target.GetMessageByID(conversationid.ToString(), ObjectId.GenerateNewId().ToString()));
		}

		[Test]
		public void GetMessageByID_FilterArchivedMessages()
		{
			var conversationid = ObjectId.GenerateNewId();
			var userId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			var archivedMessageId = ObjectId.GenerateNewId();
			var massages = new List<Message>
								{
									new Message {Id = messageId, Status = ConversationStatus.Unread,Author = new Entity()},
									new Message {Id = archivedMessageId, Status = ConversationStatus.Archive,Author = new Entity()}
								};
			MockGetConversation(conversationid, userId, massages);
			MockCurrentEntity(userId);
			MockGetMessageContentById(messageId.ToString(), "Some Content");

			var modelJson = _target.GetMessageByID(conversationid.ToString(), messageId.ToString());
			var model = (SystemMessageModel)modelJson.Data;

			Assert.That(model.TotalMessagesCount, Is.EqualTo(massages.Count(x => x.Status != ConversationStatus.Archive)));
		}


		[Test]
		public void GetMoreMessages_ReturnTheRightAmountOfMessages()
		{
			var conversationId = ObjectId.GenerateNewId();
			var currentEntityId = ObjectId.GenerateNewId();
			MockCurrentEntity(currentEntityId);

			var messages = new List<Message>();
			for (int i = 0; i < 20; i++)
				messages.Add(new Message{Author = new Entity{Name = "User" + i}, ConversationId = conversationId});

			MockGetConversation(conversationId, currentEntityId, messages);

			var viewResult = (PartialViewResult)_target.GetMoreMessages(conversationId.ToString(), 1, 15);
			var model = (IEnumerable<MessagePreviewModel>)viewResult.Model;
			Assert.That(model.Count(), Is.EqualTo(15));
		}

		private void MockGetConversation(ObjectId conversationId, ObjectId userId, List<Message> conversationMessagesList)
		{
			_target.Stubs().Get<IConversationService>().Stub(x => x.Get(Arg<string>.Is.Equal(conversationId.ToString()), Arg<ObjectId>.Is.Equal(userId)))
					.Return(new Conversation
								{
									Id = conversationId,
									Messages = conversationMessagesList,
									Participants = new List<Entity>
														{
															new Entity {_id = userId, Name = "gilly", OriginalId = 1},
															new Entity {_id = ObjectId.GenerateNewId(), Name = "nir", OriginalId = 2}
														}
								});
		}

		private void MockCurrentEntity(ObjectId currentEntityId)
		{
			_target.Stubs().Get<IEntityContextProvider>().Stub(x => x.CurrentEntity())
					.Return(new EntityContext
								{
									EntityType = EntityType.User,
									Name = "gilly",
									OriginalId = 1,
									ObjectId = currentEntityId.ToString()
								});
		}
		private void MockGetMessageContentById(string messageId, string content)
		{
			_target.Stubs().Get<IMessageRepository>().Stub(x => x.GetMessageContentById(Arg<string>.Is.Equal(messageId))).Return(content);
		}
	}
}