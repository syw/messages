using System;
using System.Collections.Generic;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messages.Web.Controllers;
using SYW.App.Messages.Web.Conversations;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messages.UnitTests.Controllers
{
	public class SettingsControllerTests
	{
		private SettingsController _target;
		private const string EmailDomain = "@test.test";

		[SetUp]
		public void Setup()
		{
			_target = new AutoStubber<SettingsController>().Create();

			_target.Stubs().Get<IConversationPageSettings>().EmailDomain = EmailDomain;

//			ReturnEntityWithIdAndEmail(null);
//
//
//
//			ValidatorResultForEmail(GoodEmail, EmailValidationStatus.Ok);
//			_target.Stubs().Get<IEmailValidator>().Stub(x => x.ValidateEmail(EmailDomain)).Return(EmailValidationStatus.Bad);
//			_target.Stubs().Get<IEmailValidator>().Stub(x => x.ValidateEmail(ExistingEmail + EmailDomain)).Return(EmailValidationStatus.Exists);
		}

		private void ValidatorResultForEmail(string emailPrefix, EmailValidationStatus validationStatus)
		{
			_target.Stubs().Get<IEmailValidator>().Stub(x => x.ValidateEmail(emailPrefix + EmailDomain)).Return(validationStatus);
		}

		private void ReturnEntityWithIdAndEmail(string email)
		{
			var objectId = new ObjectId();

			_target.Stubs().Get<IEntityContextProvider>().Stub(x => x.CurrentEntity())
					.Return(new EntityContext
								{
									ObjectId = objectId.ToString()
								});

			_target.Stubs().Get<IEntitiesRepository>().Stub(x => x.GetEntitiesByIds(Arg.Is(new List<ObjectId> {objectId})))
					.Return(new List<Entity>
								{
									new Entity
										{
											_id = objectId,
											Email = email
										}
								});
		}

		[Test]
		public void CheckEmail_WhenEmailIsOk_ReturnOk()
		{
			ValidatorResultForEmail("good", EmailValidationStatus.Ok);
			var result = (CustomJsonResult)_target.CheckEmail("good");
			Assert.That(result.Data, Is.EqualTo(EmailValidationStatus.Ok));
		}

		[Test]
		public void CheckEmail_WhenEmailIsNull_ReturnBad()
		{
			ValidatorResultForEmail(null, EmailValidationStatus.Bad);
			var result = (CustomJsonResult)_target.CheckEmail(null);
			Assert.That(result.Data, Is.EqualTo(EmailValidationStatus.Bad));
		}


		[Test]
		public void UpdateEmail_WhenEmailExists_ReturnExists()
		{
			ReturnEntityWithIdAndEmail("exists");
			ValidatorResultForEmail("exists", EmailValidationStatus.Exists);

			var result = (CustomJsonResult)_target.UpdateSettings("exists");
			_target.Stubs().Get<IEntitiesRepository>().AssertWasNotCalled(x => x.Update(Arg<Entity>.Is.Anything));
			Assert.That(result.Data, Is.EqualTo(EmailValidationStatus.Exists));
		}

		[Test]
		public void UpdateEmail_WhenEmailAlreadyUpdated_ThrowExcetion()
		{
			ReturnEntityWithIdAndEmail("old");
			ValidatorResultForEmail("new", EmailValidationStatus.Ok);
			Assert.Throws<ForbiddenOperationException>(() => _target.UpdateSettings("new"));

			_target.Stubs().Get<IEntitiesRepository>().AssertWasNotCalled(x => x.Update(Arg<Entity>.Is.Anything));
		}

		[Test]
		public void UpdateEmail_WhenEmailOk_CallUpdateAndReturnOk()
		{
			ReturnEntityWithIdAndEmail(null);
			ValidatorResultForEmail("new", EmailValidationStatus.Ok);
			var result = (CustomJsonResult)_target.UpdateSettings("new");

			_target.Stubs().Get<IEntitiesRepository>().AssertWasCalled(x => x.Update(Arg<Entity>.Matches(a => a.Email.Equals("new" + EmailDomain)
																						)));
			Assert.That(result.Data, Is.EqualTo(EmailValidationStatus.Ok));
		}
	}
}