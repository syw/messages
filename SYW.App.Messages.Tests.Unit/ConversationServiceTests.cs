﻿using System;
using System.Collections.Generic;
using CommonGround.Utilities;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using System.Linq;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messages.UnitTests
{
	[TestFixture]
	public class ConversationServiceTests
	{
		private ConversationService _target;

		[SetUp]
		public void SetUp()
		{
			_target = new AutoStubber<ConversationService>().Create();
			_target.Stubs().Get<IConversationStatusSettings>().DaysToKeepConversationAsNew = 1;
		}

		[Test]
		public void GetAll_StandardMessageList_JoinLastMessageAndStatus()
		{
			var conversationId = ObjectId.GenerateNewId();
			var firstMessageId = ObjectId.GenerateNewId();
			var secondMessageId = ObjectId.GenerateNewId();
			var thirdMessageId = ObjectId.GenerateNewId();
			var GillyEntity = new Entity { Name = "gilly" };

			_target.Stubs().Get<IConversationRepository>().Stub(x => x.GetAll(Arg<ObjectId>.Is.Anything, Arg<DateTime>.Is.Anything))
					.Return(new List<ConversationDescriptor>
								{
									new ConversationDescriptor
										{
											Id = conversationId,
											Messages = new List<Message>
															{
																new Message {Id = firstMessageId, Content = "1", Date = new DateTime(2012, 1, 3), Author = GillyEntity},
																new Message {Id = secondMessageId, Content = "2", Date = new DateTime(2012, 1, 1), Author = GillyEntity},
																new Message {Id = thirdMessageId, Content = "3", Date = new DateTime(2012, 1, 2), Author = GillyEntity}
															},
											Participants = new List<Entity> {GillyEntity, GillyEntity}
										}
								});

			_target.Stubs().Get<IMessageRepository>().Stub(x => x.GetLatest(Arg<ObjectId[]>.Is.Anything))
					.Return(new Dictionary<ObjectId, Message>
								{
									{conversationId, new Message {Content = "1", Author = GillyEntity}}
								});

			_target.Stubs().Get<IUserConversationStatusRepository>().Stub(
				x => x.Get(Arg<ObjectId[]>.Is.Anything, Arg<ObjectId>.Is.Anything))
					.Return(new List<UserConversationStatus>()
								{
									new UserConversationStatus {ConversationId = conversationId, Status = (int)ConversationStatus.Read}
								});

			var actual = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 3);

			Assert.That(actual, Is.TypeOf<List<ConversationDescriptor>>()
								.And.Matches<List<ConversationDescriptor>>(x => x.Count == 1));

			Assert.That(actual[0].LastMessage.Content, Is.EqualTo("1"));
			Assert.That(actual[0].Status, Is.EqualTo(ConversationStatus.Read));
		}

		[Test]
		public void GetAll_WhenThereAreSystemMessageWitoutConversationStatus_ShouldReturnThemAsNew()
		{
			var conversationId = ObjectId.GenerateNewId();
			SetConversationRepository(new[] { new ConversationDescriptor { Id = conversationId, IsSystemMessage = true, Status = ConversationStatus.Read } });
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, new Message { Content = "Wooow", Author = new Entity { Name = "Nir" }, Date = SystemTime.Now() } } });
			SetConversationStatus(new List<UserConversationStatus>());

			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 1).FirstOrDefault();

			Assert.That(result.Status, Is.EqualTo(ConversationStatus.New));
		}

		[Test]
		public void GetAll_WhenThereAreSystemMessageWitoutConversationStatusThatAreOld_ShouldReturnThemAsUnread()
		{
			var conversationId = ObjectId.GenerateNewId();
			var conversations = new[] { new ConversationDescriptor { Id = conversationId, IsSystemMessage = true, Status = ConversationStatus.Read } };
			SetConversationRepository(conversations);
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, new Message { Content = "Wooow", Author = new Entity { Name = "Nir" }, Date = SystemTime.Now().AddDays(-2) } } });
			SetConversationStatus(new List<UserConversationStatus>());

			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 1).FirstOrDefault();

			Assert.That(result.Status, Is.EqualTo(ConversationStatus.Unread));
		}

		[Test]
		public void GetAll_WhenThereAreSystemMessageWitConversationStatus_ShouldReturnThemAccordingToStatus()
		{
			var conversationId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			SetConversationRepository(new[] { new ConversationDescriptor { Id = conversationId, IsSystemMessage = true, Status = ConversationStatus.Read } });
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, new Message { Id = messageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } } });
			SetConversationStatus(new List<UserConversationStatus>{ new UserConversationStatus { ConversationId = conversationId, Status = (int)ConversationStatus.Read, Date = SystemTime.Now() } });
			SetMessageStatus(new List<UserMessageStatus>{ new UserMessageStatus { ConversationId = conversationId, MessageId = messageId, Status = (int)ConversationStatus.Read } });

			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 1).FirstOrDefault();

			Assert.That(result.Status, Is.EqualTo(ConversationStatus.Read));
		}

		[Test]
		public void GetAll_WhenThereAreUnreadSystemMessages_ShouldReturnThemAsUnread()
		{
			var conversationId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			SetConversationRepository(new[] { new ConversationDescriptor { Id = conversationId, IsSystemMessage = true, Status = ConversationStatus.Unread } });
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, new Message { Id = messageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } } });
			SetConversationStatus(new List<UserConversationStatus> { new UserConversationStatus { ConversationId = conversationId, Status = (int)ConversationStatus.Unread, Date = SystemTime.Now() } });
			SetMessageStatus(new List<UserMessageStatus>{ new UserMessageStatus { ConversationId = conversationId, MessageId = messageId, Status = (int)ConversationStatus.Unread } });

			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 1).FirstOrDefault();

			Assert.That(result.Status, Is.EqualTo(ConversationStatus.Unread));
		}

		[Test]
		public void GetAll_WhenThereAreUnreadUserMessages_ShouldReturnThemAsUnread()
		{
			var conversationId = ObjectId.GenerateNewId();
			var messageId = ObjectId.GenerateNewId();
			SetConversationRepository(new[] { new ConversationDescriptor { Id = conversationId, IsSystemMessage = false, Status = ConversationStatus.Unread, Participants = new List<Entity> { new Entity { Name = "Shay" } } } });
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, new Message { Id = messageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } } });
			SetConversationStatus(new List<UserConversationStatus>{ new UserConversationStatus { ConversationId = conversationId, Status = (int)ConversationStatus.Unread, Date = SystemTime.Now() } });
			SetMessageStatus(new List<UserMessageStatus> { new UserMessageStatus { ConversationId = conversationId, MessageId = messageId, Status = (int)ConversationStatus.Unread } });
			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 1).FirstOrDefault();

			Assert.That(result.Status, Is.EqualTo(ConversationStatus.Unread));
		}

		[Test]
		public void GetAll_WhenThereAreUnreadUserAndSystemMessages_ShouldReturnThemAsUnread()
		{
			var systemConversationId = ObjectId.GenerateNewId();
			var userConversationId = ObjectId.GenerateNewId();
			var systemMessageId = ObjectId.GenerateNewId();
			var userMessageId = ObjectId.GenerateNewId();
			SetConversationRepository(new[] { new ConversationDescriptor { Id = userConversationId, IsSystemMessage = false, Status = ConversationStatus.Unread, Participants = new List<Entity> { new Entity { Name = "Shay" } },
																			Messages = new List<Message>{new Message { Id = userMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } }}},
												new ConversationDescriptor { Id = systemConversationId, IsSystemMessage = true, Status = ConversationStatus.Unread, Participants = new List<Entity> { new Entity { Name = "Shay" } },
																			Messages = new List<Message>{new Message { Id = systemMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } }}}
												});

			SetMessagesRepository(systemConversationId, new Dictionary<ObjectId, Message> { { systemConversationId, new Message { Id = systemMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } },
																							{ userMessageId, new Message { Id = userMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } }});

			SetConversationStatus(new List<UserConversationStatus>{ new UserConversationStatus { ConversationId = systemConversationId, Status = (int)ConversationStatus.Unread, Date = SystemTime.Now() },
									  	new UserConversationStatus { ConversationId = userConversationId, Status = (int)ConversationStatus.Unread, Date = SystemTime.Now() }});
			SetMessageStatus(new List<UserMessageStatus>{ new UserMessageStatus { ConversationId = systemConversationId, MessageId = systemMessageId, Status = (int)ConversationStatus.Unread },
										new UserMessageStatus { ConversationId = userConversationId, MessageId = userMessageId, Status = (int)ConversationStatus.Unread }});

			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 2).ToList();

			Assert.That(result, Has.Count.EqualTo(2));
			Assert.That(result.First(x => x.IsSystemMessage).Status, Is.EqualTo(ConversationStatus.Unread));
			Assert.That(result.First(x => !x.IsSystemMessage).Status, Is.EqualTo(ConversationStatus.Unread));
		}

		[Test]
		public void GetAll_WhenThereAreUnreadUserMessageAndReadSystemMessages_ShouldReturnThemAccordingToStatus()
		{
			var systemConversationId = ObjectId.GenerateNewId();
			var userConversationId = ObjectId.GenerateNewId();
			var systemMessageId = ObjectId.GenerateNewId();
			var userMessageId = ObjectId.GenerateNewId();
			SetConversationRepository(new[] { new ConversationDescriptor { Id = userConversationId, IsSystemMessage = false, Status = ConversationStatus.Unread, Participants = new List<Entity> { new Entity { Name = "Shay" } },
																			Messages = new List<Message>{new Message { Id = userMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } }}},
												new ConversationDescriptor { Id = systemConversationId, IsSystemMessage = true, Status = ConversationStatus.Read, Participants = new List<Entity> { new Entity { Name = "Shay" } },
																			Messages = new List<Message>{new Message { Id = systemMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } }}}
												});

			SetMessagesRepository(systemConversationId, new Dictionary<ObjectId, Message> { { systemConversationId, new Message { Id = systemMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } },
																							{ userMessageId, new Message { Id = userMessageId, Content = "Wooow", Author = new Entity { Name = "Nir" } } }});

			SetConversationStatus(new List<UserConversationStatus>{ new UserConversationStatus { ConversationId = systemConversationId, Status = (int)ConversationStatus.Read, Date = SystemTime.Now() },
										new UserConversationStatus { ConversationId = userConversationId, Status = (int)ConversationStatus.Unread, Date = SystemTime.Now() }});
			SetMessageStatus(new List<UserMessageStatus>{ new UserMessageStatus { ConversationId = systemConversationId, MessageId = systemMessageId, Status = (int)ConversationStatus.Read },
									 new UserMessageStatus { ConversationId = userConversationId, MessageId = userMessageId, Status = (int)ConversationStatus.Unread }});

			var result = _target.GetAll(ObjectId.GenerateNewId(), SystemTime.Now(), 1, 2).ToList();

			Assert.That(result, Has.Count.EqualTo(2));
			Assert.That(result.First(x => x.IsSystemMessage).Status, Is.EqualTo(ConversationStatus.Read));
			Assert.That(result.First(x => !x.IsSystemMessage).Status, Is.EqualTo(ConversationStatus.Unread));
		}

		[Test]
		public void Get_WhenThereAreMessagesWithoutConversationStatus_ReturnsThemAsNew()
		{
			var conversationId = ObjectId.GenerateNewId();
			var message = new Message { Content = "Wooow", Author = new Entity { Name = "Anton" }, Date = SystemTime.Now() };
			SetConversationRepository(new Conversation { Id = conversationId, IsSystemMessage = true, Messages = new[] { message } });
			SetCacheSystemMessageProvider(new Conversation { Id = conversationId, IsSystemMessage = true, Messages = new[] { message } });
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, message } });
			SetMessageStatus(new List<UserMessageStatus>());
			StubEntitiesResolver("Anton");

			var result = _target.Get(conversationId.ToString(), ObjectId.GenerateNewId());

			Assert.That(result.Messages.All(x => x.Status == ConversationStatus.New));
		}

		[Test]
		public void Get_WhenThereAreMessagesWithoutConversationStatusAndTheyAreOld_ReturnsThemAsUnread()
		{
			var conversationId = ObjectId.GenerateNewId();
			var message = new Message { Content = "Wooow", Author = new Entity { Name = "Nir" }, Date = SystemTime.Now().AddDays(-2) };
			SetConversationRepository(new Conversation { Id = conversationId, IsSystemMessage = true, Messages = new[] { message } });
			SetCacheSystemMessageProvider(new Conversation { Id = conversationId, IsSystemMessage = true, Messages = new[] { message } });
			SetMessagesRepository(conversationId, new Dictionary<ObjectId, Message> { { conversationId, message } });
			SetMessageStatus(new List<UserMessageStatus>());
			StubEntitiesResolver("Anton");

			var result = _target.Get(conversationId.ToString(), ObjectId.GenerateNewId());

			Assert.That(result.Messages.All(x => x.Status == ConversationStatus.Unread));
		}

		[Test]
		public void ReOrderParticipantsAccordingToSendingOrder_WhenConversationIsSystemMessage_ReturnsConversationUnchanged()
		{
			var conversation = InitConversation(null, 2, true);
			
			_target.ReOrderParticipantsAccordingToSendingOrder(conversation, ObjectId.GenerateNewId());
			
			_target.Stubs().Get<IMessageRepository>().AssertWasNotCalled(x=>x.GetAll(Arg<ObjectId>.Is.Anything));
		}

		[Test]
		public void ReOrderParticipantsAccordingToSendingOrder_WhenUserConversationWithNoParticipants_ReturnsConversationUnchanged()
		{
			var conversation = new Conversation
			{
				IsSystemMessage = false
			};

			_target.ReOrderParticipantsAccordingToSendingOrder(conversation, ObjectId.GenerateNewId());

			_target.Stubs().Get<IMessageRepository>().AssertWasNotCalled(x => x.GetOrderedConversationSenders(Arg<ObjectId>.Is.Anything));
		}

		[Test]
		public void ReOrderParticipantsAccordingToSendingOrder_WhenUserConversationWithMultipleParticipantsAndCurrentUserHasSentTheOnlyMessage_ReturnsConversationUnchanged()
		{
			var currentUserId = ObjectId.GenerateNewId();
			var converstationId = ObjectId.GenerateNewId();
			var conversation = InitConversation(converstationId, 2);
			var participantA = conversation.Participants.ElementAt(0)._id;
			var participantB = conversation.Participants.ElementAt(1)._id;

			var conversationSenders = new List<ObjectId>   
				                           {
											   currentUserId
				                           };

			SetMessagesRepositoryGetOrderedConversationSenders(conversationSenders);

			_target.ReOrderParticipantsAccordingToSendingOrder(conversation, currentUserId);

			Assert.That(conversation.Participants.ElementAt(0)._id, Is.EqualTo(participantA));
			Assert.That(conversation.Participants.ElementAt(1)._id, Is.EqualTo(participantB));
		}

		[Test]
		public void ReOrderParticipantsAccordingToSendingOrder_WhenUserConversationWithMultipleParticipantsAndOneParticipantHasSentAMessage_ReturnsConversationWithSenderParticipantAtTop()
		{
			var currentUserId = ObjectId.GenerateNewId();
			var converstationId = ObjectId.GenerateNewId();
			var conversation = InitConversation(converstationId, 2);
			var participantA = conversation.Participants.ElementAt(0)._id;
			var participantB = conversation.Participants.ElementAt(1)._id;

			var conversationSenders = new List<ObjectId>   
				                           {
											   currentUserId,
											   participantA
				                           };

			SetMessagesRepositoryGetOrderedConversationSenders(conversationSenders);

			_target.ReOrderParticipantsAccordingToSendingOrder(conversation, currentUserId);

			Assert.That(conversation.Participants.ElementAt(0)._id, Is.EqualTo(participantA));
			Assert.That(conversation.Participants.ElementAt(1)._id, Is.EqualTo(participantB));
		}

		[Test]
		public void ReOrderParticipantsAccordingToSendingOrder_WhenUserConversationWithMultipleParticipantsAndAllHasSentAMessage_ReturnsConversationWithOrderedParticipant()
		{
			var currentUserId = ObjectId.GenerateNewId();
			var converstationId = ObjectId.GenerateNewId();
			var conversation = InitConversation(converstationId, 3);
			var conversationSenders = new List<ObjectId>   
				                           {
											   currentUserId,
											   conversation.Participants.ElementAt(0)._id,
											   conversation.Participants.ElementAt(1)._id,
											   conversation.Participants.ElementAt(2)._id
				                           };

			SetMessagesRepositoryGetOrderedConversationSenders(conversationSenders);

			_target.ReOrderParticipantsAccordingToSendingOrder(conversation, currentUserId);

			Assert.That(conversation.Participants.ElementAt(0)._id, Is.EqualTo(conversationSenders.ElementAt(1)));
			Assert.That(conversation.Participants.ElementAt(1)._id, Is.EqualTo(conversationSenders.ElementAt(2)));
			Assert.That(conversation.Participants.ElementAt(2)._id, Is.EqualTo(conversationSenders.ElementAt(3)));
		}

		private void StubEntitiesResolver(string name)
		{
			_target.Stubs().Get<IEntitiesResolver>().Stub(x => x.Resolve(Arg<IList<Entity>>.Is.Anything)).Do(
				new Action<IList<Entity>>(x =>
				{
					foreach (var entity in x)
					{
						entity.Name = name;
					}
				}));
		}

		private void SetConversationStatus(List<UserConversationStatus> userConversationStatuses)
		{
			_target.Stubs().Get<IUserConversationStatusRepository>().Stub(
				x => x.Get(Arg<ObjectId[]>.Is.Anything, Arg<ObjectId>.Is.Anything))
					.Return(userConversationStatuses);
		}

		private void SetMessagesRepositoryGetOrderedConversationSenders(IList<ObjectId> conversationSenders)
		{
			_target.Stubs().Get<IMessageRepository>().Stub(x => x.GetOrderedConversationSenders(Arg<ObjectId>.Is.Anything))
					.Return(conversationSenders);
		}

		private void SetMessagesRepository(ObjectId conversationId, Dictionary<ObjectId, Message> objToReturn)
		{
			_target.Stubs().Get<IMessageRepository>().Stub(x => x.GetAllDescriptors(Arg<ObjectId[]>.Is.Anything))
					.Return(objToReturn.Values.Select(MapMessage).ToList());

			_target.Stubs().Get<IMessageRepository>().Stub(x => x.GetLatest(Arg<ObjectId[]>.Is.Anything))
					.Return(objToReturn);

			_target.Stubs().Get<IMessageRepository>().Stub(x => x.GetAll(Arg<ObjectId>.Is.Anything))
					.Return(new List<Message> { objToReturn[conversationId] });
		}

		private void SetConversationRepository(ConversationDescriptor[] objToReturn)
		{
			_target.Stubs().Get<IConversationRepository>().Stub(
				x => x.GetAll(Arg<ObjectId>.Is.Anything, Arg<DateTime>.Is.Anything))
					.Return(objToReturn);
		}

		private void SetConversationRepository(Conversation objToReturn)
		{
			_target.Stubs().Get<IConversationRepository>().Stub(
				x => x.Get(Arg<ObjectId>.Is.Anything))
					.Return(objToReturn);
		}

		private void SetCacheSystemMessageProvider(Conversation objToReturn)
		{
			var messagesDescriptor = objToReturn.Messages.Select(MapMessage).ToList();

			_target.Stubs().Get<ICacheSystemMessageProvider>().Stub(
				x => x.GetAll(Arg<ObjectId>.Is.Anything))
					.Return(messagesDescriptor);
		}

		private void SetMessageStatus(List<UserMessageStatus> userConversationStatuses)
		{
			_target.Stubs().Get<IUserMessageStatusRepository>().Stub(
				x => x.GetAll(Arg<ObjectId[]>.Is.Anything, Arg<ObjectId>.Is.Anything))
					.Return(userConversationStatuses);

			_target.Stubs().Get<IUserMessageStatusRepository>().Stub(
				x => x.GetAll(Arg<ObjectId>.Is.Anything, Arg<ObjectId>.Is.Anything))
					.Return(userConversationStatuses);
		}

		private MessageDescriptor MapMessage(Message message)
		{
			return new MessageDescriptor
				       {
							AuthorId = message.Author._id,
							ContentHeight = message.ContentHeight,
							ContentType = (int)message.ContentType,
							ContentWidth = message.ContentWidth,
							ConversationId = message.ConversationId,
							Date = message.Date,
							Id = message.Id
						};
		}

		private Conversation InitConversation(ObjectId? conversationId, int participantsNumber, bool isSystemMessage = false)
		{
			var converstion = new Conversation
			{
				IsSystemMessage = isSystemMessage,
				Id = (conversationId != null) ? conversationId.Value : ObjectId.GenerateNewId(),
				Participants = new List<Entity>()
			};

			for (var i = 0; i < participantsNumber; i++)
				converstion.Participants.Add(new Entity { _id = ObjectId.GenerateNewId() });

			return converstion;
		}
	}
}