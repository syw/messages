﻿using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messages.UnitTests.Domain.Conversations
{
	[TestFixture]
	public class ConversationStatusServiceTests
	{
		private ConversationStatusService _conversationStatusService;

		[SetUp]
		public void Setup()
		{
			_conversationStatusService = new AutoStubber<ConversationStatusService>().Create();
		}

		[Test]
		public void Archive_WhenConversationDoesnotExist_ShouldThrowConversationNotFoundException()
		{
			Assert.Throws<ConversationNotFoundException>(() => _conversationStatusService.UpdateStatus(ObjectId.GenerateNewId(), ObjectId.GenerateNewId(), ConversationStatus.Archive));
		}

		[Test]
		public void Archive_WhenNotPartOfTheConversationAndNotSystemMessage_ShouldThrowForbiddenOperationException()
		{
			StubConversationRepository(new Conversation {IsSystemMessage = false, Participants = new[] {new Entity {_id = ObjectId.GenerateNewId()}}});

			Assert.Throws<ForbiddenArchiveConversationException>(() => _conversationStatusService.UpdateStatus(ObjectId.GenerateNewId(), ObjectId.GenerateNewId(), ConversationStatus.Archive));
		}

		[Test]
		public void Archive_WhenSystemMessage_ShouldNotThrowForbiddenOperationException()
		{
			StubConversationRepository(new Conversation {IsSystemMessage = true});

			Assert.DoesNotThrow(() => _conversationStatusService.UpdateStatus(ObjectId.GenerateNewId(), ObjectId.GenerateNewId(), ConversationStatus.Archive));
		}

		[Test]
		public void Archive_WhenPartOfTheConversationAndNotSystemMessage_ShouldNotThrowForbiddenOperationException()
		{
			var currentEntityId = ObjectId.GenerateNewId();

			StubConversationRepository(new Conversation {IsSystemMessage = false, Participants = new[] {new Entity {_id = currentEntityId}}});

			Assert.DoesNotThrow(() => _conversationStatusService.UpdateStatus(ObjectId.GenerateNewId(), currentEntityId, ConversationStatus.Archive));
		}

		private void StubConversationRepository(Conversation objToReturn)
		{
			_conversationStatusService.Stubs().Get<IConversationRepository>().Stub(x => x.Get(Arg<ObjectId>.Is.Anything))
									.Return(objToReturn);
		}
	}
}