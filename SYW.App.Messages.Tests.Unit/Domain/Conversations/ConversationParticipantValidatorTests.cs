﻿using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.UnitTests.Domain.Conversations
{
	[TestFixture]
	public class ConversationParticipantValidatorTests
	{
		private ConversationParticipantValidator _validator;

		[SetUp]
		public void Setup()
		{
			_validator = new AutoStubber<ConversationParticipantValidator>().Create();
		}

		[Test]
		public void Validate_EntityIsPartOfTheConversation_ShouldNotThrowException()
		{
			var entityId = ObjectId.GenerateNewId();
			StubConversationParticipants(new Conversation {Participants = new[] {new Entity {_id = entityId}}});

			Assert.DoesNotThrow(() => _validator.Validate(entityId, ObjectId.GenerateNewId()));
		}

		[Test]
		public void Validate_EntityIsNotPartOfTheConversation_ShouldThrowException()
		{
			var entityId = ObjectId.GenerateNewId();
			StubConversationParticipants(new Conversation { Participants = new[] { new Entity { _id = ObjectId.GenerateNewId() } } });

			Assert.Throws<EntityIsNotPartOfConversationException>(() => _validator.Validate(entityId, ObjectId.GenerateNewId()));
		}

		[Test]
		public void Validate_ConversationIsSystemAndEntityIsNotPartOfTheConversation_ShouldNotThrowException()
		{
			var entityId = ObjectId.GenerateNewId();
			StubConversationParticipants(new Conversation { IsSystemMessage = true, Participants = new[] { new Entity { _id = ObjectId.GenerateNewId() } } });

			Assert.DoesNotThrow(() => _validator.Validate(entityId, ObjectId.GenerateNewId()));
		}

		private void StubConversationParticipants(Conversation objToReturn)
		{
			_validator.Stubs().Get<IConversationRepository>().Stub(x => x.Get(Arg<ObjectId>.Is.Anything))
					.Return(objToReturn);
		}
	}
}