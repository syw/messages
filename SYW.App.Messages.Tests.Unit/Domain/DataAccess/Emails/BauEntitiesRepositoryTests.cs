﻿using Mongo;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.UnitTests.Domain.DataAccess.Emails
{
	[TestFixture]
	class BauEntitiesRepositoryTests
	{
		private IMongoStorage<BauEntity,ObjectId> _storage;
		private IBauEntitiesRepository _target;

		[SetUp]
		public void SetUp()
		{
			_storage = MockRepository.GenerateStub<IMongoStorage<BauEntity, ObjectId>>();

			_target = new BauEntitiesRepository(_storage);
		}

		[Test]
		public void Add_WhenArgumentHasLeadingOrTrailingSpaces_ShouldInsertTrimmedValue()
		{
			var testEntity = new BauEntity { BauType = "   any-bau-type   ", EntityId = ObjectId.GenerateNewId() };

			_target.Add(testEntity.BauType, testEntity.EntityId);

			_storage.AssertWasCalled(x => x.Insert(Arg<BauEntity>.Matches(c => c.BauType == testEntity.BauType.Trim())));
		}
	}
}
