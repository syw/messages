﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Caching;
using CommonGround.Utilities;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Services;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messages.UnitTests.Domain.DataAccess.Messages
{
	[TestFixture]
	public class CacheSystemMessageProviderTests
	{
		private IHttpContextProvider _httpContextProvider;
		private IMessageRepository _messageRepository;
		private IConversationRepository _conversationRepository;
		private ISystemMessagesSettings _systemMessagesSettings;

		private ICacheSystemMessageProvider _target;

		[SetUp]
		public void SetUp()
		{
			_httpContextProvider = MockRepository.GenerateStub<IHttpContextProvider>();
			_messageRepository = MockRepository.GenerateStub<IMessageRepository>();
			_conversationRepository = MockRepository.GenerateStub<IConversationRepository>();
			_systemMessagesSettings = MockRepository.GenerateStub<ISystemMessagesSettings>();

			_target = new CacheSystemMessageProvider(
				_httpContextProvider,
				_messageRepository,
				_conversationRepository,
				_systemMessagesSettings
				);
		}

		[Test]
		public void GetAll_WhenCachedMessagesExist_ShouldReturnCachedMessages()
		{
			IList<ObjectId> conversationsIds;
			CacheExists(true, out conversationsIds);

			var result = _target.GetAll();

			Assert.That(result.Count == 10);
			Assert.That(result.Select(x => x.ConversationId).Distinct(), Is.EquivalentTo(conversationsIds));
			_messageRepository.AssertWasNotCalled(x => x.GetAllDescriptors(Arg<List<ObjectId>>.Is.Anything));
		}

		[Test]
		public void GetAll_WhenNoMessages_ShouldReturnEmptySequence()
		{
			NoMessages();

			var result = _target.GetAll();
			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetAll_WhenCacheDoesNotExist_ShouldGetMessagesFromDb()
		{
			IList<ObjectId> conversationsIds;
			CacheExists(false, out conversationsIds);

			var result = _target.GetAll();
			
			Assert.That(result.Count == 10);
			Assert.That(result.Select(x => x.ConversationId).Distinct(), Is.EquivalentTo(conversationsIds));
			_messageRepository.AssertWasCalled(x => x.GetAllDescriptors(Arg<List<ObjectId>>.Is.Anything));
		}
		
		[Test]
		public void GetAllWithParam_WhenNoMessages_ShouldReturnEmptySequence()
		{
			var conversationsId = ObjectId.GenerateNewId();
			NoMessages();

			var result = _target.GetAll(conversationsId);
			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetAllWithParam_WhenCalled_ShouldReturnMessagesFromSpecifiedConversation()
		{
			IList<ObjectId> conversationsIds;
			CacheExists(true, out conversationsIds);

			var result = _target.GetAll(conversationsIds[0]);
			Assert.That(result.Count == 5);
			Assert.That(result.Select(x => x.ConversationId).Distinct().SingleOrDefault(), Is.EqualTo(conversationsIds[0]));
		}

		[Test]
		public void GetLatest_WhenNoMessages_ShouldReturnEmptySequence()
		{
			var conversationsIds = new[] {ObjectId.GenerateNewId(), ObjectId.GenerateNewId()};
			NoMessages();

			var result = _target.GetLatest(conversationsIds);
			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetLatest_WhenCalled_ShouldReturnLastMessageFromEachConversation()
		{
			IList<ObjectId> conversationsIds;
			CacheExists(true, out conversationsIds);

			var result = _target.GetLatest(conversationsIds);
			Assert.That(result.Count == 2);
			Assert.That(result.Select(x => x.Value.ConversationId).Distinct(), Is.EquivalentTo(conversationsIds));
		}

		private void CacheExists(bool exists, out IList<ObjectId> conversationsIds )
		{
			conversationsIds = new[] {ObjectId.GenerateNewId(), ObjectId.GenerateNewId()};

			var messages = new List<MessageDescriptor>();
			for (var i = 0; i < 10; i++)
			{
				messages.Add(new MessageDescriptor
								{
									Id = ObjectId.GenerateNewId(),
									ConversationId = i%2 == 0 ? conversationsIds[0] : conversationsIds[1],
									Date = DateTime.UtcNow.AddHours(-i),
									PreviewText = i.ToString()
								});
			}

			var context = GetContext();

			if (exists)
			{
				context.Cache.Add("SystemMessagesDescriptorsCache", messages, null,
								SystemTime.Now().AddHours(4),
								Cache.NoSlidingExpiration,
								CacheItemPriority.High, null);
			}

			_conversationRepository.Stub(x => x.GetSystemConversationIds()).Return(new List<ObjectId> {ObjectId.GenerateNewId()});
			_messageRepository.Stub(x => x.GetAllDescriptors(Arg<IList<ObjectId>>.Is.Anything)).Return(messages);
		}

		private HttpContext GetContext()
		{
			var context = new HttpContext(new HttpRequest("", "http://www.google.com", ""), new HttpResponse(new StringWriter()));
			context.Cache.Remove("SystemMessagesDescriptorsCache");
			_httpContextProvider.Stub(x => x.GetContext()).Return(context);
			return context;
		}

		private void NoMessages()
		{
			GetContext();
			_conversationRepository.Stub(x => x.GetSystemConversationIds()).Return(new List<ObjectId>());
			_messageRepository.Stub(x => x.GetAllDescriptors(Arg<IList<ObjectId>>.Is.Anything)).Return(new List<MessageDescriptor>());
		}
	}
}