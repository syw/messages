﻿using System;
using System.Xml.Linq;
using NUnit.Framework;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.UnitTests.Domain.Feeds
{
	[TestFixture]
	public class BauEmailFeedParserTests
	{
		private IBauEmailFeedParser _target;

		[SetUp]
		public void SetUp()
		{
			_target = new BauEmailFeedParser();
		}

		[Test]
		public void Parse_RegularFeed_ReturnBauFeedObject()
		{
			var feed = XElement.Parse(GetMockedFeed("Lands\' End"));

			var bauFeedObject = _target.Parse(feed);

			//ID
			Assert.That(bauFeedObject.EmailId, Is.EqualTo("tag:sears.com,2013-01-15:8b7b29d8-02f7-449a-8b26-ff3e981c3a90"));
			//Logo
			Assert.That(bauFeedObject.Logo, Is.EqualTo("http%3A%2F%2Fs1.sywcdn.net%2FgetImage%3Furl%3D%252f%252fs3.sywcdn.net%252fuser%252f921c_513617.jpg%26t%3DBrand%26w%3D130%26h%3D130%26s%3Dc8feb111dab1f646103074f7b2bca0a3"));
			//Subject line:
			Assert.That(bauFeedObject.SubjectLine, Is.EqualTo("Brighten UP"));
			//Start date:
			Assert.That(bauFeedObject.Published.Date, Is.EqualTo(new DateTime(2013, 1, 14)));
			//End date:
			Assert.That(bauFeedObject.EndDate.Date, Is.EqualTo(new DateTime(2013, 1, 21)));
			//Content
			Assert.That(bauFeedObject.Content, Is.EqualTo("ODE HTML content"));
			//Brand Name
			Assert.That(bauFeedObject.BrandName, Is.EqualTo("Lands\' End"));
		}

		[Test]
		public void Parse_TrailingSpaceInBrandName_BrandNameIsTrimmed()
		{
			var feed = XElement.Parse(GetMockedFeed("Lands\' End "));

			var bauFeedObject = _target.Parse(feed);

			Assert.That(bauFeedObject.BrandName, Is.EqualTo("Lands\' End"));
		}

		[Test]
		public void Parse_InvalidFeed_ThrowException()
		{
			Assert.Throws<NullReferenceException>(() => _target.Parse(null));
		}

		[Test]
		public void Parse_NullFeed_ThrowException()
		{
			var badFeed = XElement.Parse(_mockedBadFeed);

			Assert.Throws<InvalidOperationException>(() => _target.Parse(badFeed));
		}

		private string GetMockedFeed(string brandName)
		{
			return string.Format(_mockedFeed, brandName);
		}

		private const string _mockedFeed =
			"<entry xmlns=\"http://www.w3.org/2005/Atom\" xmlns:ccs=\"http://ccsvip.prod.ch4.s.com/ccs\">" +
			"<id>tag:sears.com,2013-01-15:8b7b29d8-02f7-449a-8b26-ff3e981c3a90</id>" +
			"<title type=\"text\">Brighten UP</title>" +
			"<category term=\"{0}\" label=\"{0}\"></category>" +
			"<ccs:type>ODE</ccs:type>" +
			"<ccs:expires>2013-01-21T06:00:00.000Z</ccs:expires>" +
			"<source>" +
			"<id>Lands'End_BrightenUP_2013-Jan-14</id>" +
			"<author>" +
			"<name>Ondemand Email</name>" +
			"<uri></uri>" +
			"</author>" +
			"</source>" +
			"<summary type=\"text\">{0}</summary>" +
			"<updated>2013-01-15T06:15:15.080Z</updated>" +
			"<published>2013-01-14T06:00:00.000Z</published>" +
			"<link href=\"http%3A%2F%2Fs1.sywcdn.net%2FgetImage%3Furl%3D%252f%252fs3.sywcdn.net%252fuser%252f921c_513617.jpg%26t%3DBrand%26w%3D130%26h%3D130%26s%3Dc8feb111dab1f646103074f7b2bca0a3\" rel=\"self\" type=\"image/jpeg\" title=\"brandLogo\" length=\"0\"></link>" +
			"<content type=\"html\">ODE HTML content</content>" +
			"</entry>";

		private const string _mockedBadFeed = "<entry>This is just a bad feed entry...</entry>";
	}
}