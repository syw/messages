﻿using NUnit.Framework;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.UnitTests.Domain.Messages
{
	[TestFixture]
	public class AttachmentTests
	{
		[Test]
		public void IsValid_AttachmentHasOnlyName_ReturnsTrue()
		{
			var attachment = new Attachment("name", null, null);

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasOnlyLink_ReturnsTrue()
		{
			var attachment = new Attachment(null, "link", null);

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasOnlyImageUrl_ReturnsTrue()
		{
			var attachment = new Attachment(null, null, "url");

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasNameAndLink_ReturnsTrue()
		{
			var attachment = new Attachment("name", "link", null);

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasLinkAndImageUrl_ReturnsTrue()
		{
			var attachment = new Attachment(null, "link", "url");

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasNameAndImageUrl_ReturnsTrue()
		{
			var attachment = new Attachment("name", null, "url");

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasAllProps_ReturnsTrue()
		{
			var attachment = new Attachment("name", "link", "url");

			Assert.That(attachment.IsValid(), Is.True);
		}

		[Test]
		public void IsValid_AttachmentHasNoProps_ReturnsFalse()
		{
			var attachment = new Attachment(null, null, null);

			Assert.That(attachment.IsValid(), Is.False);
		}
	}
}
