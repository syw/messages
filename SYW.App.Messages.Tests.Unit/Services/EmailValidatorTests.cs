using System.Collections.Generic;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.UnitTests.Services
{
	public class EmailValidatorTests
	{
		private EmailValidator _target;
		private const string BlackListEmail = "blacklist@test.test";
		private const string ExistingEmail = "exists@test.test";
		private const string SelfEmail = "test@test.test";

		[SetUp]
		public void Setup()
		{
			_target = new AutoStubber<EmailValidator>().Create();

			var objectid = new ObjectId();

			_target.Stubs().Get<IEntityContextProvider>().Stub(x => x.CurrentEntity()).Return(new EntityContext {ObjectId = objectid.ToString()});
			_target.Stubs().Get<IEntitiesRepository>().Stub(x => x.GetEntitiesByIds(Arg.Is(new List<ObjectId>{objectid}))).Return(new List<Entity> {new Entity {_id = objectid, Email = SelfEmail}});
			_target.Stubs().Get<IEntitiesRepository>().Stub(x => x.GetEntitiesIdsByEmail(Arg.Is(new List<string> {ExistingEmail}))).Return(new List<ObjectId> {new ObjectId()});
			_target.Stubs().Get<IBlacklistEmailsRepository>().Stub(x => x.IsEmailInBlackList(Arg.Is(BlackListEmail))).Return(true);
		}

		[Test]
		public void ValidateEmail_WhenEmailIsNull_ReturnBad()
		{
			var result = _target.ValidateEmail(null);

			Assert.That(result, Is.EqualTo(EmailValidationStatus.Bad));
		}

		[Test]
		public void ValidateEmail_WhenEmailEqualtoCurrentUserEmail_ReturnOk()
		{
			var result = _target.ValidateEmail(SelfEmail);

			Assert.That(result, Is.EqualTo(EmailValidationStatus.Ok));
		}

		[Test]
		public void ValidateEmail_WhenEmailAlreadyExists_ReturnExists()
		{
			var result = _target.ValidateEmail(ExistingEmail);

			Assert.That(result, Is.EqualTo(EmailValidationStatus.Exists));
		}

		[Test]
		public void ValidateEmail_WhenEmailInBlackList_ReturnExists()
		{
			var result = _target.ValidateEmail(BlackListEmail);

			Assert.That(result, Is.EqualTo(EmailValidationStatus.Exists));
		}

		[Test]
		public void ValidateEmail_WhenEmailNotinAnyList_ReturnOk()
		{
			var result = _target.ValidateEmail("good@test.test");

			Assert.That(result, Is.EqualTo(EmailValidationStatus.Ok));
		}

		[Test]
		public void ValidateEmail_WhenEmailNotValidEmailFormat_ReturnBad()
		{
			var result = _target.ValidateEmail("bad@bad@test.test");

			Assert.That(result, Is.EqualTo(EmailValidationStatus.Bad));
		}
	}
}