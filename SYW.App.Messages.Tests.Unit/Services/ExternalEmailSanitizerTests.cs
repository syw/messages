﻿using NUnit.Framework;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messsages.Domain.Services.ExternalEmails;

namespace SYW.App.Messages.UnitTests.Services
{
	public class ExternalEmailSanitizerTests
	{
		private IExternalEmailSanitizer _target;

		[SetUp]
		public void SetUp()
		{
			_target = new AutoStubber<ExternalEmailSanitizer>().Create();
		}

		[Test]
		public void DecodeAndSanitize_WhenInputIsNull_ReturnsNull()
		{
			var result = _target.DecodeAndSanitize(null);

			Assert.That(result, Is.EqualTo(null));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputIsEmpty_ReturnsNull()
		{
			var result = _target.DecodeAndSanitize(string.Empty);

			Assert.That(result, Is.EqualTo(null));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputIsNormal_ItIsReturned()
		{
			const string content = "content";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>content</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasNormalHtmlTags_ItIsReturned()
		{
			//some text, <b>bold text</b>
			const string content = "some%20text%2C%20%3Cb%3Ebold%20text%3C%2Fb%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>some text, <b>bold text</b></div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasNormalHtmlTags_TheyAreReturned()
		{
			//some text, <font color="#ff0000">colored text</font>
			const string content = "some%20text%2C%20%3Cfont%20color%3D%22%23ff0000%22%3Ecolored%20text%3C%2Ffont%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>some text, <font color=\"#ff0000\">colored text</font></div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasMoreThanOneLine_ItIsConvertedToLineBreaks()
		{
			//some text,
			//<p>text in new line</p>
			const string content = "some%20text%2C%20%3Cp%3Etext%20in%20new%20line%3C%2Fp%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>some text,\r\n<p>text in new line</p>\r\n</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasDivs_TheyAreReturned()
		{
			//<div>separate title,</div>stand alone input
			const string content = "%3Cdiv%3Eseparate%20title%2C%3C%2Fdiv%3Estand%20alone%20input";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>\r\n<div>separate title,</div>\r\nstand alone input</div>\r\n"));
		}
		
		[Test]
		public void DecodeAndSanitize_WhenInputHasJavascriptAsImageSrc_ItIsStripped()
		{
			//normal text<IMG SRC="javascript:alert('XSS');">
			const string content = "normal%20text%3CIMG%20SRC%3D%22javascript%3Aalert(%27XSS%27)%3B%22%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>normal text<img src=\"\"></div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasOnclickJavascript_ItIsStripped()
		{
			//normal text<div onclick="alert('xxs')"></div>
			const string content = "normal%20text%3Cdiv%20onclick%3D%22alert(%27xxs%27)%22%3E%3C%2Fdiv%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>normal text\r\n<div></div>\r\n</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasOnmouseoverJavascript_ItIsStripped()
		{
			//normal text<IMG SRC= onmouseover="alert('xxs')">
			const string content = "normal%20text%3CIMG%20SRC%3D%20onmouseover%3D%22alert(%27xxs%27)%22%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>normal text<img src=\"onmouseover=&quot;alert('xxs')&quot;\"></div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasJavascriptUrlEncoded_ItIsStripped()
		{
			//normal text<IMG SRC=&#0000106&#0000097&#0000118&#0000097&#0000115&#0000099&#0000114&#0000105&#0000112&#0000116&#0000058&#0000097&#0000108&#0000101&#0000114&#0000116&#0000040&#0000039&#0000088&#0000083&#0000083&#0000039&#0000041>"
			const string content = "normal%20text%3CIMG%20SRC%3D%26%230000106%26%230000097%26%230000118%26%230000097%26%230000115%26%230000099%26%230000114%26%230000105%26%230000112%26%230000116%26%230000058%26%230000097%26%230000108%26%230000101%26%230000114%26%230000116%26%230000040%26%230000039%26%230000088%26%230000083%26%230000083%26%230000039%26%230000041%3E%0A";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>normal text<img src=\"\"> </div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasIframe_ItIsStripped()
		{
			//normal text<iframe src="evilsite.com">
			const string content = "normal%20text%3Ciframe%20src%3D%22evilsite.com%22%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>normal text</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasJavascriptTableBackground_ItIsStripped()
		{
			//normal text<TABLE BACKGROUND="javascript:alert('XSS')">
			const string content = "normal%20text%3CTABLE%20BACKGROUND%3D%22javascript%3Aalert(%27XSS%27)%22%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>normal text\r\n<table background=\"\">\r\n</table>\r\n</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputIsInFullHtmlStructure_OnlyTheLegalPartIsReturned()
		{
			//<html><head><style>body{background-color:blue}</style><script>alert('xss')</script></head><body>hey!</body></html>
			const string content = "%3Chtml%3E%3Chead%3E%3Cstyle%3Ebody%7Bbackground-color%3Ablue%7D%3C%2Fstyle%3E%3Cscript%3Ealert(%27xss%27)%3C%2Fscript%3E%3C%2Fhead%3E%3Cbody%3Ehey!%3C%2Fbody%3E%3C%2Fhtml%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>hey!</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasHeadAndBody_OnlyTheLegalBodyPartIsReturned()
		{
			//<head><style>body{background-color:blue}</style><script>alert('xss')</script></head><body>hey!</body>
			const string content = "%3Chead%3E%3Cstyle%3Ebody%7Bbackground-color%3Ablue%7D%3C%2Fstyle%3E%3Cscript%3Ealert(%27xss%27)%3C%2Fscript%3E%3C%2Fhead%3E%3Cbody%3Ehey!%3C%2Fbody%3E";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>hey!</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputIsInFullOnlyHeadSection_OnlyTheLegalPartIsReturned()
		{
			//<head><style>body{background-color:blue}</style><script>alert('xss')</script></head>hey!
			const string content = "%3Chead%3E%3Cstyle%3Ebody%7Bbackground-color%3Ablue%7D%3C%2Fstyle%3E%3Cscript%3Ealert(%27xss%27)%3C%2Fscript%3E%3C%2Fhead%3Ehey!";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>hey!</div>\r\n"));
		}

		[Test]
		public void DecodeAndSanitize_WhenInputHasLineBreaks_TheyAreKept()
		{
			//first line<br/>second line
			const string content = "first%20line%3Cbr%2F%3Esecond%20line";
			var result = _target.DecodeAndSanitize(content);

			Assert.That(result, Is.EqualTo("<div>first line<br>\r\nsecond line</div>\r\n"));
		}
	}
}
