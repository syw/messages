﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.UnitTests.Services
{
	public class MessagePreviewImageServiceTests
	{
		private IStaticContentSettings _staticContentSettings;
		private IMessageRepository _messageRepository;
		private const string SystemMessageImageUrlFormat = "/content/screenshots/{0}.jpg?404=/Content/sears-logo.png&width=270&v=1";
		
		private MessagePreviewImageService _target;

		[SetUp]
		public void Setup()
		{
			_messageRepository = MockRepository.GenerateStub<IMessageRepository>();
			_staticContentSettings = MockRepository.GenerateStub<IStaticContentSettings>();

			_staticContentSettings.MissingImageUrl = "http://images.shopyourway.com/static/img/fusion/missing-images/user.png";
			_staticContentSettings.StaticContent = "";
			_staticContentSettings.Version = "1";

			_target = new MessagePreviewImageService(_staticContentSettings, _messageRepository);
		}

		[Test]
		public void InitMessagePreviewImage_WhenConversationIsNull_ShouldReturnStringEmpty()
		{
			var result = _target.InitMessagePreviewImage(new Message(), null, new ObjectId());

			Assert.That(result, Is.EqualTo(string.Empty));
		}

		[Test]
		public void InitMessagePreviewImage_WhenConversationIsSystemConversation_ShouldReturnPreviewImageUrl()
		{
			var conversation = new Conversation { IsSystemMessage = true };
			var messageId = ObjectId.GenerateNewId();
			var message = new Message { Id = messageId };

			var result = _target.InitMessagePreviewImage(message, conversation, new ObjectId());

			Assert.That(result, Is.EqualTo(string.Format(SystemMessageImageUrlFormat,messageId)));
		}

		[Test]
		public void InitMessagePreviewImage_WhenUserConversationAndUserIsTheOnlyParticipantWithNoImage_ShouldReturnMissingImageUrl()
		{
			var conversation = InitConversation(null, 1);

			var result = _target.InitMessagePreviewImage(new Message { Author = new Entity { _id = conversation.Participants.First()._id } }, 
														conversation, conversation.Participants.First()._id);

			Assert.That(result, Is.EqualTo(_staticContentSettings.MissingImageUrl));
		}

		[Test]
		public void InitMessagePreviewImage_WhenUserConversationAndUserIsTheOnlyParticipantWithImage_ShouldReturnImageUrl()
		{
			var conversation = InitConversation(null, 1);

			var result = _target.InitMessagePreviewImage(new Message { Author = new Entity
				                                                                    {
																						_id = conversation.Participants.First()._id,
																						ImageUrl = "Some Url"
																					}
			}, conversation, conversation.Participants.First()._id);

			Assert.That(result, Is.EqualTo("Some Url"));
		}

		[Test]
		public void InitMessagePreviewImage_WhenUserConversationWithTwoParticipantsAndLastMessageSenyByOtherParticipantWithNoImage_ShouldReturnMissingImageUrl()
		{
			var conversation = InitConversation(null, 2);
			
			var result = _target.InitMessagePreviewImage(new Message
				                                             {
																 Author = new Entity { _id = conversation.Participants.ElementAt(1)._id }
															 }, conversation, conversation.Participants.ElementAt(0)._id);

			Assert.That(result, Is.EqualTo(_staticContentSettings.MissingImageUrl));
		}

		[Test]
		public void InitMessagePreviewImage_WhenUserConversationWithTwoParticipantsAndLastMessageSenyByOtherParticipantHasImage_ShouldReturnParticipantImageUrl()
		{
			var conversation = InitConversation(null, 2);

			var result = _target.InitMessagePreviewImage(new Message
				                                             {
																 Author = new Entity { _id = conversation.Participants.ElementAt(1)._id, ImageUrl = "SomeUrl" }
															 }, conversation, conversation.Participants.ElementAt(0)._id);

			Assert.That(result, Is.EqualTo("SomeUrl"));
		}

		[Test]
		public void InitMessagePreviewImage_WhenUserConversationWithMultipleParticipantsAndLastMessageSentByUser_ShouldReturnOtherParticipantWhoSentTheLastMessageImageUrl()
		{
			const string userImageUrl = "User Url";
			const string participantA = "Last Sender Image Url";
			const string participantB = "Other Participant Image Url";

			var conversation = InitConversation(null, 3);

			conversation.Participants.ElementAt(0).ImageUrl = userImageUrl;
			conversation.Participants.ElementAt(1).ImageUrl = participantA;
			conversation.Participants.ElementAt(2).ImageUrl = participantB;

			SetMessagesRepositoryGetOrderedConversatiomSenders(new List<ObjectId>
				                                                   {
																	   conversation.Participants.ElementAt(0)._id,
																	   conversation.Participants.ElementAt(1)._id, 
																	   conversation.Participants.ElementAt(2)._id
				                                                   });

			var result = _target.InitMessagePreviewImage(new Message
				                                             {
					                                             Author = new Entity
						                                                      {
																				  _id = conversation.Participants.ElementAt(0)._id,
																				  ImageUrl = userImageUrl
						                                                      }
															 }, conversation, conversation.Participants.ElementAt(0)._id);

			Assert.That(result, Is.EqualTo(participantA));
		}

		private void SetMessagesRepositoryGetOrderedConversatiomSenders(IList<ObjectId> conversationSenders)
		{
			_messageRepository.Stub(x => x.GetOrderedConversationSenders(Arg<ObjectId>.Is.Anything)).Return(conversationSenders);
		}

		private Conversation InitConversation(ObjectId? conversationId, int participantsNum, bool isSystemMessage = false)
		{
			var converstion = new Conversation
			{
				IsSystemMessage = isSystemMessage,
				Id = (conversationId != null) ? conversationId.Value : ObjectId.GenerateNewId(),
				Participants = new List<Entity>()
			};

			for (var i = 0; i < participantsNum; i++)
				converstion.Participants.Add(new Entity { _id = ObjectId.GenerateNewId() });
			
			return converstion;
		}
	}
}
