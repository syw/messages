﻿using System;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.UnitTests.Services
{
	[TestFixture]
	public class SearchServiceTests
	{
		private ICachedUserFriendsProvider _cachedUserFriendsProvider;
		private IEntityContextProvider _entityContextProvider;
		private ISearchServiceSettings _searchServiceSettings;

		private ISearchFriendsService _target;

		[SetUp]
		public void SetUp()
		{
			_cachedUserFriendsProvider = MockRepository.GenerateStub<ICachedUserFriendsProvider>();
			_entityContextProvider = MockRepository.GenerateStub<IEntityContextProvider>();
			_searchServiceSettings = MockRepository.GenerateStub<ISearchServiceSettings>();

			_searchServiceSettings.MaxFriendsSearchResults = 5;

			_target = new SearchFriendsService(_cachedUserFriendsProvider, _entityContextProvider, _searchServiceSettings);
		}

		[Test]
		public void Search_WhenCachedUserFriendsProviderThrowsExcrption_ShouldReturnEmptyCollection()
		{
			_cachedUserFriendsProvider.Stub(x => x.GetFriends()).Throw(new ArgumentException());

			var result = _target.Search("");

			Assert.That(result, Is.Empty);
		}

		[Test]
		public void Search_WhenFilterParticipantsIdsIsNullOrMissing_ShouldReturnAllFriendsRespondingToQuery()
		{
			SetFriends();
			SetCurrentEntity();

			var result = _target.Search("S");

			Assert.That(result.Count == 2);
		}

		[Test]
		public void Search_WhenFilterParticipantsIdsIsNotNullOrMissing_ShouldReturnAllFriendsRespondingToQueryExceptThouseInFilterArray()
		{
			SetFriends();
			SetCurrentEntity();

			var result = _target.Search("S", new long[] {5});

			Assert.That(result.Count == 1);
		}

		private void SetFriends()
		{
			var friends = new[]
							{
								new Entity {Name = "Yossi", OriginalId = 1},
								new Entity {Name = "Nir", OriginalId = 2},
								new Entity {Name = "Saar", OriginalId = 3},
								new Entity {Name = "Anton", OriginalId = 4}
							};
			_cachedUserFriendsProvider.Stub(x => x.GetFriends()).Return(friends);
		}

		private void SetCurrentEntity()
		{
			_entityContextProvider.Stub(x => x.CurrentEntity()).Return(new EntityContext()
																			{
																				Name = "Shai",
																				ObjectId = "4f8d8c66e5a4e45396000009",
																				OriginalId = 5,
																				ImageUrl = ""
																			});
		}
	}
}