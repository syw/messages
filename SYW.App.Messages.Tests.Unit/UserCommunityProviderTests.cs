﻿using System.Collections.Generic;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Tests.Framework;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Services.Platform;

namespace SYW.App.Messages.UnitTests
{
	[TestFixture]
	public class UserCommunityProviderTests
	{
		private UserCommunityProvider _userCommunityProvider;

		[SetUp]
		public void Setup()
		{
			_userCommunityProvider = new AutoStubber<UserCommunityProvider>().Create();
		}

		[Test]
		public void GetFriends_UserIsNotLoggedIn_NoFriends()
		{
			var result = _userCommunityProvider.GetFriends();

			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetFriends_SameUserAppearInFollowersAndFollowedBy_ReturnFriends()
		{
			_userCommunityProvider.ImpersonateUser(1);
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetFollowers(Arg<long>.Is.Anything)).Return(new long[] {5, 6});
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetFollowedBy(Arg<long>.Is.Anything)).Return(new long[] {4, 5});
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetUsers(Arg<IList<long>>.List.ContainsAll(new long[] {5})))
								.Return(new List<SocialUserInfo> {new SocialUserInfo {id = 5}});

			var result = _userCommunityProvider.GetFriends();

			Assert.That(result, Has.Count.EqualTo(1));
			Assert.That(result[0].OriginalId, Is.EqualTo(5));
		}

		[Test]
		public void GetFriends_NoFollowedByForUser_ReturnEmptyList()
		{
			_userCommunityProvider.ImpersonateUser(1);
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetFollowers(Arg<long>.Is.Anything)).Return(new long[] {5, 6});
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetFollowedBy(Arg<long>.Is.Anything)).Return(new long[] {});

			var result = _userCommunityProvider.GetFriends();

			Assert.That(result, Is.Empty);
		}

		[Test]
		public void GetFriends_NoFollowersForUser_ReturnEmptyList()
		{
			_userCommunityProvider.ImpersonateUser(1);
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetFollowers(Arg<long>.Is.Anything)).Return(new long[] {});
			_userCommunityProvider.Stubs().Get<IUserService>().Stub(x => x.GetFollowedBy(Arg<long>.Is.Anything)).Return(new long[] {1, 2, 3});

			var result = _userCommunityProvider.GetFriends();

			Assert.That(result, Is.Empty);
		}
	}
}