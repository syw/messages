﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.UnitTests.Web.Filters
{
	[TestFixture]
	public class CmsFilterTests
	{
		private ICmsFilter _target;
		private ICmsUserSettings _cmsUserSettings;
		private IEntityContextProvider _entityContextProvider;

		[SetUp]
		public void SetUp()
		{
			_cmsUserSettings = MockRepository.GenerateStub<ICmsUserSettings>();
			_entityContextProvider = MockRepository.GenerateStub<IEntityContextProvider>();
			_target = new CmsFilter(_entityContextProvider, _cmsUserSettings);
		}

		[Test]
		public void OnAuthorization_WhenQueryParameterDoesNotHaveCmsAsValue_ShouldNotRedirectToCms()
		{
			var filterContext = SetupMockForHttpContext(string.Empty);

			_target.OnAuthorization(filterContext);

			var redirectResult = (RedirectResult)filterContext.Result;
			_entityContextProvider.AssertWasNotCalled(x => x.CurrentEntity());

			Assert.AreEqual("test", redirectResult.Url);
		}

		[Test]
		public void OnAuthorization_WhenCurrentUserIdIsNull_ShouldNotRedirectToCms()
		{
			var filterContext = SetupMockForHttpContext("cms");

			_target.OnAuthorization(filterContext);

			var redirectResult = (RedirectResult)filterContext.Result;
			Assert.That(redirectResult.Url, Is.EqualTo("test"));
		}

		[Test]
		public void OnAuthorization_WhenCurrentUserIsNotInTheAllowedUsersArraySettings_ShouldNotRedirectToCms()
		{
			_cmsUserSettings.AllowedUsersArray = "3";
			SetStubForCurrentEntity();

			var filterContext = SetupMockForHttpContext("cms");

			_target.OnAuthorization(filterContext);

			var redirectResult = (RedirectResult)filterContext.Result;
			Assert.That(redirectResult.Url, Is.EqualTo("test"));
		}

		[Test]
		public void OnAuthorization_WhenCmsIsInQueryAndUserIsValidAndAllowed_ShouldRedirectToCms()
		{
			var filterContext = SetupMockForHttpContext("cms");
			_cmsUserSettings.AllowedUsersArray = "5";
			SetStubForCurrentEntity();

			_target.OnAuthorization(filterContext);

			var redirectResult = (RedirectResult)filterContext.Result;
			Assert.That(redirectResult.Url, Is.EqualTo("/__/cms/system-message-view/"));
		}

		private void SetStubForCurrentEntity()
		{
			_entityContextProvider.Stub(x => x.CurrentEntity()).Return(new EntityContext { OriginalId = 5 });
		}

		private AuthorizationContext SetupMockForHttpContext(string queryValue)
		{
			var context = MockRepository.GenerateMock<HttpContextBase>();
			var request = MockRepository.GenerateMock<HttpRequestBase>();

			var query = new NameValueCollection { { "p", queryValue } };
			context.Stub(x => x.Request).Return(request);
			context.Request.Stub(x => x.QueryString).Return(query);
			var controller = MockRepository.GenerateMock<ControllerBase>();

			var actionDescriptor = MockRepository.GenerateMock<ActionDescriptor>();
			var controllerContext = new ControllerContext(context, new RouteData(), controller);

			var filterContext = new AuthorizationContext(controllerContext, actionDescriptor)
			{
				Result = new RedirectResult("test")
			};
			return filterContext;
		}
	}
}
