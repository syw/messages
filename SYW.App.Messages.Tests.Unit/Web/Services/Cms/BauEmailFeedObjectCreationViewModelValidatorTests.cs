﻿using System;
using MongoDB.Bson;
using NUnit.Framework;
using SYW.App.Messages.Web.Models.Cms;
using SYW.App.Messages.Web.Services.Cms;

namespace SYW.App.Messages.UnitTests.Web.Services.Cms
{
	[TestFixture]
	public class BauEmailFeedObjectCreationViewModelValidatorTests
	{
		private IBauEmailFeedObjectCreationViewModelValidator _target;

		[SetUp]
		public void SetUp()
		{
			_target = new BauEmailFeedObjectCreationViewModelValidator();
		}

		[Test]
		public void Validate_WhenTitleIsEmpty_ShouldThrowException()
		{
			var bauEmailFeedObjectViewModel = GetBauEmailFeedObjectViewModel();
			bauEmailFeedObjectViewModel.Title = string.Empty;

			var ex = Assert.Throws<Exception>(() => _target.Validate(bauEmailFeedObjectViewModel));
			Assert.That(ex.Message, Is.EqualTo("Title cannot be null or empty!"));
		}

		[Test]
		public void Validate_WhenBodyIsEmpty_ShouldThrowException()
		{
			var bauEmailFeedObjectViewModel = GetBauEmailFeedObjectViewModel();
			bauEmailFeedObjectViewModel.Body = string.Empty;

			var ex = Assert.Throws<Exception>(() => _target.Validate(bauEmailFeedObjectViewModel));

			Assert.That(ex.Message, Is.EqualTo("Body cannot be null or empty!"));
		}

		[Test]
		public void Validate_WhenPassingAllParametersProperly_ShouldNotThrowAnyError()
		{
			var bauEmailFeedObjectViewModel = GetBauEmailFeedObjectViewModel();

			Assert.DoesNotThrow(() => _target.Validate(bauEmailFeedObjectViewModel));
		}

		public BauEmailFeedObjectViewModel GetBauEmailFeedObjectViewModel()
		{
			return new BauEmailFeedObjectViewModel
			{
				Title = "Test",
				Body = "Test",
				PublisherSelectedId = ObjectId.GenerateNewId().ToString()
			};
		}
	}
}