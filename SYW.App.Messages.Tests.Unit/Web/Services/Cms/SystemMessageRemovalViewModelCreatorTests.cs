﻿using System.Collections.Generic;
using MongoDB.Bson;
using NUnit.Framework;
using Rhino.Mocks;
using SYW.App.Messages.Web.Services.Cms;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Feeds;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.UnitTests.Web.Services.Cms
{
	[TestFixture]
	public class SystemMessageRemovalViewModelCreatorTests
	{
		private SystemMessageRemovalViewModelCreator _target;
		private IBauEntityProvider _bauEntityProvider;
		private IMessagesService _messagesService;
		const string MessageId = "5500001cd1e8a52b04f5c35d";

		[SetUp]
		public void SetUp()
		{

			_bauEntityProvider = MockRepository.GenerateStub<IBauEntityProvider>();
			_messagesService = MockRepository.GenerateStub<IMessagesService>();
			_target = new SystemMessageRemovalViewModelCreator(_bauEntityProvider, _messagesService);
		}

		[Test]
		public void CreateSystemMessageView_WhenMessageIdIsNotEmptyAndCannotBeParsed_ShouldAssignTheExceptionToPostResult()
		{
			SetEntityProviderGetAll();
			const string invalidMessageId = "Test";

			var result = _target.Create(invalidMessageId);

			Assert.That(result.PostResult, Is.StringContaining("cannot be parsed as valid 24 digit hex string"));
		}

		[Test]
		public void CreateSystemMessageView_WhenMessageIdDoesNotExists_ShouldAssignTheExceptionToPostResult()
		{
			SetEntityProviderGetAll();

			var result = _target.Create(MessageId);

			Assert.That(result.PostResult, Is.EqualTo("Message id does not exists"));
		}

		[Test]
		public void CreateSystemMessageView_WhenMessageExists_ShouldReturnViewModelWithMessage()
		{
			var message = GetMessage();
			SetEntityProviderGetAll();
			SetEntityProviderGetByEntityId();
			SetMessageServiceGetMessageById();

			var result = _target.Create(message.Id.ToString());

			Assert.That(result.Body, Is.EqualTo("Test"));

		}
		private void SetMessageServiceGetMessageById()
		{
			_messagesService.Stub(x => x.GetMessageById(MessageId)).Return(GetMessage());
		}

		private Message GetMessage()
		{
			return new Message
			{
				Id = GetMessageObjectIdFromString(MessageId),
				Author = GetEntity(),
				Content = "Test"
			};
		}

		private Entity GetEntity()
		{
			return new Entity
			{
				_id = GetMessageObjectIdFromString(MessageId)
			};
		}

		private ObjectId GetMessageObjectIdFromString(string messageId)
		{
			return ObjectId.Parse(messageId);
		}

		private void SetEntityProviderGetAll()
		{
			_bauEntityProvider.Stub(x => x.GetAll()).Return(new List<BauEntity>());
		}

		private void SetEntityProviderGetByEntityId()
		{
			_bauEntityProvider.Stub(x => x.GetByEntityId(Arg<ObjectId>.Is.Anything)).Return(new BauEntity { EntityId = ObjectId.Parse("5500001cd1e8a52b04f5c35d") });
		}
	}
}