﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;
using CommonGround;
using CommonGround.MvcInvocation;
using CommonGround.Settings;
using Mongo;
using MongoDB.Bson;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Services.Platform;
using SYW.App.Messsages.Domain.Settings;
using log4net;
using log4net.Config;
using log4net.Core;

namespace SYW.App.Messages.Web.Config
{
	public class Bootstrapper
	{
		private readonly Assembly _currentAssembly;
		private Assembly[] _assemblies;
		private static SettingsManager _settingsManager;

		private readonly ICommonContainer _container;

		public Bootstrapper(ICommonContainer container)
		{
			_container = container;

			_currentAssembly = Assembly.GetExecutingAssembly();
			_assemblies = new[]
				              {
					              _currentAssembly,
					              typeof (ILogger).Assembly,
					              typeof (Conversation).Assembly
				              };
		}

		public void AddAssemblies(Assembly newAssembly)
		{
			_assemblies = _assemblies.Union(new[] {newAssembly}).ToArray();
		}

		public void RegisterEverything()
		{
			RegisterMongoDb();
			InitializeLogger();
			RegisterDefaultInterfaces();
			RegisterSettingsManager();
			RegisterEvents();
			RegisterMvcApp();
			RegisterGlobalFilters();
			InitializeMappers();
		}

		public void RegisterDefaultInterfaces()
		{
			_container.AutoWire(_assemblies);
		}

		public void RegisterEvents()
		{
			_container.AutoWireEvents(_assemblies);
		}

		public void RegisterMvcApp()
		{
			_container.RegisterTypes(new Dictionary<Type, Type> {{typeof (IActionInvoker), typeof (MessagesActionInvoker)}});

			RoutesRegistrar.RegisterRoutes(RouteTable.Routes, new[] {_currentAssembly});

			var controllerTypes = RoutesRegistrar.GetControllerTypes(new[] {_currentAssembly}).ToArray();
			_container.RegisterTransients(controllerTypes);

			var controllerFactory = new WindsorControllerFactory(_container);
			ControllerBuilder.Current.SetControllerFactory(controllerFactory);
		}

		public void RegisterSettingsManager()
		{
			var appSettings = _container.Resolve<IMongoStorage<SettingValue, ObjectId>>();
			_settingsManager = new SettingsManager(_container, () => appSettings.GetAll().ToDictionary(x => x.Section + "." + x.Key, x => x.Value));
			_settingsManager.RegisterSettings(_assemblies);
		}

		public void RegisterMongoDb()
		{
			_container.RegisterTypes(new Dictionary<Type, Type> {{typeof (IMongoStorage<,>), typeof (MongoStorage<,>)}});

			if (MongoAppHarborDatabaseProvider.IsViaAppHarbor)
			{
				_container.RegisterInstance<IMongoDatabaseProvider>(new MongoAppHarborDatabaseProvider());
				return;
			}

			_container.RegisterInstance<IMongoServerProvider>(new MongoServerProvider());
			_container.RegisterTypes(new Dictionary<Type, Type> {{typeof (IMongoDatabaseProvider), typeof (MongoDatabaseProvider)}});
		}

		public ILog InitializeLogger()
		{
			var log = LogManager.GetLogger(typeof (MvcApplication));
			XmlConfigurator.Configure();

			_container.RegisterInstance(log);
			return log;
		}

		public void RegisterGlobalFilters()
		{
			GlobalFilters.Filters.Add(new HandleErrorAttribute());
		}

		public void InitializeMappers()
		{
			RegisterTwoWayMapper<Conversation, ConversationDto>();
			RegisterTwoWayMapper<SocialUserInfo, Entity>();
		}

		private void RegisterTwoWayMapper<T1, T2>()
		{
			Mapper.CreateMap<T1, T2>();
			Mapper.CreateMap<T2, T1>();
		}

		public static void RefreshSettings()
		{
			_settingsManager.Refresh();
		}
	}
}