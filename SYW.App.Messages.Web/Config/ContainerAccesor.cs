﻿using CommonGround;

namespace SYW.App.Messages.Web.Config
{
	public class ContainerAccesor
	{
		public static ICommonContainer Container { get; private set; }

		public static void Init(ICommonContainer container)
		{
			Container = container;
		}
	}
}