﻿using System.Linq;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Messages;

namespace SYW.App.Messages.Web.Controllers
{
	public class BauEmailFeedObjectController : CommonGround.MvcInvocation.ControllerBase
	{
		private readonly IMongoStorage<BauEmailFeedObjectDto, ObjectId> _mongoStorage;
		private readonly IMongoStorage<MessageDto, ObjectId> _messageStorage;

		public BauEmailFeedObjectController(IMongoStorage<BauEmailFeedObjectDto, ObjectId> mongoStorage, IMongoStorage<MessageDto, ObjectId> messageStorage)
		{
			_mongoStorage = mongoStorage;
			_messageStorage = messageStorage;
		}

		[IgnoreAutoLogin]
		[IgnoreTokenExtractor]
		[PatternRoute("/bau/{id}")]
		public ActionResult RenderBauEmail(string id)
		{
			var filter = Builders<BauEmailFeedObjectDto>.Filter.Eq(x => x._id, ObjectId.Parse(id));
			var projection = Builders<BauEmailFeedObjectDto>.Projection.Include(x => x.Content);
			var bauEmail = _mongoStorage.GetFirstOrDefault(filter,projection);

			return Content(bauEmail != null ? bauEmail["Content"].AsString : string.Empty);
		}

		[IgnoreAutoLogin]
		[IgnoreTokenExtractor]
		[PatternRoute("/bau/message/{id}")]
		public ActionResult GetMessageContent(string id)
		{
			var filter = Builders<MessageDto>.Filter.Eq(x => x._id, ObjectId.Parse(id));
			var projection = Builders<MessageDto>.Projection.Include(x => x.Content);
			var message = _messageStorage.GetFirstOrDefault(filter,projection);
			
			return Content(message != null ? message["Content"].AsString : string.Empty);
		}
	}
}