﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messages.Web.Models.Cms;
using SYW.App.Messages.Web.Services.Cms;

namespace SYW.App.Messages.Web.Controllers.Cms
{
	public class SystemMessageController : Controller
	{
		private readonly ISaveBauEmailFeedObjectFlow _saveBauEmailFeedObjectFlow;
		private readonly IDeleteSystemMessageFlow _deleteSystemMessageFlow;


		public SystemMessageController
			(
			ISaveBauEmailFeedObjectFlow saveBauEmailFeedObjectFlow,
			IDeleteSystemMessageFlow deleteSystemMessageFlow)
		{
			_saveBauEmailFeedObjectFlow = saveBauEmailFeedObjectFlow;
			_deleteSystemMessageFlow = deleteSystemMessageFlow;
		}

		[PatternRoute("/__/cms/system-message/save")]
		public ActionResult SaveBauEmailFeedObject(BauEmailFeedObjectViewModel bauEmailFeedObjectViewModel)
		{
			var bauEmailFeedObject = _saveBauEmailFeedObjectFlow.Save(bauEmailFeedObjectViewModel);
			return Json(bauEmailFeedObject);
		}

		[PatternRoute("/__/cms/system-message/delete")]
		public void DeleteSystemMessage(string messageId)
		{
			_deleteSystemMessageFlow.Delete(ObjectId.Parse(messageId));
		}
	}
}
