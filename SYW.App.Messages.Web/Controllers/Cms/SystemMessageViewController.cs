﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Messages.Web.Services.Cms;

namespace SYW.App.Messages.Web.Controllers.Cms
{
	public class SystemMessageViewController : Controller
	{
		private readonly ISystemMessageRemovalViewModelCreator _systemMessageRemovalViewModelCreator;
		private readonly IBauEmailFeedObjectCreationViewModelCreator _bauEmailFeedObjectCreationViewModelCreator;

		public SystemMessageViewController(
			ISystemMessageRemovalViewModelCreator systemMessageRemovalViewModelCreator,
			IBauEmailFeedObjectCreationViewModelCreator bauEmailFeedObjectCreationViewModelCreator)
		{
			_systemMessageRemovalViewModelCreator = systemMessageRemovalViewModelCreator;
			_bauEmailFeedObjectCreationViewModelCreator = bauEmailFeedObjectCreationViewModelCreator;
		}

		[PatternRoute("/__/cms/system-message-view")]
		public ActionResult SystemMessageMainView()
		{
			return View(@"~/Views/Cms/SystemMessage/Main.cshtml");
		}

		[PatternRoute("/__/cms/system-message-view/filter")]
		public ActionResult SystemMessageFilterView(string messageId)
		{
			ViewBag.MessageId = messageId;
			return View(@"~/Views/Cms/SystemMessage/Main.cshtml");
		}

		[PatternRoute("/__/cms/system-message-view/create")]
		public PartialViewResult SystemMessageCreationView()
		{
			var model = _bauEmailFeedObjectCreationViewModelCreator.Create();
			return PartialView(@"~/Views/Cms/SystemMessage/_SystemMessage.cshtml", model);
		}

		[PatternRoute("/__/cms/system-message-view/delete")]
		public PartialViewResult SystemMessageDeletionView(string messageId)
		{
			var model = _systemMessageRemovalViewModelCreator.Create(messageId);
			return PartialView(@"~/Views/Cms/SystemMessage/_SystemMessage.cshtml", model);
		}
	}
}
