﻿using System;
using System.Text;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json;
using SYW.App.Messages.Web.Config;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using ControllerBase = CommonGround.MvcInvocation.ControllerBase;

namespace SYW.App.Messages.Web.Controllers
{
	[IgnoreForgeryAjaxAttribute]
	public class ConfigurationController : ControllerBase
	{
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly ICacheSystemMessageRemover _cacheSystemMessageRemover;

		public ConfigurationController(
			IEntityContextProvider entityContextProvider,
			ICacheSystemMessageRemover cacheSystemMessageRemover, IMongoStorage<ConversationDto, ObjectId> conversationStorage)
		{
			_entityContextProvider = entityContextProvider;
			_cacheSystemMessageRemover = cacheSystemMessageRemover;
			_conversationStorage = conversationStorage;
		}

		[PatternRoute("/-/refresh-configuration")]
		public ActionResult RefreshConfig()
		{
			return Content("Should refresh the configuration but right now it does nothing");
		}

		[PatternRoute("/-/refresh-cache")]
		public ActionResult RefreshCache()
		{
			_cacheSystemMessageRemover.Clear();
			return Content("Cache was cleared.");
		}

		[IgnoreTokenExtractor, IgnoreAutoLogin]
		[PatternRoute("/-/refresh-settings")]
		public ActionResult RefreshSettings()
		{
			Bootstrapper.RefreshSettings();
			return Content("OK");
		}

		[IgnoreTokenExtractor, IgnoreAutoLogin]
		[PatternRoute("/safari-problem")]
		public ActionResult SafariProblem()
		{
			return View();
		}

		[IgnoreTokenExtractor, IgnoreAutoLogin]
		[PatternRoute("/safari-cookie-fix")]
		public ActionResult FixSafariCookiePopUp()
		{
			return View();
		}

		[PatternRoute("/-/current-user")]
		public ActionResult CurrentUser()
		{
			return Content(_entityContextProvider.CurrentEntity().ObjectId);
		}

		[PatternRoute("/-/insertconversationwithnulldatetimes")]
		public ActionResult InsertCorruptedConversation()
		{
			var resultBuilder = new StringBuilder();
			var currentUser = _entityContextProvider.CurrentEntity();
			var corruptedConversation = new ConversationDto
			{
				_id = ObjectId.GenerateNewId(),
				CreatorId = currentUser.Id,
				IsSystemMessage = false,
				ParticipantsIds = new[] {currentUser.Id},
				StartDate = SystemTime.Now()
			};

			try
			{
				_conversationStorage.Insert(corruptedConversation);

				resultBuilder.Append("Inserted ConversationId: " + corruptedConversation._id + "<br />");

				var insertedConversation =
					_conversationStorage.GetFirstOrDefault(
						Builders<ConversationDto>.Filter.Where(x => x._id == corruptedConversation._id));

				resultBuilder.Append(JsonConvert.SerializeObject(insertedConversation));
			}
			catch (Exception e)
			{
				return Content(JsonConvert.SerializeObject(e));
			}
			return Content(resultBuilder.ToString());
		}
	}
}