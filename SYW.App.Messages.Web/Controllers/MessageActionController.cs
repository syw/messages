﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using CommonGround.Utilities;
using MongoDB.Bson;
using SYW.App.Messages.Web.Models;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Exceptions;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.Controllers
{
	public class MessageActionController : MessagesControllerBase
	{
		private readonly IConversationService _conversationService;
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IMessageStatusService _messageStatusService;
		private readonly IMessageRepository _messageRepository;
		private readonly IConversationParticipantValidator _conversationParticipantValidator;
		private readonly IMessagePreviewImageService _messagePreviewImageService;

		public MessageActionController(IConversationService conversationService,
		                               IEntityContextProvider entityContextProvider,
		                               IMessageStatusService messageStatusService,
		                               IMessageRepository messageRepository,
		                               IConversationParticipantValidator conversationParticipantValidator,
		                               IMessagePreviewImageService messagePreviewImageService)
		{
			_conversationService = conversationService;
			_entityContextProvider = entityContextProvider;
			_messageStatusService = messageStatusService;
			_messageRepository = messageRepository;
			_conversationParticipantValidator = conversationParticipantValidator;
			_messagePreviewImageService = messagePreviewImageService;
		}

		[HttpGet]
		[PatternRoute("/-/message/get-by-index")]
		public CustomJsonResult GetMessageByIndex(string conversationId, int messageIndex)
		{
			var conversation = GetConversationWithoutArchivedMessages(conversationId);

			if (conversation.Messages.Count < messageIndex)
				throw new ArgumentException("Message index is invalid", "messageIndex");

			var message = conversation.Messages[messageIndex - 1];

			var model = new SystemMessageModel
				            {
					            ConversationId = conversationId,
					            MessageId = message.Id.ToString(),
					            MessageHeight = message.ContentHeight,
					            MessageWidth = message.ContentWidth,
					            MessageIndex = messageIndex - 1,
					            TotalMessagesCount = conversation.Messages.Count,
					            TotalUnreadCount = conversation.Messages.Count(x => x.Status == ConversationStatus.Unread || x.Status == ConversationStatus.New),
					            BauName = conversation.Participants.First().Name,
					            DateTime = message.Date.ToShortDateString()
				            };

			if (message.Status == ConversationStatus.Unread || message.Status == ConversationStatus.New)
			{
				_messageStatusService.UpdateStatus(conversation.Id, message.Id, _entityContextProvider.CurrentEntity().Id, ConversationStatus.Read);
				model.TotalUnreadCount--;
			}

			return Json(model);
		}

		private string SetSystemMessageContent(Message message, IConversation conversation)
		{
			var messageContent = message.Content;

			messageContent = messageContent.Replace("<a ", "<a target=\"_blank\" ");

			messageContent = Regex.Replace(messageContent, @"^<(!DOCTYPE)([\w=""':;!/.-_@ ]+)?>$", "", RegexOptions.Multiline | RegexOptions.IgnoreCase);
			messageContent = Regex.Replace(messageContent, @"^</?(html)([\w=""':;!/.-_@ ]+)?>$", "", RegexOptions.Multiline | RegexOptions.IgnoreCase);
			messageContent = Regex.Replace(messageContent, @"^</?(head)([\w=""':;!/.-_@ ]+)?>$", "", RegexOptions.Multiline | RegexOptions.IgnoreCase);
			messageContent = Regex.Replace(messageContent, @"^</?(body)([\w=""':;!/.-_@ ]+)?>$", "", RegexOptions.Multiline | RegexOptions.IgnoreCase);

			var scripts = "<script type='text/javascript' src='/Scripts/system-message-content-analytics.js'></script> ";

			scripts += string.Format(@"<script>var model={{bauName : ""{0}"", dateTime: ""{1}""}};</script> ",
			                         conversation.Participants.First().Name.Replace("\"", "'"),
			                         message.Date.ToShortDateString());

			return scripts + messageContent;
		}

		[HttpGet]
		[PatternRoute("/-/message/get-by-id")]
		public CustomJsonResult GetMessageByID(string conversationId, string messageId)
		{
			var entityContext = _entityContextProvider.CurrentEntity();

			var conversation = GetConversationWithoutArchivedMessages(conversationId);

			var message = conversation.Messages.FirstOrDefault(x => x.Id == ObjectId.Parse(messageId));

			if (message == null || conversation.Messages.Count == 0 || string.IsNullOrEmpty(message.Content = _messageRepository.GetMessageContentById(messageId)))
				throw new ArgumentException("message was not found for the messageId", "messageId");

			_conversationParticipantValidator.Validate(entityContext.Id, new ObjectId(conversationId));

			var model = new SystemMessageModel
				            {
					            ConversationId = conversationId,
					            MessageId = message.Id.ToString(),
					            MessageHeight = message.ContentHeight,
					            MessageWidth = message.ContentWidth,
					            MessageIndex = conversation.Messages.IndexOf(message),
					            BauName = message.Author.Name,
					            TotalMessagesCount = conversation.Messages.Count,
					            TotalUnreadCount = conversation.Messages.Count(x => x.Status == ConversationStatus.Unread || x.Status == ConversationStatus.New),
					            AuthorImageUrl = message.Author.ImageUrl,
					            AuthorName = message.Author.Name,
					            MessageContent = SetSystemMessageContent(message, conversation),
					            DateTime = message.Date.ToString()
				            };

			if (message.Status == ConversationStatus.Unread || message.Status == ConversationStatus.New)
			{
				_messageStatusService.UpdateStatus(conversation.Id, message.Id, _entityContextProvider.CurrentEntity().Id, ConversationStatus.Read);
				model.TotalUnreadCount--;
			}

			return Json(model);
		}

		private Conversation GetConversationWithoutArchivedMessages(string conversationId)
		{
			var conversation = _conversationService.Get(conversationId, _entityContextProvider.CurrentEntity().Id);
			if (conversation == null)
				throw new ConversationNotFoundException(ObjectId.Parse(conversationId));

			conversation.Messages = conversation.Messages.Where(x => x.Status != ConversationStatus.Archive).ToArray();

			return conversation;
		}

		[PatternRoute("/-/message/archive")]
		public ActionResult ArchiveMessage(string conversationId, string messageId)
		{
			_messageStatusService.UpdateStatus(conversationId, messageId, _entityContextProvider.CurrentEntity().Id, ConversationStatus.Archive);
			return Content("OK");
		}

		[PatternRoute("/-/message/messages-preview")]
		public ActionResult GetMoreMessages(string conversationId, int page, int pageSize)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();
			var conversations = string.IsNullOrEmpty(conversationId) ?
				                                                         new List<IConversation>(_conversationService.GetAll(currentEntity.Id, currentEntity.MemberSince ?? SystemTime.Now(), -1, -1)) :
					                                                     new List<IConversation> {_conversationService.Get(conversationId, _entityContextProvider.CurrentEntity().Id)};

            //ILAN: Appended this line
            if (!conversations.Any())
                PartialView(@"~/Views/Messages/MessagePreview.cshtml", null);

			var messages = conversations.EmptyIfNull().SelectMany(c => c.Messages)
				.Where(x => x.Status != ConversationStatus.Archive)
				.OrderByDescending(m => m.Date)
				.Skip((page - 1)*pageSize)
				.Take(pageSize).ToList();

			if (page > 1 && messages.IsNullOrEmpty())
                PartialView(@"~/Views/Messages/MessagePreview.cshtml", null);

			var model = messages.Select(x => new MessagePreviewModel
				                                 {
					                                 CoversationId = x.ConversationId,
					                                 MessageId = x.Id,
					                                 CreatedTime = x.Date,
					                                 AuthorName = x.Author.Name,
					                                 ImageUrl = _messagePreviewImageService.InitMessagePreviewImage(x, conversations.FirstOrDefault(c => c.Id == x.ConversationId), currentEntity.Id),
					                                 Status = x.Status,
					                                 Subject = x.PreviewText,
					                                 IsSystemMessage = x.ContentWidth != 0,
					                                 Participants = conversations.FirstOrDefault(c => c.Id == x.ConversationId).Participants
				                                 });

			return PartialView(@"~/Views/Messages/MessagePreview.cshtml", model);
		}
	}
}