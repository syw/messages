﻿using System;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messages.Web.SingalR;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.Services.ExternalEmails;
using log4net;

namespace SYW.App.Messages.Web.Controllers
{
	public class MessageDelegatorController : CommonGround.MvcInvocation.ControllerBase
	{
		private readonly IConversationNotifierService _conversationNotifierService;
		private readonly ILog _logger;
		private readonly IExternalEmailReceptor _externalEmailReceptor;
		private readonly IExternalEmailSanitizer _externalEmailSanitizer;

		public MessageDelegatorController(ILog logger, IExternalEmailReceptor externalEmailReceptor, IConversationNotifierService conversationNotifierService, IExternalEmailSanitizer externalEmailSanitizer)
		{
			_logger = logger;
			_externalEmailReceptor = externalEmailReceptor;
			_conversationNotifierService = conversationNotifierService;
			_externalEmailSanitizer = externalEmailSanitizer;
		}

		[IgnoreAutoLogin]
		[IgnoreTokenExtractor]
		[IgnoreForgeryAjax]
		[PatternRoute("/-/trusted-message-post")]
		public ActionResult PostTrustedMessage(string sender, string particpantEmails, string content, string attachmentsUrl = "", string senderName = "")
		{
			try
			{
				content = _externalEmailSanitizer.DecodeAndSanitize(content);
				
				if (content.IsNullOrEmpty())
				{
					_logger.Error("Received email with no content. (sender is: '" + sender + "')");
					return Content("No content received.");
				}

				var message = _externalEmailReceptor
					.CreateMessage(sender, particpantEmails, content, attachmentsUrl, senderName);

				NotifyMessageReceived(message);
			}
			catch (Exception e)
			{
				_logger.Error(e);
				return Content("Some error occured in the page.");
			}

			return Content("OK");
		}

		private void NotifyMessageReceived(ExternalEmailMessage externalEmailMessage)
		{
			_conversationNotifierService.Notify(externalEmailMessage.ConversationId.ToString(), externalEmailMessage.AuthorId);
		}
	}
}