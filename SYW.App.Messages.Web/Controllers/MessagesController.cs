﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messages.Web.Conversations;
using SYW.App.Messages.Web.Models;
using SYW.App.Messages.Web.Services;
using SYW.App.Messages.Web.SingalR;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services.Platform;
using StackExchange.Profiling;
using SYW.App.Messsages.Domain;

namespace SYW.App.Messages.Web.Controllers
{
	public class MessagesController : CommonGround.MvcInvocation.ControllerBase
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly ISearchFriendsService _searchFriendsService;
		private readonly IMessagesService _messagesService;
		private readonly IEntitiesResolver _entitiesResolver;
		private readonly IConversationService _conversationService;
		private readonly IConversationNotifierService _conversationNotifierService;
		private readonly IConversationParticipantValidator _conversationParticipantValidator;
		private readonly IUserConversationStatusRepository _userConversationStatusRepository;
		private readonly IUserService _userService;
		private readonly INewConversationFlow _newConversationFlow;
		private readonly IEntityOpenGraphDetailsProvider _entityOpenGraphDetailsProvider;

		public MessagesController(
			IEntityContextProvider entityContextProvider,
			ISearchFriendsService searchFriendsService,
			IEntitiesResolver entitiesResolver,
			IConversationService conversationService,
			IConversationNotifierService conversationNotifierService,
			IMessagesService messagesService,
			IConversationParticipantValidator conversationParticipantValidator,
			IUserConversationStatusRepository userConversationStatusRepository,
			IUserService userService,
			INewConversationFlow newConversationFlow,
			IEntityOpenGraphDetailsProvider entityOpenGraphDetailsProvider)
		{
			_entityContextProvider = entityContextProvider;
			_searchFriendsService = searchFriendsService;
			_entitiesResolver = entitiesResolver;
			_conversationService = conversationService;
			_conversationNotifierService = conversationNotifierService;
			_messagesService = messagesService;
			_conversationParticipantValidator = conversationParticipantValidator;
			_userConversationStatusRepository = userConversationStatusRepository;
			_userService = userService;
			_newConversationFlow = newConversationFlow;
			_entityOpenGraphDetailsProvider = entityOpenGraphDetailsProvider;
		}

		[PatternRoute("/")]
		public ActionResult Home()
		{
			return Content("Working");
		}

		[PatternRoute("/mailbox/dialog/messages/new-conversation-box")]
		[PatternRoute("/dialog/messages/new-conversation-box")]
		public ActionResult NewMessageBox(long[] participantsIds, string message, string appId)
		{
			ViewBag.BodyId = "new-message-dialog";

			return View(new NewMessageBoxModel
			{
				Participants = GetParticipantsFromPlatform(participantsIds),
				Message = message,
				AppId = appId
			});
		}

		private IList<Entity> GetParticipantsFromPlatform(long[] participantsIds)
		{
			if (participantsIds.IsNullOrEmpty())
				return null;

			return _userService.GetUsers(participantsIds).EmptyIfNull().Select(p => new Entity
			{
				OriginalId = p.id,
				ImageUrl = p.imageUrl,
				Name = p.name
			}).ToList();
		}

		[PatternRoute("/-/messages/post-new-message")]
		public ActionResult PostNewMessage(string content, string conversationId)
		{
			var entityContext = _entityContextProvider.CurrentEntity();

			var conversationObjectId = ObjectId.Parse(conversationId);

			_conversationParticipantValidator.Validate(entityContext.Id, conversationObjectId);

			var message = _messagesService.Create(conversationObjectId, entityContext.Id, content);

			_conversationNotifierService.Notify(conversationId, entityContext.Id);

			_entitiesResolver.Resolve(new[] {message.Author});

			return PartialView(@"~/Views/Messages/Message.cshtml",
				new MessageModel {Message = message, ConversationId = conversationObjectId});
		}

		[PatternRoute("/-/messages/post")]
		public ActionResult Post(long[] participants, string content, string entityLink)
		{
			var attachment = CreateAttachment(entityLink);

			var currentEntity = _entityContextProvider.CurrentEntity() ?? new EntityContext {OriginalId = 23};
				//this can't possibly be good.

			var conversation = _newConversationFlow.Start(currentEntity.OriginalId, participants, content, new[] {attachment});

			conversation.Participants = conversation.Participants.Where(p => p.OriginalId != currentEntity.OriginalId).ToList();

			_conversationNotifierService.Notify(conversation.Id.ToString(), currentEntity.Id);

			_userConversationStatusRepository.UpdateStatus(conversation.Id, currentEntity.Id, ConversationStatus.Read);

			return PartialView(@"~/Views/Conversations/Conversation.cshtml", new ConversationModel
			{
				Conversation = conversation,
				EntityContext = currentEntity
			});
		}

	    private Attachment CreateAttachment(string entityLink)
	    {
            var entityDetails = _entityOpenGraphDetailsProvider.Provide(entityLink).Result;

            return new Attachment(entityDetails.EntityTitle, entityDetails.EntityLink, entityDetails.EntityImageUrl);
	    }

		private string ReformatContent(string content)
		{
			var reformatted = new StringBuilder(content);
			reformatted.Replace("&quot;", "\"").Replace("&lt;", "<").Replace("&gt;", ">");
			//   reformatted.Replace("&amp;lt;", "<");
			return reformatted.ToString();
		}

		[PatternRoute("/-/mailbox/get-conversation")]
		public ActionResult GetSingleConversation(string conversationId)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();

			_conversationParticipantValidator.Validate(currentEntity.Id, ObjectId.Parse(conversationId));

			var conversation = _conversationService.Get(conversationId, currentEntity.Id);

			foreach (var message in conversation.Messages)
				message.Content = ReformatContent(message.Content);

			conversation.Participants = conversation.Participants.Where(p => p._id != currentEntity.Id).ToList();
			_conversationService.ReOrderParticipantsAccordingToSendingOrder(conversation, currentEntity.Id);

			var conversationDescriptor = new ConversationDescriptor
			{
				Id = conversation.Id,
				Messages = conversation.Messages,
				Participants = conversation.Participants,
				StartDate = conversation.StartDate,
				Timestamp = conversation.Timestamp,
				Status = ConversationStatus.New
			};

			return PartialView(@"~/Views/Conversations/Conversation.cshtml", new ConversationModel
			{
				Conversation = conversationDescriptor,
				EntityContext = currentEntity
			});
		}

		[PatternRoute("/-/search")]
		public ActionResult Search(string term, long[] participants)
		{
			using (MiniProfiler.Current.Step("Search"))
			{
				var friends = _searchFriendsService.Search(term, participants);

				// value = term is a hack which will make the autocomplete not changing the input text
				return Json(friends.Select(f => new {image = f.ImageUrl, id = f.OriginalId, name = f.Name, value = term}).ToArray());
			}
		}
	}
}