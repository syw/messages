﻿using System.Web;
using System.Web.Mvc;

namespace SYW.App.Messages.Web.Controllers
{
	public class MessagesControllerBase : CommonGround.MvcInvocation.ControllerBase
	{
		public bool IsMobileBrowser { get; set; }

		protected ViewResult View<T>(string viewName, T model)
		{
			ViewData["IsMobileBrowser"] = HttpContext != null ? HttpContext.Request.IsMobileDevice() : false;
			return base.View(viewName, model);
		}
	}

	public static class HttpRequestExtensions
	{
		public static bool IsMobileDevice(this HttpRequestBase request)
		{
			if (request.Browser.IsMobileDevice)
				return true;

			if (string.IsNullOrEmpty(request.UserAgent))
				return false;

			return request.UserAgent.ToLower().Contains("iphone");
		}
	}
}