﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Messages.Web.Filters;

namespace SYW.App.Messages.Web.Controllers
{
	public class MonitoringController : CommonGround.MvcInvocation.ControllerBase
	{
		[IgnoreAutoLogin]
		[IgnoreTokenExtractor]
		[PatternRoute("/monitor")]
		public ActionResult IsSiteOnline()
		{
			return Content("OK");
		}

		[IgnoreAutoLogin]
		[IgnoreTokenExtractor]
		[PatternRoute("/warmup")]
		public ActionResult DeploymentWarmup()
		{
			return Content("Messages app server is working!");
		}
	}
}