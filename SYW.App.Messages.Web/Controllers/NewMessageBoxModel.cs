using System.Collections.Generic;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.Web.Controllers
{
	public class NewMessageBoxModel
	{
		public IList<Entity> Participants { get; set; }
		public string Message { get; set; }
		public string AppId { get; set; }
	}
}