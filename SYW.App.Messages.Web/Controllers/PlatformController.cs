﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Services;
using SYW.App.Messsages.Domain.Services.Installer;
using SYW.App.Messsages.Domain.Services.Settings;
using ControllerBase = CommonGround.MvcInvocation.ControllerBase;

namespace SYW.App.Messages.Web.Controllers
{
	public class PlatformController : ControllerBase
	{
		private readonly IEntityProvider _entityProvider;
		private readonly IAppInstaller _appInstaller;
		private readonly IApplicationSettings _applicationSettings;
		private readonly IRoutes _routes;

		public PlatformController(IEntityProvider entityProvider, IAppInstaller appInstaller, IApplicationSettings applicationSettings, IRoutes routes)
		{
			_entityProvider = entityProvider;
			_appInstaller = appInstaller;
			_applicationSettings = applicationSettings;
			_routes = routes;
		}

		[IgnoreAutoLogin]
		[PatternRoute("/landing")]
		public ActionResult LandingPage()
		{
			_appInstaller.Install();

			var currentEntity = _entityProvider.GetEntityFromPlatform();

			_appInstaller.SendWelcomeMessage(currentEntity._id);

			var siteUrl = _applicationSettings.BaseSiteUrl + _routes.DefaultAppUrl;
			return View(new LandingPageModel {DefaultAppUrl = siteUrl});
		}
	}

	public class LandingPageModel
	{
		public string DefaultAppUrl { get; set; }
	}
}