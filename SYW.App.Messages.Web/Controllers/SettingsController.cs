﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using SYW.App.Messages.Web.Conversations;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messages.Web.Controllers
{
	public class SettingsController : CommonGround.MvcInvocation.ControllerBase
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IEntitiesRepository _entitiesRepository;
		private readonly IEmailValidator _emailValidator;
		private readonly IConversationPageSettings _conversationPageSettings;


		public SettingsController(IEntityContextProvider entityContextProvider, IEntitiesRepository entitiesRepository, IEmailValidator emailValidator, IConversationPageSettings conversationPageSettings)
		{
			_entityContextProvider = entityContextProvider;
			_entitiesRepository = entitiesRepository;
			_emailValidator = emailValidator;
			_conversationPageSettings = conversationPageSettings;
		}

		[PatternRoute("/settings")]
		public ActionResult Settings()
		{
			var entity = _entitiesRepository.Get(_entityContextProvider.CurrentEntity().Id);

			var email = string.IsNullOrEmpty(entity.Email) ? null : entity.Email.Substring(0, entity.Email.IndexOf("@"));

			return View(@"~/Views/Settings/Settings.cshtml", new SettingsModel {Email = email, EmailDomain = _conversationPageSettings.EmailDomain});
		}

		[PatternRoute("/-/settings/update")]
		public ActionResult UpdateSettings(string email)
		{
			email += _conversationPageSettings.EmailDomain;

			var emailStatus = _emailValidator.ValidateEmail(email);

			if (emailStatus != EmailValidationStatus.Ok)
				return Json(emailStatus);

			var entity = _entitiesRepository.Get(_entityContextProvider.CurrentEntity().Id);
			if (!string.IsNullOrEmpty(entity.Email))
				throw new ForbiddenOperationException("Can't set Email address if email already exists.");

			entity.Email = email;

			_entitiesRepository.Update(entity);

			return Json(emailStatus);
		}

		[PatternRoute("/-/settings/check-email")]
		public ActionResult CheckEmail(string email)
		{
			email += _conversationPageSettings.EmailDomain;

			var status = _emailValidator.ValidateEmail(email);

			return Json(status);
		}
	}

	public class SettingsModel
	{
		public string Email { get; set; }
		public string EmailDomain { get; set; }
	}
}