﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messages.Web.Controllers;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;

namespace SYW.App.Messages.Web.Conversations
{
	public class ConversationActionController : MessagesControllerBase
	{
		private readonly IConversationStatusService _conversationStatusService;
		private readonly IEntityContextProvider _entityContextProvider;

		public ConversationActionController(IConversationStatusService conversationStatusService, IEntityContextProvider entityContextProvider)
		{
			_conversationStatusService = conversationStatusService;
			_entityContextProvider = entityContextProvider;
		}

		[PatternRoute("/-/conversation/more-actions")]
		public ActionResult MoreActions()
		{
			return PartialView(@"~/Views/Conversations/MoreActions.cshtml", null);
		}

		[PatternRoute("/-/conversation/update-status")]
		public ActionResult UpdateStatus(string conversationId, int status)
		{
			_conversationStatusService.UpdateStatus(ObjectId.Parse(conversationId), _entityContextProvider.CurrentEntity().Id, (ConversationStatus)status);

			return Content("OK");
		}

		[PatternRoute("/-/conversation/archive-all")]
		public ActionResult ArchiveAll()
		{
			_conversationStatusService.ArchiveAll(_entityContextProvider.CurrentEntity().Id);

			return Content("OK");
		}

		[PatternRoute("/-/conversation/mark-all-as-read")]
		public ActionResult MarkAllAsRead()
		{
			_conversationStatusService.MarkAllAsRead(_entityContextProvider.CurrentEntity().Id);

			return Content("OK");
		}
	}
}