﻿using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;

namespace SYW.App.Messages.Web.Conversations
{
	public class ConversationModel
	{
		public EntityContext EntityContext { get; set; }
		public ConversationDescriptor Conversation { get; set; }
	}
}