using MongoDB.Bson;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;

namespace SYW.App.Messages.Web.Conversations
{
	public class ConversationPageModel
	{
		public bool DisplayMoreMessagesTeaser { get; set; }
		public EntityContext CurrentEntity { get; set; }
		public Conversation Conversation { get; set; }
		public int TotalMessagesCount { get; set; }
		public int TotalUnreadCount { get; set; }
		public string MessageId { get; set; }
		public bool IsReadonly { get; set; }
		public string InvitationUrl { get; set; }

		public string ImageUrl
		{
			get { return "https://s2.sywcdn.net/user/148e_23887162.jpg"; }
		}
	}
}