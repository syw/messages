﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CommonGround.Events;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messages.Web.Controllers;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services;
using SYW.App.Messsages.Domain.Services.Settings;
using StackExchange.Profiling;

namespace SYW.App.Messages.Web.Conversations
{
	public class ConversationsPageController : MessagesControllerBase
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IConversationService _conversationService;
		private readonly ICachedUserFriendsProvider _cachedUserFriendsProvider;
		private readonly IUpdateConversationStatusService _updateConversationStatusService;
		private readonly IConversationPageSettings _conversationPageSettings;
		private readonly IConversationParticipantValidator _conversationParticipantValidator;
		private readonly IApplicationSettings _applicationSettings;
		private readonly IUrlifier _urlifier;

		private readonly IEvent<NewSystemMessageEvent> _newSystemMessageEvent;

		public ConversationsPageController(IConversationService conversationService, IEntityContextProvider entityContextProvider, ICachedUserFriendsProvider cachedUserFriendsProvider, IUpdateConversationStatusService updateConversationStatusService, IConversationPageSettings conversationPageSettings, IConversationParticipantValidator conversationParticipantValidator, IEvent<NewSystemMessageEvent> newSystemMessageEvent, IUrlifier urlifier, IApplicationSettings applicationSettings)
		{
			_conversationService = conversationService;
			_conversationPageSettings = conversationPageSettings;
			_conversationParticipantValidator = conversationParticipantValidator;
			_newSystemMessageEvent = newSystemMessageEvent;
			_entityContextProvider = entityContextProvider;
			_cachedUserFriendsProvider = cachedUserFriendsProvider;
			_updateConversationStatusService = updateConversationStatusService;
			_urlifier = urlifier;
			_applicationSettings = applicationSettings;
		}

		[PatternRoute("/mailbox")]
		//[PatternRoute("/mailbox/mobile")]
		[OfflineTokenProviderFilter]
		public ActionResult DisplayConversationsPage()
		{
			using (MiniProfiler.Current.Step("DisplayConversationsPage"))
			{
				var model = new ConversationsPageModel
					            {
						            EntityContext = _entityContextProvider.CurrentEntity(),
						            IsSettingsEnabled = _conversationPageSettings.SettingsTabEnabled
					            };

				return View(@"~/Views/Conversations/ConversationsPage.cshtml", model);
			}
		}

		[PatternRoute("/-/mailbox/more")]
		public ActionResult MoreConversations(int page, int pageSize)
		{
			var conversations = GetConverstations(page, pageSize);

			var model = new ConversationsPageModel
				            {
					            Conversations = conversations
				            };

			return PartialView(@"~/Views/Conversations/Conversations.cshtml", model);
		}

		private IList<ConversationModel> GetConverstations(int page, int pageSize)
		{
			var entity = _entityContextProvider.CurrentEntity();

			var conversations = _conversationService.GetAll(entity.Id, entity.MemberSince.GetValueOrDefault(), page, pageSize);

			// Remove the current user from the conversation in order to display everybody else
			foreach (var conversation in conversations)
			{
				conversation.Participants = conversation
					.Participants
					.Where(p => p._id != entity.Id)
					.ToList();
				_conversationService.ReOrderParticipantsAccordingToSendingOrder(conversation, entity.Id);
			}

			return conversations.Select(c => new ConversationModel {Conversation = c, EntityContext = entity}).ToList();
		}

		[PatternRoute("/mailbox/all")]
		public ActionResult AllMessagesPage()
		{
			return View(@"~/Views/Messages/AllMessagesContainer.cshtml", new ConversationPageModel
				                                                             {
					                                                             CurrentEntity = _entityContextProvider.CurrentEntity(),
					                                                             Conversation = new Conversation {Id = new ObjectId(), Messages = new List<Message> {new Message {Author = new Entity()}}},
					                                                             TotalMessagesCount = 0,
					                                                             TotalUnreadCount = 0,
					                                                             MessageId = "0"
				                                                             });
		}

		[PatternRoute("/mailbox/conversations/")]
		[HttpPost]
		public ActionResult ConversationPage(string conversationId, string messageId = null)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();
			var conversation = _conversationService.Get(conversationId, currentEntity.Id);

			_conversationParticipantValidator.Validate(currentEntity.Id, ObjectId.Parse(conversationId));

			return conversation.IsSystemMessage ? SystemConversationPage(conversation, messageId) : RegularConversationPage(conversation);
		}

		private ActionResult SystemConversationPage(Conversation conversation, string messageId)
		{
			var unreadCount = conversation.Messages.Count(x => x.Status == ConversationStatus.Unread);
			var newCount = conversation.Messages.Count(x => x.Status == ConversationStatus.New);

			return View(@"~/Views/Messages/SystemMessagesContainer.cshtml", new ConversationPageModel
				                                                                {
					                                                                CurrentEntity = _entityContextProvider.CurrentEntity(),
					                                                                Conversation = conversation,
					                                                                TotalMessagesCount = conversation.Messages.Count(m => m.Status != ConversationStatus.Archive),
					                                                                TotalUnreadCount = unreadCount + newCount,
					                                                                MessageId = messageId
				                                                                });
		}

		private ActionResult RegularConversationPage(Conversation conversation)
		{
			var messagesCount = conversation.Messages.Count;
			conversation.Messages = conversation.Messages.Take(10).Reverse().ToList();

			_updateConversationStatusService.UpdateStatus(conversation.Id, _entityContextProvider.CurrentEntity().Id, ConversationStatus.Read);

			var currentEntity = _entityContextProvider.CurrentEntity();

			return View(@"~/Views/Messages/RegularMessagesContainer.cshtml", new ConversationPageModel
				                                                                 {
					                                                                 CurrentEntity = currentEntity,
					                                                                 Conversation = conversation,
					                                                                 DisplayMoreMessagesTeaser = messagesCount > 10,
					                                                                 IsReadonly = conversation.Participants.Any(x => x.PlatformOfflineToken == null && x.OriginalId == 0),
					                                                                 InvitationUrl = string.Format(_applicationSettings.InvitationUrlFormat, _urlifier.Urlify(currentEntity.Name), currentEntity.OriginalId)
				                                                                 });
		}

		[PatternRoute("/-/messages/more")]
		public ActionResult MoreMessages(string lastMessageId, string conversationId)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();
			var conversation = _conversationService.Get(conversationId, currentEntity.Id);

			_conversationParticipantValidator.Validate(currentEntity.Id, ObjectId.Parse(conversationId));

			var messageId = ObjectId.Parse(lastMessageId);
			var lastMessage = conversation.Messages.First(m => m.Id == messageId);

			conversation.Messages = conversation.Messages.Where(m => m.Date < lastMessage.Date).ToList();

			var displayMoreMessagesTeaser = conversation.Messages.Count > 5;

			conversation.Messages = conversation.Messages.Take(5).Reverse().ToList();

			if (conversation.Messages.IsNullOrEmpty())
				return Content("OK");

			return PartialView("~/Views/Messages/Messages.cshtml", new ConversationPageModel
				                                                       {
					                                                       CurrentEntity = _entityContextProvider.CurrentEntity(),
					                                                       Conversation = conversation,
					                                                       DisplayMoreMessagesTeaser = displayMoreMessagesTeaser
				                                                       });
		}

		[PatternRoute("/-/init-friends")]
		public ActionResult InitFriends()
		{
			_cachedUserFriendsProvider.Init();
			return Content("OK");
		}
	}
}