using System.Collections.Generic;
using SYW.App.Messages.Web.Services;

namespace SYW.App.Messages.Web.Conversations
{
	public class ConversationsPageModel
	{
		public EntityContext EntityContext { get; set; }
		public IList<ConversationModel> Conversations { get; set; }
		public bool IsSettingsEnabled { get; set; }
	}
}