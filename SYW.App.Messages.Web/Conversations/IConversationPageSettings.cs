﻿using CommonGround.Settings;

namespace SYW.App.Messages.Web.Conversations
{
	public interface IConversationPageSettings
	{
		[Default(true)]
		bool SettingsTabEnabled { get; set; }

		[Default("@sywmail.com")]
		string EmailDomain { get; set; }
	}
}