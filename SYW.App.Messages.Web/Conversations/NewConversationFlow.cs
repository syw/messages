﻿using System.Collections.Generic;
using System.Linq;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Exceptions;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.Conversations
{
	public interface INewConversationFlow
	{
		ConversationDescriptor Start(long originalCreatorId, long[] originalParticipantsIds, string content, IList<Attachment> attachments = null);
	}

	public class NewConversationFlow : INewConversationFlow
	{
		private readonly IStartNewConversationService _startNewConversationService;
		private readonly ICachedUserFriendsProvider _cachedUserFriendsProvider;

		public NewConversationFlow(
			IStartNewConversationService startNewConversationService, 
			ICachedUserFriendsProvider cachedUserFriendsProvider)
		{
			_startNewConversationService = startNewConversationService;
			_cachedUserFriendsProvider = cachedUserFriendsProvider;
		}

		public ConversationDescriptor Start(long originalCreatorId, long[] originalParticipantsIds, string content, IList<Attachment> attachments = null)
		{
			originalParticipantsIds = FilterFriends(originalParticipantsIds, originalCreatorId);

			if (!originalParticipantsIds.Any())
				throw new ForbiddenOperationException("Can't send message to empty participants list");

			return _startNewConversationService.Start(originalCreatorId, originalParticipantsIds, content, attachments);
		}

		private long[] FilterFriends(IEnumerable<long> originalParticipantsIds, long originalCreatorId)
		{
			var allowedParticipants = _cachedUserFriendsProvider
				.GetFriends()
				.Select(entity => entity.OriginalId)
				.Union(new[] { originalCreatorId });

			return allowedParticipants
				.Join(
					originalParticipantsIds,
					friendId => friendId,
					participantId => participantId,
					(friendId, participantId) => participantId)
				.ToArray();
		}
	}
}