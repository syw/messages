﻿using System;
using PlatformClient;
using SYW.App.Messages.Web.Config;
using SYW.App.Messages.Web.Filters;

namespace SYW.App.Messages.Web.Cookies
{
	public class SessionSaltProvider
	{
		private readonly IForgeryAjaxRequestBlockSettings _forgeryAjaxRequestBlockSettings;

		public SessionSaltProvider()
		{
			_forgeryAjaxRequestBlockSettings = ContainerAccesor.Container.Resolve<IForgeryAjaxRequestBlockSettings>();
		}

		public SessionSalt Provide()
		{
			return new SessionSalt(
				_forgeryAjaxRequestBlockSettings.SessionSaltHeaderKey,
				Guid.NewGuid().ToByteArray().ToHexString());
		}
	}

	public class SessionSalt
	{
		public SessionSalt(string key, string value)
		{
			Key = key;
			Value = value;
		}

		public string Key { get; private set; }
		public string Value { get; private set; }


	}
}