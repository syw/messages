﻿using System.Web.Mvc;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.Web.Filters
{
	public interface ICmsFilter : IAuthorizationFilter {}

	public class CmsFilter : ICmsFilter
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly ICmsUserSettings _cmsUserSettings;

		public CmsFilter(
			IEntityContextProvider entityContextProvider,
			ICmsUserSettings cmsUserSettings)
		{
			_entityContextProvider = entityContextProvider;
			_cmsUserSettings = cmsUserSettings;
		}


		public void OnAuthorization(AuthorizationContext filterContext)
		{
			var query = filterContext.HttpContext.Request.QueryString["p"];
			if (query != "cms")
				return;

			var userId = _entityContextProvider.CurrentEntity();
			if (userId == null)
				return;

			if (!_cmsUserSettings.AllowedUsersArray.Contains(userId.OriginalId.ToString()))
				return;

			filterContext.Result = new RedirectResult("/__/cms/system-message-view/");
		}
	}
}