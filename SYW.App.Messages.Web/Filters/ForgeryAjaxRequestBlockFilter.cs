using System;
using System.Linq;
using System.Web.Mvc;
using CommonGround.Settings;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messages.Web.Filters
{
	public interface IForgeryAjaxRequestBlockFilter : IAuthorizationFilter
	{
	}

	public class ForgeryAjaxRequestBlockFilter : IForgeryAjaxRequestBlockFilter
	{
		private readonly Type _ignoreAttributeType = typeof (IgnoreForgeryAjaxAttribute);
		private readonly IForgeryAjaxRequestBlockSettings _forgeryAjaxRequestBlockSettings;

		public ForgeryAjaxRequestBlockFilter(IForgeryAjaxRequestBlockSettings forgeryAjaxRequestBlockSettings)
		{
			_forgeryAjaxRequestBlockSettings = forgeryAjaxRequestBlockSettings;
		}

		public void OnAuthorization(AuthorizationContext filterContext)
		{
			if (!_forgeryAjaxRequestBlockSettings.Enabled)
				return;

			if (filterContext.HttpContext.Request.Url != null &&
			    !filterContext.HttpContext.Request.Url.PathAndQuery.StartsWith("/-/"))
				return;

			if (HasIgnoreAttribute(filterContext)) 
				return;

			var sessionSaltHeaderKey = _forgeryAjaxRequestBlockSettings.SessionSaltHeaderKey;

			var sessionSaltHeaderReceived = filterContext.HttpContext.Request.Headers[sessionSaltHeaderKey];
			if (string.IsNullOrEmpty(sessionSaltHeaderReceived))
				throw new ForgeryAjaxRequestBlockException();
		}

		private bool HasIgnoreAttribute(AuthorizationContext filterContext)
		{
			var ignoreForgery =
				filterContext.ActionDescriptor.ControllerDescriptor.GetCustomAttributes(_ignoreAttributeType, false);

			if (ignoreForgery.Any())
				return true;

			ignoreForgery = filterContext.ActionDescriptor.GetCustomAttributes(_ignoreAttributeType, false);

			if (ignoreForgery.Any())
				return true;

			return false;
		}
	}

	public class IgnoreForgeryAjaxAttribute : Attribute
	{
	}

	public interface IForgeryAjaxRequestBlockSettings
	{
		[Default(true)]
		bool Enabled { get; set; }

		[Default("messages-session-salt")]
		string SessionSaltHeaderKey { get; set; }
	}
}