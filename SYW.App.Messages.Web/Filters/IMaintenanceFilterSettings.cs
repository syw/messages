using CommonGround.Settings;

namespace SYW.App.Messages.Web.Filters
{
	public interface IMaintenanceFilterSettings
	{
		[Default(false)]
		bool IsOn { get; set; }
	}
}