using System;
using System.Web.Mvc;
using PlatformClient.Platform;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Services.Platform;

namespace SYW.App.Messages.Web.Filters
{
	public interface IMaintenanceFilter : IAuthorizationFilter {}

	public class MaintenanceFilter : IMaintenanceFilter
	{
		private readonly IMaintenanceFilterSettings _maintenanceFilterSettings;

		public MaintenanceFilter(IMaintenanceFilterSettings maintenanceFilterSettings)
		{
			_maintenanceFilterSettings = maintenanceFilterSettings;
		}

		public void OnAuthorization(AuthorizationContext filterContext)
		{
			if (_maintenanceFilterSettings.IsOn)
			{
				filterContext.Result = new ViewResult {ViewName = "Maintenance"};
			}
		}
	}
}