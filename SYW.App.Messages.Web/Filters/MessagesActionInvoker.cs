﻿using System.Web.Mvc;
using CommonGround.MvcInvocation;

namespace SYW.App.Messages.Web.Filters
{
	public class MessagesActionInvoker : ActionInvoker
	{
		private readonly ITokenExtractorFilter _tokenExtractorFilter;
		private readonly IAutoLoginFilter _autoLoginFilter;
		private readonly IOfflineTokenProviderFilter _offlineTokenProviderFilter;
		private readonly IProfilerFilter _profilerFilter;
		private readonly IExceptionFilter _exceptionFilter;
		private readonly IMaintenanceFilter _maintenanceFilter;
		private readonly ICmsFilter _cmsFilter;
		private readonly IForgeryAjaxRequestBlockFilter _forgeryAjaxRequestBlockFilter;

		public MessagesActionInvoker(
			ITokenExtractorFilter tokenExtractorFilter,
			IAutoLoginFilter autoLoginFilter,
			IOfflineTokenProviderFilter offlineTokenProviderFilter,
			IProfilerFilter profilerFilter,
			IExceptionFilter exceptionFilter,
			IMaintenanceFilter maintenanceFilter,
			ICmsFilter cmsFilter,
			IForgeryAjaxRequestBlockFilter forgeryAjaxRequestBlockFilter)
		{
			_tokenExtractorFilter = tokenExtractorFilter;
			_autoLoginFilter = autoLoginFilter;
			_offlineTokenProviderFilter = offlineTokenProviderFilter;
			_profilerFilter = profilerFilter;
			_exceptionFilter = exceptionFilter;
			_maintenanceFilter = maintenanceFilter;
			_cmsFilter = cmsFilter;
			_forgeryAjaxRequestBlockFilter = forgeryAjaxRequestBlockFilter;
		}

		protected override FilterInfo GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
		{
			var filterInfo = base.GetFilters(controllerContext, actionDescriptor);

			filterInfo.AuthorizationFilters.Add(_cmsFilter);
			filterInfo.AuthorizationFilters.Add(_maintenanceFilter);
			filterInfo.AuthorizationFilters.Add(_tokenExtractorFilter);
			filterInfo.AuthorizationFilters.Add(_autoLoginFilter);
			filterInfo.AuthorizationFilters.Add(_offlineTokenProviderFilter);
			filterInfo.AuthorizationFilters.Add(_forgeryAjaxRequestBlockFilter);
			filterInfo.AuthorizationFilters.Add(_profilerFilter);

			filterInfo.ExceptionFilters.Add(_exceptionFilter);

			return filterInfo;
		}
	}
}