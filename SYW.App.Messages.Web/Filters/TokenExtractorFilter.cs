﻿using System;
using System.Web.Mvc;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain.Services.Platform;
using log4net;

namespace SYW.App.Messages.Web.Filters
{
	public interface ITokenExtractorFilter : IAuthorizationFilter {}

	public class TokenExtractorFilter : ITokenExtractorFilter
	{
		private readonly IPlatformTokenProvider _platformTokenProvider;
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly ILog _logger;

		public TokenExtractorFilter(IPlatformTokenProvider platformTokenProvider, IEntityContextProvider entityContextProvider, ILog logger)
		{
			_platformTokenProvider = platformTokenProvider;
			_entityContextProvider = entityContextProvider;
			_logger = logger;
		}

		public void OnAuthorization(AuthorizationContext filterContext)
		{
			var ignoreAttributes = filterContext.ActionDescriptor.GetCustomAttributes(typeof (IgnoreTokenExtractor), false);
			if (ignoreAttributes.Length > 0)
				return;

			var token = filterContext.HttpContext.Request.QueryString["token"];
			var currentUrl = filterContext.HttpContext.Request.Url;
			var referrer = filterContext.HttpContext.Request.UrlReferrer;

			_logger.Debug(string.Format("Token Acquired from context : {0}, url: /'{1}/', referrer: /'{2}/'", token, currentUrl, referrer));

			// This mean that we arrived from a page via the platform
			if (!string.IsNullOrEmpty(token))
			{
				// Set the token for platform request
				_platformTokenProvider.SetToken(token);

				// If the cookie authentication cookie exist we need to clear it in order to reset it
				_entityContextProvider.Clear();
			}
				// This mean that we are going through internal pages and we need to get the info from the cookie
			else
			{
				// TODO : Need to think how to hanlde cases where the cookie expired
				var entityContext = _entityContextProvider.CurrentEntity();

				token = entityContext.Token;
				_logger.Debug(string.Format("Token Acquired from cookie: {0}, url: /'{1}/', referrer: /'{2}/'", token, currentUrl, referrer));

				// Set the token for platform request
				_platformTokenProvider.SetToken(token);
			}
		}
	}

	public class InvalidTokenException : Exception
	{
		public InvalidTokenException(string myMessage)
			: base(myMessage) {}
	}

	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class IgnoreTokenExtractor : ActionFilterAttribute {}
}