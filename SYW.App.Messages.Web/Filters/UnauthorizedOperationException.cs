using System;

namespace SYW.App.Messages.Web.Filters
{
	public class UnauthorizedOperationException : Exception {}
}