﻿using System.Web;
using CommonGround;
using SYW.App.Messages.Web.Config;
using SYW.App.Messages.Web.Services;
using StackExchange.Profiling;

namespace SYW.App.Messages.Web
{
	public class MvcApplication : HttpApplication
	{
		private ICommonContainer _container;

		protected void Application_Start()
		{
			Initialize();

			DateTimeUIExt.TimeOffsetResolver = _container.Resolve<IUserTimeCookiedOffsetResolver>();
			UrlHelpExtensions.VersionService = _container.Resolve<IVersionService>();
		}

		private void Initialize()
		{
			_container = new CommonContainer();

			ContainerAccesor.Init(_container);

			var bootstrapper = new Bootstrapper(_container);
			bootstrapper.RegisterEverything();
		}

		protected void Application_EndRequest()
		{
			MiniProfiler.Stop();
		}
	}
}