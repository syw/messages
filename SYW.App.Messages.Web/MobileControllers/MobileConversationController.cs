﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messages.Web.Controllers;
using SYW.App.Messages.Web.Filters;
using SYW.App.Messages.Web.Services;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messages.Web.MobileControllers
{
	public class MobileConversationController : MessagesControllerBase
	{
		private readonly IStartNewConversationService _startNewConversationService;
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IConversationService _conversationService;
		private readonly IConversationParticipantValidator _conversationParticipantValidator;
		private readonly IApplicationSettings _applicationSettings;

		public MobileConversationController(IStartNewConversationService startNewConversationService,
		                                    IEntityContextProvider entityContextProvider,
		                                    IConversationService conversationService,
		                                    IConversationParticipantValidator conversationParticipantValidator,
		                                    IApplicationSettings applicationSettings)
		{
			_startNewConversationService = startNewConversationService;
			_entityContextProvider = entityContextProvider;
			_conversationService = conversationService;
			_conversationParticipantValidator = conversationParticipantValidator;
			_applicationSettings = applicationSettings;
		}

		[PatternRoute("/mailbox/mobile/conversation")]
		public ActionResult ConversationPage(string p)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();
			if (currentEntity == null) throw new UnauthorizedOperationException();

			ViewBag.OpponentId = p;

			return View(@"~/Views/Mobile/RegularConversationContainer.cshtml");
		}

		[PatternRoute("/-/mobile/start-new-conversation")]
		public ActionResult StartNewConversation(long[] participants)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();

			if (currentEntity == null)
				throw new Exception("User Not Found!");

			var conversationDescriptor = _startNewConversationService.Start(currentEntity.OriginalId, participants);

			if (conversationDescriptor != null)
			{
				var conversation = _conversationService.Get(conversationDescriptor.Id.ToString(), currentEntity.Id);
				if (conversation != null)
				{
					conversation.Participants = conversation.Participants.Where(p => p.OriginalId != currentEntity.OriginalId).ToList();

					_conversationParticipantValidator.Validate(currentEntity.Id, conversation.Id);
					return RegularConversationPage(conversation, currentEntity);
				}
			}
			return null;
		}

		private JsonResult RegularConversationPage(Conversation conversation, EntityContext currentEntity)
		{
			conversation.Messages = conversation.Messages.Take(10).Reverse().ToList();

			var regularConversationJsonModel = new RegularConversationJsonModel
				                                   {
					                                   ConversationId = conversation.Id.ToString(),
					                                   CurrentEntityId = currentEntity.Id.ToString(),
					                                   CurrentEntityImage = currentEntity.ImageUrl ?? _applicationSettings.MissingUserImage,
					                                   CurrentEntityName = currentEntity.Name,
					                                   Messages = new List<MessageJsonModel>()
				                                   };

			regularConversationJsonModel.Messages = GetMessagesJsonModel(conversation.Messages);

			return Json(regularConversationJsonModel, JsonRequestBehavior.AllowGet);
		}

		[PatternRoute("/-/mobile/messages/more")]
		public ActionResult MoreMessages(string lastMessageId, string conversationId)
		{
			var currentEntity = _entityContextProvider.CurrentEntity();
			var conversation = _conversationService.Get(conversationId, currentEntity.Id);

			_conversationParticipantValidator.Validate(currentEntity.Id, ObjectId.Parse(conversationId));

			var messageId = ObjectId.Parse(lastMessageId);
			var lastMessage = conversation.Messages.First(m => m.Id == messageId);

			conversation.Messages = conversation.Messages.Where(m => m.Date < lastMessage.Date).ToList();

			conversation.Messages = conversation.Messages.Take(5).Reverse().ToList();

			return conversation.Messages.IsNullOrEmpty() ? null : Json(GetMessagesJsonModel(conversation.Messages), JsonRequestBehavior.AllowGet);
		}

		private List<MessageJsonModel> GetMessagesJsonModel(IList<Message> messages)
		{
			List<MessageJsonModel> messageJsonModels = new List<MessageJsonModel>();
			foreach (var message in messages)
			{
				messageJsonModels.Add(new MessageJsonModel
					                      {
						                      AuthorId = message.Author._id.ToString(),
						                      AuthorImage = message.Author.ImageUrl ?? _applicationSettings.MissingUserImage,
						                      AuthorName = message.Author.Name,
						                      MessageId = message.Id.ToString(),
						                      Content = message.Content,
						                      DateTime = message.Date
					                      });
			}
			return messageJsonModels;
		}
	}
}