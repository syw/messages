﻿using System.Collections.Generic;
using System.Web.Mvc;
using CommonGround.MvcInvocation;
using MongoDB.Bson;
using SYW.App.Messages.Web.Services;
using SYW.App.Messages.Web.SingalR;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.MobileControllers
{
	public class MobileMessagesController : CommonGround.MvcInvocation.ControllerBase
	{
		private readonly IStartNewConversationService _startNewConversationService;
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IMessagesService _messagesService;
		private readonly IEntitiesResolver _entitiesResolver;
		private readonly IConversationNotifierService _conversationNotifierService;
		private readonly IConversationParticipantValidator _conversationParticipantValidator;

		public MobileMessagesController(IStartNewConversationService startNewConversationService,
		                                IEntityContextProvider entityContextProvider,
		                                IEntitiesResolver entitiesResolver,
		                                IConversationNotifierService conversationNotifierService,
		                                IMessagesService messagesService,
		                                IConversationParticipantValidator conversationParticipantValidator)
		{
			_startNewConversationService = startNewConversationService;
			_entityContextProvider = entityContextProvider;
			_entitiesResolver = entitiesResolver;
			_conversationNotifierService = conversationNotifierService;
			_messagesService = messagesService;
			_conversationParticipantValidator = conversationParticipantValidator;
		}

		[PatternRoute("/-/mobile/messages/post-new-message")]
		public JsonResult PostNewMessage(long[] participants, string conversationId, string content)
		{
			var entityContext = _entityContextProvider.CurrentEntity();
			ObjectId conversationObjectId;
			Message message;
			ConversationDescriptor conversationDescriptor = null;

			if (!string.IsNullOrEmpty(conversationId))
			{
				conversationObjectId = ObjectId.Parse(conversationId);
				message = _messagesService.Create(conversationObjectId, entityContext.Id, content);
			}
			else
			{
				conversationDescriptor = _startNewConversationService.Start(entityContext.OriginalId, participants, content);
				conversationObjectId = conversationDescriptor.Id;
				message = conversationDescriptor.LastMessage;
			}

			_conversationParticipantValidator.Validate(entityContext.Id, conversationObjectId);

			_conversationNotifierService.Notify(conversationId, entityContext.Id);

			_entitiesResolver.Resolve(new[] {message.Author});

			if (conversationDescriptor != null)
			{
				var regularConversationJsonModel = new RegularConversationJsonModel
					                                   {
						                                   ConversationId = conversationDescriptor.Id.ToString(),
						                                   CurrentEntityId = entityContext.Id.ToString(),
						                                   CurrentEntityImage = entityContext.ImageUrl,
						                                   CurrentEntityName = entityContext.Name,
						                                   Messages = new List<MessageJsonModel>
							                                              {
								                                              new MessageJsonModel
									                                              {
										                                              AuthorId = message.Author._id.ToString(),
										                                              AuthorImage = message.Author.ImageUrl,
										                                              AuthorName = message.Author.Name,
										                                              MessageId = message.Id.ToString(),
										                                              Content = message.Content,
										                                              DateTime = message.Date
									                                              }
							                                              }
					                                   };

				return Json(regularConversationJsonModel, JsonRequestBehavior.AllowGet);
			}

			return null;
		}
	}
}