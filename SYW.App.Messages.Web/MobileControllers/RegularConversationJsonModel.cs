﻿using System;
using System.Collections.Generic;

namespace SYW.App.Messages.Web.MobileControllers
{
	public class RegularConversationJsonModel
	{
		public string ConversationId { get; set; }
		public string CurrentEntityId { get; set; }
		public string CurrentEntityName { get; set; }
		public string CurrentEntityImage { get; set; }
		public List<MessageJsonModel> Messages { get; set; }
	}

	public class MessageJsonModel
	{
		public string MessageId { get; set; }
		public string AuthorId { get; set; }
		public string AuthorName { get; set; }
		public string AuthorImage { get; set; }
		public DateTime DateTime { get; set; }
		public string Content { get; set; }
	}
}