﻿using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using SYW.App.Messages.Web.Services.Cms;

namespace SYW.App.Messages.Web.Models.Cms
{
	public class BauEmailFeedObjectViewModel
	{
		public SelectList Publisher;
		public string PublisherSelectedId { get; set; }
		public string Title { get; set; }
		public string Body { get; set; }
		public string TargetControllerName { get; set; }
		public string TargetActionName { get; set; }
		public string PostResult { get; set; }
		public Color PostResultColor { get; set; }
		public string MessageId { get; set; }
		public IList<ActionType> ActionTypes { get; set; }

		public BauEmailFeedObjectViewModel()
		{
			ActionTypes = new List<ActionType>();
		}
	}
}