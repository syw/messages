﻿using MongoDB.Bson;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.Models
{
	public class MessageModel
	{
		public Message Message { get; set; }
		public ObjectId ConversationId { get; set; }
	}
}