﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messages.Web.Models
{
	public class MessagePreviewModel
	{
		public ObjectId CoversationId { get; set; }
		public ObjectId MessageId { get; set; }
		public string AuthorName { get; set; }
		public IList<Entity> Participants { get; set; }
		public DateTime CreatedTime { get; set; }
		public string Subject { get; set; }
		public string ImageUrl { get; set; }
		public ConversationStatus Status { get; set; }
		public bool IsSystemMessage { get; set; }
		public int Index { get; set; }
	}
}