﻿namespace SYW.App.Messages.Web.Models
{
	public class SystemMessageModel
	{
		public string ConversationId { get; set; }
		public string MessageId { get; set; }
		public int MessageWidth { get; set; }
		public int MessageHeight { get; set; }
		public int MessageIndex { get; set; }
		public int TotalMessagesCount { get; set; }
		public int TotalUnreadCount { get; set; }
		public string BauName { get; set; }
		public string DateTime { get; set; }
		public string AuthorName { get; set; }
		public string AuthorImageUrl { get; set; }
		public string MessageContent { get; set; } 
	}
}