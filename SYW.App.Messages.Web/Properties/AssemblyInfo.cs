﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("HelloApps")]
[assembly: AssemblyProduct("HelloApps")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("b434e5ca-8499-46ba-81b0-23a63139a972")]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]

// Version info is in SolutionAssemblyInfo.cs