/// <reference path="~/static/js/analytics/omniture-data.js"/>
/// <reference path="~/static/js/analytics/analytics.js" />

var _anlHandlers = {
	/*
	* Possible data values:
	* name - MUST, click action name
	* productId - a product id if there is one related
	* feature - a name of a feature if there is one related (for revenue calculation)
	* delay - should a 500ms delay occur?
	* customDelay - delay in ms in case a custom one is needed
	*/
	GeneralActionReport: function() {
		return {
			event: 'general-action-report',
			handler: function(event, data) {
				var od = new omnitureData();
				var value = data.name;
				od.props.ClickActions.val(value);

				if (data.productId != undefined) {
					od.vars.Products.val(";" + data.productId);
					od.props.ProductId.val(data.productId);
				}

				if (data.feature != undefined) {
					od.props.MainFeatures.val(data.feature);
				}

				analytics.reportAction(od, value, data.delay);

				if (data.redirectionUrl != undefined) {
					setTimeout(function() { window.location.href = data.redirectionUrl; }, data.customDelay != undefined ? data.customDelay : 500);
				}
			}
		};
	}(),

	/*
	* Possible values:
	* page - page name - MUST
	* section - page section
	*/
	ReportPage: function() {
		return {
			event: 'report-page',
			handler: function(event, data) {
				var od = new omnitureData();
				od.vars.PageName.val(data.page);

				if (data.section) {
					od.vars.Channel.val(data.section);
				}
				analytics.reportPageView(od);
			}
		};
	}(),

	/*
	* Possible values:
	* page - prefix page name
	* message - a single message
	* list - a list of objects with a message field for each
	*/
	ErrorReport: function() {
		return {
			event: 'error-report',
			handler: function(event, data) {
				var od = new omnitureData();
				var value = "";
				if (data.page) {
					value = data.page + " Page > ";
				}
				if (data.message) {
					value += data.message;
				} else if (data.list) {
					for (error in data.list) {
						value += data.list[error].message
					}
					;
				}
				od.props.ErrorTracking.val(value);
				analytics.reportAction(od, "Error > " + value);
			}
		};
	}()
};

$(function() {
	$.each(_anlHandlers, function(i, handler) {
		$(document).bind("analytics-" + handler.event, { handler: handler.handler }, function(event, data) {
			try {
				event.data.handler(event, data);
			} catch(e) {
				console.error("error in analytics report : " + e.toString());
			}
		});
	});
});

//Analytics report implementation
var analyticsReports = new (function() {

	this.report = function(eventType, data) {
		$(document).trigger("analytics-" + eventType, data);
	};

})();