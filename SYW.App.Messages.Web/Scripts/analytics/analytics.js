/// <reference path="omniture-data.js"/>

var analytics = new (function() {

	function init() {
		if (analyticsModel.enabled) {
			return s_gi(analyticsModel.reportSuiteId);
		}
		return null;
	}

	function addAdditionalTrackVars(s) {
		s.linkTrackVars += ',channel,prop1,prop2,prop3,prop10,prop19,prop27,prop28,prop71,prop72';
	}

	// fills Omniture's javascript parameter with OmnitureData.

	function populateWithOmnitureData(s, od) {

		s.linkTrackVars = "None";
		s.linkTrackEvents = "None";

		var firstTrackVar = true;
		$.each(od.vars, function(i, v) {
			if (v.val()) {
				if (firstTrackVar) {
					firstTrackVar = false;
					s.linkTrackVars = "";
				}
				s.linkTrackVars += v.name + ",";
				s[v.name] = v.val();
			}
		});

		$.each(od.props, function(i, p) {
			if (p.val()) {
				if (firstTrackVar) {
					firstTrackVar = false;
					s.linkTrackVars = "";
				}
				var propName = "prop" + p.index;
				s.linkTrackVars += propName + ",";
				s[propName] = p.val();
			}
		});

		$.each(od.eVars, function(i, e) {
			if (e.val()) {
				if (firstTrackVar) {
					firstTrackVar = false;
					s.linkTrackVars = "";
				}
				var evarName = "eVar" + e.index;
				s.linkTrackVars += evarName + ",";
				s[evarName] = e.val();
			}
		});

		var firstEvent = true;
		$.each(od.events, function(eventName, e) {
			if (e) {
				if (firstEvent) {
					if (firstTrackVar) {
						firstTrackVar = false;
						s.linkTrackVars = "";
					}
					firstEvent = false;
					s.linkTrackVars += "events";
					s.linkTrackEvents = "";
					s.events = "";
				}
				s.linkTrackEvents += eventName + ",";
				s.events += eventName + ",";
			}
		});

		addAdditionalTrackVars(s);

		s.linkTrackVars = trimSeparator(s.linkTrackVars, ",");
		s.linkTrackEvents = trimSeparator(s.linkTrackEvents, ",");
		s.events = trimSeparator(s.events, ",");
	}

	function trimSeparator(str, separator) {
		if (str && str.length > 0 && str.substr(str.length - 1, 1) == separator)
			str = str.substr(0, str.length - 1);
		return str;
	}

	function resetTrackingParams() {
		s.events = "";
		s.linkTrackVars = "";
		for (var prop in s)
			if (prop.indexOf('prop') == 0 || prop.indexOf('eVar') == 0)
				delete s[prop];
	}

	var clearRedundantData = function(s) {
		s.events = "";
	};

	this.reportPageView = function(od) {

		try {
			var s = init();
			if (!s)
				return;

			clearRedundantData(s);

			populateWithOmnitureData(s, od);

			s.t();

			resetTrackingParams();
		} catch(e) {
			console.log("error in analytics action report : " + e);
		}
	};

	this.reportAction = function(od, linkName, delay) {
		try {
			var disableDelay = !delay;

			if (!linkName)
				linkName = '';

			var s = init();
			if (!s)
				return;

			clearRedundantData(s);
			populateWithOmnitureData(s, od);

			//trackLink
			s.tl(disableDelay, 'o', linkName);

			//resetTrackingParams();
			resetTrackingParams();
		} catch(e) {
			console.log("error in analytics action report : " + e);
		}
	};
})();

function omnitureData(section, pageName) {
	this.vars = new omnitureVars();
	this.props = new omnitureProps();
	this.eVars = new omnitureEVars();
	this.events = {};

	if (section)
		this.vars.Section.val(section);

	if (pageName)
		this.vars.PageName.val(pageName);
}

omnitureData.prototype = {
	events: {},
	addEvent: function(event) { this.events[event] = true; }
};

function omnitureProp(i) {
	this.index = i;
	this.v = undefined;
	this.val = function(value) {
		if (value == undefined)
			return this.v;
		else {
			this.v = value.toString();
		}
	}
}

function omnitureEVar(i, isCounter) {
	this.index = i;
	this.isCounter = isCounter;
	this.v = undefined;
	this.val = function(value) {
		if (value == undefined)
			return this.v;
		else {
			this.v = value.toString();
		}
	}
}

function omnitureVar(name) {
	this.name = name;
	this.v = undefined;
	this.val = function(value) {
		if (value == undefined)
			return this.v;
		else {
			this.v = value.toString();
		}
	}
}