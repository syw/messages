omnitureProps = function() {
	this["Category"] = new omnitureProp(1);
	this["SubCategory"] = new omnitureProp(2);
	this["BrandVertical"] = new omnitureProp(3);
	this["VisitorId"] = new omnitureProp(9);
	this["ProductId"] = new omnitureProp(10);
	this["InternalSearchKeyword"] = new omnitureProp(11);
	this["ClickActions"] = new omnitureProp(12);
	this["ExternalCampaigns"] = new omnitureProp(13);
	this["AuthenticationType"] = new omnitureProp(24);
	this["PageType"] = new omnitureProp(27);
	this["PageTypeByVertical"] = new omnitureProp(28);
	this["FacebookConnectImpressions"] = new omnitureProp(36);
	this["OmnitureVisitorId"] = new omnitureProp(60);
	this["UserType"] = new omnitureProp(61);
	this["ErrorTracking"] = new omnitureProp(68);
	this["ProfileStatus"] = new omnitureProp(71);
	this["MainFeatures"] = new omnitureProp(72);
	this["ReferingDomainPathing"] = new omnitureProp(73);
}
omnitureEVars = function() {
	this["VisitorId"] = new omnitureEVar(1, false);
	this["TrackingCode"] = new omnitureEVar(2, false);
	this["InternalSearchKeyword"] = new omnitureEVar(4, false);
	this["InternalCampaigns"] = new omnitureEVar(5, false);
	this["ExternalCampaigns"] = new omnitureEVar(7, false);
	this["ZipCode"] = new omnitureEVar(17, false);
	this["ProfileStatus"] = new omnitureEVar(30, false);
	this["UserType"] = new omnitureEVar(45, false);
	this["FullRegistrationEntryPoint"] = new omnitureEVar(46, false);
	this["FullRegistrationWhiteLabelStatus"] = new omnitureEVar(64, false);
	this["AuthenticationType"] = new omnitureEVar(65, false);
	this["MemberEngagement"] = new omnitureEVar(72, false);
	this["MainFeatures"] = new omnitureEVar(73, false);
}
omnitureVars = function() {
	this["PageName"] = new omnitureVar('pageName');
	this["Channel"] = new omnitureVar('channel');
	this["Products"] = new omnitureVar('products');
	this["ExternalCampaigns"] = new omnitureVar('campaign');
}
var omnitureEvent = {
	"ProductViews": "event10",
	"ProductReviewWritten": "event17",
	"ProductQuickViews": "event27",
	"SYWRNewMember": "event46",
	"FullRegistrationAccountDetails": "event50",
	"FullRegistrationSYWRInternalLandingPage": "event51",
	"FullRegistrationAuthenticationRegularSSO": "event52",
	"FullRegistrationAuthenticationFacebook": "event53",
	"FullRegistrationAuthenticationOpenID": "event54",
	"SYWCoreRegistration": "event55",
	"SYWRActivation": "event56",
	"Shares": "event61",
	"FullRegistrationStoreAccountPage": "event85",
	"FullRegistrationPersonalDetailsPage": "event86",
	"FullRegistrationPreferencesPage": "event87",
	"SYWLogin": "event93",
	"CatalogsCreated": "event96",
	"FullRegistrationStartPage": "event97",
	"FullRegistrationSelectionOfIdentification": "event98",
	"FullRegistrationAuthenticationNoAccount": "event100",
	"CartOpens": "scOpen",
	"CartAdds": "scAdd",
	"ProductView": "prodView"
}
var omnitureSection = {
	"Catalogs": "Catalogs",
	"Profile": "Profile",
	"Search": "Search",
	"Home": "Home",
	"Tools": "Tools",
	"Help": "Help",
	"Products": "Products",
	"Static": "Static",
	"Registration": "Registration",
	"Error": "Error",
	"Homepage": "Homepage",
	"Landing": "Landing",
	"Discover": "Discover",
	"Cart": "Cart",
	"Checkout": "Checkout",
	"Clique": "Clique",
	"SWYR": "S W Y R"
}