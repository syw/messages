﻿$(function () {
	if ($("#saving-banner").length > 0) {

		$("#saving-banner").on('click', function reportClick() {
			analyticsReports.report("general-action-report", { name: "User Click > Saving Banner" });
		});
		analyticsReports.report("general-action-report", { name: "Impression > Saving Banner" });
	}
});
