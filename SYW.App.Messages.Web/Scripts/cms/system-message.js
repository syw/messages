﻿$(function () {
	$("#preview-button").on("click", function () {
		var w = window.open();
		$(w.document.body).html($("#message-body").val());
	});

	$("#publish-message").on("click", function () {
		$("#post-result").empty();
		if (!$("#title").val().length) {
			alert("Please enter a title!");
			return false;
		}
		if (!$("#message-body").val().length) {
			alert("Please enter a body content!");
			return false;
		}

		var result = confirm("Are you sure you want to upload the system message?");
		if (result == false)
			return false;

		$("#message-body").val(encodeURIComponent($("#message-body").val()));
	});

	$("#delete-system-message").on("click", function () {
		var result = confirm("Are you sure you want to delete the system message?");
		if (result == false)
			return false;

		var messageId = $("#message-id").val();

		$.ajax({
			dataType: "json",
			url: '/__/cms/system-message/delete',
			data: { messageId: messageId },
			success: function () {
				alert('system message was deleted successfully');
				location.href = "/__/cms/system-message-view";
			}
		});
	});

	if ($('#publish-message').length) {
		$("#create-new-message").hide();
	}
});

function ResetFields() {
	$("#message-body").val(String.empty);
	$("#title").val(String.empty);
}

function SuccessPost(data) {
	var currentStyle = $("#post-result").attr("style");
	$("#post-result").attr("style", currentStyle + ";background-color:" + data.PostResultColor.Name);
	$("#post-result").html(data.PostResult);

	if (data.PostResultColor.Name == 'Green')
		ResetFields();
}

function BeginPost() {
	$("#publish-message").attr("disabled", true);
}

function FinishedPost() {
	$("#message-body").val(decodeURIComponent($("#message-body").val()));
	$("#publish-message").removeAttr("disabled");
};