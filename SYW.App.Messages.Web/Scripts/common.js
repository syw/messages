﻿$(function() {
	$.ajaxSetup({
		error: function(xhr) {
			// TODO : Decide how to handle the error
			// alert("error");
		}
	});

	$.browser.safari = function() {
		return (($.browser.webkit && !(/chrome/.test(navigator.userAgent.toLowerCase()))))
	};

	$('.tab.messages-header').on('click touchstart', function() {
		$('.conversation').removeClass('selected');
		$('#messages-frame').attr('src', '/mailbox/all');
	});

});

var ImageFixer = new function() {
	return {
		Fix: function(images) {
			if (images == null)
				images = $("img.entity-image");

			$.each(images, function() {
				var image = $(this);

				image.unbind("load").bind("load", function() {
					image.fadeIn('fast');
				});

				image.unbind("error").bind("error", function() {
					image.attr("src", "http://images.shopyourway.com/static/img/fusion/missing-images/user.png");
				});

				if (image.data("src") == "")
					image.attr("src", "http://images.shopyourway.com/static/img/fusion/missing-images/user.png");
				else
					image.attr("src", image.data("src"));
			});
		},
		FixMobile: function(userImageDiv) {
			if (!userImageDiv) {
				$(".msg-userfirend-img").each(function(i, element) {
					fixBackgroundImage(element);
				});
			} else {
				fixBackgroundImage(userImageDiv);
			}

			function fixBackgroundImage(element) {
				var imageUrl = $(element).css("background-image").match(/url\(["']?([^()]*)["']?\)/).pop();
				var image = new Image();
				$(image).error(function() {
					$(element).css("background-image", "url('http://images.shopyourway.com/static/img/fusion/missing-images/user.png')");
				});
				$(image).attr("src", imageUrl);
			}
		}
	};
}();