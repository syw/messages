﻿// This files handle the conversation click on the left navigation area
// Possible clicks are:
// - Clicks on a conversation that should present the conversation or the aggregated conversation
// - Archive the conversation
// ===============================================================================================

$(function() {
	$(".conversation").live('click touchstart', function(e) {
		var self = $(this);

		if (self.hasClass("new-message-box"))
			return false;

		if (self.hasClass('selected')) // Message was already selected, the user clicked on it again.
		{
			if (self.data('is-system-message') == true)
				$.pm({
					target: window.frames["messages-frame"],
					type: 'viewAllSystemMessages'
				});
			return false;
		}

		$('.conversation.selected').removeClass('selected');

		if (self.data('is-system-message') == false) {
			self.removeClass('unread');
			self.removeClass('new');
			self.addClass('read');
		}

		self.addClass('selected');

		$('.conversations-non-selected').hide();
		$('.messages-frame').show();

		$.pm({
			target: window.frames["messages-frame"],
			type: 'submitOpenConversationVirtualForm',
			data: self.data("conversation-id")
		});
	});

	$.pm.bind("checkConversationSelected", function(conversationId) {
		var currentConversation = $('.conversation[data-conversation-id=' + conversationId + ']')[0];
		if (!currentConversation || $(currentConversation).hasClass('selected'))
			return;
		$('.conversation').removeClass('selected');
		if ($('.conversation[data-conversation-id=' + conversationId + ']').data('is-system-message') == false)
			$(currentConversation).removeClass('new');
		$(currentConversation).addClass('selected');
	});

	if ($('.system-message').length > 0)
		return;

	$("body").keydown(function(e) {
		if ($(".conversation-container").is(":visible")) {
			if (e.keyCode == 46) { // delete button
				$('.archive').click();
			}
		}
	});

	$('.archive').click(function() {
		var currentConversationId = $(this).parent('.conversation-container').data('conversation-id');

		analyticsReports.report("general-action-report", { name: "Personal Conversation Header > Archive" });

		$.ajax({
			type: 'post',
			url: '/-/conversation/update-status',
			data: { conversationId: currentConversationId, status: 3 },
			success: function(data) {
				if (data == 'OK') {
					$.pm({
						target: window.parent,
						type: 'clearConversation',
						data: currentConversationId
					});
				}
			}
		});
	});
});