﻿var conversationsPagingViewModel = {
	isAjaxCallActive: false,
	reachedEnd: false,
	page: 1
};

$(function() {

	var getMoreConversations = function(callback) {

		conversationsPagingViewModel.isAjaxCallActive = true;
	    
		$.ajax({
			type: 'GET',
			url: '/-/mailbox/more',
			data: { page: conversationsPagingViewModel.page, pageSize: layout.pageSize },
			success: function(data) {
			    
				data = $.trim(data);

				var moreConversations = $(data);

				if (data == "" || data == null || moreConversations.find(".conversation").length == 0) {
					conversationsPagingViewModel.reachedEnd = true;
					slideEmptyMailboxIfNeeded();

					if (callback != null)
						callback();

					return false;
				}

				if (callback != null)
					callback();

				var container = $("<div />");

				container.append(moreConversations);
				container.hide();
				ImageFixer.Fix(container.find("img.entity-image"));

				var conversations = $(".conversations");

				conversations.append(container);

				container.slideDown();

				conversationsPagingViewModel.page++;
				conversationsPagingViewModel.isAjaxCallActive = false;

				layout.initConversationsScrollPane();
			},
			complete: function() {
				conversationsPagingViewModel.isAjaxCallActive = false;

				if (callback != null)
					callback();
			}
		});
	};

	function slideEmptyMailboxIfNeeded() {
		if (conversationsPagingViewModel.page == 1) {
			$('.empty-mailbox').slideDown();
			$('#more').hide();

			return false;
		}
	}

	function displayLoader() {
		$(".conversations").append("<div class='loader-container'><span class='loader'></span></div>");
	}

	function hideLoader() {
		$('.conversations .loader-container').remove();
	}

	if ($.browser.safari()) {
		$('#conversations-scroll').bind('scroll', function() {
			if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight)
				bindConversationsScroll();
		});
	} else {
		$('#conversations-scroll').bind(
			'jsp-scroll-y',
			function(event, scrollPositionY, isAtTop, isAtBottom) {
				if (isAtBottom)
					bindConversationsScroll();
			}
		);
	}

	function bindConversationsScroll() {
		if (conversationsPagingViewModel.isAjaxCallActive) return;
		if (conversationsPagingViewModel.reachedEnd) return;

		displayLoader();

		getMoreConversations(function() {
			hideLoader();
		});
	}

	// Get the first conversation without displaying any loader
	getMoreConversations(function() {
	});
});