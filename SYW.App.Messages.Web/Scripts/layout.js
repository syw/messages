﻿var layout = {
	pageSize: 50,
	regularMessageMarginBottom: 55,
	systemMessageMarginBottom: 30,
	isResizingMessages: false,
	isMessagesFirstInit: true,

	initMessagesScrollPane: function () {
		//The timeout is requeired because there are animations running and the JScrollPane needs them to end before it calculates if a scroll is needed
		setTimeout(function () {
			
			var container = $('#messages-scroll');

			// Temporarily make the container tiny so it doesn't influence the
			// calculation of the size of the document
			container.css({ 'width': 1, 'height': 1 });

			//Calculating available space for messages
			var systemMessageContainer = $('#systemMessageContainer');
			var systemMessageDiffs = 0;
			var regularMessageDiffs = 0;

			if (systemMessageContainer.length == 0) {
				regularMessageDiffs = $('.new-message-container').outerHeight(true) + $('.messages-loader').outerHeight(true) + layout.regularMessageMarginBottom;
			} else {
				systemMessageDiffs = $('.content.box').outerHeight(true) + $('.header').outerHeight() + layout.systemMessageMarginBottom;
			}

			var calculatedHeight = $(window).height() - systemMessageDiffs - regularMessageDiffs;

			if ($.browser.safari())
				$('.system-message').width($(window).width() - 50);

			container.css({ 'width': '100%', 'height': calculatedHeight });

			var spacerHeight = 0;

			//Creating a spacer that will push down the messages
			//We could'nt use position absolute with bottom 0 because of JScrollPane elements with absolute positions wrapping our scrollable area!!!		
			if (systemMessageContainer.length == 0) {
				$('#messages-spacer').height(0);
				var messagesHeight = $('.messages').outerHeight();
				spacerHeight = calculatedHeight - messagesHeight;
				if (spacerHeight > 0) {
					$('#messages-spacer').height(spacerHeight);
				}

				if ($.browser.safari())
					container.css("overflow", "scroll");
				else
					layout.initJScrollpane(container);
			}
			
			if (systemMessageContainer.length > 0) {

				var content = $('.content.box');

				if (content.width() < (systemMessageContainer.data('width'))) {
					
					systemMessageContainer.width((systemMessageContainer.data('width')));
					systemMessageContainer.css('zoom', content.width() / systemMessageContainer.data('width'));

					// FireFox zoom support
					var scale = content.width() / systemMessageContainer.data('width');
					if (scale != 0) {
						systemMessageContainer.css("-moz-transform", "scale(" + scale + ")");
						systemMessageContainer.css("-moz-transform-origin", "top left");
					}

				} else {
					systemMessageContainer.width(content.width());
					systemMessageContainer.css('zoom', "0");
					systemMessageContainer.css("-moz-transform", "scale(1)");
					systemMessageContainer.css("-moz-transform-origin", "top center");
				}

				$('.truncate').width($('.system-message').width() - 100);

				if (!($.browser.msie && $.browser.version <= 8)) {

					if ($.browser.safari()) 
						container.css("overflow", "scroll");
					else
						layout.initJScrollpane(container, { contentWidth: content.width() });


					var keepResizingSystemMessage = true;

					systemMessageContainer.hammer().on("pinchin", function () {
						keepResizingSystemMessage = true;
						if (keepResizingSystemMessage && (parseFloat(systemMessageContainer.css('zoom')) - 0.005) >= content.width() / systemMessageContainer.data('width')) {
							systemMessageContainer.css('zoom', parseFloat(systemMessageContainer.css('zoom')) - 0.005);
						}
					});

					systemMessageContainer.hammer().on("pinchout", function () {
						keepResizingSystemMessage = true;
						if (keepResizingSystemMessage) {
							systemMessageContainer.css('zoom', parseFloat(systemMessageContainer.css('zoom')) + 0.005);
						}
					});

					systemMessageContainer.hammer().on('doubletap', function () {
						systemMessageContainer.css('zoom', content.width() / systemMessageContainer.data('width'));
						container.jScrollPane({
							contentWidth: systemMessageContainer.width() * parseFloat(systemMessageContainer.css('zoom'))
						});
					});

					systemMessageContainer.hammer().on('transformend', function () {
						keepResizingSystemMessage = false;
						container.jScrollPane({
							contentWidth: systemMessageContainer.width() * parseFloat(systemMessageContainer.css('zoom'))
						});
					});
				} else {
					layout.initJScrollpane(container);
				}
			}

			//For some reason the scroller doesnt want to disapear when it should... 
			//we hide it when we know it is not needed
			var verticalBar = $('.jspVerticalBar');
			if (spacerHeight > 0 && verticalBar) {
				verticalBar.hide();
			}

			layout.fixVerticalBarPadding();
			layout.isMessagesFirstInit = false;

		}, (layout.isMessagesFirstInit ? 0 : 500));
	},

	initConversationsScrollPane: function () {
		if ($.browser.safari()) {
			$('#conversations-scroll').css("overflow", "scroll");

			return false;
		}

		setTimeout(function () {
			$('#conversations-scroll').height($(window).height() - layout.regularMessageMarginBottom);

			var container = $('#conversations-scroll');
			var api = container.data('jsp');

			if (api) {
				api.reinitialise();
				layout.fixVerticalBarPadding();
				return;
			}

			container.jScrollPane({ contentWidth: '0px' });
			layout.fixVerticalBarPadding();

			var throttleTimeout;

			$(window).bind(
				'resize',
				function () {
					container = $('#conversations-scroll');
					api = container.data('jsp');
					if ($.browser.msie) {
						// IE fires multiple resize events while you are dragging the browser window which
						// causes it to crash if you try to update the scrollpane on every one. So we need
						// to throttle it to fire a maximum of once every 50 milliseconds...
						if (!throttleTimeout) {
							throttleTimeout = setTimeout(
								function () {
									$('#conversations-scroll').height($(window).height() - layout.regularMessageMarginBottom);

									if (api) {
										api.reinitialise();
										layout.fixVerticalBarPadding();
									}
									throttleTimeout = null;
								},
								50
							);
						}
					} else {
						$('#conversations-scroll').height($(window).height() - layout.regularMessageMarginBottom);
						if (api) {
							api.reinitialise();
							layout.fixVerticalBarPadding();
						}
					}
				}
			);
		}, 500);
	},

	initMessagesPreviewScrollPane: function () {
		var container = $('#messages-preview-scroll');
		var systemMessageDiffs = $('.header').outerHeight() + layout.systemMessageMarginBottom;
		container.height($(window).height() - systemMessageDiffs);

		if ($.browser.safari()) {
			container.css("overflow", "scroll");

			return false;
		}

		layout.initJScrollpane(container);
		layout.fixVerticalBarPadding();
	},

	fixVerticalBarPadding: function () {
		var jspVerticalBar = $('.jspVerticalBar');
		var jspHorizontalBar = $('.jspHorizontalBar');
		if (!jspVerticalBar)
			return;

		// 10 is 6px in css + 4px to fill the blank space between the scroller and the content
		jspVerticalBar.width(10);

		if ($('.conversations').length > 0) {
			$('.conversations').width($('.conversations-left').width());
		}

		if (jspVerticalBar.is(":visible")) {
			$('.jspContainer').hover(
				function () {
					jspVerticalBar.fadeIn();
				},
				function () {
					jspVerticalBar.fadeOut();
				}
			);
		}

		if (jspHorizontalBar && jspHorizontalBar.is(":visible")) {
			$('.jspContainer').hover(
				function () {
					jspHorizontalBar.fadeIn();
				},
				function () {
					jspHorizontalBar.fadeOut();
				}
			);
		}
	},

	scrollToTop: function (scrollerId) {
		//The timeout is so other animations will finish and the content will be fully rendered
		//That way, the scrollToTop animation will start after other animations
		setTimeout(function () {
			var scroller = $('#' + scrollerId);
			var api = scroller.data('jsp');
			if (api) {
				api.scrollToPercentY(0, true);
			}
		}, 500);
	},

	scrollToBottom: function (scrollerId) {
		//The timeout is so other animations will finish and the content will be fully rendered
		//That way if a new content is added to the scroll area, the JScrollPane will know how to render the scroller correctly
		setTimeout(function () {
			var scroller = $('#' + scrollerId);
			var api = scroller.data('jsp');
			if (api) {
				api.scrollToPercentY(100, true);
			}

		}, 500);
	},

	initJScrollpane: function (container, options) {
		var api = container.data('jsp');
		if (!options)
			options = {};

		if (api)
			api.reinitialise(options);
		else
			container.jScrollPane(options);
	}
};

$(function () {
	var win = $(window);
	win.bind('resize', function () {
		if (!layout.isResizingMessages) {
			layout.isResizingMessages = true;
			layout.initMessagesScrollPane();
			layout.initMessagesPreviewScrollPane();
			layout.isResizingMessages = false;
		}
	});

	layout.initMessagesScrollPane();

	//Scrolling down only when we are viewing a regular message
	if ($('.messages').length > 0)
		layout.scrollToBottom('messages-scroll');
});