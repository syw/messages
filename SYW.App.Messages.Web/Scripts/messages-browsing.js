﻿$(function() {
	if (!systemMessagesViewModel.IsAllMessagesView()) {
		if ($($('.total-messages')[0]).text() == '1') {
			$('#page-right').removeClass('page-right').addClass('page-right-disabled');
		}

		if ($('#total-unread').text() == '0') {
			systemMessagesViewModel.NotifyAllMessagesRead();
		}
	}
});

var systemMessageBrowsingViewModel =
	{
		currentMessage: 1,

		SetBrowsingArrowsState: function(currentMessageIndex) {
			systemMessageBrowsingViewModel.currentMessage = currentMessageIndex;
			var nextSystemMessage = systemMessageBrowsingViewModel.GetNextSystemMessage();
			var previousSystemMessageElement = systemMessageBrowsingViewModel.GetPreviousSystemMessage();
			if (previousSystemMessageElement.length == 0) {
				$('#page-left').removeClass('page-left').addClass('page-left-disabled');

			} else if ($('#page-left').hasClass('page-left-disabled')) {
				$('#page-left').removeClass('page-left-disabled').addClass('page-left');
			}

			if (nextSystemMessage.length == 0) {
				$('#page-right').removeClass('page-right').addClass('page-right-disabled');

			} else if ($('#page-right').hasClass('page-right-disabled')) {
				$('#page-right').removeClass('page-right-disabled').addClass('page-right');
			}
		},

		GetNextSystemMessage: function() {
			var conversationId = $('.conversation-container').first().data('conversation-id');
			var messageId = $('#systemMessageContainer').data('message-id');
			var currentSystemMessageElement = $('.message-preview-container[data-conversation-id = "' + conversationId + '"][data-message-id = "' + messageId + '"]').first();
			return currentSystemMessageElement.nextAll('.message-preview-container[data-is-system-message = True]').first();
		},

		GetPreviousSystemMessage: function() {
			var conversationId = $('.conversation-container').first().data('conversation-id');
			var messageId = $('#systemMessageContainer').data('message-id');
			var currentSystemMessageElement = $('.message-preview-container[data-conversation-id = "' + conversationId + '"][data-message-id = "' + messageId + '"]');
			return currentSystemMessageElement.prevAll('.message-preview-container[data-is-system-message = True]').first();
		},

		BindSystemMessagePageEvents: function() {
			$("body").keydown(function(e) {
				if ($(".conversation-container").is(":visible")) {
					if (e.keyCode == 37) { // left
						$('#page-left').click();
					} else if (e.keyCode == 39) { // right
						$('#page-right').click();
					}
				}
			});

			$('#page-right').click(function() {
				if ($('#page-right').hasClass('page-right-disabled'))
					return;

				analyticsReports.report("general-action-report", { name: "System Message Header > Page right" });

				var nextSystemMessageElement = systemMessageBrowsingViewModel.GetNextSystemMessage();
				var nextSystemMessageConversationId = nextSystemMessageElement.data('conversation-id');
				var nextSystemMessageId = nextSystemMessageElement.data('message-id');
				$.ajax({
					type: 'GET',
					url: '/-/message/get-by-id',
					data: { conversationId: nextSystemMessageConversationId, messageId: nextSystemMessageId },
					success: function(data) {
						systemMessagesViewModel.SetCurrentMessage(data);
					}
				});
			});

			$('#page-left').click(function() {
				if ($('#page-left').hasClass('page-left-disabled'))
					return;

				analyticsReports.report("general-action-report", { name: "System Message Header > Page left" });

				var previousSystemMessageElement = systemMessageBrowsingViewModel.GetPreviousSystemMessage();
				var previousSystemMessageConversationId = previousSystemMessageElement.data('conversation-id');
				var previousSystemMessageId = previousSystemMessageElement.data('message-id');
				$.ajax({
					type: 'GET',
					url: '/-/message/get-by-id',
					data: { conversationId: previousSystemMessageConversationId, messageId: previousSystemMessageId },
					success: function(data) {
						systemMessagesViewModel.SetCurrentMessage(data);
					}
				});
			});
		}
	};