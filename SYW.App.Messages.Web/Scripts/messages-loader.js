﻿$(function() {
	var context = $('.conversation-container');
	if (context.length == 0)
		return;

	setImages();
	bindMessagesLoaderButton();

	function setImages() {
		var images = $('img.entity-image', context);
		if (images.length == 0)
			return;

		ImageFixer.Fix(images);
	}

	function bindMessagesLoaderButton() {
		var messagesLoaderButton = $('.messages-loader', context);
		if (messagesLoaderButton.length == 0)
			return;

		messagesLoaderButton.click(function() {
			if (messagesLoaderButton.hasClass("loading"))
				return false;

			messagesLoaderButton.addClass("loading");
			messagesLoaderButton.html("<div class='loader-container white'>Loading<span class='loader'></span></div>");

			$.ajax({
				type: 'GET',
				url: '/-/messages/more',
				data: {
					conversationId: $(".conversation-container").data("conversation-id"),
					lastMessageId: $($(".message-container")[0]).data("message-id")
				},
				success: function(response) {
					if (response == "OK") {
						messagesLoaderButton.remove();
					} else {
						var previousMessages = $(response);
						var container = $("<div />");

						container.append(previousMessages);
						container.hide();

						$(".messages").prepend(container);

						ImageFixer.Fix(container.find("img.entity-image"));

						container.slideDown();

						//Added a timeout because sometimes the load is very fast and it looks like a blink - very ugly....
						setTimeout(function() {
							messagesLoaderButton.html("Load previous messages");
							messagesLoaderButton.removeClass("loading");
						}, 500);

					}
				},
				complete: function() {
					layout.initMessagesScrollPane();
				}
			});
		});
	}
});