﻿(function(window, angular) {

	'use strict';

	angular.module('sywMessages.controllers')
		.controller('messagesCtrl', ['$scope', '$http', '$location', '$timeout', 'signalRSvc',
			function($scope, $http, $location, $timeout, signalr) {

				$scope.noMoreMessages = false;
				$scope.isRetrievingMessages = false;
				$scope.newMeesage = null;
				$scope.updatePanNewMessage = null;
				$scope.typing = null;

				function getMessagesFromServer() {
					var conversationData = {
						"participants": [window.opponentId]
					};

					$http({
						url: '/-/mobile/start-new-conversation',
						method: "GET",
						params: conversationData
					}).success(function(data) {
						if (!data) {
							return;
						}
						if (!$scope.messagesData) {
							$scope.messagesData = data;
							$scope.conversation = signalr.initialize($scope.messagesData.ConversationId, $scope.messagesData.CurrentEntityId, $scope.messagesData.CurrentEntityName, $scope.messagesData.CurrentEntityImage);
						}
						else {
							$scope.messagesData.Messages = data.Messages.concat($scope.messagesData.Messages);
						}
					});
				}

				function getMoreMessages() {
					$scope.isRetrievingMessages = true;

					var params = {};
					if ($scope.messagesData) {
						params = {
							lastMessageId: $scope.messagesData.Messages[0].MessageId,
							conversationId: $scope.messagesData.ConversationId
						};
					}
					$http({
						url: '/-/mobile/messages/more',
						method: "GET",
						params: params
					}).success(function(data) {
						if (!data) {
							$scope.noMoreMessages = true;
							$timeout(function() { $scope.isRetrievingMessages = false; }, 500);
							return;
						}
						$scope.messagesData.Messages = data.concat($scope.messagesData.Messages);
						$timeout(function() { $scope.isRetrievingMessages = false; }, 500);
					}).error(function(error) {
						$timeout(function() { $scope.isRetrievingMessages = false; }, 500);
					});
				}

				getMessagesFromServer();

				$scope.$on("getMoreMessages", function() {
					getMoreMessages();
				});

				//send new message to server
				$scope.sendNewMessage = function(newMeesage) {
					if (!newMeesage) return;

					$scope.newMeesageContent = newMeesage;

					if (!$scope.messagesData) {
						$scope.messagesData = {};
					}

					//new message data to send the server
					var msgDataServer = {
						"ConversationId": $scope.messagesData.ConversationId,
						"participants": Helpers.getParameterByName("p"),
						"Content": newMeesage
					};

					var msgDataLocal = {
						"AuthorId": $scope.messagesData.CurrentEntityId,
						"AuthorName": $scope.messagesData.CurrentEntityName,
						"AuthorImage": $scope.messagesData.CurrentEntityImage,
					};

					msgDataLocal = angular.extend(msgDataServer, msgDataLocal);

					//update new message to html before retrive new message from server
					if ($scope.messagesData.Messages) {
						$scope.messagesData.Messages = $scope.messagesData.Messages.concat([msgDataLocal]);
					} else {
						$scope.messagesData.Messages = [msgDataLocal];
					}

					$http({
						url: "/-/mobile/messages/post-new-message",
						method: 'POST',
						data: msgDataServer
					}).success(function(data) {
						if (typeof ($scope.conversation) == 'undefined') {
							$scope.messagesData = data;
							$scope.conversation = signalr.initialize(data.ConversationId, data.CurrentEntityId, data.CurrentEntityName, data.CurrentEntityImage);
						}
						else {
							$scope.conversation.sendMessage($scope.messagesData.ConversationId, msgDataLocal.Content);
						}
					});

					$scope.newMeesage = null;
				};

				$scope.typing = function() {
					return;
					$scope.conversation.typing();
				};

				$scope.untyping = function() {
					return;
					$scope.conversation.untyping();
				};

				$scope.convertDateTime = function(dateTime, format) {
					var timestamp = moment(dateTime);
					return timestamp.format(format);
				};

				$scope.$parent.$on("addMessage", function(e, data) {
					$scope.$apply(function() {
						if (data.Status == 1)
							typing(data);
						else if (data.Status == 2)
							untyping(data);
						else if (data.Status == 3)
							addMessage(data);
					});
				});

				$scope.$parent.$on("sendMessage", function(e, conversation) {
					$scope.$apply(function() {
						conversation.sendMessage($scope.messagesData.ConversationId, $scope.newMeesageContent);
					});
				});

				var untyping = function(data) {
					return;
					var messages = $(".messages");

					var typingMessage = messages.find('.typing');
					if (typingMessage.length == 0)
						return false;

					typingMessage.each(function() {
						var self = $(this);
						if (self.data("typer-id") == data.EntityId) {
							self.remove();
						}
					});
				};

				var typing = function(data) {
					return;
					if (data.EntityId == $scope.messagesData.CurrentEntityId)
						return false;
					$scope.typing = true;
					//var newMessage = createNewMessageObject(data, "Typing...");
					//addMessageToScope(newMessage);
				};

				var createNewMessageObject = function(data, content) {
					return {
						AuthorId: data.EntityId,
						AuthorName: data.EntityName,
						AuthorImage: data.EntityImage,
						DateTime: $scope.convertDateTime(data.Datetime, 'MMMM D YYYY - HH:MM'),
						Content: content
					};
				};

				var addMessage = function(data) {
					if (data.EntityId == $scope.messagesData.CurrentEntityId)
						return false;
					var newMessage = createNewMessageObject(data, data.Message);
					return addMessageToScope(newMessage);
				};

				var addMessageToScope = function(messages) {
					//retrive only new masseges from server according to the timestep of last massege and add them to object
					$scope.messagesData.Messages = $scope.messagesData.Messages.concat(messages);
				};
			}]);
})(window, window.angular);