﻿(function(window, angular) {

	'use strict';

	angular.module('sywMessages.directives')
		.directive('messeges', function($compile, $timeout) {
			return {
				priority: 0,
				restrict: 'A',
				replace: true,
				scope: {
					messagesData: '='
				},
				link: function(scope, iElement) {

					var preventAutoScrollDown = false;
					var messagesContainer = $(iElement).find('#messages');
					var textAreaResize = $(iElement).find("#newMessage");

					var resize = function() {
						var windowWidth = $(window).width();
						var windowHeight = $(window).height();
						var newMessageArea = $('.msg-write');

						$('.new-message-input-wrapper').width(windowWidth - 20);
						$(messagesContainer).width(windowWidth - 20);
						$(messagesContainer).height(windowHeight - $(newMessageArea).height() - 10);
						$('#newMessage').width(windowWidth - 147);
						$('.msg-user-msg').width(windowWidth - 200);

						scrollToBottom();
					};

					var scrollToBottom = function(timeout) {
						if (!timeout) timeout = 0;
						$timeout(function() {
							messagesContainer.animate({ scrollTop: messagesContainer[0].scrollHeight }, 500, 'linear');
						}, timeout);
					};

					var autoHeight = function(element) {
						if (!$(element).prop('scrollTop')) {
							do {
								var b = $(element).prop('scrollHeight');
								var h = $(element).height();
								$(element).height(h - 5);
							} while (b && (b != $(element).prop('scrollHeight')));
						}
						$(element).height($(element).prop('scrollHeight') - 20);
						resize();
					};

					var bindEvents = function() {
						scope.$watch('messagesData.Messages', function(newMesseges, oldMessages) {
							if (newMesseges === oldMessages) {
								return;
							}
							if (!preventAutoScrollDown) {
								scrollToBottom();
							} else {
								preventAutoScrollDown = false;
							}
						});

						$(iElement).hammer({ preventDefault: false }).bind('swipedown', function(ev) {
							if (scope.$parent.noMoreMessages || scope.$parent.isRetrievingMessages || messagesContainer.scrollTop() > 0) return;
							preventAutoScrollDown = true;
							scope.$emit("getMoreMessages");
						});

						textAreaResize.keyup(function() {
							autoHeight(this);
						});

						$(window).on("resize", resize);
						$(document.body).on('orientation', resize);
					};

					bindEvents();
					autoHeight(textAreaResize);
					scrollToBottom(200);
				}
			};
		})
		.directive('fixImageDirective', function() {
			return function(scope, element, attrs) {
				if (attrs.fixImageDirective == '') return;

				$(element).css('background-image', 'url("' + attrs.fixImageDirective + '")');
				ImageFixer.FixMobile($(element));
			};
		})
		.directive('fixMessageWidth', function() {
			return function(scope, element) {
				$(element).width($(window).width() - 200);
			};
		});
})(window, window.angular);