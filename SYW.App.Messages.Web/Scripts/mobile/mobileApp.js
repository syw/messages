﻿angular.module('sywMessages.controllers', []);
angular.module('sywMessages.services', []);
angular.module('sywMessages.directives', []);
angular.module('sywMessages', ['sywMessages.controllers', 'sywMessages.services', 'sywMessages.directives']);