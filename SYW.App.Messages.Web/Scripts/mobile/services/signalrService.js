﻿angular.module('sywMessages.services')
	.service('signalRSvc', function($rootScope) {

		var initialize = function(conversationId, currentEntityId, entityName, entityImage) {

			var conversation = $.connection.conversation;

			conversation.addMessage = function(data) {
				$rootScope.$emit("addMessage", data);
			};

			var conversationModel = {
				conversationId: conversationId,
				entityId: currentEntityId,
				entityName: entityName,
				entityImage: entityImage
			};

			$.connection.hub.start(function() {
				conversation.join(conversationModel.conversationId, conversationModel.entityId, conversationModel.entityName, conversationModel.entityImage);
			}).done(function() {
				$rootScope.$emit("sendMessage", conversation);
			});

			return conversation;
		};

		return {
			initialize: initialize
		};
	});
