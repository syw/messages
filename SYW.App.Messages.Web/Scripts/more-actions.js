﻿// TODO : Bind using SYW.ready

$(function() {

	$("#more").click(function() {

		if ($('#more-actions').length != 0)
			return false;

		analyticsReports.report("general-action-report", { name: "Header > More" });

		var content = $('' +
			'<div class="container">' +
			'	<div class="container-content">' +
			'		<div id="markAllAsRead" class="action">Mark all as read</div>' +
			'		<div id="archiveAll" class="action">Remove all</div>' +
			'       <div id="settings" class="action">Settings</div>' +
			'	</div>' +
			'</div>');

		$(this).contextualPopover({
			createBackdrop: true,
			content: content,
			modalId: "more-actions",
			position: { x: 'left', y: 'bottom', primaryAxis: 'y' },
			offset: { x: -41, y: 17 }
		});

		if ($.browser.msie) {
			$('.triangle.top').addClass('ie');
		}
	});

	$('body').on('click touchstart', '#archiveAll', function() {
		showArchiveAllConfirmation();

		analyticsReports.report("general-action-report", { name: "Header > More > Archive All" });
	});

	$('body').on('click touchstart', '#markAllAsRead', function() {

		analyticsReports.report("general-action-report", { name: "Header > More > Mark all as read" });

		$.ajax({
			type: 'post',
			url: '/-/conversation/mark-all-as-read',
			success: function() {
				$('.conversation').removeClass('unread');
				$('.conversation').removeClass('new');
				$('.conversation').removeClass('selected');
				$('.conversation').addClass('read');
				$('.conversations-non-selected').show();
				$('.messages-frame').attr('src', '');
				$('.messages-frame').hide();

				hideActions();
			}
		});
	});

	$('body').on('click touchstart', '#settings', function() {
		analyticsReports.report("general-action-report", { name: "Header > More > Settings" });

		$("#messages-frame").attr("src", "/settings");

		hideActions();
	});

	$('body').on('click touchstart', '#btnCancelArchiveAll', function() {
		hideActions();
	});

	$('body').on('click touchstart', '#btnArchiveAll', function() {
		if ($(this).hasClass("disabled"))
			return false;

		$(this).addClass("disabled");

		analyticsReports.report("general-action-report", { name: "Header > More > Archive All > Confirm" });

		$.ajax({
			type: 'post',
			url: '/-/conversation/archive-all',
			success: function() {
				$('.conversation').fadeOut(250, function() {
					$('.conversation').remove();
					layout.initConversationsScrollPane();
					$('.empty-mailbox').slideDown();
				});

				$('#more').fadeOut();
				$('.conversations-non-selected').show();
				hideActions();
			}
		});
	});

	var hideActions = function() {
		$('.backdrop').click();
	};

	var showArchiveAllConfirmation = function() {
		var content = $('' +
			'<div class="confirmation-container">' +
			'	<div class="container-content">' +
			'		<div class="header">Remove</div>' +
			'		<div class="separator"></div>' +
			'		<div class="breaker"></div>' +
			'		<div class="content">Are you sure you want to remove all messages?</div>' +
			'	</div>' +
			'	<div class="footer">' +
			'		<span id="btnCancelArchiveAll" class="btn-cancel confirmation-button">Cancel</span>' +
			'		<span id="btnArchiveAll" class="btn-do confirmation-button">Remove</span>' +
			'	</div>' +
			'</div>');

		$('.modal-content').empty().append(content);
	};
});