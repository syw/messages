﻿$(function () {

	var conversations = $(".conversations");
	if (conversations.length == 0)
		return false;

	var conversationsPageModel = {
		entityId: conversations.data("entity-id")
	};

	if (!$.browser.mobile) {
		var conversationsHub = $.connection.conversations;

		$.connection.hub.start(function() {
			conversationsHub.join(conversationsPageModel.entityId);
		});

		conversationsHub.say = function(data) {

			$.ajax({
				type: 'GET',
				url: '/-/mailbox/get-conversation',
				data: { conversationId: data },
				success: function (response) {
					var isConversationSelected = false;
					$(".conversation").each(function() {
						var self = $(this);

						if (self.data("conversation-id") == data) {
							self.slideUp(500);

							if (self.hasClass('selected')) {
								isConversationSelected = true;
							}
						}
					});

					if ($('.empty-mailbox').length > 0) {
						$('.empty-mailbox').slideUp();
						$('#more').show();
					}

					var converstaion = $(response);
					converstaion.hide();
					if (isConversationSelected) {
						$.ajax({
							type: 'post',
							url: '/-/conversation/update-status',
							async: false,
							data: { conversationId: data, status: 0 },
							success: function() {
								converstaion.removeClass('new').addClass('selected');
							}
						});
					}
					
					$("#main .conversations .conversations-container").prepend(converstaion);
					ImageFixer.Fix(converstaion.find("img.entity-image"));

					converstaion.slideDown(500);

					layout.initConversationsScrollPane();
				}
			});
		};
	}
});