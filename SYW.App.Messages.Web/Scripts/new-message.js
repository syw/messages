﻿$(function() {

	var initFriends = function() {
		$.ajax({
			type: 'post',
			url: '/-/init-friends'
		});
	};

	$('#new-message').click(function() {
		analyticsReports.report("general-action-report", { name: "Header > New message dialog" });

		initFriends();

		Platform.Dialog.open({
			url: '/messages/new-conversation-box',
			title: 'Create new message',
			blackBackground: true,

			size:
			{
				width: 500,
				height: 250
			},
			buttons:
			[
				{
					title: "Cancel",
					type: 'cancel'
				},
				{
					title: 'SEND',
					type: 'ok'
				}
			]
		});

		$.pm.bind("newMessageSent", function (content) {
			var conversation = $(content)[0];
			$(conversation).hide();
			var conversationId = $(conversation).data('conversation-id');
			
			var existingConversations = $.grep($('[data-conversation-id]'), function(element) {
				return $(element).data('conversation-id') == conversationId;
			}, false);
			
			if (existingConversations.length > 0) {
				$(existingConversations).each(function(index, element) {
					$(element).slideUp();
					$(element).remove();
				});
			}

			$("#main .conversations .conversations-container").prepend(conversation);
			
			ImageFixer.Fix($(conversation).find("img.entity-image"));

			$(".new-message-box").slideUp(500, function() {
				$(this).remove();
			});

			if ($('.empty-mailbox').length > 0) {
				$('.empty-mailbox').slideUp();
				$('#more').show();
			}
			
			$(conversation).slideDown(500);

			$("#new-message").show();
			$(".separator").show();

			layout.initConversationsScrollPane();

			$.pm.unbind('newMessageSent');
		});
	});

	var newMessageBox = $('.new-message-box');
	if (newMessageBox.length == 0 || $('.new-message-container').length > 0)
		return;

	var bindAutoComplete = function(textbox) {

		textbox.keydown(function(event) {
			if (event.which == 8) { // chr(8) == backspace
				if (textbox.val().length == 0) {
					var participants = $(".participant");
					if (participants.length > 0) {
						participants.last().remove();
					}
				}
			}
			$(this).siblings('.watermark').hide();
		});

		$(textbox.parent()).click(function() {
			textbox.focus();
		});

		textbox.blur(function() {
			if (textbox.val().length == 0 && $('.participant').length == 0)
				$(this).siblings('.watermark').show();

			$(textbox.parent()).removeClass('textbox-shadow');
		});

		textbox.focus(function() {
			$(textbox.parent()).addClass('textbox-shadow');
		});

		textbox.autocomplete({
			source: function(request, response) {
				$.ajax({
					type: 'post',
					url: "/-/search",
					data: {
						participants: getParticipents($('.participant')),
						term: request.term
					},
					success: function(data) {
						response(data);
					}
				});
			},
			minLength: 1,
			appendTo: '.message',
			open: function() {
				$(".ui-autocomplete")
					.css("position", "absolute")
					.css("width", $('.recipients').innerWidth())
					.css("top", "33px") // There is a bug which the poistion is 1px misaligned in chrome
					.css("left", "49px");

				ImageFixer.Fix($(".ui-autocomplete").find("img.entity-image"));
			},
			select: function(event, ui) {
				var participant = $(
					"<span class='participant'>" +
						ui.item.name +
						"<span class='remove'></span>" +
						"</span>");

				participant.data("id", ui.item.id);
				textbox.before(participant);
				this.value = "";
				return false;
			},
			close: function() {
				//textbox.val("");
			}
		}).data('autocomplete')._renderItem = function(ul, item) {
			var autocompleteItem = $('<a tab-index="-1"></a>').addClass('ui-corner-all');
			autocompleteItem.append($('<img data-src="' + item.image + '"/>').addClass('entity-image'));
			autocompleteItem.append($('<span>' + item.name + '</span>').addClass('entity-name'));

			var ret = $('<li></li>')
				.data("item.autocomplete", item)
				.append(autocompleteItem)
				.appendTo(ul);
			return ret;
		};
	};

	$(".participant").find(".remove").live('click', function() {
		$(this).parent().remove();
	});

	var getParticipents = function(participants) {
		var participantsIds = new Array();
		participants.each(function() {
			participantsIds.push($(this).data("id"));
		});
		return participantsIds;
	};

	var validateNewMessage = function(formContainer, args) {

		var participants = $('.participant', formContainer);
		var recipients = $('.recipients', formContainer);
		var content = $('#content', formContainer);
		var errorMessage = $('.error-message', formContainer);

		recipients.removeClass('input-error');
		content.removeClass('input-error');
		errorMessage.hide();

		if (getParticipents(participants).length == 0) {
			errorMessage.text('You must enter at least one participant to start this conversation');
			errorMessage.show();
			recipients.addClass('input-error');
			return false;
		}

		if ($.trim(content.val()) == "" && args === "SEND") {
			errorMessage.text('You cannot send an empty message');
			errorMessage.show();
			content.addClass('input-error');
			return false;
		}
		return true;
	};

	var sendMessage = function(args, callbackOnSuccess) {
		if (!validateNewMessage($('.new-message-box'), args))
			return false;

		var participants = getParticipents($('.participant'));
		var messageContent = $('#content').val();

		$.ajax({
			type: 'post',
			url: '/-/messages/post',
			data: {
				participants: participants,
				content: messageContent,
				entityLink: args.entityLink
			},
			success: function(content) {
				callbackOnSuccess(content);
			}
		});
		return true;
	};

	function appendNewMessageToInboxPage(newMessage) {
		//update the inbox:
		try {
			$.pm({
				target: window.parent.frames["app-frame"],
				type: 'newMessageSent',
				data: newMessage
			});
		} catch(err) {
			console.log(err);
		}
	}

	;

	function closeNewMessageDialog() {
		Platform.Events.unsubscribe(Platform.Events.DialogButtonClicked, window.buttonsCallback);
		Platform.Dialog.close();
	}

	;

	//TODO we need a more refined way to do this.
	window.buttonsCallback = function(args) {
		if (args === 'SEND' || args.buttonText === 'Share') {
			analyticsReports.report("general-action-report", { name: "Header > New message dialog > Send" });

			sendMessage(args, function(data) {
				appendNewMessageToInboxPage(data);
				closeNewMessageDialog();
				window.pm({ target: window.top, type: "messages:message:sent" });
			});
		} else {
			analyticsReports.report("general-action-report", { name: "Header > New message dialog > Close" });

			closeNewMessageDialog();
		}
	};

	var bindMessageWatermarkBehaviour = function(messages) {
		$(messages.parent()).click(function() {
			messages.focus();
		});

		messages.keydown(function() {
			$(this).siblings('.watermark').hide();
		});

		messages.blur(function() {
			if (messages.val().length == 0)
				$(this).siblings('.watermark').show();

			$(messages.parent()).removeClass('textbox-shadow');
		});

		messages.focus(function() {
			$(messages.parent()).addClass('textbox-shadow');
		});
	};

	var tb = newMessageBox.find('#participants');
	bindAutoComplete(tb);
	bindMessageWatermarkBehaviour($('#content'));

	$(tb.parent()).click();

	Platform.Events.subscribe(Platform.Events.DialogButtonClicked, window.buttonsCallback);
});