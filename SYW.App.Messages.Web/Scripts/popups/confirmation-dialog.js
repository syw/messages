﻿/**
Usage:
	$(target).confirmationDialog({options})

Options:
okButtonText: (string) Text that would appear for the OK button (default: 'OK')
okButtonCallback: (delegate) Action to perform when clicking on OK button (default - do nothing)
cancelButtonText: (string) Text that would appear for the Cancel button (default: 'Cancel')
cancelButtonCallback: (delegate) Action to perform when clicking on Cancel button (default - do nothing)
title: (string) Title of the confirmation box (default - '')
confirmationMessage: (string) The confirmation message (the question) (default - '')
*/
(function($) {
	$.fn.confirmationDialog = function(options) {
		var settings = $.extend({
			okButtonText: 'OK',
			okButtonCallback: $.noop(),
			cancelButtonText: 'Cancel',
			cancelButtonCallback: $.noop(),
			title: '',
			confirmationMessage: '',
			offset: { x: 14, y: -10 },
			targetContainer: null,
			createBackdrop: true,
			stopEventPropagation: false
		}, options || {});

		var container = $('<div/>');
		var breaker = $('<div/>').addClass('breaker');

		var confirmationDialogContainer = $('<div/>').addClass('content-container');
		var title = $('<div />').addClass('title').text(settings.title);
		var titleSeparator = $('<div/>').addClass('title-separator');
		var content = $('<div/>').addClass('content').text(settings.confirmationMessage);

		var buttonsContainer = $('<div/>').addClass('footer');
		var okButton = $('<span/>').text(settings.okButtonText).addClass('btn-do confirmation-button');
		var cancelButton = $('<span/>').text(settings.cancelButtonText).addClass('btn-cancel confirmation-button');

		buttonsContainer
			.append(cancelButton)
			.append(okButton);

		confirmationDialogContainer
			.append(title)
			.append(breaker)
			.append(titleSeparator)
			.append(breaker)
			.append(content);

		container.addClass('confirmation-dialog')
			.append(confirmationDialogContainer)
			.append(buttonsContainer);

		var confirmationModal = $(this).contextualPopover({ content: container, modalId: settings.modalId, position: settings.position, offset: settings.offset, onClose: settings.onClose, targetContainer: settings.targetContainer, createBackdrop: settings.createBackdrop });
		okButton.click(function() {
			confirmationModal.close();
			if (settings.okButtonCallback)
				settings.okButtonCallback();
			if (settings.stopEventPropagation)
				return false;
		});
		cancelButton.click(function() {
			confirmationModal.close();
			if (settings.cancelButtonCallback)
				settings.cancelButtonCallback();
			if (settings.stopEventPropagation)
				return false;
		});
	};
})(jQuery);