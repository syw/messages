﻿(function($) {
	$.fn.contextualPopover = function(options) {
		var settings = $.extend({
			// Defaults go here..
			layout: 'context',
			extraClass: 'contextual-popover',	// Well... just an extra class to add to the popup's root element.
			hasCallout: true,
			offset: { x: 14, y: -10 }, 			// Offset parameter to do 'fine tuning' on the contextual popup calculated position.
			position: { x: 'right', y: 'top' }	// The default positioning priority (Will be overridden when not posible due to page dimensions).
			// Other optional parameters are all documented on popup.js.
		}, options || {});

		var self = $(this);

		return self.popup(settings);
	};
})(jQuery);