﻿$('.contextual-tooltip').live('click', function() {
	var title = $(this).data('tooltip-title');
	var body = $('<div/>').text($(this).data('tooltip-text'));

	var contentCreator = {
		title: title,
		innerElem: body
	};
	$(this).contextualPopover({
		contentCreator: contentCreator,
		extraClass: 'contextual-popover tooltip-popup',
		offset: { x: 24, y: -13 }
	});
});

$(function() {
	$('.contextual-tooltip[data-tooltip-trigger="hover"]').each(function() {
		var title = $(this).data('tooltip-title');
		var body = $('<div/>').text($(this).data('tooltip-text'));
		var contentCreator = {
			title: title,
			innerElem: body
		};

		var popup = null;
		$(this).hover(function() {
			if (popup)
				popup.close();

			popup = $(this).contextualPopover({
				contentCreator: contentCreator,
				extraClass: 'contextual-popover tooltip-popup',
				offset: { x: 24, y: -13 },
				createBackdrop: false
			});
		}, function() {
			popup.close();
		});
	});
});