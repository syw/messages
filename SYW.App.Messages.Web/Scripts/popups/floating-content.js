﻿/* jQuery lightweight plugin boilerplate */
(function($) {
	var defaults = {
		delay: { show: 200, hide: 200 }, // Relevant only when the 'hover' event is used.
		offset: { x: 0, y: 0 }, // The default positioning is exactly under the trigger element. Use this to give offset to the content element.
		position: { x: 'right', y: 'top' }, // The default positioning is exactly under the trigger element. Use this to give offset to the content element.
		isContentDescendant: false, // Context optimization. Set to true if the content is an actual decendant of the trigger element.
		anchor: null, // The element to position the content to. The default is the trigger element.
		targetContainer: 'body', // The element to which the floating content will be appended.
		callout: false, // Should a triangular callout be added to the floating content to indicate its relation to the trigger element
		items: [], //jQuery element set of items that only one of which can be open at a time. So if one is clicked the others in set will be closed
		beforeShow: function() {
		},
		closeOnDocumentClick: true, //Should the content be closed whenever the user clicks anywhere in the document 
		afterShow: function() {
		},
		afterHide: function() {
		}
	};

	function FloatingContent(element, options) {
		this.element = $(element);
		this.options = $.extend({}, defaults, options);
		this._defaults = defaults;
		this._name = pluginName;
		this.timeoutIn = this.timeoutOut = this.content = this.targetContainer = false;
	}

	FloatingContent.prototype.click = function(clickEvent) {
		clickEvent.stopPropagation();
		if (this.element.hasClass('open')) {
			this.hideContentForTriggers(this.element);
		} else {
			if (this.options.items.length) {
				this.hideContentForTriggers(this.options.items);
			}
			this.showContent(this.element);

			this.bindDocEsc();
			if (this.options.closeOnDocumentClick) {
				this.bindDocClick();
			}
		}
	};

	FloatingContent.prototype.mouseenter = function() {
		if (this.timeoutOut)
			clearTimeout(this.timeoutOut);

		var that = this;
		this.timeoutIn = setTimeout(function() {
			if (!!that.content) return;

			that.content = that.showContent(that.element);
			if (that.content.data('hoverBound')) return;
			that.content.hover(
				function() {
					if (that.timeoutOut)
						clearTimeout(that.timeoutOut);
				},
				function() {
					if (that.timeoutIn)
						clearTimeout(that.timeoutIn);

					that.timeoutOut = setTimeout(function() {
						that.hideContentForTriggers(that.element);
						that.content = null;
					}, that.options.delay.hide);
				}
			);
			that.content.data('hoverBound', true);
		}, this.options.delay.show);
	};

	FloatingContent.prototype.mouseleave = function() {
		if (this.timeoutIn)
			clearTimeout(this.timeoutIn);

		var that = this;
		this.timeoutOut = setTimeout(function() {
			that.hideContentForTriggers(that.element);
			that.content = null;
		}, this.options.delay.hide);
	};

	FloatingContent.prototype.hideContent = function(content) {
		var container = content.data('parent');
		container.append(content);
		content.data('trigger').removeClass('open');
		this.options.afterHide(content);
	};

	FloatingContent.prototype.hideContentForTriggers = function(triggers) {
		this.unbindEvents();

		triggers = triggers.filter('.open');

		var that = this;
		triggers.each(function() {
			var trigger = $(this);
			var content = that.getContentForTrigger(trigger);

			if (content.length > 0)
				that.hideContent(content);
		});
	};

	FloatingContent.prototype.addCallout = function(content, position) {
		content.find(".triangle").remove();
		var callout = $("<div/>").addClass("triangle");
		var that = this;
		$.each(["left", "right"], function(_, direction) {
			var isAlignmentDirection = !!position[direction];
			callout.toggleClass(direction, isAlignmentDirection);
			if (isAlignmentDirection)
				callout.css(direction, -that.options.offset.x);
		});
		callout.appendTo(content);
	};

	FloatingContent.prototype.positionContent = function(content, trigger) {
		var position = content.nextTo(trigger, {
			position: this.options.position,
			offset: this.options.offset
		});
		content.css(position);
		return position;
	};

	FloatingContent.prototype.getTargetContainer = function() {
		return this.targetContainer || (typeof(this.options.targetContainer) == 'object' ? this.options.targetContainer : $(this.options.targetContainer));
	};

	FloatingContent.prototype.getContentForTrigger = function(trigger) {
		var content = trigger.data('content');
		if (content) {
			return content;
		}
		var contentSelector = trigger.data('content-selector');
		var context = this.options.isContentDescendant ? trigger : $(document.body);
		return $(contentSelector, context);
	};

	FloatingContent.prototype.showContent = function(trigger) {
		this.options.beforeShow(this.element);

		var content = this.getContentForTrigger(trigger);

		trigger.addClass('open');
		content.data('parent', content.parent());
		content.data('trigger', trigger);
		trigger.data('content', content);

		var container = this.getTargetContainer();
		container.append(content);

		if (this.options.anchor)
			trigger = $(this.options.anchor);
		var position = this.positionContent(content, trigger);
		if (this.options.callout) {
			this.addCallout(content, position);
		}

		this.bindViewportWidthChange(content, trigger);

		this.options.afterShow(content, trigger);

		return content;
	};

	FloatingContent.prototype.bindDocEsc = function() {
		var that = this;
		this.docEscHandler = function(docKeydownEvent) {
			if (docKeydownEvent.keyCode != 27)
				return;
			that.hideContentForTriggers(that.element);
		};
		$(document).keydown(this.docEscHandler);
	};

	FloatingContent.prototype.bindDocClick = function() {
		var that = this;
		this.docClickHandler = function(docClickEvent) {
			var clickTarget = $(docClickEvent.target);
			if (that.clickedOnContent(clickTarget) || that.clickedOnTrigger(clickTarget))
				return;
			that.hideContentForTriggers(that.element);
		};
		$(document).bind('click touchstart', this.docClickHandler);
	};

	FloatingContent.prototype.clickedOnContent = function(clickTarget) {
		return clickTarget.closest(this.element.data('content-selector')).length > 0;
	};

	FloatingContent.prototype.clickedOnTrigger = function(clickTarget) {
		return this.element.get(0) == clickTarget.get(0) || this.element.find(clickTarget).length > 0;
	};

	FloatingContent.prototype.bindViewportWidthChange = function(content, trigger) {
		var that = this;
		that.viewportWidthChangeHandler = function() {
			that.positionContent(content, trigger);
		};
		$(document.body).bind('viewport-width-change', that.viewportWidthChangeHandler);
	};

	FloatingContent.prototype.unbindEvents = function() {
		if (typeof this.docEscHandler == 'function')
			$(document).unbind('keydown', this.docEscHandler);
		if (typeof this.docClickHandler == 'function')
			$(document).unbind('click touchstart', this.docClickHandler);
		if (typeof this.viewportWidthChangeHandler == 'function')
			$(document.body).unbind('viewport-width-change', this.viewportWidthChangeHandler);
	};

	var pluginName = 'floatingContent';
	$.fn[pluginName] = function(arg) {
		var args = arguments;
		return this.each(function() {
			var instance = $.data(this, 'plugin_' + pluginName);
			if (!instance) { //if not instantiated yet
				$.data(this, 'plugin_' + pluginName, new FloatingContent(this, arg)); //then instantiate and save result in data
				return;
			}
			if (arg instanceof $.Event) { //if passed arg is jquery event
				instance[arg.type].apply(instance, args); //go to method according to event type e.g. 'click', 'mouseenter', 'mouseleave'
				return;
			}
			if (typeof arg === 'string') { //if passed arg is just string
				instance[arg].apply(instance, Array.prototype.slice.call(args, 1)); //go to method according to that method name
			}
		});
	};
})(jQuery);

(function($) {
	$.fn.bindFloatingContent = function(options) {
		options = $.extend({
			event: 'click' // The event that triggers the hide/show functionality. optional: 'hover'
		}, options || {});

		if (($(document.body).hasClass('hybrid') || $(document.body).hasClass('anonymous-landing-page')) && options.event.toLowerCase() == 'hover') { //old jquery sucks
			options.event = 'mouseenter mouseleave';
		}

		return this.bind(options.event, function(e) {
			var elem = $(this);
			elem.floatingContent(options);
			elem.floatingContent(e);
		});
	};
})(jQuery);