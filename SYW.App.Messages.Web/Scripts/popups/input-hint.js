﻿(function($) {
	$.fn.inputHint = function(options) {
		var settings = $.extend({
			offset: { x: 19, y: -10 },
			extraClass: 'contextual-popover input-hint',
			onClose: popupClosed,
			createBackdrop: false
			// Offset parameter to do 'fine tuning' on the contextual popup calculated position.
			// Other optional parameters are all documented on popup.js.
		}, options || {});

		var input = $(this);
		var isPopupOpen = false;
		var popup;

		function popupClosed() {
			isPopupOpen = false;
		}

		settings.content = $('<div/>').text(input.data('hint-text'));
		input.focus(function() {
			if (!isPopupOpen) {
				isPopupOpen = true;
				popup = input.contextualPopover(settings);
			}
		});
		input.blur(function() {
			popup.close();
		});
	};
})(jQuery);

$(document).ready(function() {
	$('input[data-hint-text],textarea[data-hint-text]').each(function() {
		$(this).inputHint();
	});
});