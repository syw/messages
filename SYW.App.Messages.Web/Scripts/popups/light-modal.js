﻿(function($) {
	$.fn.lightModal = function(options) {
		if (!options.modalId) return;

		var settings = $.extend({
			layout: 'lightbox',
			showBackdrop: true
			// Other optional parameters are all documented on popup.js.
		}, options);

		var self = $(this);
		return self.popup(settings);

	};
})(jQuery);