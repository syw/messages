﻿(function($) {
	$.fn.nextTo = function(baseElement, options) {
		if (this.length == 0) return false;

		var settings = $.extend({
			offset: { x: 0, y: 0 },
			position: { x: 'right', y: 'top', primaryAxis: 'x' },
			container: $('body'),
			cssPosition: 'absolute'
		}, options || {});

		var looseElement = this;
		var offsetParent = looseElement.offsetParent();

		var targetOffset = offsetParent.offset();
		var baseRelativeOffset = calcBaseRelativeOffset(baseElement, targetOffset);

		var container = settings.container;
		var containerHeight = container.innerHeight ? container.innerHeight() : container.height();
		var containerWidth = container.innerWidth ? container.innerWidth() : container.width();
		var containerOffset = container.offset();

		looseElement.css('position', settings.cssPosition);
		var cssObj = { position: settings.cssPosition };

		function calcBaseRelativeOffset(anchor, targetOffset) {
			var anchorOffset = anchor.offset();

			return {
				left: anchorOffset.left - targetOffset.left,
				top: anchorOffset.top - targetOffset.top
			};
		}

		function calcPosition() {
			if (settings.position.primaryAxis == 'y')
				setVerticalPositionFirst();
			else
				setHorizontalPositionFirst();

			addOffset();
		}

		function addOffset() {
			var vertical = cssObj.top ? 'top' : 'bottom';
			cssObj[vertical] += settings.offset.y;

			var horizontal = cssObj.right ? 'right' : 'left';
			cssObj[horizontal] += settings.offset.x;
		}

		function setVerticalPositionFirst() {
			if (settings.position.y == 'top')
				cssObj.bottom = containerHeight - baseRelativeOffset.top; //set bottom value.
			else if (settings.position.y == 'bottom')
				cssObj.top = baseRelativeOffset.top + baseElement.outerHeight(); // set top value

			if (settings.position.x == 'right')
				cssObj.right = containerWidth - (baseRelativeOffset.left + baseElement.outerWidth()); //set right value
			else if (settings.position.x == 'left')
				cssObj.left = baseRelativeOffset.left; //set left value
		}

		function setHorizontalPositionFirst() {
			var possibleRight = tryToTheRight();
			var possibleLeft = tryToTheLeft();

			if ((settings.position.x == 'right' && (possibleRight || !possibleLeft)) || (settings.position.x == 'left' && !possibleLeft && possibleRight))
				positionToTheRight();
			else
				positionToTheLeft();

			if (settings.position.y == 'top')
				cssObj.top = baseRelativeOffset.top; //set top value.
			else if (settings.position.y == 'bottom')
				cssObj.bottom = containerHeight - (baseRelativeOffset.top + baseElement.outerHeight()); // set bottom value
		}

		function positionToTheLeft() {
			cssObj.right = offsetParent.outerWidth(true) - baseRelativeOffset.left; // position to the left of the base element.
		}

		function positionToTheRight() {
			// position to the right of the base element.
			cssObj.left = baseRelativeOffset.left + baseElement.outerWidth();
		}

		function tryToTheRight() {
			return targetOffset.left + baseRelativeOffset.left + baseElement.outerWidth(true) + looseElement.outerWidth(true) + settings.offset.x < containerWidth + containerOffset.left;
		}

		function tryToTheLeft() {
			return targetOffset.left + baseRelativeOffset.left > looseElement.outerWidth(true) + settings.offset.x + containerOffset.left;
		}

		calcPosition();

		return cssObj;
	};
})(jQuery);