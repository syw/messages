﻿(function($) {
	$.fn.popup = function(options) {
		var settings = $.extend({
			// Defaults go here..
			layout: 'context', 										// Popup layout style. also available: 'lightbox'
			createBackdrop: true, 									// Should the popup create a background div that blocks everything
			showBackdrop: true,                                    // Should the background div be visible (transperant grey stylish)
			content: null, 											// The HTML content for the popup.
			contentCreator: null,
			contentAjaxUrl: null, 									// Url to fetch (ajax) the popup content from.
			contentAjaxData: {}, 									// The 'data' to be sent by the Ajax call to fetch the content.
			contentAjaxSuccess: function() {
			}, 						// Fetch content Ajax success callback.
			contentAjaxError: function() {
			}, 						// Fetch content Ajax error callback.
			extraClass: null, 										// Well... just an extra class to add to the popup's root element.
			modalId: null, 											// Really...? need an explanation for this one too?
			offset: { x: 0, y: 0 }, 								// Offset parameter to do 'fine tuning' on the popup's calculated position.
			position: { x: 'right', y: 'top', primaryAxis: 'x' }, 	// Only usefull if the layout parameter is 'contextual'
			onClose: null, 										// A function to execute when the popup is closed,
			targetContainer: null, 								// The target element to add the popup to, or null to add it at end-of-body; only works for context popups without backdrops,
			cssPosition: 'absolute'									// The position type of thie element. The default is absolute positioningcl
		}, options || {});

		var sender = $(this);
		var modal;
		var modalContent;
		var modalCloseBtn;
		var blocker;
		var loader;
		var shouldModifyNoScrollState = false;

		if (settings.contentCreator)
			settings.content = createContent();

		if (!settings.targetContainer)
			settings.targetContainer = $('body');

		function createContent() {
			var contentCreator = settings.contentCreator;
			var wrapper = $('<div />');
			var content = $('<div />').addClass('content-container').appendTo(wrapper);
			if (contentCreator.title) {
				$('<div />').addClass('title').html(contentCreator.title).appendTo(content);
				$('<div />').addClass('title-separator').appendTo(content);
			}
			if (contentCreator.innerElem) {
				contentCreator.innerElem.appendTo(content);
			}
			if (contentCreator.buttons) {
				var footer = $('<div />').addClass('footer').appendTo(wrapper);
				for (var i = 0; i < contentCreator.buttons.length; i++) {
					var button = contentCreator.buttons[i];
					$('<span />').addClass(button['class']).html(button.text).click(button.callback).appendTo(footer);
				}
			}
			return wrapper.children();
		}

		function showModal() {
			if (settings.createBackdrop)
				createBlocker();

			if (settings.layout == 'context')
				createContextPopup();
			else
				createLightboxPopup();

			setPopupContent();
			bindCloseEvents();
		}

		// Creates the Modal's Backgroune (whether with or without a drop shadow).

		function createBlocker() {
			blocker = $('<div/>').addClass('modal-blocker')
				.appendTo('body');
			if (settings.showBackdrop) {
				blocker.addClass('backdrop');
				blocker.css({ 'height': $(window).height() });
			} else
				blocker.css({ 'height': $(document).outerHeight(true) });
		}

		// Creates the Modal object for contextual-style modal.

		function createContextPopup() {
			modal = $('<div/>').addClass(settings.layout);

			if (settings.extraClass) modal.addClass(settings.extraClass);
			if (settings.modalId) modal.attr('id', settings.modalId);

			modalContent = $('<div/>').addClass('modal-content');
			modal.append(modalContent);
			modal.css('visibility', 'hidden').appendTo(settings.targetContainer);
		}

		// Creates the Modal object for lightbox-style modal.

		function createLightboxPopup() {
			//to prevent the body scroll when lightbox is open.
			if (!$(document.body).hasClass('no-scroll')) {
				$(document.body).addClass('no-scroll');
				shouldModifyNoScrollState = true;
			}

			if (settings.createBackdrop)
				blocker.addClass('light-modal-container');

			modal = $('<div/>').addClass('light-modal').attr('tabindex', '0');

			modalContent = $('<div/>').addClass('modal-content');
			modalCloseBtn = $('<div/>').addClass('modal-close-button');

			if (settings.extraClass) modal.addClass(settings.extraClass);
			if (settings.modalId) modal.attr('id', settings.modalId);

			modal.append(modalContent)
				.append(modalCloseBtn);

			if (settings.createBackdrop)
				blocker.append(modal);
			else
				$("body").append(modal);

			modal.focus(); //to enable scrolling with keyboard arrow keys when popup is opened (and heigher than viewport), in unison with tabindex
		}

		// Sets the popup's content (via parameter or via Ajax call).

		function setPopupContent() {
			if (settings.content) {
				setContent(settings.content);
			} else if (settings.contentAjaxUrl) {
				loader = $('<span/>').addClass('preloader');
				modal.addClass('loading');
				modalContent.html(loader);
				positionPopup(); // just to put the modal with the 'preloader' on the page.
				$.ajax({
					url: settings.contentAjaxUrl,
					type: 'POST',
					data: settings.contentAjaxData,
					success: function(responseData) {
						responseData = $(responseData);
						modal.removeClass('loading');
						loader.remove();
						setContent(responseData);
						settings.contentAjaxSuccess(responseData);
					},
					error: settings.contentAjaxError
				});
			}
		}

		function setContent(content) {
			modalContent.html(content);
			// modalContent.lazyImagesLoaded().progress(positionPopup).done(positionPopup);
			positionPopup();
			// SYW.ready("popup", modal);
		}

		function positionPopup() {
			if (settings.layout == 'context')
				positionContextPopup();
			else if (settings.layout == 'lightbox')
				positionLightboxPopup();

			modal.css('visibility', '');
		}

		// Positions lightbox-style modal (center of view port).

		function positionLightboxPopup() {
			modal.center();
		}

		// Positions contextual-style modal next to the 'sender' element.

		function positionContextPopup() {
			var position = modal.nextTo(sender, {
				position: settings.position,
				offset: settings.offset,
				container: settings.targetContainer,
				cssPosition: settings.cssPosition
			});

			// Resets Modal positioning css declarations, before re-position of a modal.
			modal.css({ top: 'auto', bottom: 'auto', left: 'auto', right: 'auto' });

			modal.css(position);
			addCallout(position);
		}

		// Ads the Callout (/pointer/triangle...) to the modal, with the apropriate css classes.

		function addCallout(position) {
			$('.triangle', modal).remove();
			modalPointer = $('<div/>').addClass('triangle');
			if (settings.position.primaryAxis == 'y') {
				if (position.top)
					modalPointer.addClass('top');
				else if (position.bottom)
					modalPointer.addClass('bottom');
			} else {
				if (position.left)
					modalPointer.addClass('left');
				else if (position.right)
					modalPointer.addClass('right');
			}

			modal.append(modalPointer);
			if (settings.position.y == 'bottom') {
				var borderWidth = modal.css('border-width').replace('px', '') * 1;
				//				This code is supposed to work, removing it for now.				
				//				modalPointer
				//					.css('bottom', modalPointer.position().top + borderWidth + 'px')
				//					.css('top', 'auto');
			}
		}

		// Binds all of the events that are closing the popup.

		function bindCloseEvents() {
			$(document).keydown(function closeOnEscape(event) {
				if (event.keyCode == 27) {
					close();
					$(document).unbind('keydown', closeOnEscape);
				}
			});

			if (modalCloseBtn)
				modalCloseBtn.click(function() {
					close();
					return false;
				});

			if (settings.createBackdrop) {
				if (settings.layout == 'context')
					blocker.click(close);
				else {
					blocker.click(function(e) {
						if ($(e.target).parents('.light-modal').length > 0) return;
						close();
						return false;
					});
				}
			}
		}

		function close() {
			modal.detach();
			if (blocker) {
				blocker.remove();
			}
			if (modal.hasClass("light-modal") && shouldModifyNoScrollState) {
				$('body').removeClass('no-scroll');
			}
			if (settings.onClose) {
				settings.onClose();
			}
			modal.remove();
			analytics.resetPageValues();
		}

		showModal();

		return {
			close: close,
			elem: modal
		};
	};
})(jQuery);