﻿$(function() {
	var conversationContainer = $(".conversation-container");

	if (conversationContainer.length == 0)
		return false;

	var conversationModel = {
		conversationId: conversationContainer.data("conversation-id"),
		entityId: conversationContainer.data("entity-id"),
		entityName: conversationContainer.data("entity-name"),
		entityImage: conversationContainer.data("entity-image")
	};

	var conversation = null;
	if (!$.browser.mobile) {
		conversation = $.connection.conversation;
	}
	var untyping = function(data) {
		var messages = $(".messages");

		var typingMessage = messages.find('.typing');
		if (typingMessage.length == 0)
			return false;

		typingMessage.each(function() {
			var self = $(this);
			if (self.data("typer-id") == data.EntityId) {
				self.remove();
			}
		});
	};

	var typing = function(data) {
		if (data.EntityId == conversationModel.entityId)
			return false;

		var messages = $(".messages");

		if (messages.find('.typing').length > 0)
			return false;

		var typingMessage = CreateNewMessageHtml(data, "Typing...").addClass("typing");

		typingMessage.data("typer-id", data.EntityId);

		messages.append(typingMessage);

		ImageFixer.Fix(typingMessage.find("img.entity-image"));
	};

	var CreateNewMessageHtml = function(data, content) {
		return $("<div class='message-container'>" +
			"<div class='entities float'>" +
			"<img data-src='" + data.EntityImage + "' class='entity-image'>" +
			"</div>" +
			"<div class='message'>" +
			"<div class='header'>" +
			"<div class='entity-name float link truncate'>" + data.EntityName + "</div>" +
			"<div class='actions float-right'>" +
			"<span class='time'>Now</span>" +
			"</div>" +
			"<div class='breaker'></div>" +
			"</div>" +
			"<pre class='message-content content pre'>" + content + "</pre>" +
			"</div>" +
			"<div class='breaker'></div>" +
			"</div>");
	};

	var addMessage = function(data) {
		var newMessage = CreateNewMessageHtml(data, data.Message);
		var container = $("<div />");

		container.append(newMessage);
		container.hide();
		$(".messages").append(container);

		container.slideDown();

		ImageFixer.Fix(container.find("img.entity-image"));

		layout.initMessagesScrollPane();
		layout.scrollToBottom('messages-scroll');
	};

	if (!$.browser.mobile) {
		conversation.addMessage = function (data) {
			if (data.Status == 1)
				typing(data);
			else if (data.Status == 2)
				untyping(data);
			else if (data.Status == 3)
				addMessage(data);
		};
	}

	$("#content").focus(function() {
		layout.scrollToBottom('messages-scroll');
		// conversationModel.conversationId

		if (!$.browser.mobile) {
			conversation.typing();
		}
	});

	if (!$.browser.mobile) {

		$("#content").focusout(function() {

			// conversationModel.conversationId
			conversation.untyping();
		});

		$.connection.hub.start(function() {
			conversation.join(conversationModel.conversationId, conversationModel.entityId, conversationModel.entityName, conversationModel.entityImage);
		});
	}

	$(".new-message-container .btn-do").live('click', function() {

		var self = $(this);

		if (self.hasClass("disabled"))
			return false;

		var content = $.trim($("#content").val());

		if (content == "")
			return false;

		self.addClass("disabled");

		$.ajax({
			type: 'post',
			url: '/-/messages/post-new-message',
			data: {
				content: content,
				conversationId: self.data("conversation-id")
			},
			success: function(data) {
				if (!$.browser.mobile) {
					conversation.sendMessage(self.data("conversation-id"), content);
				} else {

					var messageHtml = $(data);

					$(".messages").append(messageHtml);
					ImageFixer.Fix(messageHtml.find("img.entity-image"));
					messageHtml.slideDown();
					layout.initMessagesScrollPane();
					layout.scrollToBottom('messages-scroll');
				}
				$("#content").val("");
				self.removeClass("disabled");
			}
		});
	});

	$(".iframe-content-type").load(function() {
		var self = $(this);

		var height = self.data("height");
		var width = self.data("width");

		self.css("height", height);
		self.css("width", width);

		layout.initConversationsScrollPane();

		$(self.parent()).find(".loader-container").remove();
		self.slideDown();
	});
});