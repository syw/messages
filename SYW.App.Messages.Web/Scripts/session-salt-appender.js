﻿(function() {

    $(document).ajaxSend(function (event, xhr, settings) {
        append(xhr, settings.url);
    });

    function append(xhr, url) {
        if (typeof url != 'string' || url.indexOf('/-/') == -1) {
            return;
        }

        try {
            // adding the request header so we will be able to throw out forgery requests
            var sessionSalt = window.sessionSalt || null;
            if (sessionSalt != null && sessionSalt.key.length > 0) {
                xhr.setRequestHeader(sessionSalt.key, sessionSalt.value);
            }
        } catch (exception) {
            console.log('failed to add new value to the header');
        }
    }

}());