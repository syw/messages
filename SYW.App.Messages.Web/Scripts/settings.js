﻿$(function() {
	if ($(".settings-container").length == 0)
		return false;

	$('#email-input').keyup(function() {
		var self = $(this);

		$.ajax({
			type: 'GET',
			url: '/-/settings/check-email',
			data: { email: self.val() },
			success: function(data) {
				if (data == 2) { //Exists
					$('#email-save-settings').addClass("disabled");

					$("#email-check-result")
						.addClass("bad")
						.removeClass("empty")
						.removeClass("good");
					$("#email-check-message").text("Taken");
				} else {
					$('#email-save-settings').removeClass("disabled");

					$("#email-check-result")
						.addClass("good")
						.removeClass("empty")
						.removeClass("bad");
					$("#email-check-message").text("");
				}
			}
		});
	});

	$("#email-save-settings").click(function() {
		var self = $(this);

		if (self.hasClass("disabled"))
			return false;

		self.addClass("disabled");

		$.ajax({
			type: 'POST',
			url: '/-/settings/update',
			data: { email: $('#email-input').val() },
			success: function(response) {
				if (response == 0) {
				    analyticsReports.report("general-action-report", { name: "Set email" });

				    $('#email-input').attr("disabled", "disabled");
				    
				} else {
					self.addClass("disabled");
				}
			}
		});
	});
});