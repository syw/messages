﻿var systemMessagesViewModel = {
	currentMessage: 1,
	currentPage: 1,
	pageSize: 25,
	isAjaxCallActive: false,
	isReachedEnd: false,
	isNotifiedNoNewMessages: false,
	isNotifiedAllMessagesRead: false,

	SetCurrentPreviewIndex: function(messageId) {
		var messagesPreviewContainer = $(".message-preview-container");
		var currentMessagePreviwContainer = messagesPreviewContainer.filter('.message-preview-container[data-message-id="' + messageId + '"]');
		systemMessagesViewModel.currentMessage = messagesPreviewContainer.index(currentMessagePreviwContainer) + 1;
	},

	SetCurrentMessage: function(model) {
		systemMessagesViewModel.SetCurrentPreviewIndex(model.messageId);

		if (systemMessagesViewModel.IsAllMessagesView()) {
			systemMessagesViewModel.SetAllMessagesCurrentMessageHeader(model.authorImageUrl, model.authorName, model.conversationId);
		}

		var systemMessageContainer = $('#systemMessageContainer');

		systemMessageContainer.data('message-id', model.messageId);
		systemMessageContainer.data('width', model.messageWidth);
		systemMessageContainer.data('height', model.messageHeight);
		systemMessageContainer.css('height', model.messageHeight).css('width', model.messageWidth);

		systemMessageContainer.html(model.messageContent);

		$('#current-message').text(systemMessagesViewModel.currentMessage);
		$('.total-messages').text(model.totalMessagesCount);
		$('#total-unread').text(model.totalUnreadCount);

		layout.initMessagesScrollPane();

		var message = $('.message-preview-container[data-message-id="' + model.messageId + '"]');
		if (message.hasClass('Unread') || message.hasClass('New')) {
			message.removeClass('Unread').removeClass('New').addClass('Read');
		}

		if (model.totalUnreadCount == 0) {
			this.NotifyAllMessagesRead();
		}

		if ($('.message-preview-container').length < systemMessagesViewModel.pageSize && $('.message-preview-container.New').length == 0) {
			systemMessagesViewModel.NotifyNoMoreNewMessages();
		}
		analyticsReports.report("report-page", { page: "System Message", section: model.bauName + " - BAU - " + model.dateTime });

		systemMessageBrowsingViewModel.SetBrowsingArrowsState(systemMessagesViewModel.currentMessage);
		if (systemMessagesViewModel.currentMessage >= (systemMessagesViewModel.pageSize * (systemMessagesViewModel.currentPage - 1)) && !systemMessagesViewModel.isReachedEnd) {
			systemMessagesViewModel.MoreMessages(function() { systemMessageBrowsingViewModel.SetBrowsingArrowsState(systemMessagesViewModel.currentMessage); });
		}
	},

	SetAllMessagesCurrentMessageHeader: function(authorImageUrl, authorName, conversationId) {
		$('.all-messages-header').hide();
		$('.header').show();
		$('.header').find('.entity-image').attr('src', authorImageUrl);
		$('.header').find('.content').find('.entity-name').html(authorName);
		$('.entity-messages-details').hide();
		$('.tab.action.page-count').hide();
		$('.conversation-container').first().data('conversation-id', conversationId);
	},

	NotifyAllMessagesRead: function() {
		if (this.isNotifiedAllMessagesRead)
			return;

		this.isNotifiedAllMessagesRead = true;
		this.isNotifiedNoNewMessages = true;

		$.pm({
			target: window.parent,
			type: 'markConversationAsRead',
			data: $('.conversation-container').data('conversation-id')
		});
	},

	NotifyNoMoreNewMessages: function() {
		if (this.isNotifiedNoNewMessages)
			return;

		if ($('.message-preview-container.Read').length == $('.message-preview-container').length)
			return;

		this.isNotifiedNoNewMessages = true;

		$.pm({
			target: window.parent,
			type: 'removeNewMessageIndicator',
			data: $('.conversation-container').data('conversation-id')
		});
	},

	MoreMessages: function(callback) {
		systemMessagesViewModel.isAjaxCallActive = true;
		var conversationId = $('.conversation-container').first().data('conversation-id');
		if (systemMessagesViewModel.IsAllMessagesView())
			conversationId = '';

		$.ajax({
			type: 'GET',
			url: '/-/message/messages-preview',
			data: { conversationId: conversationId, page: systemMessagesViewModel.currentPage, pageSize: systemMessagesViewModel.pageSize },
			success: function(data) {
				if (data == null || $.trim(data) == "") {
					systemMessagesViewModel.isReachedEnd = true;

					if (!systemMessagesViewModel.IsAllMessagesView() && $('.message-preview-container.New').length == 0)
						systemMessagesViewModel.NotifyNoMoreNewMessages();
				}

				var messages = $(data);

				messages.find(".message-preview-image").each(function() {
					$(this).css("background-image", "url(" + $(this).data("src") + ")");
				});

				$('.messages-preview-container').append(messages);

				if (!systemMessagesViewModel.IsAllMessagesView() && messages.length < systemMessagesViewModel.pageSize && $('.message-preview-container.New').length == 0)
					systemMessagesViewModel.NotifyNoMoreNewMessages();

				layout.initMessagesPreviewScrollPane();
				systemMessagesViewModel.currentPage++;
			},

			complete: function() {
				if (callback && typeof(callback) === "function")
					callback();
				systemMessagesViewModel.isAjaxCallActive = false;
			}
		});
	},

	UpdateConversationStatus: function(conversationId, status, callBack) {
		$.ajax({
			type: 'post',
			url: '/-/conversation/update-status',
			data: { conversationId: conversationId, status: status },

			success: function() {
				callBack();
			}
		});
	},

	BindGlobalEvents: function() {
		$.pm.bind("viewAllSystemMessages", function() {
			$('.view-all-container').click();
		});

		$.pm.bind("clearConversation", function(conversationId) {
			$('.conversations-non-selected').show();
			$('.messages-frame').attr('src', '');
			$('.messages-frame').hide();

			var conversation = $('.conversation[data-conversation-id="' + conversationId + '"]:visible').first();
			var isSystemConversation = conversation.data('is-system-message');
			var nextConversation = conversation.nextAll('.conversation[data-is-system-message =' + isSystemConversation + ']:visible').first();

			var previousConversation = conversation.prevAll('.conversation[data-is-system-message =' + isSystemConversation + ']:visible').first();
			var nextConversationToDisplay = nextConversation;

			if (nextConversation.length == 0)
				nextConversationToDisplay = previousConversation;

			conversation.slideUp(250, function() {
				conversation.remove();
				if ($('.conversation').length == 0) {
					$('.empty-mailbox').slideDown();
					$('#more').hide();
				} else {
					if (nextConversationToDisplay.length != 0) {
						nextConversationToDisplay.click();
					} else {
						$('.messages-frame').attr('src', '/mailbox/all');
						$('.messages-frame').show();
					}
				}
			});

			layout.initConversationsScrollPane();
		});

		$.pm.bind("markConversationAsRead", function(conversationId) {
			systemMessagesViewModel.UpdateConversationStatus(conversationId, 0, function() {
				var conversation = $('[data-conversation-id="' + conversationId + '"]');
				conversation.removeClass('unread').addClass('read');
			});
		});

		$.pm.bind("removeNewMessageIndicator", function(conversationId) {
			systemMessagesViewModel.UpdateConversationStatus(conversationId, 1, function() {
				var conversation = $('[data-conversation-id="' + conversationId + '"]');
				conversation.removeClass('new').addClass('unread');
			});
		});

		$.pm.bind("submitOpenConversationVirtualForm", function(conversationId) {
			systemMessagesViewModel.SubmitOpenConversationVirtualForm(conversationId, '');
		});
	},

	BindSystemMessagePageEvents: function() {
		systemMessageBrowsingViewModel.BindSystemMessagePageEvents();

		$('.archive').click(function() {
			analyticsReports.report("general-action-report", { name: "System Message Header > Archive" });
			$.ajax({
				type: 'GET',
				url: '/-/message/archive',
				data: { conversationId: $('.conversation-container').first().data('conversation-id'), messageId: $('#systemMessageContainer').data('message-id') },

				success: function(response) {
					if (response != "OK")
						return;

					var messageId = $('#systemMessageContainer').data('message-id');
					var nextSystemMessage = systemMessageBrowsingViewModel.GetNextSystemMessage();
					var previousSystemMessage = systemMessageBrowsingViewModel.GetPreviousSystemMessage();

					if (!systemMessagesViewModel.IsAllMessagesView()) {
						var totalMessagesElement = $('.total-messages')[0];
						var totalMessages = parseInt($(totalMessagesElement).text());
						totalMessages--;

						if (totalMessages <= 0) {
							$.pm({
								target: window.parent,
								type: 'clearConversation',
								data: $('.conversation-container').first().data('conversation-id')
							});

							return;
						}

						if (systemMessagesViewModel.currentMessage > totalMessages) {
							systemMessagesViewModel.currentMessage = totalMessages;
						}
					} else {

						if (nextSystemMessage.length == 0 && previousSystemMessage.length == 0) {
							$('.view-all-container').click();
							return;
						}
					}

					$('.message-preview-container[data-message-id="' + messageId + '"]').remove();
					if (nextSystemMessage.length == 0) {
						nextSystemMessage = previousSystemMessage;
					}
					$.ajax({
						type: 'GET',
						url: '/-/message/get-by-id',
						data: { conversationId: nextSystemMessage.data('conversation-id'), messageId: nextSystemMessage.data('message-id') },
						success: function(data) {
							systemMessagesViewModel.SetCurrentMessage(data);
						}
					});
				}
			});
		});

		$("body").keydown(function(e) {
			if ($(".conversation-container").is(":visible")) {
				if (e.keyCode == 46) { // delete button
					$('.archive').click();
				}
			}
		});

		$('.view-all-container').click(function() {
			analyticsReports.report("general-action-report", { name: "System Message Header > Grid" });

			$('.conversation-container').hide();
			$('.pager').hide();
			$('#messages-preview-scroll').show();

			if (systemMessagesViewModel.IsAllMessagesView()) {
				$('.all-messages-header').show();
				$('.header').hide();
			}
			layout.initMessagesPreviewScrollPane();
		});

		$(document).on('click', '.message-preview-container', function() {
			analyticsReports.report("general-action-report", { name: "System Message Header > Grid > Message" });

			return systemMessagesViewModel.MessagePreviewContainerClick($(this).data('conversation-id'), $(this).data('message-id'), $(this).data('is-system-message'));
		});

		if ($.browser.safari()) {
			$('#messages-preview-scroll').bind('scroll', function() {
				if ($(this).scrollTop() + $(this).innerHeight() >= this.scrollHeight)
					systemMessagesViewModel.BindMessagesPreviewScroll();
			});
		} else {
			$('#messages-preview-scroll').bind(
				'jsp-scroll-y',
				function(event, scrollPositionY, isAtTop, isAtBottom) {
					if (isAtBottom)
						systemMessagesViewModel.BindMessagesPreviewScroll();
				}
			);
		}
	},

	MessagePreviewContainerClick: function(conversationId, messageId, isSystemMessage) {

		if (systemMessagesViewModel.IsAllMessagesView() && isSystemMessage == 'False') {
			$.pm({
				target: window.parent,
				type: 'checkConversationSelected',
				data: conversationId
			});

			return systemMessagesViewModel.SubmitOpenConversationVirtualForm(conversationId, messageId);
		}

		$.ajax({
			type: 'GET',
			url: '/-/message/get-by-id',
			data: { conversationId: conversationId, messageId: messageId },
			success: function(data) {
				systemMessagesViewModel.SetCurrentMessage(data);
				$('#messages-preview-scroll').hide();
				$('.conversation-container').show();
				$('.pager').show();
			}
		});
	},

	BindMessagesPreviewScroll: function() {
		if (systemMessagesViewModel.isAjaxCallActive) return;
		if (systemMessagesViewModel.isReachedEnd) return;

		analyticsReports.report("general-action-report", { name: "System Message Header > Grid > Scroll" });
		systemMessagesViewModel.MoreMessages();
	},

	IsAllMessagesView: function() {
		return ($('.system-message.all-messages').length > 0);
	},

	SubmitOpenConversationVirtualForm: function(conversationId, messageId) {

	    var form = $('<form name="dummyForm" method="post" action="/mailbox/conversations/" target="messages-frame">' +
			'<input type="hidden" name="conversationId" value="' + conversationId + '">' +
			'<input type="hidden" name="messageId" value="' + messageId + '">' +
			'</form>');

		$('body').append(form);

		document.dummyForm.submit();
	}
};

$(function() {
	systemMessagesViewModel.BindGlobalEvents();

	if ($('.system-message').length == 0)
		return;

	systemMessagesViewModel.BindSystemMessagePageEvents();
	systemMessagesViewModel.MoreMessages();

	$('#messages-preview-scroll').show();
	layout.initMessagesPreviewScrollPane();
});