﻿//knows to add the timezone cookie.
//the jquery.cookie,.js api knows to do this once (without any problems)
//the cookie will save the user's time-zone so it will be easier to calc him dates and times in client side.
$(function() {

	function addTimeZoneCookie() {
		var cookieName = "messages_locale_tz";
		var currDate = new Date();
		var timeZoneOffset = getTimeZoneOffset();

		var currentTimezoneOffset = $.cookie(cookieName);

		// If timezone has changed, remove the cookie so it could be reseted
		if (currentTimezoneOffset &&
			(currentTimezoneOffset != timeZoneOffset)) {
			$.cookie(cookieName, null);
		}

		var expirationTime = tommorowAtOneAM(currDate);

		var options = { path: '/', expires: expirationTime };
		$.cookie(cookieName, timeZoneOffset, options);
	}

	;

	//    addTimeZoneCookie;
	addTimeZoneCookie();

	// the takes the timezone (js timezone offset will look like this -180 for GMT+3,
	// we should divide by 60 (minutes) and multiply by 1. (to see the offset and not the [utc - local] subtraction.
	// if the time zone is 5:45.

	function getTimeZoneOffset() {
		var offsetInMinutes = new Date().getTimezoneOffset();
		var timeZoneOffset = offsetInMinutes / 60; // translate from minutes to hours
		var offset = -1 * timeZoneOffset;
		return offset;
	}

	;

	//set the time to tommorow at 1 AM.
	// (so the cookie will be cleaned before DayLightSaving)

	function tommorowAtOneAM(yourDate) {
		var tommorow = new Date();
		tommorow.setFullYear(yourDate.getFullYear());
		tommorow.setMonth(yourDate.getMonth());
		tommorow.setDate(yourDate.getDate() + 1)
		tommorow.setHours(1);
		tommorow.setMinutes(0);
		tommorow.setSeconds(0);
		return tommorow;
	}

	;
});