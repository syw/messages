namespace SYW.App.Messages.Web.Services.Cms
{
	public enum ActionType
	{
		Create,
		Delete
	}
}