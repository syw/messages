﻿using System.Collections.Generic;
using System.Web.Mvc;
using SYW.App.Messages.Web.Models.Cms;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.Web.Services.Cms
{
	public interface IBauEmailFeedObjectCreationViewModelCreator
	{
		BauEmailFeedObjectViewModel Create();
	}

	public class BauEmailFeedObjectCreationViewModelCreator : IBauEmailFeedObjectCreationViewModelCreator
	{

		private readonly IBauEntityProvider _bauEntityProvider;

		public BauEmailFeedObjectCreationViewModelCreator(IBauEntityProvider bauEntityProvider)
		{
			_bauEntityProvider = bauEntityProvider;
		}

		public BauEmailFeedObjectViewModel Create()
		{
			var publishers = _bauEntityProvider.GetAll();
			return new BauEmailFeedObjectViewModel
			{
				Publisher = new SelectList(publishers, "_id", "BauType", 0),
				TargetActionName = "SaveBauEmailFeedObject",
				TargetControllerName = "SystemMessage",
				ActionTypes = new List<ActionType>{ActionType.Create}
			};
		}
	}
}