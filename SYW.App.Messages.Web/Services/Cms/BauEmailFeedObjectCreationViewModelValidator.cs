﻿using System;
using System.Web.WebPages;
using SYW.App.Messages.Web.Models.Cms;

namespace SYW.App.Messages.Web.Services.Cms
{
	public interface IBauEmailFeedObjectCreationViewModelValidator
	{
		void Validate(BauEmailFeedObjectViewModel bauEmailFeedObjectViewModel);
	}

	public class BauEmailFeedObjectCreationViewModelValidator : IBauEmailFeedObjectCreationViewModelValidator
	{
		public void Validate(BauEmailFeedObjectViewModel bauEmailFeedObjectViewModel)
		{
			if (bauEmailFeedObjectViewModel.Title.IsEmpty())
				throw new Exception("Title cannot be null or empty!");
			if (bauEmailFeedObjectViewModel.Body.IsEmpty())
				throw new Exception("Body cannot be null or empty!");
		}
	}
}