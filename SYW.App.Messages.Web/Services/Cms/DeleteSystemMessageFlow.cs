﻿using System;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Audits;
using SYW.App.Messsages.Domain.DataAccess.Audits;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.Services.Cms
{
	public interface IDeleteSystemMessageFlow
	{
		void Delete(ObjectId messageId);
	}

	public class DeleteSystemMessageFlow : IDeleteSystemMessageFlow
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IAuditRepository _auditRepository;
		private readonly IMessagesService _messagesService;

		public DeleteSystemMessageFlow(
			IMessagesService messagesService,
			IAuditRepository auditRepository,
			IEntityContextProvider entityContextProvider)
		{
			_messagesService = messagesService;
			_auditRepository = auditRepository;
			_entityContextProvider = entityContextProvider;
		}

		public void Delete(ObjectId messageId)
		{
			_messagesService.Delete(messageId);
			var deletedValueAuditMessage = String.Format("Deleted message id is: {0}", messageId);
			_auditRepository.Save(new Audit
				{
					UserId = _entityContextProvider.CurrentEntity().OriginalId,
					ActionPerformed = ActionPerformed.DeleteSystemMessage.ToString(),
					Time = DateTime.Now,
					NewValuesJsonString = deletedValueAuditMessage.ToJson()
				});
		}
	}
}

