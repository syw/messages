﻿using System.Collections.Generic;
using System.Drawing;
using System.Web.Mvc;
using MongoDB.Bson;
using SYW.App.Messages.Web.Models.Cms;
using SYW.App.Messsages.Domain.Feeds;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.Services.Cms
{
	public interface ISystemMessageRemovalViewModelCreator
	{
		BauEmailFeedObjectViewModel Create(string messageId);
	}

	public class SystemMessageRemovalViewModelCreator : ISystemMessageRemovalViewModelCreator
	{
		private readonly IBauEntityProvider _bauEntityProvider;
		private readonly IMessagesService _messagesService;

		public SystemMessageRemovalViewModelCreator(
			IBauEntityProvider bauEntityProvider,
			IMessagesService messagesService)
		{
			_bauEntityProvider = bauEntityProvider;
			_messagesService = messagesService;
		}

		public BauEmailFeedObjectViewModel Create(string messageId)
		{
			var model = new BauEmailFeedObjectViewModel();
			var publishers = _bauEntityProvider.GetAll();

			var validationResult = Validate(messageId);
			if (validationResult != "ok")
			{
				model.Publisher = new SelectList(publishers, "_id", "BauType", 0);
				model.ActionTypes = new List<ActionType> { ActionType.Create };
				model.PostResult = validationResult;
				model.PostResultColor = Color.Red;
				return model;
			}

			var message = _messagesService.GetMessageById(messageId);
			var bauEntity = _bauEntityProvider.GetByEntityId(message.Author._id);

			return new BauEmailFeedObjectViewModel
			{
				Publisher = new SelectList(publishers, "_id", "BauType", bauEntity._id),
				MessageId = messageId,
				Title = message.PreviewText,
				Body = message.Content,
				ActionTypes = new List<ActionType> { ActionType.Delete },
				PostResultColor = Color.Green
			};
		}

		private string Validate(string messageId)
		{
			var messageObjectId = new ObjectId();
			if (!ObjectId.TryParse(messageId, out messageObjectId))
			{
				return "'" + messageId + "'" + " cannot be parsed as valid 24 digit hex string";
			}

			var message = _messagesService.GetMessageById(messageId);
			if (message == null)
			{
				return "Message id does not exists";
			}

			var bauEntity = _bauEntityProvider.GetByEntityId(message.Author._id);
			if (bauEntity == null)
			{
				return "Could not find bauEntity";
			}
			return "ok";
		}
	}
}