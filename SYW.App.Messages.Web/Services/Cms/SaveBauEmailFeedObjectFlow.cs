﻿using System;
using System.Web;
using MongoDB.Bson;
using SYW.App.Messages.Web.Models.Cms;
using SYW.App.Messsages.Domain.Audits;
using SYW.App.Messsages.Domain.DataAccess.Audits;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messages.Web.Services.Cms
{
	public interface ISaveBauEmailFeedObjectFlow
	{
		BauEmailFeedObjectViewModel Save(BauEmailFeedObjectViewModel bauEmailFeedObjectViewModel);
	}

	public class SaveBauEmailFeedObjectFlow : ISaveBauEmailFeedObjectFlow
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IAuditRepository _auditRepository;
		private readonly IBauEntityProvider _bauEntityProvider;
		private readonly IEntityProvider _entityProvider;
		private readonly IBauEmailFeedObjectUpdater _bauEmailFeedObjectUpdater;
		private readonly IBauEmailFeedObjectCreationViewModelValidator _bauEmailFeedObjectCreationViewModelValidator;
		

		public SaveBauEmailFeedObjectFlow(
			IBauEntityProvider bauEntityProvider,
			IEntityProvider entityProvider, 
			IBauEmailFeedObjectUpdater bauEmailFeedObjectUpdater,
			IBauEmailFeedObjectCreationViewModelValidator bauEmailFeedObjectCreationViewModelValidator, 
			IEntityContextProvider entityContextProvider, 
			IAuditRepository auditRepository)
		{
			_bauEntityProvider = bauEntityProvider;
			_entityProvider = entityProvider;
			_bauEmailFeedObjectUpdater = bauEmailFeedObjectUpdater;
			_bauEmailFeedObjectCreationViewModelValidator = bauEmailFeedObjectCreationViewModelValidator;
			_entityContextProvider = entityContextProvider;
			_auditRepository = auditRepository;
		}


		public BauEmailFeedObjectViewModel Save(BauEmailFeedObjectViewModel bauEmailFeedObjectViewModel)
		{
			try
			{
				bauEmailFeedObjectViewModel.Body = HttpUtility.UrlDecode(bauEmailFeedObjectViewModel.Body);
				_bauEmailFeedObjectCreationViewModelValidator.Validate(bauEmailFeedObjectViewModel);
				CreateBauEmailFeedObject(bauEmailFeedObjectViewModel);

				_auditRepository.Save(new Audit
				{
					UserId = _entityContextProvider.CurrentEntity().OriginalId,
					ActionPerformed = ActionPerformed.SaveBauEmailFeedObject.ToString(),
					Time = DateTime.Now,
					NewValuesJsonString = bauEmailFeedObjectViewModel.ToJson()
				});
			}
			catch (Exception ex)
			{
				bauEmailFeedObjectViewModel.PostResult = "There was a problem with uploading system message: " + ex.Message;
				bauEmailFeedObjectViewModel.PostResultColor = System.Drawing.Color.Red;
			}
			return bauEmailFeedObjectViewModel;

		}
		private void CreateBauEmailFeedObject(BauEmailFeedObjectViewModel bauEmailFeedObjectViewModel)
		{
			var bauEntity = _bauEntityProvider.GetEntityById(ObjectId.Parse(bauEmailFeedObjectViewModel.PublisherSelectedId));
			if (bauEntity == null)
				throw new Exception("There was a problem getting bauEntity by Objectid");
			var entity = _entityProvider.GetEntityById(bauEntity.EntityId);
			_bauEmailFeedObjectUpdater.Create(new BauEmailFeedObject
			{
				BrandName = entity.Name,
				EmailId = ObjectId.GenerateNewId().ToString(),
				Content = bauEmailFeedObjectViewModel.Body,
				Logo = entity.ImageUrl,
				Published = DateTime.Now,
				SubjectLine = bauEmailFeedObjectViewModel.Title
			});

			bauEmailFeedObjectViewModel.PostResult = "Your message was uploaded successfully";
			bauEmailFeedObjectViewModel.PostResultColor = System.Drawing.Color.Green;
		}
	}
}