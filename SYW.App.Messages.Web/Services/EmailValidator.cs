using System;
using System.Linq;
using System.Text.RegularExpressions;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Entities;

namespace SYW.App.Messages.Web.Services
{
	public enum EmailValidationStatus
	{
		Ok,
		Bad,
		Exists
	}

	public interface IEmailValidator
	{
		EmailValidationStatus ValidateEmail(string email);
	}

	public class EmailValidator : IEmailValidator
	{
		private readonly IEntityContextProvider _entityContextProvider;
		private readonly IEntitiesRepository _entitiesRepository;
		private readonly IBlacklistEmailsRepository _blacklistEmailsRepository;
		private static readonly Regex emailRegex = new Regex("^[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);

		public EmailValidator(IEntityContextProvider entityContextProvider, IEntitiesRepository entitiesRepository, IBlacklistEmailsRepository blacklistEmailsRepository)
		{
			_entityContextProvider = entityContextProvider;
			_entitiesRepository = entitiesRepository;
			_blacklistEmailsRepository = blacklistEmailsRepository;
		}

		public EmailValidationStatus ValidateEmail(string email)
		{
			if (string.IsNullOrEmpty(email))
				return EmailValidationStatus.Bad;

			if (email.StartsWith("@"))
				return EmailValidationStatus.Bad;

			if (!emailRegex.IsMatch(email))
			{
				return EmailValidationStatus.Bad;
			}

			var entity = _entitiesRepository.Get(_entityContextProvider.CurrentEntity().Id);
			if (email.Equals(entity.Email))
				return EmailValidationStatus.Ok;

			var result = _entitiesRepository.GetEntitiesIdsByEmail(new[] {email}).EmptyIfNull().Count();
			if (result > 0)
				return EmailValidationStatus.Exists;

			var inBlackList = _blacklistEmailsRepository.IsEmailInBlackList(email);
			if (inBlackList)
				return EmailValidationStatus.Exists;

			return EmailValidationStatus.Ok;
		}
	}
}