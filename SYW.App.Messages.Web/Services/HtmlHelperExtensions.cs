﻿using System.Web.Mvc;
using Microsoft.Security.Application;

namespace SYW.App.Messages.Web.Services
{
	public static class HtmlHelperExtensions
	{
		public static string Linkifier<T>(this HtmlHelper<T> htmlHelper, string content)
		{
			return content;
		}

		public static string Truncate<T>(this HtmlHelper<T> htmlHelper, string content, int length)
		{
			if (content == null)
				return string.Empty;

			if (content.Length < length)
				return content;

			if (content.Contains("\\s") == false)
				return content.Substring(0, length - 3) + "...";

			var truncated = content.Substring(0, length - 3);
			if (!truncated.Contains("\\s"))
				return truncated + "...";

			return truncated.Substring(0, truncated.LastIndexOf("\\s")) + "...";
		}

		public static string Xss<T>(this HtmlHelper<T> htmlHelper, string content)
		{
			return Encoder.HtmlEncode(content);
		}
	}
}