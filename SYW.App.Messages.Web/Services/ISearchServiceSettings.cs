﻿using CommonGround.Settings;

namespace SYW.App.Messages.Web.Services
{
	public interface ISearchServiceSettings
	{
		[Default(5)]
		int MaxFriendsSearchResults { get; set; }
	}
}