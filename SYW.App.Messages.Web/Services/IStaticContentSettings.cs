﻿using CommonGround.Settings;

namespace SYW.App.Messages.Web.Services
{
	public interface IStaticContentSettings
	{
		[Default("")]
		string StaticContent { get; set; }

		[Default("1")]
		string Version { get; set; } 

		[Default("http://images.shopyourway.com/static/img/fusion/missing-images/user.png")]
		string MissingImageUrl{ get; set; } 
	}
}