﻿using System.Linq;
using MongoDB.Bson;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messages.Web.Services
{
	public interface IMessagePreviewImageService
	{
		string InitMessagePreviewImage(Message message, IConversation conversation, ObjectId userId);
	}

	public class MessagePreviewImageService : IMessagePreviewImageService
	{
		private readonly IStaticContentSettings _staticContentSettings;
		private readonly IMessageRepository _messageRepository;

		public MessagePreviewImageService(IStaticContentSettings staticContentSettings, IMessageRepository messageRepository)
		{
			_staticContentSettings = staticContentSettings;
			_messageRepository = messageRepository;
		}

		public string InitMessagePreviewImage(Message message, IConversation conversation, ObjectId userId)
		{
			if (conversation == null)
				return string.Empty;

			if (conversation.IsSystemMessage)
				return _staticContentSettings.StaticContent + "/content/screenshots/" + message.Id + ".jpg?404=/Content/sears-logo.png&width=270&v=" + _staticContentSettings.Version;

			var otherParticipants = conversation.Participants.Where(x => x._id != userId).ToList();

			if (message.Author != null && (message.Author._id != userId || (!otherParticipants.Any())) && !string.IsNullOrEmpty(message.Author.ImageUrl))
				return message.Author.ImageUrl;

			if (message.Author != null && message.Author._id == userId && otherParticipants.Any())
			{
				var lastSendersAuthorsIds = _messageRepository.GetOrderedConversationSenders(conversation.Id).Where(c => c != userId).EmptyIfNull().ToList();

				if(lastSendersAuthorsIds.Any())
				{
					var lastSenderEntity = conversation.Participants.FirstOrDefault(p => p._id == lastSendersAuthorsIds.First());
					if (lastSenderEntity != null && !string.IsNullOrEmpty(lastSenderEntity.ImageUrl))
						return lastSenderEntity.ImageUrl;
				}
			}
			 
			return _staticContentSettings.MissingImageUrl;
		}
	}
}