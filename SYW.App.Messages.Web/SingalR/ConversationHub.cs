﻿using SYW.App.Messsages.Domain.Entities;
using SignalR.Hubs;
using System.Linq;
using System;

namespace SYW.App.Messages.Web.SingalR
{
	[HubName("conversation")]
	public class ConversationHub : Hub
	{
		public void Join(string conversationId, string entityId, string entityName, string entityImage)
		{
			Caller.ConversationId = conversationId;
			Caller.EntityId = entityId;
			Caller.EntityName = entityName;
			Caller.EntityImage = entityImage;

			Groups.Add(Context.ConnectionId, conversationId);
		}

		public void Typing()
		{
			Clients[Caller.ConversationId].addMessage(new ConversationState
														{
															Status = 1,
															EntityId = Caller.EntityId,
															EntityName = Caller.EntityName,
															EntityImage = Caller.EntityImage
														});
		}

		public void Untyping()
		{
			Clients[Caller.ConversationId].addMessage(new ConversationState
														{
															Status = 2,
															EntityId = Caller.EntityId,
															EntityName = Caller.EntityName,
															EntityImage = Caller.EntityImage
														});
		}

		public void SendMessage(string conversationId, string content)
		{
			Clients[Caller.ConversationId].addMessage(new ConversationState
														{
															Status = 3,
															EntityId = Caller.EntityId,
															EntityName = Caller.EntityName,
															EntityImage = Caller.EntityImage,
															Message = content,
                                                            DateTime = DateTime.Now
														});
		}
	}

	public class ConversationState
	{
		public int Status { get; set; }
		public string Message { get; set; }
		public string EntityId { get; set; }
		public string EntityName { get; set; }
		public string EntityImage { get; set; }
        public DateTime DateTime { get; set; }
	}
}