﻿using System;
using System.Linq;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SignalR;
using log4net;

namespace SYW.App.Messages.Web.SingalR
{
	public interface IConversationNotifierService
	{
		void Notify(string conversationId, ObjectId authorId);
		void Notify(string conversationId, ObjectId authorId, bool notifyAuthor);
	}

	public class ConversationNotifierService : IConversationNotifierService
	{
		private readonly IConversationService _conversationService;
		private readonly IConversationStatusService _conversationStatusService;
		private readonly ILog _log;

		public ConversationNotifierService(IConversationService conversationService, ILog log, IConversationStatusService conversationStatusService)
		{
			_conversationService = conversationService;
			_log = log;
			_conversationStatusService = conversationStatusService;
		}

		public void Notify(string conversationId, ObjectId authorId)
		{
			Notify(conversationId, authorId, false);
		}

		public void Notify(string conversationId, ObjectId authorId, bool notifyAuthor)
		{
			try
			{
				var conversation = _conversationService.Get(conversationId, authorId);

				if (!notifyAuthor)
					conversation.Participants = conversation.Participants.Where(p => p._id != authorId).ToList();

				var context = GlobalHost.ConnectionManager.GetHubContext<ConversationsHub>();

				foreach (var participant in conversation.Participants)
				{
					try
					{
						context.Clients[participant._id].say(conversation.Id);
					}
					catch (Exception exception)
					{
						_log.Warn("Couldn't notify " + participant._id + " all participants on new conversation", exception);
					}

					_conversationStatusService.DeleteStatus(ObjectId.Parse(conversationId), participant._id);
				}
			}
			catch (Exception exception)
			{
				_log.Warn("Couldn't notify all participants on new conversation", exception);
			}
		}
	}
}