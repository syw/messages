using SignalR.Hubs;

namespace SYW.App.Messages.Web.SingalR
{
	[HubName("conversations")]
	public class ConversationsHub : Hub
	{
		public void Join(string entityId)
		{
			Caller.EntityId = entityId;

			Groups.Add(Context.ConnectionId, entityId);
		}
	}
}