﻿using System;
using System.ComponentModel;
using CommonGround;
using SignalR;

namespace SYW.App.Messages.Web.SingalR
{
	public class SingalRDependencyResolver : DefaultDependencyResolver
	{
		private readonly ICommonContainer _container;

		public SingalRDependencyResolver(ICommonContainer container)
		{
			if (container == null)
			{
				throw new ArgumentNullException("container");
			}

			_container = container;
		}

		public override object GetService(Type serviceType)
		{
			return _container.Resolve(serviceType);
		}
	}
}