﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.Audits
{
	public class Audit
	{
		[BsonId]
		public ObjectId Id { get; set; }
		public long? UserId { get; set; }
		public string ActionPerformed { get; set; }
		public string NewValuesJsonString { get; set; }
		public DateTime Time { get; set; }
	}
}