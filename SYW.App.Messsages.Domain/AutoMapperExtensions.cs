﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;

namespace SYW.App.Messsages.Domain
{
	public static class AutoMapperExtensions
	{
		public static IEnumerable<TR> AutoMap<T, TR>(this IEnumerable<T> enumerable)
		{
			if (enumerable == null || enumerable.Any() == false)
				return Enumerable.Empty<TR>();

			return enumerable.Where(t => t != null)
							.Select(t => Mapper.Map<T, TR>(t))
							.Where(tr => tr != null);
		}
	}
}