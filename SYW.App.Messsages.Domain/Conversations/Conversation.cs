using System;
using System.Collections.Generic;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.Conversations
{
	public class Conversation : IConversation
	{
		public ObjectId Id { get; set; }

		public IList<Entity> Participants { get; set; }

		public IList<Message> Messages { get; set; }

		public bool IsSystemMessage { get; set; }

		public DateTime Timestamp { get; set; }

        public DateTime StartDate { get; set; }
    }
}