using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;

namespace SYW.App.Messsages.Domain.Conversations
{
	public interface IConversationCounterService
	{
		int CountUnread(ObjectId entityId);
		int CountPersonalConversations(ObjectId entityId, IEnumerable<ConversationStatus> statuses);
		int CountSystemMessages(ObjectId userId, IEnumerable<ConversationStatus> statuses);
	}

	/// <summary>
	/// The ConversationCounterService is responsible for cacluatlting the personal (not global) notification counter
	/// This means that it is trigger when the user is viewing or changing the status of a 
	/// conversation, message from the system.
	/// For each view we are re-calculating the amount of unread/new conversation / system messages and
	/// notifying the platform in a different service.
	/// </summary>
	public class ConversationCounterService : IConversationCounterService
	{
		private readonly IUserMessageStatusRepository _userMessageStatusRepository;
		private readonly IUserConversationStatusRepository _userConversationStatusRepository;
		private readonly IConversationRepository _conversationRepository;
		private readonly ICacheSystemMessageProvider _cacheSystemMessageProvider;

		public ConversationCounterService(
			IUserMessageStatusRepository userMessageStatusRepository, 
			IUserConversationStatusRepository userConversationStatusRepository,
			IConversationRepository conversationRepository,
			ICacheSystemMessageProvider cacheSystemMessageProvider)
		{
			_userMessageStatusRepository = userMessageStatusRepository;
			_userConversationStatusRepository = userConversationStatusRepository;
			_conversationRepository = conversationRepository;
			_cacheSystemMessageProvider = cacheSystemMessageProvider;
		}

		public int CountUnread(ObjectId entityId)
		{
			var personalConversationCount = CountPersonalConversations(entityId, new[] {ConversationStatus.Unread, ConversationStatus.New});

		    return personalConversationCount;
		}

		public int CountPersonalConversations(ObjectId entityId, IEnumerable<ConversationStatus> statuses)
		{
			var conversations = _conversationRepository.GetPersonalConversations(entityId);

			foreach (var conversation in conversations)
				conversation.Status = ConversationStatus.Unread;

			var convesationStatuses = _userConversationStatusRepository.Get(conversations.Select(c => c.Id).ToArray(), entityId)
																		.OrderByDescending(s => s.Date)
																		.GroupBy(c => c.ConversationId)
																		.Select(g => g.First())
																		.EmptyIfNull()
																		.ToArray();

			conversations.JoinDo(convesationStatuses, c => c.Id, s => s.ConversationId, (m, s) => m.Status = (ConversationStatus)s.Status);
			return conversations.Join(statuses, c => c.Status, s => s, (c, s) => c.Status = s).Count();
		}

		public int CountSystemMessages(ObjectId userId, IEnumerable<ConversationStatus> statuses)
		{
			var messages = _cacheSystemMessageProvider.GetAll();
			var messagesStatuses = _userMessageStatusRepository.GetAll(userId);

			messages.JoinDo(messagesStatuses, m => m.Id, s => s.MessageId, (m, s) => m.Status = (ConversationStatus)s.Status);

			return messages.Join(statuses, m => m.Status, s => s, (m, s) => m.Status = s).Count();
		}
	}
}