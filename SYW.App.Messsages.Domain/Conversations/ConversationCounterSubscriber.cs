using System;
using System.Diagnostics;
using CommonGround.Events;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Services.Platform;
using log4net;

namespace SYW.App.Messsages.Domain.Conversations
{
    public class ConversationCounterSubscriber : IEventSubscriber
    {
        private readonly IEvent<ConversationStatusChangedEvent> _conversationStatusChangedEvent;

        private readonly IConversationCounterUpdater _conversationCounterUpdater;

        public ConversationCounterSubscriber(IEvent<ConversationStatusChangedEvent> conversationStatusChangedEvent, IConversationCounterUpdater conversationCounterUpdater)
        {
            _conversationStatusChangedEvent = conversationStatusChangedEvent;
            _conversationCounterUpdater = conversationCounterUpdater;
        }

        public void Subscribe()
        {
            _conversationStatusChangedEvent.Subscribe(x => _conversationCounterUpdater.UpdateUnreadNumber(x.AuthorId));
        }
    }
}