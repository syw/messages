﻿using System;
using System.Diagnostics;
using log4net;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Services.Platform;

namespace SYW.App.Messsages.Domain.Conversations
{
    public interface IConversationCounterUpdater
    {
        void UpdateUnreadNumber(ObjectId entityId);
        void UpdateUserNotification(ObjectId entityId, int unreadCounter);
    }

    public class ConversationCounterUpdater : IConversationCounterUpdater
    {
        private readonly ILog _logger;
        private readonly IAppLinksService _appLinksService;
        private readonly IOfflineTokenProvider _offlineTokenProvider;
        private readonly IConversationCounterService _conversationCounterService;

        public ConversationCounterUpdater(ILog logger, IAppLinksService appLinksService, IOfflineTokenProvider offlineTokenProvider, IConversationCounterService conversationCounterService)
        {
            _logger = logger;
            _appLinksService = appLinksService;
            _offlineTokenProvider = offlineTokenProvider;
            _conversationCounterService = conversationCounterService;
        }

        public void UpdateUnreadNumber(ObjectId entityId)
        {
            var unreadCounter = _conversationCounterService.CountUnread(entityId);

            UpdateUserNotification(entityId, unreadCounter);
        }

        public void UpdateUserNotification(ObjectId entityId, int unreadCounter)
        {
            try
            {
                _logger.Info("updating user's notifications - userid=" + entityId + "; count=" + unreadCounter);

                UpdateEntityOfflineToken(entityId);

                try
                {
                    _appLinksService.UpdateNotifications(entityId, unreadCounter);
                }
                catch (Exception e)
                {
                    _logger.Warn("Couldn't update user " + entityId + " conversations counter", e);
                }
            }
            catch (Exception)
            {
                Trace.TraceInformation("An error occurred while trying to update the notifications...");

                throw;
            }
        }

        private void UpdateEntityOfflineToken(ObjectId userId)
        {
            try
            {
                _offlineTokenProvider.UpdateEntityOfflineToken(userId);
            }
            catch (Exception ex)
            {
                _logger.Warn("failed to update offline token for user " + userId, ex);
            }
        }
    }
}
