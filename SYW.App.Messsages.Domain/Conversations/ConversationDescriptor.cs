﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Entities;
using System.Linq;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.Conversations
{
	public class ConversationDescriptor : IConversation
	{
		public ObjectId Id { get; set; }

		public IList<Entity> Participants { get; set; }

		private IList<Message> _messages;

		public IList<Message> Messages
		{
			get
			{
				if (_messages == null)
					_messages = new[] {_lastMessage};
				return _messages;
			}
			set
			{
				_messages = value;
				_lastMessage = _messages.EmptyIfNull().FirstOrDefault();
			}
		}

		private Message _lastMessage;

		public Message LastMessage
		{
			get
			{
				if (_lastMessage == null)
					_lastMessage = Messages.EmptyIfNull().FirstOrDefault();
				return _lastMessage;
			}
			set { _lastMessage = value; }
		}

		public ConversationStatus Status { get; set; }

		public bool IsSystemMessage { get; set; }

        public DateTime Timestamp { get; set; }

	    public DateTime StartDate { get; set; }
    }
}