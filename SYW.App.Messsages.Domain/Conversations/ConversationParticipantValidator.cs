﻿using System.Linq;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;

namespace SYW.App.Messsages.Domain.Conversations
{
	public interface IConversationParticipantValidator
	{
		void Validate(ObjectId entityId, ObjectId conversationId);
	}

	public class ConversationParticipantValidator : IConversationParticipantValidator
	{
		private readonly IConversationRepository _conversationRepository;

		public ConversationParticipantValidator(IConversationRepository conversationRepository)
		{
			_conversationRepository = conversationRepository;
		}

		public void Validate(ObjectId entityId, ObjectId conversationId)
		{
			var conversation = _conversationRepository.Get(conversationId);

			if (conversation.IsSystemMessage == false && conversation.Participants.EmptyIfNull().All(x => x._id != entityId))
				throw new EntityIsNotPartOfConversationException(entityId, conversationId);
		}
	}
}