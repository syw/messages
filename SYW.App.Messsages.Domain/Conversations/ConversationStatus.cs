namespace SYW.App.Messsages.Domain.Conversations
{
	public enum ConversationStatus
	{
		Read = 0,
		Unread = 1,
		Unresolved = 2,
		Archive = 3,
		New = 4,
		Temp = 5 // (Should never be inserted into the mongo db)
	}
}