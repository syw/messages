using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Conversations
{
	public class ConversationStatusChangedEvent
	{
		public ObjectId ConversationId { get; set; }
		public ObjectId AuthorId { get; set; }
	}
}