﻿using System.Collections.Generic;
using CommonGround.Events;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using System.Linq;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Exceptions;
using SYW.App.Messsages.Domain.Messages;
using StackExchange.Profiling;

namespace SYW.App.Messsages.Domain.Conversations
{
    public interface IConversationStatusService
    {
        void UpdateStatus(ObjectId conversationId, ObjectId entityId, ConversationStatus status);
        void ArchiveAll(ObjectId userId);
        void MarkAllAsRead(ObjectId userId);
        void DeleteStatus(ObjectId conversationId, ObjectId userId);
    }

    public class ConversationStatusService : IConversationStatusService
    {
        private readonly IUpdateConversationStatusService _updateConversationStatusService;
        private readonly IConversationService _conversationService;
        private readonly IConversationRepository _conversationRepository;
        private readonly IMongoStorage<UserConversationStatus, ObjectId> _userConversationStatusStorage;
        private readonly IEvent<ConversationStatusChangedEvent> _conversationStatusChangedEvent;
        private readonly IMessagesService _messagesService;
        private readonly IMongoStorage<UserMessageStatus, ObjectId> _userMessageStatusStorage;

        public ConversationStatusService(IUpdateConversationStatusService updateConversationStatusService, IConversationService conversationService, IMongoStorage<UserConversationStatus, ObjectId> userConversationStatusStorage, IEvent<ConversationStatusChangedEvent> conversationStatusChangedEvent, IConversationRepository conversationRepository, IMessagesService messagesService, IMongoStorage<UserMessageStatus, ObjectId> userMessageStatusStorage)
        {
            _updateConversationStatusService = updateConversationStatusService;
            _conversationService = conversationService;
            _userConversationStatusStorage = userConversationStatusStorage;
            _conversationStatusChangedEvent = conversationStatusChangedEvent;
            _conversationRepository = conversationRepository;
            _messagesService = messagesService;
            _userMessageStatusStorage = userMessageStatusStorage;
        }

        public void UpdateStatus(ObjectId conversationId, ObjectId entityId, ConversationStatus status)
        {
            var conversation = _conversationRepository.Get(conversationId);

            if (conversation == null)
                throw new ConversationNotFoundException(conversationId);

            if (!conversation.IsSystemMessage && conversation.Participants.EmptyIfNull().All(x => x._id != entityId))
                throw new ForbiddenArchiveConversationException(conversation.Id, entityId);

            _updateConversationStatusService.UpdateStatus(conversationId, entityId, status);
        }

        public void ArchiveAll(ObjectId userId)
        {
            var statuslessConversations = _conversationService.GetAllWithNoStatus(userId);
            var statuslessMessages = _messagesService.GetAllWithNoStatus(userId);

            InsertConversationStatus(statuslessConversations, ConversationStatus.Archive, userId);
            InsertMessagesStatus(statuslessMessages, ConversationStatus.Archive, userId);

            using (MiniProfiler.Current.Step("_userConversationStatusRepository.UpdateAll"))
            {
                var filterListConversationStatusStorage = new List<FilterDefinition<UserConversationStatus>>
					             {
									 Builders<UserConversationStatus>.Filter.Eq(x=>x.UserId,userId),
									 Builders<UserConversationStatus>.Filter.Ne(x=>x.Status,(int)ConversationStatus.Unresolved)
					             };
                var filterConversationStatusStorage = Builders<UserConversationStatus>.Filter.And(filterListConversationStatusStorage);

                var updateConversationStatusStorage = Builders<UserConversationStatus>.Update.Set("Status", (int)ConversationStatus.Archive).Set("Date", SystemTime.Now());


                _userConversationStatusStorage.UpdateBulk(filterConversationStatusStorage, updateConversationStatusStorage);

                var filterListStatusStorage = new List<FilterDefinition<UserMessageStatus>>
					             {
									 Builders<UserMessageStatus>.Filter.Eq(x=>x.UserId,userId),
									 Builders<UserMessageStatus>.Filter.Ne(x=>x.Status,(int)ConversationStatus.Unresolved)
					             };
                var filterStatusStorage = Builders<UserMessageStatus>.Filter.And(filterListStatusStorage);

                var updateStatusStorage = Builders<UserMessageStatus>.Update.Set("Status", (int)ConversationStatus.Archive).Set("Date", SystemTime.Now());

                _userMessageStatusStorage.UpdateBulk(filterStatusStorage, updateStatusStorage);
            }

            NotifyUnreadCount(userId);
        }

        public void MarkAllAsRead(ObjectId userId)
        {
            var statuslessConversations = _conversationService.GetAllWithNoStatus(userId);
            var statuslessMessages = _messagesService.GetAllWithNoStatus(userId);

            InsertConversationStatus(statuslessConversations, ConversationStatus.Read, userId);
            InsertMessagesStatus(statuslessMessages, ConversationStatus.Read, userId);

            using (MiniProfiler.Current.Step("_userConversationStatusRepository.UpdateAll"))
            {
                var filterListConversationStatusStorage = new List<FilterDefinition<UserConversationStatus>>
					                 {
                                      Builders<UserConversationStatus>.Filter.Eq(x=>x.Status, (int)ConversationStatus.Read),
									  Builders<UserConversationStatus>.Filter.Eq(x=>x.Status, (int)ConversationStatus.Unread),
									  Builders<UserConversationStatus>.Filter.Eq(x=>x.Status, (int)ConversationStatus.New)
					                 };
                var filterOrConversationStatusStorage = Builders<UserConversationStatus>.Filter.Or(filterListConversationStatusStorage);
                var filterConversationStatusStorage = Builders<UserConversationStatus>.Filter.Eq(x => x.UserId, userId);
                var filterAndConversationStatusStorage = Builders<UserConversationStatus>.Filter.And(filterOrConversationStatusStorage, filterConversationStatusStorage);

                var updateConversationStatusStorage = Builders<UserConversationStatus>.Update.Set(p => p.Status, (int)ConversationStatus.Read).Set(p => p.Date, SystemTime.Now());

                _userConversationStatusStorage.UpdateBulk(filterAndConversationStatusStorage, updateConversationStatusStorage);


                var filterListStatusStorage = new List<FilterDefinition<UserMessageStatus>>
					                 {
                                      Builders<UserMessageStatus>.Filter.Eq(x=>x.Status, (int)ConversationStatus.Read),
									  Builders<UserMessageStatus>.Filter.Eq(x=>x.Status, (int)ConversationStatus.Unread),
									  Builders<UserMessageStatus>.Filter.Eq(x=>x.Status, (int)ConversationStatus.New)
					                 };
                var filterOrStatusStorage = Builders<UserMessageStatus>.Filter.Or(filterListStatusStorage);
                var filterStatusStorage = Builders<UserMessageStatus>.Filter.Eq(x => x.UserId, userId);
                var filterAndStatusStorage = Builders<UserMessageStatus>.Filter.And(filterOrStatusStorage, filterStatusStorage);

                var updateStatusStorage = Builders<UserMessageStatus>.Update.Set(p => p.Status, (int)ConversationStatus.Read);


                _userMessageStatusStorage.UpdateBulk(filterAndStatusStorage, updateStatusStorage);



                var filter2 = Builders<UserConversationStatus>.Filter.Eq(x => x.UserId, userId);
                var update2 = Builders<UserConversationStatus>.Update.Set(y => y.Date, SystemTime.Now());

                _userConversationStatusStorage.UpdateBulk(filter2, update2);
            }

            NotifyUnreadCount(userId);
        }

        public void DeleteStatus(ObjectId conversationId, ObjectId userId)
        {
            var filterList = new List<FilterDefinition<UserConversationStatus>>
				                 {
					                 Builders<UserConversationStatus>.Filter.Eq(x => x.ConversationId, conversationId),
					                 Builders<UserConversationStatus>.Filter.Eq(x => x.UserId, userId)
				                 };
            var filter = Builders<UserConversationStatus>.Filter.And(filterList);
            var userConversationStatus = _userConversationStatusStorage.GetList(filter);

            foreach (var status in userConversationStatus)
            {
                var deleteFilter = Builders<UserConversationStatus>.Filter.Eq(x => x._id, status._id);
                _userConversationStatusStorage.Remove(deleteFilter);
            }

            NotifyUnreadCount(userId);
        }

        private void InsertConversationStatus(IList<ConversationDescriptor> statuslessConversations, ConversationStatus status, ObjectId userId)
        {
            if (statuslessConversations.IsNullOrEmpty())
                return;

            using (MiniProfiler.Current.Step("_userConversationStatusRepository.InsertAll_ConversationWithNoStatus"))
            {
                var newUserConverationStatuses = statuslessConversations.Select(c => CreateNewConversationStatues(userId, c.Id, status)).ToList();
                _userConversationStatusStorage.InsertBulk(newUserConverationStatuses);
            }
        }

        private void InsertMessagesStatus(IList<Message> statuslessMessages, ConversationStatus status, ObjectId userId)
        {
            if (statuslessMessages.IsNullOrEmpty())
                return;

            using (MiniProfiler.Current.Step("_userConversationStatusRepository.InsertAll_ConversationWithNoStatus"))
            {
                var newUserConverationStatuses = statuslessMessages.Select(m => CreateNewMessageStatus(userId, m.ConversationId, m.Id, status)).ToList();
                _userMessageStatusStorage.InsertBulk(newUserConverationStatuses);
            }
        }

        private UserConversationStatus CreateNewConversationStatues(ObjectId userId, ObjectId conersationId, ConversationStatus status)
        {
            //Ilan: Modified here.
            return new UserConversationStatus
                        {
                            _id = ObjectId.GenerateNewId(),
                            ConversationId = conersationId,
                            UserId = userId,
                            Status = (int)status,
                            CreatedDateTime = SystemTime.Now(),
                            Date = SystemTime.Now()
                        };
        }

        private UserMessageStatus CreateNewMessageStatus(ObjectId userId, ObjectId conversationId, ObjectId messageId, ConversationStatus status)
        {
            //Ilan: Modified here.
            return new UserMessageStatus
                        {
                            _id = ObjectId.GenerateNewId(),
                            UserId = userId,
                            ConversationId = conversationId,
                            MessageId = messageId,
                            Status = (int)status,
                            CreatedDateTime = SystemTime.Now(),
                            Date = SystemTime.Now()
                        };
        }

        private void NotifyUnreadCount(ObjectId userId)
        {
            _conversationStatusChangedEvent.Trigger(new ConversationStatusChangedEvent
                                                        {
                                                            ConversationId = ObjectId.Empty,
                                                            AuthorId = userId
                                                        });
        }
    }
}