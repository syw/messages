using System;
using System.Collections.Generic;
using System.Linq;
using CommonGround.Events;
using CommonGround.Utilities;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services.Settings;
using StackExchange.Profiling;

namespace SYW.App.Messsages.Domain.Conversations
{
	public interface IConversationService
	{
		IList<ConversationDescriptor> GetAllWithNoStatus(ObjectId entityId);
		IList<ConversationDescriptor> GetAll(ObjectId entityId, DateTime memberSince, int page, int limit);
		Conversation Get(string conversationId, ObjectId userId);
		void ReOrderParticipantsAccordingToSendingOrder(IConversation conversation, ObjectId currentUserId);
	}

	public class ConversationService : IConversationService
	{
		private readonly IConversationRepository _conversationRepository;
		private readonly IMessageRepository _messageRepository;
		private readonly IEntitiesResolver _entitiesResolver;
		private readonly IUserConversationStatusRepository _userConversationStatusRepository;
		private readonly IEvent<UnresolvedConversationEvent> _unresolvedConversationEvent;
		private readonly IUserMessageStatusRepository _userMessageStatusRepository;
		private readonly IConversationStatusSettings _conversationStatusSettings;
		private readonly ICacheSystemMessageProvider _cacheSystemMessageProvider;
		private readonly IApplicationSettings _applicationSettings;

		public ConversationService(
			IConversationRepository conversationRepository,
			IMessageRepository messageRepository,
			IEntitiesResolver entitiesResolver,
			IUserConversationStatusRepository userConversationStatusRepository,
			IEvent<UnresolvedConversationEvent> unresolvedConversationEvent,
			IUserMessageStatusRepository userMessageStatusRepository,
			IConversationStatusSettings conversationStatusSettings,
			ICacheSystemMessageProvider cacheSystemMessageProvider,
			IApplicationSettings applicationSettings)
		{
			_conversationRepository = conversationRepository;
			_messageRepository = messageRepository;
			_entitiesResolver = entitiesResolver;
			_userConversationStatusRepository = userConversationStatusRepository;
			_unresolvedConversationEvent = unresolvedConversationEvent;
			_userMessageStatusRepository = userMessageStatusRepository;
			_conversationStatusSettings = conversationStatusSettings;
			_cacheSystemMessageProvider = cacheSystemMessageProvider;
			_applicationSettings = applicationSettings;
		}

		public IList<ConversationDescriptor> GetAllWithNoStatus(ObjectId entityId)
		{
			var conversations = _conversationRepository.GetAll(entityId, SystemTime.Now()).EmptyIfNull().ToArray();
            //ILAN: Appeneded this line
            if (!conversations.Any())
                return conversations;

			using (MiniProfiler.Current.Step("_messageRepository.GetLatest"))
			{
				var lastMessages = GetLastMessages(conversations);

				conversations.JoinDo(lastMessages, c => c.Id, m => m.Key, (c, m) => c.LastMessage = m.Value);
			}

			foreach (var conversation in conversations)
				conversation.Status = ConversationStatus.Temp;

			using (MiniProfiler.Current.Step("_userConversationStatusRepository GetAllWithNoStatus"))
			{
				var conversationStatuses = _userConversationStatusRepository.GetUserConversations(entityId);
				conversations.JoinDo(conversationStatuses, c => c.Id, s => s.ConversationId, (c, s) => c.Status = (ConversationStatus)s.Status);
			}

			conversations = conversations.Where(c => c.Status == ConversationStatus.Temp).ToArray();

			return ResolveAndFilter(conversations)
				.OfType<ConversationDescriptor>()
				.OrderByDescending(c => c.LastMessage.Date)
				.ToList();
		}

		public IList<ConversationDescriptor> GetAll(ObjectId entityId, DateTime memberSince, int page, int limit)
		{
			// Because we dont have a lot of regular conversations and system messages there is nothing wrong
			// with fetching it like this
			var conversations = _conversationRepository.GetAll(entityId, memberSince).EmptyIfNull().ToArray();
            //ILAN: Appeneded this line
            if (!conversations.Any())
                return conversations;

			using (MiniProfiler.Current.Step("_messageRepository.GetLatest"))
			{
				var lastMessages = GetLastMessages(conversations);
				var systemMessages = _cacheSystemMessageProvider.GetAll().EmptyIfNull();

				conversations.JoinDo(lastMessages, c => c.Id, m => m.Key, (c, m) =>
					                                                          {
						                                                          c.LastMessage = m.Value;
						                                                          c.Messages = new[] {m.Value};
					                                                          });

				conversations.JoinDo(
					systemMessages.GroupBy(
						sm => sm.ConversationId,
						(conversationID, messages) => new {ConversationId = conversationID, Messages = messages.Select(MessageMapper.MapFromDescriptor)}),
					conversation => conversation.Id, agregatedMessages => agregatedMessages.ConversationId,
					(conversation, agregatedMessages) => conversation.Messages = agregatedMessages.Messages.ToList());
			}

			foreach (var conversation in conversations)
			{
				conversation.Status = GetInitialStatus(conversation.LastMessage);
			}

			using (MiniProfiler.Current.Step("_userConversationStatusRepository.Get"))
			{
				// The migration of the aggregation feature caused a problem that users can have more than 1 conversation status 
				// In order to handle it we are always taking the last conversation status
				// Properly handling the data curroption is on our backlog
				var conversationStatuses =
					_userConversationStatusRepository.Get(conversations.Select(c => c.Id).ToArray(), entityId)
						.OrderByDescending(s => s.Date)
						.GroupBy(c => c.ConversationId)
						.Select(g => g.First())
						.EmptyIfNull();

				conversations.JoinDo(conversationStatuses, c => c.Id, s => s.ConversationId, (conversation, userConversationStatus) =>
					                                                                             {
						                                                                             if (!conversation.IsSystemMessage || conversation.LastMessage == null || userConversationStatus.Date > conversation.LastMessage.Date)
							                                                                             conversation.Status = (ConversationStatus)userConversationStatus.Status;
					                                                                             });
			}

			InitMessagesStatuses(entityId, ref conversations);

			conversations = conversations.Where(c => c.Status != ConversationStatus.Archive).ToArray();

			if (page != -1 && limit != -1)
			{
				return ResolveAndFilter(conversations)
					.OfType<ConversationDescriptor>()
					.OrderByDescending(c => c.LastMessage.Date)
					.Skip((page - 1)*limit)
					.Take(limit)
					.ToList();
			}

			return ResolveAndFilter(conversations)
				.OfType<ConversationDescriptor>()
				.OrderByDescending(c => c.LastMessage.Date)
				.ToList();
		}

		public Conversation Get(string conversationId, ObjectId userId)
		{
			using (MiniProfiler.Current.Step("ConversationService.Get"))
			{
				var conversation = _conversationRepository.Get(ObjectId.Parse(conversationId));

				var messages = GetMessages(conversationId, userId, conversation);

				if (!messages.IsNullOrEmpty())
					conversation.Messages = messages;

				return ResolveAndFilter(new[] {conversation}).OfType<Conversation>().FirstOrDefault();
			}
		}

		private IList<Message> GetMessages(string conversationId, ObjectId userId, IConversation conversation)
		{
			if (conversation.IsSystemMessage)
			{
				var messages = _cacheSystemMessageProvider.GetAll(ObjectId.Parse(conversationId)).EmptyIfNull().Select(MessageMapper.MapFromDescriptor).ToList();

				foreach (var message in messages)
					message.Status = GetInitialStatus(message);

				var messagesStatuses = _userMessageStatusRepository.GetAll(conversation.Id, userId);
				messages.JoinDo(messagesStatuses, m => m.Id, s => s.MessageId, (m, s) => m.Status = (ConversationStatus)s.Status);

				return messages;
			}

			return _messageRepository.GetAll(conversation.Id)
				.Where(m => m != null)
				.OrderByDescending(c => c.Date)
				.ToList();
		}

		private IEnumerable<IConversation> ResolveAndFilter(IList<IConversation> conversations)
		{
			using (MiniProfiler.Current.Step("Conversations.ResolveAndFilter"))
			{
				var entities = conversations.
                               //Where(c=>c.Messages!=null).
                               SelectMany(c => c.Messages.EmptyIfNull()).Where(m => m != null).Select(m => m.Author)
					          .Concat(conversations.SelectMany(c => c.Participants.EmptyIfNull()))
					          .Where(e => e != null)
					          .ToList();

				_entitiesResolver.Resolve(entities);

				foreach (var conversation in conversations)
				{
					conversation.Participants = conversation.Participants.EmptyIfNull()
						.Where(p => p != null && !string.IsNullOrEmpty(p.Name))
						.ToList();

					conversation.Messages = conversation.Messages.EmptyIfNull()
						.Where(m => m != null && m.Author != null && !string.IsNullOrEmpty(m.Author.Name))
						.ToList();

					if (conversation.IsSystemMessage) continue;

					foreach (var message in conversation.Messages.Where(x => !string.IsNullOrEmpty(x.AttachedImagesUrl)))
					{
						message.AttachedImagesUrl = InitAttachmentCdnPath(message.AttachedImagesUrl);
					}
				}

				var resolvedConversations = conversations
					.Where(c => c != null)
					.Where(c => !c.Messages.IsNullOrEmpty())
					.Where(c => !c.Participants.IsNullOrEmpty() || c.IsSystemMessage)
					.ToList();

				UpdateUnresolvedConversations(conversations.Except(resolvedConversations).ToList());

				return resolvedConversations;
			}
		}

		private string InitAttachmentCdnPath(string attachedImagesPath)
		{
			return attachedImagesPath.Split(';').Aggregate(string.Empty, (current, imagePath) => current + (!string.IsNullOrEmpty(imagePath) ? _applicationSettings.EmailAttachmentsUrl + "/" + imagePath + ";" : string.Empty));
		}

		private void UpdateUnresolvedConversations(IList<IConversation> conversations)
		{
			if (conversations.IsNullOrEmpty())
				return;

			_unresolvedConversationEvent.Trigger(new UnresolvedConversationEvent
				                                     {
					                                     ConversationsIds = conversations.Select(c => c.Id).ToList()
				                                     });
		}

		private ConversationStatus GetInitialStatus(Message message)
		{
			return message != null && message.Date.AddDays(_conversationStatusSettings.DaysToKeepConversationAsNew) < SystemTime.Now()
				       ? ConversationStatus.Unread
				       : ConversationStatus.New;
		}

		private void InitMessagesStatuses(ObjectId entityId, ref ConversationDescriptor[] conversations)
		{
			var messagesStatuses = _userMessageStatusRepository.GetAll(conversations.Select(x => x.Id).ToArray(), entityId);

			foreach (var conversation in conversations)
			{
				foreach (var message in conversation.Messages.Where(msg => msg != null))
				{
					if (conversation.IsSystemMessage && messagesStatuses != null && messagesStatuses.Select(x => x.MessageId).Contains(message.Id))
						message.Status = (ConversationStatus)messagesStatuses.First(x => x.MessageId == message.Id).Status;
					else if (!conversation.IsSystemMessage)
						message.Status = conversation.Status;
					else
					{
						message.Status = message.Date.AddDays(_conversationStatusSettings.DaysToKeepConversationAsNew) < SystemTime.Now()
							                 ? ConversationStatus.Unread
							                 : ConversationStatus.New;
					}
				}
			}
		}

		private IEnumerable<KeyValuePair<ObjectId, Message>> GetLastMessages(ConversationDescriptor[] conversations)
		{
			var lastPersonalMessages = _messageRepository.GetLatest(conversations.Where(c => !c.IsSystemMessage).Select(c => c.Id).ToArray());

			var lastSystemMessages = _cacheSystemMessageProvider.GetLatest(conversations.Where(c => c.IsSystemMessage).Select(c => c.Id)).EmptyIfNull().ToDictionary(x => x.Key, x => MessageMapper.MapFromDescriptor(x.Value));

			return lastPersonalMessages.Concat(lastSystemMessages);
		}

		public void ReOrderParticipantsAccordingToSendingOrder(IConversation conversation, ObjectId currentUserId)
		{
			if ((conversation.IsSystemMessage || conversation.Participants == null || !conversation.Participants.Any())) return;

			var sentMessagesAuthors = _messageRepository.GetOrderedConversationSenders(conversation.Id).Where(c => c != currentUserId).EmptyIfNull().ToList();
			if (sentMessagesAuthors.Any())
				sentMessagesAuthors.Reverse();
			foreach (var authorId in sentMessagesAuthors)
			{
				var lastSender = conversation.Participants.FirstOrDefault(p => p._id == authorId);
				if (lastSender == null) continue;
				conversation.Participants.Remove(lastSender);
				conversation.Participants.Insert(0, lastSender);
			}
		}
	}
}