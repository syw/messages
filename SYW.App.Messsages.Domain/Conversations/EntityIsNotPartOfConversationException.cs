using MongoDB.Bson;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messsages.Domain.Conversations
{
	public class EntityIsNotPartOfConversationException : ForbiddenOperationException
	{
		public EntityIsNotPartOfConversationException(ObjectId entityId, ObjectId conversationId)
			: base(string.Format("Entity {0} is not part of conversation {1}", entityId, conversationId)) {}
	}
}