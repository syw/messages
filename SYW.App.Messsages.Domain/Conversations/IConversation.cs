using System;
using System.Collections.Generic;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.Conversations
{
    public interface IConversation
    {
        ObjectId Id { get; set; }
        IList<Entity> Participants { get; set; }
        IList<Message> Messages { get; set; }
        bool IsSystemMessage { get; set; }
        DateTime Timestamp { get; set; }
        DateTime StartDate { get; set; }
    }
}