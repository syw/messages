﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.Conversations
{
	public interface IStartNewConversationService
	{
		ConversationDescriptor Start(long originalCreatorId, long[] originalParticipantsIds);
		ConversationDescriptor Start(long originalCreatorId, long[] originalParticipantsIds, string content, IList<Attachment> attachments = null);
		ConversationDescriptor Start(ObjectId creatorId, List<ObjectId> participantsIds);
	}

	public class StartNewConversationService : IStartNewConversationService
	{
		private readonly IConversationRepository _conversationRepository;
		private readonly IEntitiesResolver _entitiesResolver;
		private readonly IEntitiesInitializer _entitiesInitializer;
		private readonly IEntitiesRepository _entitiesRepository;
		private readonly IMessagesService _messagesService;

		public StartNewConversationService(
			IConversationRepository conversationRepository,
			IEntitiesResolver entitiesResolver,
			IEntitiesInitializer entitiesInitializer,
			IEntitiesRepository entitiesRepository,
			IMessagesService messagesService)
		{
			_conversationRepository = conversationRepository;
			_entitiesResolver = entitiesResolver;
			_entitiesInitializer = entitiesInitializer;
			_entitiesRepository = entitiesRepository;
			_messagesService = messagesService;
		}

		public ConversationDescriptor Start(long originalCreatorId, long[] originalParticipantsIds, string content, IList<Attachment> attachments = null)
		{
			var creator = _entitiesRepository.GetEntitiesByIds(new[] {originalCreatorId}).First();

			_entitiesInitializer.Init(originalParticipantsIds);

			var participants = _entitiesRepository.GetEntitiesByIds(originalParticipantsIds)
				.EmptyIfNull()
				.Where(p => p != null)
				.Select(p => p._id)
				.ToArray();

			var conversation = GetOrCreateConversation(creator._id, participants);

			conversation.LastMessage = _messagesService.Create(conversation.Id, creator._id, content, attachments);

			ResolveAndFilter(conversation);

			return conversation;
		}

	    public ConversationDescriptor Start(ObjectId creatorId, List<ObjectId> participantsIds)
		{
			//var creator = _entitiesRepository.GetEntitiesByIds(new[] { creatorId }).First();

			var participants = _entitiesRepository.GetEntitiesByIds(participantsIds)
				.EmptyIfNull()
				.Where(p => p != null)
				.Select(p => p._id)
				.ToArray();

			//var conversation = GetOrCreateConversation(creator.Id, participants);
			var conversation = GetOrCreateConversation(creatorId, participants);

			ResolveAndFilter(conversation);

			return conversation;
		}

		public ConversationDescriptor Start(long originalCreatorId, long[] originalParticipantsIds)
		{
			var creator = _entitiesRepository.GetEntitiesByIds(new[] {originalCreatorId}).First();

			_entitiesInitializer.Init(originalParticipantsIds);

			var participants = _entitiesRepository.GetEntitiesByIds(originalParticipantsIds)
				.EmptyIfNull()
				.Where(p => p != null)
				.Select(p => p._id)
				.ToArray();

			var allParticipants = (participants.Concat(new[] { creator._id })).Distinct().ToArray();
			var conversation = _conversationRepository.GetPersonalConversationsByParticipantsIds(allParticipants).OrderByDescending(x => x.Timestamp).FirstOrDefault();

			if (conversation != null)
				ResolveAndFilter(conversation);

			return conversation;
		}

		private void ResolveAndFilter(ConversationDescriptor conversation)
		{
			_entitiesResolver.Resolve(conversation.Participants);

			conversation.Participants = conversation.Participants.Where(p => !string.IsNullOrEmpty(p.Name)).ToList();
		}

		private ConversationDescriptor GetOrCreateConversation(ObjectId creatorId, ObjectId[] participantsIds)
		{
			var allParticipants = (participantsIds.Concat(new[] {creatorId})).Distinct().ToArray();

			return _conversationRepository.GetPersonalConversationsByParticipantsIds(allParticipants).OrderByDescending(x => x.Timestamp).FirstOrDefault()
			       ?? _conversationRepository.Create(creatorId, participantsIds);
		}
	}
}