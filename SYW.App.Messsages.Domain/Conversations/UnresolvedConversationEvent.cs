using System.Collections.Generic;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Conversations
{
	public class UnresolvedConversationEvent
	{
		public IList<ObjectId> ConversationsIds { get; set; }
	}
}