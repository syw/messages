﻿using System.Collections.Generic;
using CommonGround.Events;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using System.Linq;

namespace SYW.App.Messsages.Domain.Conversations
{
	public class UnresolvedConversationsHandler : IAsyncEventSubscriber
	{
		private readonly IEvent<UnresolvedConversationEvent> _unresolvedConversationEvent;
		private readonly IUserConversationStatusRepository _userConversationStatusRepository;

		public UnresolvedConversationsHandler(IEvent<UnresolvedConversationEvent> unresolvedConversationEvent, IUserConversationStatusRepository userConversationStatusRepository)
		{
			_unresolvedConversationEvent = unresolvedConversationEvent;
			_userConversationStatusRepository = userConversationStatusRepository;
		}

		public void Subscribe()
		{
			_unresolvedConversationEvent.Subscribe(x => UpdateUnresolvedConversations(x.ConversationsIds));
		}

		private void UpdateUnresolvedConversations(IList<ObjectId> conversationsIds)
		{
			var conversationsStatus = _userConversationStatusRepository.GetConversationsStatuses(conversationsIds.ToArray());

			foreach (var conversation in conversationsStatus)
				_userConversationStatusRepository.UpdateStatus(conversation.ConversationId, conversation.UserId, ConversationStatus.Unresolved);
		}
	}
}