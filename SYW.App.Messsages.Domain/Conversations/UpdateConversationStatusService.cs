﻿using System.Linq;
using CommonGround.Events;
using CommonGround.Utilities;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;

namespace SYW.App.Messsages.Domain.Conversations
{
	public interface IUpdateConversationStatusService
	{
		void UpdateStatus(ObjectId conversationId, ObjectId userId, ConversationStatus status);
	}

	public class UpdateConversationStatusService : IUpdateConversationStatusService
	{
		private readonly IUserConversationStatusRepository _userConversationStatusRepository;
		private readonly IEvent<ConversationStatusChangedEvent> _conversationStatusChangedEvent;

		public UpdateConversationStatusService(IUserConversationStatusRepository userConversationStatusRepository, IEvent<ConversationStatusChangedEvent> conversationStatusChangedEvent)
		{
			_userConversationStatusRepository = userConversationStatusRepository;
			_conversationStatusChangedEvent = conversationStatusChangedEvent;
		}

		public void UpdateStatus(ObjectId conversationId, ObjectId userId, ConversationStatus status)
		{
			_userConversationStatusRepository.UpdateStatus(conversationId, userId, status);

			_conversationStatusChangedEvent.Trigger(new ConversationStatusChangedEvent
														{
															ConversationId = conversationId,
															AuthorId = userId
														});
		}

		public void UpdateNewConversationsAsUnRead(ObjectId[] conversationIds, ObjectId userId)
		{
			var userConversationStatuses = conversationIds.Select(x => new UserConversationStatus
																			{
																				_id = ObjectId.GenerateNewId(),
																				ConversationId = x,
																				UserId = userId,
																				Date = SystemTime.Now(),
																				Status = (int)ConversationStatus.Unread
																			}).ToArray();
		}
	}
}