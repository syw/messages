﻿using System;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.DataAccess.Audits
{
	public class AuditDto : BaseEntity<ObjectId>
	{
		public long? UserId { get; set; }
		public string ActionPerformed { get; set; }
		public string NewValuesJsonString { get; set; }
		public DateTime Time { get; set; } 
	}
}