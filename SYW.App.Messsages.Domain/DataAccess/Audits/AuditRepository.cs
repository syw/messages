﻿using Mongo;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Audits;

namespace SYW.App.Messsages.Domain.DataAccess.Audits
{
	public interface IAuditRepository
	{
		void Save(Audit audit);
	}

	public class AuditRepository : IAuditRepository
	{
		private readonly IMongoStorage<AuditDto,ObjectId> _cmsAuditStorage;
		private readonly IAuditMapper _mapper;

		public AuditRepository(
			IMongoStorage<AuditDto, ObjectId> cmsAuditStorage,
			IAuditMapper mapper)
		{
			_cmsAuditStorage = cmsAuditStorage;
			_mapper = mapper;
		}

		public void Save(Audit audit)
		{
			var cmsAuditDto = _mapper.ToAuditDto(audit);
			_cmsAuditStorage.Insert(cmsAuditDto);
		}
	}
}