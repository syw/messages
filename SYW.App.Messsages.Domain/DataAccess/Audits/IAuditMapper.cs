﻿using SYW.App.Messsages.Domain.Audits;

namespace SYW.App.Messsages.Domain.DataAccess.Audits
{
	public interface IAuditMapper
	{
		AuditDto ToAuditDto(Audit audit);
		Audit ToAudit(AuditDto auditDto);
	}

	public class AuditMapper : IAuditMapper
	{
		public AuditDto ToAuditDto(Audit audit)
		{
			return new AuditDto
			{
				Time = audit.Time,
				ActionPerformed = audit.ActionPerformed,
				NewValuesJsonString = audit.NewValuesJsonString,
				UserId = audit.UserId
			};
		}

		public Audit ToAudit(AuditDto auditDto)
		{
			return new Audit
			{
				Time = auditDto.Time,
				ActionPerformed = auditDto.ActionPerformed,
				NewValuesJsonString = auditDto.NewValuesJsonString,
				UserId = auditDto.UserId,
			};
		}
	}
}