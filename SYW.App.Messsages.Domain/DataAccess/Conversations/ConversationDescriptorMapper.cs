﻿using System;
using System.Linq;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.DataAccess.Conversations
{
	public class ConversationDescriptorMapper
	{
		// Not sure it should be static but we can live with that
		public static ConversationDescriptor Map(ConversationDto source)
		{
			return new ConversationDescriptor
						{
							Id = source._id,
							Participants = source.ParticipantsIds.Select(p => new Entity {_id = p}).ToList(),
							IsSystemMessage = source.IsSystemMessage,
                            StartDate = source.StartDate ?? default(DateTime),
                            Timestamp = source.Timestamp ?? default(DateTime)
						};
		}
	}

	public class ConversationMapper
	{
		// Not sure it should be static but we can live with that
		public static Conversation Map(ConversationDto source)
		{
			return new Conversation
						{
							Id = source._id,
							Participants = source.ParticipantsIds.Select(p => new Entity { _id = p }).ToList(),
							IsSystemMessage = source.IsSystemMessage,
                            StartDate = source.StartDate ?? default(DateTime),
                            Timestamp = source.Timestamp ?? default(DateTime)
						};
		}
	}
}