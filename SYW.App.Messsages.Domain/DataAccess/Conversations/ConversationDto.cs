﻿using System;
using System.Collections.Generic;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.DataAccess.Conversations
{
	public class ConversationDto : BaseEntity<ObjectId>
	{
		public ObjectId CreatorId { get; set; }

		public IList<ObjectId> ParticipantsIds { get; set; }

		[BsonIgnoreIfDefault, BsonIgnoreIfNull]
		public DateTime? StartDate { get; set; }

		[BsonIgnoreIfDefault, BsonIgnoreIfNull]
		public DateTime? Timestamp { get; set; }

		public bool IsSystemMessage { get; set; }
	}
}