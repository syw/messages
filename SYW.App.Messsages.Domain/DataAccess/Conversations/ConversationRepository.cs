using System;
using System.Collections.Generic;
using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Conversations;
using StackExchange.Profiling;

namespace SYW.App.Messsages.Domain.DataAccess.Conversations
{
	public interface IConversationRepository
	{
		ConversationDescriptor Create(ObjectId creatorId, ObjectId[] participantsIds);
		ConversationDescriptor CreateSystemConversation(ObjectId creatorId);
		IList<ConversationDescriptor> GetAll(ObjectId author, DateTime memberSince);
		IList<ConversationDescriptor> GetPersonalConversations(ObjectId author);
		IList<ConversationDescriptor> GetPersonalConversationsByParticipantsIds(ObjectId[] participantsIds);
		Conversation Get(ObjectId conversationId);
		Conversation GetByAuthor(ObjectId authorId);
		void Touch(ObjectId conversationId);
		IList<ObjectId> GetSystemConversationIds();
	}

	public class ConversationRepository : IConversationRepository
	{
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;

		public ConversationRepository(IMongoStorage<ConversationDto, ObjectId> conversationStorage)
		{
			_conversationStorage = conversationStorage;
		}

		public ConversationDescriptor Create(ObjectId creatorId, ObjectId[] participantsIds)
		{
			var conversationDto = new ConversationDto
				                      {
					                      CreatorId = creatorId,
					                      ParticipantsIds = participantsIds.Union(new[] {creatorId}).ToArray(),
					                      StartDate = SystemTime.Now(),
					                      Timestamp = SystemTime.Now()
				                      };

			var result = _conversationStorage.Insert(conversationDto);

			return ConversationDescriptorMapper.Map(result);
		}

		public ConversationDescriptor CreateSystemConversation(ObjectId creatorId)
		{
			var conversationDto = new ConversationDto
				                      {
					                      CreatorId = creatorId,
					                      IsSystemMessage = true,
					                      StartDate = SystemTime.Now(),
					                      Timestamp = SystemTime.Now(),
					                      ParticipantsIds = new[] {creatorId} // The creator is always part of the participants
				                      };

			var result = _conversationStorage.Insert(conversationDto);

			return ConversationDescriptorMapper.Map(result);
		}

		public IList<ObjectId> GetSystemConversationIds()
		{
			var projection = Builders<ConversationDto>.Projection.Include(x => x._id);
			var filter = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			return _conversationStorage.SelectAs(filter, projection).Select(x => (x["_id"].AsObjectId)).ToList();
		}

		public IList<ConversationDescriptor> GetAll(ObjectId author, DateTime memberSince)
		{
			try
			{
				using (MiniProfiler.Current.Step("ConversationRepository.GetAll"))
				{
					var filterList = new List<FilterDefinition<ConversationDto>>
						                 {
							                 Builders<ConversationDto>.Filter.AnyEq("ParticipantsIds", author),
							                 Builders<ConversationDto>.Filter.Eq("IsSystemMessage", true)
						                 };
					var filter = Builders<ConversationDto>.Filter.Or(filterList);
					return _conversationStorage.GetList(filter).Select(ConversationDescriptorMapper.Map).ToList();
				}
			}
			catch (Exception)
			{
				return new List<ConversationDescriptor>();
			}
		}

		public IList<ConversationDescriptor> GetPersonalConversations(ObjectId author)
		{
			using (MiniProfiler.Current.Step("ConversationRepository.GetPersonalConversations"))
			{
				var filterList = new List<FilterDefinition<ConversationDto>>
					                 {
						                 Builders<ConversationDto>.Filter.AnyEq("ParticipantsIds", author),
						                 Builders<ConversationDto>.Filter.Eq("IsSystemMessage", false)
					                 };
				var filter = Builders<ConversationDto>.Filter.And(filterList);
				return _conversationStorage.GetList(filter).Select(ConversationDescriptorMapper.Map).ToList();
			}
		}

		public IList<ConversationDescriptor> GetPersonalConversationsByParticipantsIds(ObjectId[] participantsIds)
		{
			using (MiniProfiler.Current.Step("ConversationRepository.GetPersonalConversations"))
			{
				var filterList = new List<FilterDefinition<ConversationDto>>
					                 {
						                 Builders<ConversationDto>.Filter.All("ParticipantsIds", participantsIds),
						                 Builders<ConversationDto>.Filter.Eq("IsSystemMessage", false),
						                 Builders<ConversationDto>.Filter.Size("ParticipantsIds", participantsIds.Length)
					                 };
				var filter = Builders<ConversationDto>.Filter.And(filterList);
				return _conversationStorage.GetList(filter).Select(ConversationDescriptorMapper.Map).ToList();
			}
		}

		public Conversation Get(ObjectId conversationId)
		{
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversationId);
			var conversation = _conversationStorage.GetFirstOrDefault(filter);
			return ConversationMapper.Map(conversation);
		}

		public Conversation GetByAuthor(ObjectId authorId)
		{
			var filter = Builders<ConversationDto>.Filter.Eq(x => x.CreatorId, authorId);
			var conversation = _conversationStorage.GetFirstOrDefault(filter);

			return conversation == null ? null : ConversationMapper.Map(conversation);
		}

		public void Touch(ObjectId conversationId)
		{
			var filter = Builders<ConversationDto>.Filter.Eq(x => x._id, conversationId);
			var conversation = _conversationStorage.GetFirstOrDefault(filter);

			conversation.Timestamp = SystemTime.Now();

			_conversationStorage.Upsert(conversation);
		}
	}
}