﻿using CommonGround.Events;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.DataAccess.Conversations
{
	public interface ISystemConversationService
	{
		void CreateSystemConversation(SystemMessage systemMessage, ObjectId messageId);
		Conversation GetSystemConversation(ObjectId authorId);
		void AddMessage(ObjectId conversationId, SystemMessage systemMessage, ObjectId messageId);
	}

	public class SystemConversationService : ISystemConversationService
	{
		private readonly IConversationRepository _conversationRepository;
		private readonly IMessageRepository _messageRepository;
		private readonly IEvent<NewSystemMessageEvent> _newSystemMessageEvent;

		public SystemConversationService(IConversationRepository conversationRepository, IMessageRepository messageRepository, IEvent<NewSystemMessageEvent> newSystemMessageEvent)
		{
			_conversationRepository = conversationRepository;
			_messageRepository = messageRepository;
			_newSystemMessageEvent = newSystemMessageEvent;
		}

		public Conversation GetSystemConversation(ObjectId authorId)
		{
			return _conversationRepository.GetByAuthor(authorId);
		}

		public void CreateSystemConversation(SystemMessage systemMessage, ObjectId messageId)
		{
			var conversation = _conversationRepository.CreateSystemConversation(systemMessage.AuthorId);

		    AddMessage(conversation.Id, systemMessage, messageId);
		}

		public void AddMessage(ObjectId conversationId, SystemMessage systemMessage, ObjectId messageId)
		{
			_messageRepository.CreateSystemMessage(conversationId, systemMessage, messageId);
		}
	}
}