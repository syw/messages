﻿using System;
using Mongo;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.DataAccess.Conversations
{
	public class UserConversationStatus : BaseEntity<ObjectId>
	{
        /* 
         * Ilan: Added CreatedDateTime
         * Date: 2015-12-27 14:15 
         */

        public ObjectId UserId { get; set; }
		public ObjectId ConversationId { get; set; }
		public int Status { get; set; }
        /// <summary>
        /// Gets or sets the updated DateTime.
        /// </summary>
        /// <value>
        /// The date.
        /// </value>
		public DateTime Date { get; set; }
	    public DateTime CreatedDateTime { get; set; }
	}
}