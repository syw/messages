﻿using System.Collections.Generic;
using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using SYW.App.Messsages.Domain.Conversations;

namespace SYW.App.Messsages.Domain.DataAccess.Conversations
{
	public interface IUserConversationStatusRepository
	{
		void UpdateStatus(ObjectId conversationId, ObjectId userId, ConversationStatus status);
		UserConversationStatus Get(ObjectId conversationId, ObjectId userId);
		List<UserConversationStatus> Get(ObjectId[] conversationIds, ObjectId userId);
		IList<UserConversationStatus> GetUserConversations(ObjectId userId);
		IList<UserConversationStatus> GetConversationsStatuses(ObjectId[] conversationsIds);

		IList<UserConversationStatus> GetConversationsStatuses(ObjectId conversationsId, int limit, int page);
		void Delete(ObjectId id);
	}

	public class UserConversationStatusRepository : IUserConversationStatusRepository
	{
		private readonly IMongoStorage<UserConversationStatus,ObjectId> _userConversationStatusStorage;

		public UserConversationStatusRepository(IMongoStorage<UserConversationStatus, ObjectId> userConversationStatusStorage)
		{
			_userConversationStatusStorage = userConversationStatusStorage;
		}

		public void UpdateStatus(ObjectId conversationId, ObjectId userId, ConversationStatus status)
		{
			var userConversationStatus = Get(conversationId, userId) ?? new UserConversationStatus
																			{
																				_id = ObjectId.GenerateNewId(),
																				ConversationId = conversationId,
																				UserId = userId,
																			};

			userConversationStatus.Status = (int)status;
			userConversationStatus.Date = SystemTime.Now();

			_userConversationStatusStorage.Upsert(userConversationStatus);
		}

		public UserConversationStatus Get(ObjectId conversationId, ObjectId userId)
		{
			var filtersList = new List<FilterDefinition<UserConversationStatus>>
				                  {
					                  Builders<UserConversationStatus>.Filter.Eq(x => x.ConversationId, conversationId),
					                  Builders<UserConversationStatus>.Filter.Eq(x => x.UserId, userId)
				                  };
			var filter = Builders<UserConversationStatus>.Filter.And(filtersList);
			return _userConversationStatusStorage.GetFirstOrDefault(filter);
		}

		public List<UserConversationStatus> Get(ObjectId[] conversationIds, ObjectId userId)
		{
			var filtersList = new List<FilterDefinition<UserConversationStatus>>
				                  {
					                  Builders<UserConversationStatus>.Filter.In(x => x.ConversationId, conversationIds),
					                  Builders<UserConversationStatus>.Filter.Eq(x => x.UserId, userId)
				                  };
			var filter = Builders<UserConversationStatus>.Filter.And(filtersList);

			return _userConversationStatusStorage.GetList(filter);
		}

		public IList<UserConversationStatus> GetUserConversations(ObjectId userId)
		{
			var filter = Builders<UserConversationStatus>.Filter.Eq(x => x.UserId, userId);
			return _userConversationStatusStorage.GetList(filter);
		}

		public IList<UserConversationStatus> GetConversationsStatuses(ObjectId[] conversationsIds)
		{
			var filter = Builders<UserConversationStatus>.Filter.In(x => x.ConversationId, conversationsIds);
			return _userConversationStatusStorage.GetList(filter);
		}

		public IList<UserConversationStatus> GetConversationsStatuses(ObjectId conversationsId, int limit, int page)
		{
			var filtersList = new List<FilterDefinition<UserConversationStatus>>
				                  {
					                  Builders<UserConversationStatus>.Filter.Eq(x => x.ConversationId, conversationsId),
					                  Builders<UserConversationStatus>.Filter.Ne(x => x.Status, (int) ConversationStatus.Unresolved)
				                  };

			var filter = Builders<UserConversationStatus>.Filter.And(filtersList);
			return _userConversationStatusStorage.GetList(filter).Skip((page - 1) * limit).Take(limit).ToList();
		}

		public void Delete(ObjectId id)
		{
			var removeFilter = Builders<UserConversationStatus>.Filter.Eq(x => x._id, id);
			_userConversationStatusStorage.Remove(removeFilter);
		}
	}
}