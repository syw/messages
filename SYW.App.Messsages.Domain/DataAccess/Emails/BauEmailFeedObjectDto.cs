﻿using System;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public class BauEmailFeedObjectDto : BaseEntity<ObjectId>
	{
		public string EmailId { get; set; }
		public string Logo { get; set; }
		public DateTime Published { get; set; }
		public DateTime EndDate { get; set; }
		public string SubjectLine { get; set; }
		public string Content { get; set; }
		public string BrandName { get; set; }
		public bool HasScreenshot { get; set; }
		public int ContentWidth { get; set; }
		public int ContentHeight { get; set; }
	}
}