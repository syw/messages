﻿using System.Diagnostics;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public interface IBauEmailFeedObjectMapper
	{
		BauEmailFeedObjectDto Map(BauEmailFeedObject source);
		BauEmailFeedObject Map(BauEmailFeedObjectDto source);
	}

	public class BauEmailFeedObjectMapper : IBauEmailFeedObjectMapper
	{
		public BauEmailFeedObjectDto Map(BauEmailFeedObject source)
		{
			return new BauEmailFeedObjectDto
			{
				BrandName = source.BrandName,
				Content = source.Content,
				EmailId = source.EmailId,
				EndDate = source.EndDate,
				Logo = source.Logo,
				Published = source.Published,
				SubjectLine = source.SubjectLine
			};
		}

		public BauEmailFeedObject Map(BauEmailFeedObjectDto source)
		{
			return new BauEmailFeedObject
			{
				BrandName = source.BrandName,
				Content = source.Content,
				EmailId = source.EmailId,
				EndDate = source.EndDate,
				Logo = source.Logo,
				Published = source.Published,
				SubjectLine = source.SubjectLine
			};
		}
	}
}