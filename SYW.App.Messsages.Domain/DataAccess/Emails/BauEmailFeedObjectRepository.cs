﻿using Mongo;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public interface IBauEmailFeedObjectRepository
	{
		void Add(BauEmailFeedObject bauEmailFeedObject);
	}

	public class BauEmailFeedObjectRepository : IBauEmailFeedObjectRepository
	{

		private readonly IMongoStorage<BauEmailFeedObjectDto,ObjectId> _bauEmailFeedObjectStorage;
		private readonly IBauEmailFeedObjectMapper _bauEmailFeedObjectMapper;

		public BauEmailFeedObjectRepository(IMongoStorage<BauEmailFeedObjectDto, ObjectId> bauEmailFeedObjectStorage, IBauEmailFeedObjectMapper bauEmailFeedObjectMapper)
		{
			_bauEmailFeedObjectStorage = bauEmailFeedObjectStorage;
			_bauEmailFeedObjectMapper = bauEmailFeedObjectMapper;
		}

		public void Add(BauEmailFeedObject bauEmailFeedObject)
		{
			_bauEmailFeedObjectStorage.Insert(_bauEmailFeedObjectMapper.Map(bauEmailFeedObject));
		}
	}
}