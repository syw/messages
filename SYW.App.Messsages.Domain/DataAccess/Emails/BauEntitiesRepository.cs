using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
    public class BauEntitiesRepository : IBauEntitiesRepository
    {
        private readonly IMongoStorage<BauEntity, ObjectId> _bauEntitiesStorage;

        public BauEntitiesRepository(IMongoStorage<BauEntity, ObjectId> bauEntitiesStorage)
        {
            _bauEntitiesStorage = bauEntitiesStorage;
        }

        public ObjectId? GetEntityByBrandName(string bauType)
        {
	        var allPublishers = _bauEntitiesStorage.GetAll();

	        var result = allPublishers.FirstOrDefault(p => p.BauType.ToLowerInvariant().Trim() == bauType.ToLowerInvariant().Trim());

            if (result == null)
                return null;

            return result.EntityId;
        }

        public BauEntity Add(string brandName, ObjectId entityId)
        {
            var bauEntity = new BauEntity { BauType = brandName.Trim(), EntityId = entityId, UpdatedDateTime = SystemTime.Now(), CreatedDateTime = SystemTime.Now() };
            _bauEntitiesStorage.Insert(bauEntity);
            return bauEntity;
        }

        public IList<BauEntity> GetAll()
        {
            return _bauEntitiesStorage.GetAll();
        }

        public BauEntity GetEntityById(ObjectId objectId)
        {
            var filter = Builders<BauEntity>.Filter.Eq(x => x._id, objectId);
            return _bauEntitiesStorage.GetFirstOrDefault(filter);
        }

        public BauEntity GetByEntityId(ObjectId objectId)
        {
            var filter = Builders<BauEntity>.Filter.Eq(x => x.EntityId, objectId);
            return _bauEntitiesStorage.GetFirstOrDefault(filter);
        }

		public void Remove(FilterDefinition<BauEntity> filterDefinition)
		{
			_bauEntitiesStorage.Remove(filterDefinition);
		}
    }
}