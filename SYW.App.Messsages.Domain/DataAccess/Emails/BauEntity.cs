﻿using System;
using Mongo;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public class BauEntity : BaseEntity<ObjectId>
	{
        /* 
         * Ilan: Created CreatedDateTime and UpdatedDateTime fields
         * Date: 2015-12-27 14:15 
         */

		public ObjectId EntityId { get; set; }

		public string BauType { get; set; }

        public DateTime CreatedDateTime { get; set; }
        
        public DateTime UpdatedDateTime { get; set; }  
	}
}