using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public class BauToEntitiesService : IBauToEntitiesService
	{
		private readonly IBauEntitiesRepository _bauEntitiesRepository;
		private readonly IEntitiesRepository _entitiesRepository;

		public BauToEntitiesService(IBauEntitiesRepository bauEntitiesRepository, IEntitiesRepository entitiesRepository)
		{
			_bauEntitiesRepository = bauEntitiesRepository;
			_entitiesRepository = entitiesRepository;
		}
		
		public Entity GetOrCreateEntityByName(string brandName, string logoUrl)
		{
			var entityId = _bauEntitiesRepository.GetEntityByBrandName(brandName);
			if (entityId == null)
				return CreateEntityAndLinkItToBauEntity(brandName, logoUrl);

			var linkedEntity = _entitiesRepository.Get((ObjectId)entityId);
			if (linkedEntity == null)
				throw new EntityNotFoundException((ObjectId)entityId, brandName);

			UpdateEntityLogoIfNew(logoUrl, linkedEntity);

			return linkedEntity;
		}

		private void UpdateEntityLogoIfNew(string logoUrl, Entity result)
		{
			if (result.ImageUrl == logoUrl) return;

			result.ImageUrl = logoUrl;
			_entitiesRepository.Update(result);
		}

		private Entity CreateEntityAndLinkItToBauEntity(string brandName, string logoUrl)
		{
			var result = new Entity { ImageUrl = logoUrl, Name = brandName};
			_entitiesRepository.Add(result);

			_bauEntitiesRepository.Add(brandName, result._id);

			return result;
		}
	}
}