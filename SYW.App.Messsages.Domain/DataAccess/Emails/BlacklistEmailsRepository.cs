using System.Linq;
using System.Text.RegularExpressions;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public interface IBlacklistEmailsRepository
	{
		bool IsEmailInBlackList(string email);
	}

	public class BlacklistEmailsRepository : IBlacklistEmailsRepository
	{
		private readonly IMongoStorage<BlacklistEmail, ObjectId> _blackListEmailstorage;

		public BlacklistEmailsRepository(IMongoStorage<BlacklistEmail, ObjectId> blackListEmailstorage)
		{
			_blackListEmailstorage = blackListEmailstorage;
		}

		public bool IsEmailInBlackList(string email)
		{
			var filter = Builders<BlacklistEmail>.Filter.Regex("Email", new BsonRegularExpression(new Regex(email, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace)));
			return _blackListEmailstorage.GetFirstOrDefault(filter)!=null;
		}
	}
}