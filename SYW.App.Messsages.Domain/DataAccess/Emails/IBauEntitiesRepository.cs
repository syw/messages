﻿using System;
using System.Collections;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Feeds;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public interface IBauEntitiesRepository
	{
		ObjectId? GetEntityByBrandName(string bauType);
		BauEntity Add(string brandName, ObjectId entityId);
		IList<BauEntity> GetAll();
		BauEntity GetEntityById(ObjectId objectId);
		BauEntity GetByEntityId(ObjectId objectId);
		void Remove(FilterDefinition<BauEntity> publisherFilter);
	}
}