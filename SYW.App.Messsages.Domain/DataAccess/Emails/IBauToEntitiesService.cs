﻿using MongoDB.Bson;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.DataAccess.Emails
{
	public interface IBauToEntitiesService
	{
		Entity GetOrCreateEntityByName(string brandName, string logoUrl);
	}
}