﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.DataAccess.Entities
{
    public interface IEntitiesRepository
    {
        void Add(Entity entity);
        void Update(Entity entity);
        IList<Entity> GetEntitiesByIds(long[] ids);
        IList<Entity> GetEntitiesByIds(List<ObjectId> ids);
        IList<Entity> GetAll();
        List<ObjectId> GetEntitiesIdsByEmail(IList<string> emails);
        ObjectId? GetEntityIdByOriginalEmail(string originalEmail);
    }

    public class EntitiesRepository : IEntitiesRepository
    {
        private readonly IMongoStorage<Entity, ObjectId> _entitiesStorage;

        public EntitiesRepository(IMongoStorage<Entity, ObjectId> entitiesStorage)
        {
            _entitiesStorage = entitiesStorage;
        }

        public void Add(Entity entity)
        {
            if (entity.MemberSince == null)
                entity.MemberSince = SystemTime.Now();

            _entitiesStorage.Insert(entity);
        }

        public void Update(Entity entity)
        {
            _entitiesStorage.Upsert(entity);
        }

        public IList<Entity> GetEntitiesByIds(long[] ids)
        {
            var filter = Builders<Entity>.Filter.In("OriginalId", ids);

            return _entitiesStorage.GetList(filter);
        }

        public IList<Entity> GetEntitiesByIds(List<ObjectId> ids)
        {
            var filter = Builders<Entity>.Filter.In("_id", ids);
            return _entitiesStorage.GetList(filter);
        }

        public IList<Entity> GetAll()
        {
            return _entitiesStorage.GetAll();
        }

        public ObjectId? GetEntityIdByOriginalEmail(string originalEmail)
        {
            var filter = Builders<Entity>.Filter.Regex("OriginalEmail", new BsonRegularExpression(new Regex(originalEmail, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace)));
            var projection = Builders<Entity>.Projection.Include(x => x._id);
            var entity = _entitiesStorage.GetFirstOrDefault(filter, projection);

            return entity == null ? null : (ObjectId?)entity["_id"];
        }

        public List<ObjectId> GetEntitiesIdsByEmail(IList<string> emails)
        {
            var regexFilter = "(" + string.Join("|", emails) + ")";
            var projection = Builders<Entity>.Projection.Include(x => x._id);
            var filter = Builders<Entity>.Filter.Regex("Email", new BsonRegularExpression(new Regex(regexFilter, RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace)));
            var matchedEntities = _entitiesStorage.SelectAs(filter, projection);
            return matchedEntities.Select(x => x["_id"].AsObjectId).ToList();
        }
    }

    public static class EntitiesRepositoryExtentions
    {
        public static ObjectId? GetEntitiesIdsByEmail(this IEntitiesRepository repository, string emails)
        {
            var ids = repository.GetEntitiesIdsByEmail(new[] { emails });

            if (ids == null) return null;

            return ids.Any() ? (ObjectId?)ids.FirstOrDefault() : null;
        }
    }
}