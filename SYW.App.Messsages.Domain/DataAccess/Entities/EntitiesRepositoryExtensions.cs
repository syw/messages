using System.Collections.Generic;
using MongoDB.Bson;
using System.Linq;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.DataAccess.Entities
{
	public static class EntitiesRepositoryExtensions
	{
		public static Entity Get(this IEntitiesRepository entitiesRepository, ObjectId id)
		{
			return entitiesRepository.GetEntitiesByIds(new List<ObjectId> {id}).EmptyIfNull().FirstOrDefault();
		}

		public static Entity Get(this IEntitiesRepository entitiesRepository, long originalId)
		{
			return entitiesRepository.GetEntitiesByIds(new[] {originalId}).EmptyIfNull().FirstOrDefault();
		}
	}
}