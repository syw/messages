﻿using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.DataAccess.Entities
{
	public interface IEntityNetworkRepository
	{
		void UpdateNetwork(ObjectId entityId, Entity[] friends);
		EntityNetwork GetNetwork(ObjectId entityId);
	}

	public class EntityNetworkRepository : IEntityNetworkRepository
	{
		private readonly IMongoStorage<EntityNetwork,ObjectId> _storage;

		public EntityNetworkRepository(IMongoStorage<EntityNetwork, ObjectId> storage)
		{
			_storage = storage;
		}

		public void UpdateNetwork(ObjectId entityId, Entity[] friends)
		{
			var filter = Builders<EntityNetwork>.Filter.Eq(x => x.EntityId, entityId);
			var network = _storage.GetFirstOrDefault(filter);

			if (network == null)
			{
				_storage.Insert(new EntityNetwork {EntityId = entityId, Friends = friends, CreatedDateTime = SystemTime.Now()});
			}
			else
			{
				network.Friends = friends;
			    network.UpdatedDateTime = SystemTime.Now();
				_storage.Upsert(network);
			}
		}

		public EntityNetwork GetNetwork(ObjectId entityId)
		{
			var filter = Builders<EntityNetwork>.Filter.Eq(x => x.EntityId, entityId);
			var network = _storage.GetFirstOrDefault(filter);
			return network;
		}
	}
}