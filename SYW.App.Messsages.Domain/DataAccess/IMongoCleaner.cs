﻿using Mongo;

namespace SYW.App.Messsages.Domain.DataAccess
{
	public interface IMongoCleaner
	{
		void Clean();
	}

	public class MongoCleaner : IMongoCleaner
	{
		private readonly IMongoDatabaseProvider _mongoDatabaseProvider;

		public MongoCleaner(IMongoDatabaseProvider mongoDatabaseProvider)
		{
			_mongoDatabaseProvider = mongoDatabaseProvider;
		}

		public void Clean()
		{
			var database = _mongoDatabaseProvider.Database;
			database.Client.DropDatabaseAsync(_mongoDatabaseProvider.Connection.DatabaseName);
		}
	}
}