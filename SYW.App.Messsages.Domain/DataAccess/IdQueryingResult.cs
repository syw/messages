﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.DataAccess
{
	public class IdQueryingResult
	{
		[BsonId]
		public ObjectId Id { get; set; }
	}
}
