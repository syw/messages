﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using CommonGround.Utilities;
using SYW.App.Messsages.Domain.Services;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public interface ICacheSystemMessageProvider
	{
		IList<MessageDescriptor> GetAll();
		IList<MessageDescriptor> GetAll(ObjectId conversationId);
		IDictionary<ObjectId, MessageDescriptor> GetLatest(IEnumerable<ObjectId> conversationIds);
	}

	public class CacheSystemMessageProvider : ICacheSystemMessageProvider
	{
		private readonly IHttpContextProvider _httpContextProvider;
		private readonly IMessageRepository _messageRepository;
		private readonly IConversationRepository _conversationRepository;
		private readonly ISystemMessagesSettings _systemMessagesSettings;

		private const string CACHE_KEY = "SystemMessagesDescriptorsCache";

		public CacheSystemMessageProvider(
			IHttpContextProvider httpContextProvider, 
			IMessageRepository messageRepository,
			IConversationRepository conversationRepository,
			ISystemMessagesSettings systemMessagesSettings)
		{
			_httpContextProvider = httpContextProvider;
			_messageRepository = messageRepository;
			_conversationRepository = conversationRepository;
			_systemMessagesSettings = systemMessagesSettings;
		}

		public IList<MessageDescriptor> GetAll()
		{
			var messages = _httpContextProvider.GetContext().Cache.Get(CACHE_KEY) as IList<MessageDescriptor>;
			if (messages != null && messages.Any())
				return messages;

			messages = _messageRepository.GetAllDescriptors(_conversationRepository.GetSystemConversationIds());

			CacheSystemMessages(messages);

			return messages;
		}

		public IList<MessageDescriptor> GetAll(ObjectId conversationId)
		{
			return GetAll().Where(x => x.ConversationId == conversationId).ToList();
		}

		public IDictionary<ObjectId, MessageDescriptor> GetLatest(IEnumerable<ObjectId> conversationIds)
		{
			var messageDtos = GetAll().Where(m => conversationIds.Contains(m.ConversationId)).ToList();

			var result = messageDtos.GroupBy(m => m.ConversationId)
							.ToDictionary(x => x.Key, x => x.OrderBy(m => m.Date).Last());
			return result;
		}

		private void CacheSystemMessages(IList<MessageDescriptor> messages)
		{
			_httpContextProvider.GetContext().Cache.Add(CACHE_KEY, messages, null,
														SystemTime.Now().AddHours(_systemMessagesSettings.CacheExpirationTimeInHours),
														Cache.NoSlidingExpiration,
														CacheItemPriority.High, null);
		}
	}
}