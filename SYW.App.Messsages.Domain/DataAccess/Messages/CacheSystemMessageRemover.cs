﻿using SYW.App.Messsages.Domain.Services;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public interface ICacheSystemMessageRemover
	{
		void Clear();
	}

	public class CacheSystemMessageRemover : ICacheSystemMessageRemover
	{
		private readonly IHttpContextProvider _httpContextProvider;

		public CacheSystemMessageRemover(IHttpContextProvider httpContextProvider)
		{
			_httpContextProvider = httpContextProvider;
		}

		public void Clear()
		{
			_httpContextProvider.GetContext().Cache.Remove("SystemMessagesDescriptorsCache");
		}
	}
}