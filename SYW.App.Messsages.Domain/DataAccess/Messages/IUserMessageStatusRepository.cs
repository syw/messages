﻿using System.Collections.Generic;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public interface IUserMessageStatusRepository
	{
		void UpdateStatus(ObjectId conversationId, ObjectId messageId, ObjectId userId, int status);
		UserMessageStatus Get(ObjectId messageId, ObjectId userId);
		List<UserMessageStatus> GetAll(ObjectId conversationId, ObjectId userId);
		List<UserMessageStatus> GetAll(ObjectId[] conversationId, ObjectId userId);
		List<UserMessageStatus> GetAll(ObjectId userId);
	}
}