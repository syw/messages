﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SYW.App.Messsages.Domain.Conversations;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public class MessageDescriptor
	{
		[BsonId]
		public ObjectId Id { get; set; }

		public ObjectId ConversationId { get; set; }

		public ObjectId AuthorId { get; set; }

		public DateTime Date { get; set; }

		[BsonIgnoreIfDefault, BsonIgnoreIfNull]
		public DateTime? EndDate { get; set; }

		public int ContentType { get; set; }

		public string PreviewText { get; set; }

		public int ContentHeight { get; set; }

		public int ContentWidth { get; set; }

		[BsonIgnore]
		public ConversationStatus Status { get; set; }

		public MessageDescriptor()
		{
			Status = ConversationStatus.Unread;
		}
	}
}