﻿using System;
using System.Collections.Generic;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public class MessageDto : BaseEntity<ObjectId>
	{
		public ObjectId ConversationId { get; set; }

		public ObjectId AuthorId { get; set; }

		public DateTime Date { get; set; }

		public string Subject { get; set; }

		[BsonIgnoreIfDefault, BsonIgnoreIfNull]
		public DateTime? EndDate { get; set; }

		public string Content { get; set; }

		public IList<Attachment> Attachments { get; set; }

		public int ContentType { get; set; }

		public string PreviewText { get; set; }

		public int ContentHeight { get; set; }

		public int ContentWidth { get; set; }

		public string FeedMessageId { get; set; }

		public string AttachedImagesUrl { get; set; }
	}

	public enum MessageContentType
	{
		Text = 0,
		Html = 1,
		FormattedText = 2
	}
}