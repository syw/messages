﻿using System;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public static class MessageMapper
	{
		public static Message MapFromDto(MessageDto messageDto)
		{
			return new Message
						{
							Author = new Entity { _id = messageDto.AuthorId },
							ConversationId = messageDto.ConversationId,
							Content = messageDto.Content,
							Attachments = messageDto.Attachments,
							Date = messageDto.Date,
							EndDate = messageDto.EndDate ?? default(DateTime),
							Id = messageDto._id,
							PreviewText = messageDto.PreviewText,
							ContentType = (MessageContentType)messageDto.ContentType,
							ContentHeight = messageDto.ContentHeight,
							ContentWidth = messageDto.ContentWidth,
							AttachedImagesUrl = messageDto.AttachedImagesUrl
						};
		}

		public static Message MapFromDescriptor(MessageDescriptor messageDescriptor)
		{
			return new Message
						{
							Author = new Entity { _id = messageDescriptor.AuthorId },
							ConversationId = messageDescriptor.ConversationId,
							Date = messageDescriptor.Date,
                            EndDate = messageDescriptor.EndDate ?? default(DateTime),
							Id = messageDescriptor.Id,
							PreviewText = messageDescriptor.PreviewText,
							ContentType = (MessageContentType)messageDescriptor.ContentType,
							ContentHeight = messageDescriptor.ContentHeight,
							ContentWidth = messageDescriptor.ContentWidth
						};
		}

		public static MessageDto MapToDto(Message message)
		{
			return new MessageDto
			{
				AttachedImagesUrl = message.AttachedImagesUrl,
				Attachments = message.Attachments,
				AuthorId = message.Author._id,
				Content = message.Content,
				ContentHeight = message.ContentHeight,
				ContentType = (int)message.ContentType,
				ContentWidth = message.ContentWidth,
				ConversationId = message.ConversationId,
				Date = message.Date,
				EndDate = message.EndDate,
				_id = message.Id,
				PreviewText = message.PreviewText,
				FeedMessageId = message.FeedMessageId
			};
		}
	}
}