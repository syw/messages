﻿using System.Collections.Generic;
using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Messages;
using StackExchange.Profiling;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public interface IMessageRepository
	{
		Message Create(ObjectId conversationId, ObjectId authorId, string content, string attachedImages = null, IList<Attachment> attachments = null);
		Message CreateFormattedMessage(ObjectId conversationId, ObjectId authorId, string content, string attachmentsUrl = "", IList<Attachment> attachments = null);
		Message CreateSystemMessage(ObjectId conversationId, SystemMessage systemMessage);
		Message CreateSystemMessage(ObjectId conversationId, SystemMessage systemMessage, ObjectId id);

		Message GetLatest(ObjectId conversationId);
		IDictionary<ObjectId, Message> GetLatest(ObjectId[] conversationIds);
		IList<Message> GetAll(ObjectId conversationId);
		IList<Message> GetAll(ObjectId[] conversationIds);
		Message GetMessageById(string messageId);
		IList<MessageDescriptor> GetAllDescriptors(IEnumerable<ObjectId> conversationsIds);

		string GetMessageContentById(string messageId);
		IList<ObjectId> GetOrderedConversationSenders(ObjectId conversationId);
		void Delete(ObjectId messageId);
	}

	public class MessageRepository : IMessageRepository
	{
		private readonly IMongoStorage<MessageDto, ObjectId> _messagesStorage;

		public MessageRepository(IMongoStorage<MessageDto, ObjectId> messagesStorage)
		{
			_messagesStorage = messagesStorage;
		}

		public Message Create(ObjectId conversationId, ObjectId authorId, string content, string attachedImages = null, IList<Attachment> attachments = null)
		{
			if (attachments != null)
				attachments = attachments.Where(a => a.IsValid()).ToList();

			return CreateMessage(new MessageDto
									{
										_id = ObjectId.GenerateNewId(),
										AuthorId = authorId,
										Content = content,
										Attachments = attachments,
										ConversationId = conversationId,
										Date = SystemTime.Now(),
                                        EndDate = null,
										ContentType = (int)MessageContentType.Text,
										AttachedImagesUrl = attachedImages});
		}

		public Message CreateFormattedMessage(ObjectId conversationId, ObjectId authorId, string content, string attachmentsUrl = "", IList<Attachment> attachments = null)
		{
			return CreateMessage(new MessageDto
			{
				_id = ObjectId.GenerateNewId(),
				AuthorId = authorId,
				Content = content,
				Attachments = attachments,
				ConversationId = conversationId,
				Date = SystemTime.Now(),
                EndDate = null,
				ContentType = (int)MessageContentType.FormattedText,
                AttachedImagesUrl = attachmentsUrl
			});
		}

		public Message CreateSystemMessage(ObjectId conversationId, SystemMessage systemMessage)
		{
			return CreateSystemMessage(conversationId, systemMessage, ObjectId.GenerateNewId());
		}

		public Message CreateSystemMessage(ObjectId conversationId, SystemMessage systemMessage, ObjectId id)
		{
			return CreateMessage(new MessageDto
									{
										_id = id,
										FeedMessageId = systemMessage.FeedId,
										AuthorId = systemMessage.AuthorId,
										Content = systemMessage.Content,
										PreviewText = systemMessage.PreviewText,
										ConversationId = conversationId,
										Date = systemMessage.PublishDate,
										ContentType = (int)(systemMessage.IsHtml ? MessageContentType.Html : MessageContentType.Text),
										ContentWidth = systemMessage.Width,
										ContentHeight = systemMessage.Height,
										EndDate = systemMessage.endDate
									});
		}

		private Message CreateMessage(MessageDto messageDto)
		{
			_messagesStorage.Upsert(messageDto);
			return MessageMapper.MapFromDto(messageDto);
		}

		public IList<Message> GetAll(ObjectId conversationId)
		{
			var filter = Builders<MessageDto>.Filter.Eq(x => x.ConversationId, conversationId);
			return _messagesStorage.GetList(filter).Select(MessageMapper.MapFromDto).ToList();
		}

		public IList<Message> GetAll(ObjectId[] conversationIds)
		{
			var filter = Builders<MessageDto>.Filter.In(x => x.ConversationId, conversationIds);
			return _messagesStorage.GetList(filter).Select(MessageMapper.MapFromDto).ToList();
		}

		public Message GetMessageById(string messageId)
		{
			using (MiniProfiler.Current.Step("MessageRepository.GetMessageById"))
			{
				var filter = Builders<MessageDto>.Filter.Eq(x => x._id, ObjectId.Parse(messageId));
				var message = _messagesStorage.GetFirstOrDefault(filter);
				return message == null ? null : MessageMapper.MapFromDto(message);
			}
		}

		public IDictionary<ObjectId, Message> GetLatest(ObjectId[] conversationIds)
		{
			var filter = Builders<MessageDto>.Filter.In(x => x.ConversationId, conversationIds);
			var messageDto = _messagesStorage.GetList(filter);

			return messageDto.GroupBy(m => m.ConversationId).ToDictionary(x => x.Key, x => MessageMapper.MapFromDto(x.OrderBy(m => m.Date).Last()));
		}

		public Message GetLatest(ObjectId conversationId)
		{
			var filter = Builders<MessageDto>.Filter.Eq(x => x.ConversationId, conversationId);
			var orderBy = Builders<MessageDto>.Sort.Descending(x => x.Date);
			var messageDto = _messagesStorage.GetFirstOrDefault(filter, orderBy);

			return MessageMapper.MapFromDto(messageDto);
		}

		public IList<MessageDescriptor> GetAllDescriptors(IEnumerable<ObjectId> conversationsIds)
		{
			var filter = Builders<MessageDto>.Filter.In("ConversationId", conversationsIds);
			var orderBy = Builders<MessageDto>.Sort.Descending(x=>x.Date);
			return _messagesStorage.GetList(filter, orderBy).Select(y=>new MessageDescriptor{AuthorId = y.AuthorId,PreviewText = y.PreviewText,Id = y._id,ConversationId = y.ConversationId,ContentWidth = y.ContentWidth,Date = y.Date,ContentHeight = y.ContentHeight,ContentType = y.ContentType}).ToList();
		}

		public string GetMessageContentById(string messageId)
		{
			var projection = Builders<MessageDto>.Projection.Include(x => x.Content);
			var filter = Builders<MessageDto>.Filter.Eq(x => x._id, new ObjectId(messageId));
			var result = _messagesStorage.GetFirstOrDefault(filter,projection);
			return (result != null) ? result["Content"].AsString : string.Empty;
		}

		public IList<ObjectId> GetOrderedConversationSenders(ObjectId conversationId)
		{
			//todo: check distinct with mongo
			var filter = Builders<MessageDto>.Filter.Eq("ConversationId", conversationId);
			var projection = Builders<MessageDto>.Projection.Include(x => x.AuthorId).Include(y => y.Date);
			var result = _messagesStorage.SelectAs(filter, projection);
			return result.OrderByDescending(m => m["Date"].ToUniversalTime()).Select(m => m["AuthorId"].AsObjectId).Distinct().ToList();
		}

		public void Delete(ObjectId messageId)
		{
			var removeFilter = Builders<MessageDto>.Filter.Eq(x => x._id, messageId);
			_messagesStorage.Remove(removeFilter);
		}
	}
}