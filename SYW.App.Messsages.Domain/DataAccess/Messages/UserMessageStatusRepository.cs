﻿using System.Collections.Generic;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SYW.App.Messsages.Domain.DataAccess.Messages
{
	public class UserMessageStatusRepository : IUserMessageStatusRepository
	{
		private readonly IMongoStorage<UserMessageStatus, ObjectId> _userMessageStatusStorage;

		public UserMessageStatusRepository(IMongoStorage<UserMessageStatus, ObjectId> userMessageStatusStorage)
		{
			_userMessageStatusStorage = userMessageStatusStorage;
		}

		public void UpdateStatus(ObjectId conversationId, ObjectId messageId, ObjectId userId, int status)
		{
			var userMessageStatus = Get(messageId, userId) ?? new UserMessageStatus
																{
																	_id = ObjectId.GenerateNewId(),
																	MessageId = messageId,
																	UserId = userId,
																	ConversationId = conversationId,
																};

			userMessageStatus.Status = status;
			userMessageStatus.Date = SystemTime.Now();

			_userMessageStatusStorage.Upsert(userMessageStatus);
		}

		public UserMessageStatus Get(ObjectId messageId, ObjectId userId)
		{
			var filtersList=new List<FilterDefinition<UserMessageStatus>>
				                {
					                Builders<UserMessageStatus>.Filter.Eq(x => x.MessageId, messageId),
					                Builders<UserMessageStatus>.Filter.Eq(x => x.UserId, userId)
				                };
			var filter = Builders<UserMessageStatus>.Filter.And(filtersList);
			return _userMessageStatusStorage.GetFirstOrDefault(filter);
		}

		public List<UserMessageStatus> GetAll(ObjectId conversationId, ObjectId userId)
		{
			var filtersList = new List<FilterDefinition<UserMessageStatus>>
				                  {
					                  Builders<UserMessageStatus>.Filter.Eq(x => x.ConversationId, conversationId),
					                  Builders<UserMessageStatus>.Filter.Eq(x => x.UserId, userId)
				                  };
			var filter = Builders<UserMessageStatus>.Filter.And(filtersList);

			return _userMessageStatusStorage.GetList(filter);
		}

		public List<UserMessageStatus> GetAll(ObjectId[] conversationId, ObjectId userId)
		{
			var filtersList = new List<FilterDefinition<UserMessageStatus>>
				                  {
					                  Builders<UserMessageStatus>.Filter.In(x => x.ConversationId, conversationId),
					                  Builders<UserMessageStatus>.Filter.Eq(x => x.UserId, userId)
				                  };
			var filter = Builders<UserMessageStatus>.Filter.And(filtersList);
			return _userMessageStatusStorage.GetList(filter);
		}

		public List<UserMessageStatus> GetAll(ObjectId userId)
		{
			var filter = Builders<UserMessageStatus>.Filter.Eq(x => x.UserId, userId);
			return _userMessageStatusStorage.GetList(filter);
		}
	}
}