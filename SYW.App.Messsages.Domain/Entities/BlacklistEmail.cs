using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.Entities
{
	public class BlacklistEmail : BaseEntity<ObjectId>
	{
		public string Email { get; set; }
	}
}