﻿using System.Collections.Generic;
using System.Linq;
using SYW.App.Messsages.Domain.DataAccess.Entities;

namespace SYW.App.Messsages.Domain.Entities
{
	public interface IEntitiesResolver
	{
		void Resolve(IList<Entity> unresolvedEntities);
	}

	public class EntitiesResolver : IEntitiesResolver
	{
		private readonly IEntitiesRepository _entitiesRepository;

		public EntitiesResolver(IEntitiesRepository entitiesRepository)
		{
			_entitiesRepository = entitiesRepository;
		}

		public void Resolve(IList<Entity> unresolvedEntities)
		{
			var ids = unresolvedEntities.Select(e => e._id).ToList();

			var resolvedEntities = _entitiesRepository.GetEntitiesByIds(ids)
													.Where(e => e != null);

			unresolvedEntities.JoinDo(resolvedEntities, u => u._id, r => r._id, (u, r) =>
																				{
																					u.OriginalId = r.OriginalId;
																					u.ImageUrl = r.ImageUrl;
																					u.Name = r.Name;
																					u.OriginalEmail = r.OriginalEmail;
																					u.Email = r.Email;
																				});
		}
	}
}