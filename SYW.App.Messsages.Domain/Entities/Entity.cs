﻿using System;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace SYW.App.Messsages.Domain.Entities
{
	public class Entity : BaseEntity<ObjectId>
	{
		public long OriginalId { get; set; }

		public string Name { get; set; }

		public string ImageUrl { get; set; }

		public string OriginalEmail { get; set; }

		public string Email { get; set; }

		public DateTime LastActivity { get; set; }

		public string PlatformOfflineToken { get; set; }

		[BsonIgnoreIfDefault, BsonIgnoreIfNull]
		public DateTime? MemberSince { get; set; }
	}
}