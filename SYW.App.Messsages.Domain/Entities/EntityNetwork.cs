﻿using System;
using Mongo;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Entities
{
	public class EntityNetwork : BaseEntity<ObjectId>
	{
        /* 
         * Ilan: Created CreatedDateTime and UpdatedDateTime fields
         * Date: 2015-12-27 14:15 
         */

		public ObjectId EntityId { get; set; }

		public Entity[] Friends { get; set; }

	    public DateTime CreatedDateTime { get; set; }

	    public DateTime UpdatedDateTime { get; set; }
	}
}