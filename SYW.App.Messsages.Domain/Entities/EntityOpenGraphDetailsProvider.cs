﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AngleSharp;
using AngleSharp.Dom;
using log4net;
using log4net.Core;
using SYW.App.Messsages.Domain.Exceptions;

namespace SYW.App.Messsages.Domain.Entities
{
    public interface IEntityOpenGraphDetailsProvider
    {
        Task<EntityOpenGraphDetails> Provide(string url);
    }

    public class EntityOpenGraphDetailsProvider : IEntityOpenGraphDetailsProvider
    {
        private readonly ILog _log;

        public async Task<EntityOpenGraphDetails> Provide(string url)
        {
            if (url.IsNullOrEmpty())
                return new EntityOpenGraphDetails();

            try
            {
                var document = await GetDocument(url);

                var cells = document.QuerySelectorAll("meta");
                var title = GetOpenGraphTag(cells, "og:title");
                var image = GetOpenGraphTag(cells, "og:image");

                return new EntityOpenGraphDetails { EntityImageUrl = image, EntityTitle = title, EntityLink = url };
            }
            catch (Exception e)
            {
                _log.Error(string.Format("Error retrieving data fro murl:[{0}]"),e);
                return new EntityOpenGraphDetails();
            }
        }

        private async Task<IDocument> GetDocument(string url)
        {
            var config = Configuration.Default.WithDefaultLoader();
            var document = await BrowsingContext.New(config).OpenAsync(url);
            return document;
        }

        private string GetOpenGraphTag(IEnumerable<IElement> elements, string openGraphTag)
        {
            return elements
                    .Where(m => m.Attributes["property"] != null && m.Attributes["property"].Value == openGraphTag)
                    .Select(x => x.Attributes["content"].Value)
                    .FirstOrDefault();
        }
    }

    public class EntityOpenGraphDetails
    {
        public string EntityTitle { get; set; }
        public string EntityImageUrl { get; set; }
        public string EntityLink { get; set; }
    }
}
