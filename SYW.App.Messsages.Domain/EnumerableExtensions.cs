﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SYW.App.Messsages.Domain
{
	public static class EnumerableExtensions
	{
		/// <summary>
		/// 	Determines whether the sequence is null or contains no elements.
		/// </summary>
		/// <typeparam name="T"> </typeparam>
		/// <param name="target"> </param>
		/// <returns> </returns>
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> target)
		{
			return target == null || !target.Any();
		}

		/// <summary>
		/// 	Replaces null with an empty sequence.
		/// </summary>
		public static IEnumerable<T> EmptyIfNull<T>(this IEnumerable<T> target)
		{
			return target ?? Enumerable.Empty<T>();
		}

		/// <summary>
		/// 	Consumes the sequence by iterating over it.
		/// </summary>
		public static void Consume<T>(this IEnumerable<T> target)
		{
			foreach (var item in target)
			{
				// nop, we just want to iterate over the sequence
			}
		}

		/// <summary>
		/// 	Correlates the elements in two sequences according to a shared key, then performs an action for each matching pair.
		/// </summary>
		public static void JoinDo<TLeft, TRight, TKey>(this IEnumerable<TLeft> left, IEnumerable<TRight> right, Func<TLeft, TKey> leftKeySelector, Func<TRight, TKey> rightKeySelector, Action<TLeft, TRight> matchAction)
		{
			left.Join(right, leftKeySelector, rightKeySelector, (l, r) =>
				                                                    {
					                                                    matchAction(l, r);
					                                                    return 0;
				                                                    }).Consume();
		}

		/// <summary>
		/// 	Yields bulks of equal size from the sequence.
		/// </summary>
		/// <returns> A deferred sequence of bulks, each of which (except maybe the last one) consisting of <see cref="bulkSize" /> items from the sequence. </returns>
		public static IEnumerable<IList<T>> Bulks<T>(this IEnumerable<T> source, int bulkSize)
		{
			return Bulks(source, bulkSize, size => size);
		}

		/// <summary>
		/// 	Yields bulks of changing size from the enumerator.
		/// </summary>
		/// <returns> A deferred sequence of bulks, where each bulk's size is the result of applying the <see
		/// 	 cref="bulkSizeModifier" /> to the previous bulk's size. </returns>
		public static IEnumerable<IList<T>> Bulks<T>(this IEnumerable<T> source, int initialBulkSize, Func<int, int> bulkSizeModifier)
		{
			if (source == null) throw new ArgumentNullException("source");

			var bulkSize = initialBulkSize;
			if (bulkSize <= 0) throw new ArgumentOutOfRangeException("bulkSize", "bulk size must be positive");

			var bulker = new BulkedEnumerator<T>(source);

			// Consume the sequence in bulks until we hit its end
			while (bulker.TakeNext(bulkSize))
			{
				yield return bulker.Current;
				bulkSize = bulkSizeModifier(bulkSize);
			}
		}

		/// <summary>
		/// 	Returns only items from the first list that are not in the second list according to a key selector.
		/// </summary>
		public static IEnumerable<T> Minus<T, TKey>(this IEnumerable<T> left, IEnumerable<T> right, Func<T, TKey> keySelector)
		{
			return Minus(left, right, keySelector, keySelector);
		}

		/// <summary>
		/// 	Returns only items from the first list that are not in the second list according to a key selector.
		/// </summary>
		public static IEnumerable<T> Minus<T, TOther, TKey>(this IEnumerable<T> left, IEnumerable<TOther> right, Func<T, TKey> leftKeySelector, Func<TOther, TKey> rightKeySelector)
		{
			if (left.IsNullOrEmpty())
				return Enumerable.Empty<T>();

			var rightKeys = new HashSet<TKey>(right.Select(rightKeySelector));
			return left.Where(x => !rightKeys.Contains(leftKeySelector(x)));
		}
	}
}