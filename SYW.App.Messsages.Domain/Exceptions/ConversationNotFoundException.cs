using System;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Exceptions
{
	public class ConversationNotFoundException : Exception
	{
		public ConversationNotFoundException(ObjectId conversationId) : base("Conversation " + conversationId + " does not exist") {}
	}
}