﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Exceptions
{
	public class EntityNotFoundException : Exception
	{
		public EntityNotFoundException(ObjectId conversationId)
			: base("Conversation " + conversationId + " does not exist") {}

		public EntityNotFoundException(ObjectId conversationId, string entityName)
			: base("Conversation " + conversationId + " does not exist, looking for" + entityName) {}
	}
}