using System;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Exceptions
{
	public class ForbiddenArchiveConversationException : Exception
	{
		public ForbiddenArchiveConversationException(ObjectId conversationId, ObjectId userId)
			: base(string.Format("User {0} is trying to archive converstaion {1} while he is not part of the conversation", userId, conversationId)) {}
	}
}