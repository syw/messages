﻿using System;

namespace SYW.App.Messsages.Domain.Exceptions
{
	public class ForbiddenOperationException : Exception
	{
		public ForbiddenOperationException(string message) : base(message) {}
	}
}