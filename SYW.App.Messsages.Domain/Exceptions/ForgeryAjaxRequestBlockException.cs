﻿namespace SYW.App.Messsages.Domain.Exceptions
{
	public class ForgeryAjaxRequestBlockException : ForbiddenOperationException
	{
		public ForgeryAjaxRequestBlockException()
			: base("Are you trying to hack into our wonderful app ?")
		{
		}
	}
}