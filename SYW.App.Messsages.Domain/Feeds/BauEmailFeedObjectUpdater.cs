﻿using SYW.App.Messsages.Domain.DataAccess.Emails;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEmailFeedObjectUpdater
	{
		void Create(BauEmailFeedObject bauEmailFeedObject);
	}

	public class BauEmailFeedObjectUpdater : IBauEmailFeedObjectUpdater
	{
		private readonly IBauEmailFeedObjectRepository _bauEmailFeedObjectRepository;

		public BauEmailFeedObjectUpdater(
			IBauEmailFeedObjectRepository bauEmailFeedObjectRepository)
		{
			_bauEmailFeedObjectRepository = bauEmailFeedObjectRepository;
		}

		public void Create(BauEmailFeedObject bauEmailFeedObject)
		{
			_bauEmailFeedObjectRepository.Add(bauEmailFeedObject);
		}
	}
}