using System;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEmailFeedParser
	{
		BauEmailFeedObject Parse(XElement feedElement);
	}

	public class BauEmailFeedParser : IBauEmailFeedParser
	{
		public BauEmailFeedObject Parse(XElement feedElement)
		{
			return new BauEmailFeedObject
			{
				EmailId = feedElement.Descendants().First(x => x.Name.LocalName == "id").Value,
				Published = DateTime.Parse(feedElement.Descendants().First(x => x.Name.LocalName == "published").Value, null, DateTimeStyles.AdjustToUniversal),
				EndDate = DateTime.Parse(feedElement.Descendants().First(x => x.Name.LocalName == "expires").Value, null, DateTimeStyles.AdjustToUniversal),
				Logo = feedElement.Descendants().First(x => x.Name.LocalName == "link").Attribute("href").Value,
				SubjectLine = feedElement.Descendants().First(x => x.Name.LocalName == "title").Value,
				Content = feedElement.Descendants().First(x => x.Name.LocalName == "content").Value,
				BrandName = feedElement.Descendants().First(x => x.Name.LocalName == "category").Attribute("label").Value.Trim()
			};
		}
	}
}