using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using log4net;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEmailFeedProvider
	{
		List<BauEmailFeedObject> GetFeed();
	}

	public class BauEmailFeedProvider : IBauEmailFeedProvider
	{
		private readonly IBauEmailFeedProviderSettings _bauEmailFeedProviderSettings;
		private readonly IExternalFeedReader _externalFeedReader;
		private readonly ILog _logger;
		private readonly IBauEmailFeedParser _bauEmailFeedParser;

		public BauEmailFeedProvider(IExternalFeedReader externalFeedReader, 
									ILog logger, 
									IBauEmailFeedProviderSettings bauEmailFeedProviderSettings, 
									IBauEmailFeedParser bauEmailFeedParser)
		{
			_externalFeedReader = externalFeedReader;
			_logger = logger;
			_bauEmailFeedProviderSettings = bauEmailFeedProviderSettings;
			_bauEmailFeedParser = bauEmailFeedParser;
		}

		public List<BauEmailFeedObject> GetFeed()
		{
			var bauEmails = _externalFeedReader.Read(_bauEmailFeedProviderSettings.Url)
												.Select(Parse)
												.Where(f => f != null)
												.ToList();
			return bauEmails;
		}

		private BauEmailFeedObject Parse(XElement feed)
		{
			try
			{
				return _bauEmailFeedParser.Parse(feed);
			}
			catch (Exception ex)
			{
				_logger.Error("Error while trying to parse feed.", ex);
				return null;
			}
		}
	}
}