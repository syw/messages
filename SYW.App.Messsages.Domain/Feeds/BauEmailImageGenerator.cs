﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using Castle.Core.Logging;
using MongoDB.Bson;
using MongoDB.Driver;
using Mongo;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using log4net;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEmailImageGenerator
	{
		void GenerateImage();
	}

	public class BauEmailImageGenerator : IBauEmailImageGenerator
	{
		private readonly IMongoStorage<BauEmailFeedObjectDto,ObjectId> _mongoStorage;
		private readonly ILog _log;
		private readonly IBauEmailImageGeneratorSettings _bauEmailImageGeneratorSettings;
		private readonly IBauScreenshotsFilesService _bauScreenshotsFilesService;
		private readonly ITumbnailCreatorService _tumbnailCreatorService;

		public BauEmailImageGenerator(IMongoStorage<BauEmailFeedObjectDto, ObjectId> mongoStorage, ILog log, IBauEmailImageGeneratorSettings bauEmailImageGeneratorSettings, IBauScreenshotsFilesService bauScreenshotsFilesService, ITumbnailCreatorService tumbnailCreatorService)
		{
			_mongoStorage = mongoStorage;
			_log = log;
			_bauEmailImageGeneratorSettings = bauEmailImageGeneratorSettings;
			_bauScreenshotsFilesService = bauScreenshotsFilesService;
			_tumbnailCreatorService = tumbnailCreatorService;
		}

		public void GenerateImage()
		{
			var filter = Builders<BauEmailFeedObjectDto>.Filter.Eq(x => x.HasScreenshot, false);
			var bauEmails = _mongoStorage.GetList(filter);

			foreach (var bulkedBauEmails in bauEmails.Bulks(5))
			{
				var unexistingScreenshots = GetUnexistingScreenshots(bulkedBauEmails);

				var phantomJsParameters = string.Join(" ", unexistingScreenshots.ToArray());

				try
				{
					var errors = RenderBauEmailUsingPhantomJs(phantomJsParameters);

					if (!string.IsNullOrEmpty(errors))
					{
						Console.WriteLine("Something went wrong when calling phantomjs with " + phantomJsParameters + "\r\n" + errors);
						_log.Error("Something went wrong when calling phantomjs with " + phantomJsParameters + "\r\n" + errors);
						continue;
					}

					foreach (var bauEmail in bulkedBauEmails)
					{
						try
						{
							MigrateScreenshotToStaticContent(bauEmail);
						}
						catch (Exception exception)
						{
							Console.WriteLine("Something went wrong when migrating the screenshot to the static content \r\n" + exception);
							_log.Error("Something went wrong when migrating the screenshot to the static content", exception);
						}
					}
				}
				catch (Exception exception)
				{
					_log.Error("Something went wrong when calling phantomjs with " + phantomJsParameters, exception);
				}

			}
		}

		private string RenderBauEmailUsingPhantomJs(string phantomJsParameters)
		{
			var parameters = "--ssl-protocol=any render_bauemails.js " + _bauEmailImageGeneratorSettings.BauImageRenderingUrl + " " + phantomJsParameters;

			var info = new ProcessStartInfo(Path.Combine(_bauEmailImageGeneratorSettings.PhatomJsPath, "phantomjs.exe"), parameters)
			{
				RedirectStandardInput = true,
				RedirectStandardOutput = true,
				RedirectStandardError = true,
				UseShellExecute = false,
				CreateNoWindow = true,
				WorkingDirectory = _bauEmailImageGeneratorSettings.PhatomJsPath
			};

			var p = Process.Start(info);
			p.Start();
			p.WaitForExit();

			var errors = p.StandardError.ReadToEnd();
			return errors;
		}

		private string[] GetUnexistingScreenshots(IList<BauEmailFeedObjectDto> bulkedBauEmails)
		{
			var unexistingScreenshots = bulkedBauEmails.Where(x => !_bauScreenshotsFilesService.BauScreenShotExists(x._id.ToString())).Select(b => b._id.ToString()).ToArray();

			return unexistingScreenshots;
		}

		private void MigrateScreenshotToStaticContent(BauEmailFeedObjectDto bauEmail)
		{
			var screenshot = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(bauEmail._id.ToString()));

			if (!screenshot.Exists)
			{
				Console.WriteLine("File {0} doesn't exist", screenshot.FullName);
				// Write to log and continue
				return;
			}

			SaveOriginalContentSize(bauEmail, screenshot.FullName);

			var resizedThumbnail = _tumbnailCreatorService.CreateThumbnail(screenshot.FullName, _bauEmailImageGeneratorSettings.ThumbnailWidth);

			if (resizedThumbnail != null)
			{
				try
				{
					Console.WriteLine("Copying resized image at {0} to {1}", screenshot.FullName, _bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(bauEmail._id.ToString()));
					resizedThumbnail.Save(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(bauEmail._id.ToString()));
					resizedThumbnail.Dispose();
				}
				catch
				{
					Console.WriteLine("Error while trying to save resized image - {0}", screenshot.FullName);
					resizedThumbnail.Dispose();
				}
			}
			else
			{
				Console.WriteLine("Error while trying to resize {0}", screenshot.FullName);
				Console.WriteLine("Copying Original {0} to {1}", screenshot.FullName, _bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(bauEmail._id.ToString()));
				screenshot.CopyTo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(bauEmail._id.ToString()), true);
			}

			bauEmail.HasScreenshot = true;

			_mongoStorage.Upsert(bauEmail);
		}

		private void SaveOriginalContentSize(BauEmailFeedObjectDto bauEmail, string fileName)
		{
			using (var img = Image.FromFile(fileName))
			{
				bauEmail.ContentWidth = img.Width;
				bauEmail.ContentHeight = img.Height;
			}
		}
	}
}

