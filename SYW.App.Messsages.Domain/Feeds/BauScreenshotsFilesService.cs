using System.IO;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauScreenshotsFilesService
	{
		bool BauScreenShotExists(string id);
		string GetBauScreenshotPath(string id);
		string GetBauStaticContentScreenshotPath(string id);
		bool MessageScreenShotExists(string id);
	}

	public class BauScreenshotsFilesService : IBauScreenshotsFilesService
	{
		private readonly IBauEmailImageGeneratorSettings _bauEmailImageGeneratorSettings;

		public BauScreenshotsFilesService(IBauEmailImageGeneratorSettings bauEmailImageGeneratorSettings)
		{
			_bauEmailImageGeneratorSettings = bauEmailImageGeneratorSettings;
		}

		public bool BauScreenShotExists(string id)
		{
			var screenshot = new FileInfo(GetBauScreenshotPath(id));

			return screenshot.Exists;
		}

		public bool MessageScreenShotExists(string id)
		{
			var screenshot = new FileInfo(GetBauStaticContentScreenshotPath(id));

			return screenshot.Exists;
		}

		public string GetBauScreenshotPath(string id)
		{
			return Path.Combine(_bauEmailImageGeneratorSettings.PhatomJsPath, id + ".jpg");
		}

		public string GetBauStaticContentScreenshotPath(string id)
		{
			return Path.Combine(_bauEmailImageGeneratorSettings.StaticContentPath, id + ".jpg");
		}
	}
}