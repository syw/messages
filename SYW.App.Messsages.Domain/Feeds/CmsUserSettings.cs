﻿using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface ICmsUserSettings
	{
		[Default("")]
		string AllowedUsersArray { get; set; }	 
	}
}