﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Xml.Linq;
using log4net;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IExternalFeedReader
	{
		IList<XElement> Read(string url);
	}

	public class ExternalFeedReader : IExternalFeedReader
	{
		private readonly ILog _logger;
		private readonly IFeedReaderSettings _feedReaderSettings;

		public ExternalFeedReader(ILog logger, IFeedReaderSettings feedReaderSettings)
		{
			_logger = logger;
			_feedReaderSettings = feedReaderSettings;
		}

		public IList<XElement> Read(string url)
		{
			var xml = FetchXmlFromUri(url);

			return xml.Descendants(XNamespace.Get(_feedReaderSettings.Namespace) + "entry")
					.Where(x => x != null)
					.ToList();
		}

		private XDocument FetchXmlFromUri(string uri)
		{
			try
			{
				var downloadedString = new WebClient.WebClient().DownloadString(uri);

				return XDocument.Parse(downloadedString.Trim());
			}
			catch (WebException e)
			{
				_logger.Error("PromotionalContentFetcher: Unable to fetch xml from uri " + uri, e);
				throw;
			}
			catch (System.Xml.XmlException e)
			{
				_logger.Error("Unable to parse Xml", e);
				throw;
			}
		}
	}
}