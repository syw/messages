using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEmailFeedProviderSettings
	{
		[Default("http://sabertooth.ecom.sears.com:8081/ccs/r/getContent/allUnexpired?source=ODE&maxResults=100")]
		string Url { get; set; }

		[Default(4)]
		int PublishTimespanInDays { get; set; }
	}
}