using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEmailImageGeneratorSettings
	{
		[Default("C:\\Dev\\bitbucket\\messages\\UtilRunner\\PhantomJs\\")]
		string PhatomJsPath { get; set; }

		[Default("c:\\StaticContent\\")]
		string StaticContentPath { get; set; }

		[Default(208)]
		int ThumbnailWidth { get; set; }

		[Default("http://messaging.shopyourway.com/bau/")]
		string BauImageRenderingUrl { get; set; }

		[Default("http://messaging.shopyourway.com/bau/message/")]
		string SystemMessageImageRenderingUrl { get; set; }
	}
}