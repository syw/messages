﻿using System.Collections.Generic;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Emails;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IBauEntityProvider
	{
		IList<BauEntity> GetAll();
		BauEntity GetEntityById(ObjectId objectId);
		BauEntity GetByEntityId(ObjectId objectId);
	}

	public class BauEntityProvider : IBauEntityProvider
	{
		private readonly IBauEntitiesRepository _bauEntitiesRepository;

		public BauEntityProvider(IBauEntitiesRepository bauEntitiesRepository)
		{
			_bauEntitiesRepository = bauEntitiesRepository;
		}


		public IList<BauEntity> GetAll()
		{
			return _bauEntitiesRepository.GetAll();
		}

		public BauEntity GetEntityById(ObjectId objectId)
		{
			return _bauEntitiesRepository.GetEntityById(objectId);
		}

		public BauEntity GetByEntityId(ObjectId objectId)
		{
			return _bauEntitiesRepository.GetByEntityId(objectId);
		}
	}
}