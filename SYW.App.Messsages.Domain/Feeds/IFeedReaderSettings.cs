using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface IFeedReaderSettings
	{
		[Default("http://www.w3.org/2005/Atom")]
		string Namespace { get; set; }
	}
}