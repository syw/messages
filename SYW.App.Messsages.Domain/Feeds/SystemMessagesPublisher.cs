﻿using System;
using System.IO;
using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface ISystemMessagesPublisher
	{
		void Publish();
	}

	public class SystemMessagesPublisher : ISystemMessagesPublisher
	{
		private readonly IMongoStorage<BauEmailFeedObjectDto,ObjectId> _bauEmailFeedObjectDtoStorage;
		private readonly IMongoStorage<MessageDto, ObjectId> _mongoStorage;
		private readonly ISystemConversationService _systemConversationService;
		private readonly IBauToEntitiesService _bauToEntitiesRepository;
		private readonly IBauScreenshotsFilesService _bauScreenshotsFilesService;
		private readonly IBauEmailFeedProviderSettings _bauEmailFeedProviderSettings;

		public SystemMessagesPublisher(
			IMongoStorage<BauEmailFeedObjectDto, ObjectId> bauEmailFeedObjectDtoStorage,
			IMongoStorage<MessageDto, ObjectId> mongoStorage,
			ISystemConversationService systemConversationService,
			IBauToEntitiesService bauToEntitiesRepository,
			IBauScreenshotsFilesService bauScreenshotsFilesService,
			IBauEmailFeedProviderSettings bauEmailFeedProviderSettings)
		{
			_bauEmailFeedObjectDtoStorage = bauEmailFeedObjectDtoStorage;
			_mongoStorage = mongoStorage;
			_systemConversationService = systemConversationService;
			_bauToEntitiesRepository = bauToEntitiesRepository;
			_bauScreenshotsFilesService = bauScreenshotsFilesService;
			_bauEmailFeedProviderSettings = bauEmailFeedProviderSettings;
		}

		public void Publish()
		{
			Console.WriteLine("Starting to read bau emails...");
			var filter = Builders<BauEmailFeedObjectDto>.Filter.Eq(x => x.HasScreenshot, true);
			var bauEmails = _bauEmailFeedObjectDtoStorage.GetList(filter);

			Console.WriteLine("Finishing to read...");

			foreach (var bauEmail in bauEmails)
			{
				Console.WriteLine("Starting to create system message...");

				if (SystemMessageWasAlreadyPublished(bauEmail))
					continue;

				var entity = GetOrCreateBauEntity(bauEmail);

				if (entity == null)
					continue;

				var messageId = ObjectId.GenerateNewId();

				Console.WriteLine("\t Copying static content of {0} to {1}", bauEmail._id, messageId);

				var isSuccessfulScreenshotCopy = CopyingBauScreenshotToSystemMessage(bauEmail, messageId);
				if (!isSuccessfulScreenshotCopy)
					continue;

				var systemMessage = MapSystemMessage(entity, bauEmail);

				var conversation = _systemConversationService.GetSystemConversation(entity._id);
				if (conversation == null)
					_systemConversationService.CreateSystemConversation(systemMessage, messageId);
				else
					_systemConversationService.AddMessage(conversation.Id, systemMessage, messageId);

				Console.WriteLine("Message published...");
				var removeFilter = Builders<BauEmailFeedObjectDto>.Filter.Eq(x => x._id, bauEmail._id);
				_bauEmailFeedObjectDtoStorage.Remove(removeFilter);
			}

			Console.WriteLine("Done :)");
		}

		private SystemMessage MapSystemMessage(Entity entity, BauEmailFeedObjectDto bauEmail)
		{
			var systmMessage = new SystemMessage
									{
										AuthorId = entity._id,
										PreviewText = bauEmail.SubjectLine,
										endDate = bauEmail.EndDate,
										Content = bauEmail.Content,
										IsHtml = true,
										FeedId = bauEmail.EmailId,
										Height = bauEmail.ContentHeight,
										Width = bauEmail.ContentWidth,
										PublishDate = bauEmail.Published
									};

			return systmMessage;
		}

		private bool CopyingBauScreenshotToSystemMessage(BauEmailFeedObjectDto bauEmail, ObjectId messageId)
		{
			try
			{
				var screenshot = new FileInfo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(bauEmail._id.ToString()));
				screenshot.CopyTo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(messageId.ToString()), true);
				screenshot.Delete();
			}
			catch (Exception)
			{
				return false;
			}
			return true;
		}

		private Entity GetOrCreateBauEntity(BauEmailFeedObjectDto bauEmail)
		{
			Entity entity;

			try
			{
				entity = _bauToEntitiesRepository.GetOrCreateEntityByName(bauEmail.BrandName, bauEmail.Logo);

				if (entity == null)
					Console.WriteLine("\t Message {0} entity type {1} does not exist", bauEmail.EmailId, bauEmail.BrandName);
			}
			catch (Exception e)
			{
				Console.WriteLine("\t Message {0} entity type {1} threw an exception trying to find its system entity. {2}",
								bauEmail.EmailId, bauEmail.BrandName, e.Message);
				return null;
			}
			return entity;
		}

		private bool SystemMessageWasAlreadyPublished(BauEmailFeedObjectDto bauEmail)
		{
			var filter = Builders<MessageDto>.Filter.Eq(x => x.FeedMessageId, bauEmail.EmailId);
			var existingConversation = _mongoStorage.GetFirstOrDefault(filter);
			if (existingConversation != null)
			{
				Console.WriteLine("\t Message {0} already exist", bauEmail.EmailId);
				return true;
			}

			if (bauEmail.Published >= SystemTime.Now() || bauEmail.Published.AddDays(_bauEmailFeedProviderSettings.PublishTimespanInDays) <= SystemTime.Now())
			{
				Console.WriteLine("\t Message {0} published date {1} is greater than now or {2} days older",
								bauEmail.EmailId, bauEmail.Published.ToShortDateString(), _bauEmailFeedProviderSettings.PublishTimespanInDays);
				return true;
			}
			return false;
		}
	}
}