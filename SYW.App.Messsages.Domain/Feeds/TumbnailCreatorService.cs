﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace SYW.App.Messsages.Domain.Feeds
{
	public interface ITumbnailCreatorService
	{
		Bitmap CreateThumbnail(string fileName, int width);
	}

	public class TumbnailCreatorService : ITumbnailCreatorService
	{
		public  Bitmap CreateThumbnail(string fileName, int width)
		{
			Bitmap resizedBitmap = null;
			try
			{
				using (var imgOriginal = Image.FromFile(fileName))
				{
					var percentWidth = ((float)imgOriginal.Width / (float)width);

					resizedBitmap = new Bitmap(width, (int)(imgOriginal.Height / percentWidth));

					using (var resizedImage = Graphics.FromImage(resizedBitmap))
					{
						resizedImage.InterpolationMode = InterpolationMode.HighQualityBicubic;
						resizedImage.CompositingQuality = CompositingQuality.HighQuality;
						resizedImage.SmoothingMode = SmoothingMode.HighQuality;
						resizedImage.DrawImage(imgOriginal, 0, 0, resizedBitmap.Width, resizedBitmap.Height);
					}
				}
			}
			catch
			{
				if (resizedBitmap != null)
					resizedBitmap.Dispose();
				return null;
			}
			return resizedBitmap;
		}
	}
}
