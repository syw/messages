﻿using System;
using System.IO;
using System.Linq;

namespace SYW.App.Messsages.Domain
{
	public interface IFileHelper
	{
		bool MoveFilesTo(string fromPath, string toPath);
		bool ClearDirectory(string path);
		bool Write(string path, string content);
		bool WriteLine(string path, string content);
	}

	public class FileHelper : IFileHelper
	{
		public bool MoveFilesTo(string fromPath, string toPath)
		{
			try
			{
				if (!Directory.Exists(toPath))
					Directory.CreateDirectory(toPath);

				ClearDirectory(toPath);

				var filesToMove = Directory.GetFiles(fromPath);
				var jsonFiles = filesToMove.Where(i => i.Contains(".json")).ToList();
				jsonFiles.ForEach(f =>
					                  {
						                  var splittedPath = f.Split(new[] {'\\', '/'});
						                  var newFilePath = toPath + "//" + splittedPath[splittedPath.Length - 1];
						                  File.Move(f, newFilePath);
					                  });

				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Atttempt to move files to " + toPath + " failed" + Environment.NewLine +
				                  "Exception details:" + Environment.NewLine +
				                  ex + Environment.NewLine +
				                  "======================");
				return false;
			}
		}

		public bool ClearDirectory(string path)
		{
			if (string.IsNullOrEmpty(path)) return false;

			Directory.GetFiles(path).ToList().ForEach(File.Delete);
			return true;
		}

		public bool Write(string path, string content)
		{
			try
			{
				using (var stream = File.Create(path))
				{
					var streamWriter = new StreamWriter(stream);

					streamWriter.Write(content);
				}

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public bool WriteLine(string path, string content)
		{
			try
			{
				using (var stream = File.Create(path))
				{
					var streamWriter = new StreamWriter(stream);

					streamWriter.WriteLine(content);
				}

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}
	}
}