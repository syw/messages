﻿namespace SYW.App.Messsages.Domain.Messages
{
	public class Attachment
	{
		public string EntityName { get; set; }
		public string EntityLink { get; set; }
		public string EntityImageUrl { get; set; }

		public Attachment(string entityName, string entityLink, string entityImageUrl)
		{
			EntityLink = entityLink;
			EntityImageUrl = entityImageUrl;
			EntityName = entityName.IsNullOrEmpty() ? entityLink : entityName;
		}
	}

	public static class AttachmentExtensions
	{
		public static bool IsValid(this Attachment attachment)
		{
			return !(attachment.EntityName.IsNullOrEmpty() && attachment.EntityLink.IsNullOrEmpty() && attachment.EntityImageUrl.IsNullOrEmpty());
		}
	}

}
