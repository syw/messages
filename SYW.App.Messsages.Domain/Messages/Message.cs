using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;

namespace SYW.App.Messsages.Domain.Messages
{
	public class Message
	{
		public ObjectId Id { get; set; }
		public ObjectId ConversationId { get; set; }
		public string Content { get; set; }
		public IList<Attachment> Attachments { get; set; }
		public MessageContentType ContentType { get; set; }
		public string PreviewText { get; set; }
		public Entity Author { get; set; }
		public DateTime Date { get; set; }
		public DateTime EndDate { get; set; }
		public int ContentWidth { get; set; }
		public int ContentHeight { get; set; }
		public ConversationStatus Status { get; set; }
		public string AttachedImagesUrl { get; set; }
		public string FeedMessageId { get; set; }
	}

	public static class MessageExtentions
	{
		public static bool IsUserMessage(this Message message)
		{
			var userMessages = new[] { MessageContentType.Text, MessageContentType.FormattedText };
			return userMessages.Contains(message.ContentType);
		}
	}
}