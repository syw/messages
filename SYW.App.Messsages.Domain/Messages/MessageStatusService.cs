﻿using CommonGround.Events;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using StackExchange.Profiling;

namespace SYW.App.Messsages.Domain.Messages
{
	public interface IMessageStatusService
	{
		void UpdateStatus(string conversationId, string messageId, ObjectId userId, ConversationStatus status);
		void UpdateStatus(ObjectId conversationId, ObjectId messageId, ObjectId userId, ConversationStatus status);
	}

	public class MessageStatusService : IMessageStatusService
	{
		private readonly IUserMessageStatusRepository _userMessageStatusRepository;
		private readonly IEvent<ConversationStatusChangedEvent> _conversationStatusChangedEvent;
		private readonly IUpdateConversationStatusService _updateConversationStatusService;
		private readonly IConversationService _conversationService;

		public MessageStatusService(IUserMessageStatusRepository userMessageStatusRepository, IEvent<ConversationStatusChangedEvent> conversationStatusChangedEvent, IUpdateConversationStatusService updateConversationStatusService, IConversationService conversationService)
		{
			_userMessageStatusRepository = userMessageStatusRepository;
			_conversationStatusChangedEvent = conversationStatusChangedEvent;
			_updateConversationStatusService = updateConversationStatusService;
			_conversationService = conversationService;
		}

		public void UpdateStatus(string conversationId, string messageId, ObjectId userId, ConversationStatus status)
		{
			UpdateStatus(ObjectId.Parse(conversationId), ObjectId.Parse(messageId), userId, status);
		}

		public void UpdateStatus(ObjectId conversationId, ObjectId messageId, ObjectId userId, ConversationStatus status)
		{
			using (MiniProfiler.Current.Step("MessageStatusService.UpdateStatus"))
			{
				_userMessageStatusRepository.UpdateStatus(conversationId, messageId, userId, (int)status);

				if (status == ConversationStatus.Archive && _conversationService.Get(conversationId.ToString(), userId) == null)
					_updateConversationStatusService.UpdateStatus(conversationId, userId, ConversationStatus.Archive);
			}

			_conversationStatusChangedEvent.Trigger(new ConversationStatusChangedEvent
														{
															ConversationId = ObjectId.Empty,
															AuthorId = userId
														});
		}
	}
}