﻿using System;
using System.Collections.Generic;
using System.Linq;
using CommonGround.Utilities;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Services.Platform;
using StackExchange.Profiling;

namespace SYW.App.Messsages.Domain.Messages
{
	public interface IMessagesService
	{
		Message Create(ObjectId conversationId, ObjectId authorId, string content, IList<Attachment> attachments = null);
		IList<Message> GetAllWithNoStatus(ObjectId entityId);
		Message GetMessageById(string messageId);
		void Delete(ObjectId messageId);
	}

	public class MessagesService : IMessagesService
	{
		private readonly IMessageRepository _messageRepository;
		private readonly IConversationRepository _conversationRepository;
		private readonly IUserMessageStatusRepository _userMessageStatusRepository;
		private readonly IEmailNotificationPublisher _emailNotificationPublisher;

		public MessagesService(IMessageRepository messageRepository, IUserMessageStatusRepository userMessageStatusRepository, IEmailNotificationPublisher emailNotificationPublisher, IConversationRepository conversationRepository)
		{
			_messageRepository = messageRepository;
			_userMessageStatusRepository = userMessageStatusRepository;
			_emailNotificationPublisher = emailNotificationPublisher;
			_conversationRepository = conversationRepository;
		}

		public Message Create(ObjectId conversationId, ObjectId authorId, string content, IList<Attachment> attachments)
		{
			var message = _messageRepository.Create(conversationId, authorId, content, null, attachments);

			_conversationRepository.Touch(conversationId);

			_emailNotificationPublisher.Publish(conversationId, authorId, content, attachments); //attachments are valid here

			return message;
		}

		public IList<Message> GetAllWithNoStatus(ObjectId entityId)
		{
			using (MiniProfiler.Current.Step("_userMessageStatusRepository GetAllWithNoStatus"))
			{
				var conversations = _conversationRepository.GetAll(entityId, SystemTime.Now()).EmptyIfNull().ToArray();
                //ILAN: Appeneded this line
                if (!conversations.Any())
                    return new List<Message>();

				var conversationIds = conversations.Where(x => x.IsSystemMessage).Select(x => x.Id).ToArray();
				var messages = _messageRepository.GetAll(conversationIds);

				foreach (var message in messages)
					message.Status = ConversationStatus.Temp;

				var messagesStatuses = _userMessageStatusRepository.GetAll(entityId);
				messages.JoinDo(messagesStatuses, m => m.Id, s => s.MessageId, (m, s) => m.Status = (ConversationStatus)s.Status);

				return messages.Where(m => m.Status == ConversationStatus.Temp).ToList();
			}
		}

		public Message GetMessageById(string messageId)
		{
			return _messageRepository.GetMessageById(messageId);
		}

		public void Delete(ObjectId messageId)
		{
			_messageRepository.Delete(messageId);
		}
	}
}