﻿using CommonGround.Events;
using SYW.App.Messsages.Domain.Services.Platform;

namespace SYW.App.Messsages.Domain.Messages
{
	public class NewSystemMessageNotificationService : IEventSubscriber
	{
		private readonly IEvent<NewSystemMessageEvent> _newSystemMessageEvent;
		private readonly IAppLinksService _appLinksService;

		public NewSystemMessageNotificationService(IEvent<NewSystemMessageEvent> newMessageEvent, IAppLinksService appLinksService)
		{
			_newSystemMessageEvent = newMessageEvent;
			_appLinksService = appLinksService;
		}

		public void Subscribe()
		{
			_newSystemMessageEvent.Subscribe(x => NotifyOfNewSystemMessage());
		}

		private void NotifyOfNewSystemMessage()
		{
			_appLinksService.UpdateGlobalNotifications();
		}
	}
}