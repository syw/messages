﻿using System;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Messages
{
	public class SystemMessage
	{
		public string Content { get; set; }
		public string PreviewText { get; set; }
		public bool IsHtml { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public DateTime endDate { get; set; }
		public string FeedId { get; set; }
		public ObjectId AuthorId { get; set; }
		public DateTime PublishDate { get; set; }
	}
}