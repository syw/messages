﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.

[assembly: AssemblyTitle("SYW.App.Messsages.Domain")]
[assembly: AssemblyProduct("SYW.App.Messsages.Domain")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM

[assembly: Guid("59dab64c-5ffd-44a9-89cb-e3868b3c75c2")]

// Version info is in SolutionAssemblyInfo.cs