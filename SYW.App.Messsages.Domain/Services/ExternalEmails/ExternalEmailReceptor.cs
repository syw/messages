using System;
using System.Collections.Generic;
using System.Linq;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Services.Settings;

using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Messages;
using log4net;

namespace SYW.App.Messsages.Domain.Services.ExternalEmails
{
	public interface IExternalEmailReceptor
	{
		ExternalEmailMessage CreateMessage(string senderEmail, string participantEmails, string content, string attachmentsUrl = "", string senderName = "");
	}

	public class ExternalEmailReceptor : IExternalEmailReceptor
	{
		private readonly IEntitiesRepository _entitiesRepository;
		private readonly IMessageRepository _messageRepository;
		private readonly IStartNewConversationService _startNewConversationService;
		private readonly ILog _logger;
		private readonly IApplicationSettings _applicationSettings;

		public ExternalEmailReceptor(IEntitiesRepository entitiesRepository, IMessageRepository messageRepository, ILog logger, IStartNewConversationService startNewConversationService, IApplicationSettings applicationSettings)
		{
			_entitiesRepository = entitiesRepository;
			_messageRepository = messageRepository;
			_logger = logger;
			_startNewConversationService = startNewConversationService;
			_applicationSettings = applicationSettings;
		}

		public ExternalEmailMessage CreateMessage(string senderEmail, string participantEmails, string content, string attachmentsUrl = "", string senderName = "")
		{
			var participantIds = FetchParticipantIds(participantEmails);
			if (participantIds.IsNullOrEmpty())
				throw new ParticipantNotFoundException(senderEmail, participantEmails);

			var authorId = FindExistingUserByEmail(senderEmail) ?? CreateNewEntity(senderEmail, senderName);

			var message = CreateMessage(authorId, participantIds, content, attachmentsUrl);

			return new ExternalEmailMessage {AuthorId = authorId, ConversationId = message.ConversationId, ParticipantIds = participantIds};
		}

		private Message CreateMessage(ObjectId authorId, List<ObjectId> participants, string content, string attachmentsUrl = "")
		{
			var conversation = _startNewConversationService.Start(authorId, participants);
			var message = _messageRepository.CreateFormattedMessage(conversation.Id, authorId, content, attachmentsUrl);

			return message;
		}

		private List<ObjectId> FetchParticipantIds(string participantEmails)
		{
			var individualEmails = participantEmails.Split(',').Select(x => x.Trim()).ToList();

			return _entitiesRepository.GetEntitiesIdsByEmail(individualEmails);
		}
		private ObjectId? FindExistingUserByEmail(string email)
		{
			return _entitiesRepository.GetEntityIdByOriginalEmail(email)
			       ?? _entitiesRepository.GetEntitiesIdsByEmail(email);
		}
		private ObjectId CreateNewEntity(string sender, string entityName = "")
		{
			_logger.InfoFormat("Creating user for this external email address: {0}", sender);

			var newEntity = new Entity
				                {
					                OriginalEmail = sender,
					                Email = sender,
					                Name = string.IsNullOrWhiteSpace(entityName) ? sender : entityName,
					                ImageUrl = _applicationSettings.StaticContentLocation + "/missing-images/email.png",
				                };
			_entitiesRepository.Add(newEntity);

			return newEntity._id;
		}
	}

	public class ParticipantNotFoundException : Exception
	{
		public ParticipantNotFoundException(string sender, string participants)
			: base(string.Format("Could not find participants: {0}. Sender is: {1}", participants, sender)) {}
	}

	public class ExternalEmailMessage
	{
		public ObjectId ConversationId;
		public List<ObjectId> ParticipantIds { get; set; }
		public ObjectId AuthorId { get; set; }
	}
}