using System.IO;
using System.Text;
using System.Web;
using Microsoft.Security.Application;

namespace SYW.App.Messsages.Domain.Services.ExternalEmails
{
	public interface IExternalEmailSanitizer
	{
		string DecodeAndSanitize(string urlEncodedContent);
	}

	public class ExternalEmailSanitizer : IExternalEmailSanitizer
	{
		public string DecodeAndSanitize(string urlEncodedContent)
		{
			urlEncodedContent = Decode(urlEncodedContent);
			urlEncodedContent = Sanitize(urlEncodedContent);

			return urlEncodedContent.IsNullOrEmpty() ? null : urlEncodedContent;
		}

		private static string Decode(string content)
		{
			var decodedContent = HttpUtility.UrlDecode(content);

			return decodedContent ?? string.Empty;
		}

		private static string Sanitize(string content)
		{
			var sb = new StringBuilder();

			using (var writer = new StringWriter(sb))
			{
				using (var reader = new StringReader(content))
				{
					Sanitizer.GetSafeHtmlFragment(reader, writer);
				}
			}

			return sb.ToString();
		}
	}
}