﻿using System;
using WinSCP;

namespace SYW.App.Messsages.Domain.Services.Ftp
{
    public interface IFtpUploader
    {
        string UploadFiles(string ftpPath, string remotePath, string userName, string password, string localPath);
    }

    public class FtpUploader : IFtpUploader
    {
        public string UploadFiles(string ftpPath, string remotePath, string userName, string password, string localPath)
        {
            try
            {
                var sessionOptions = GetSessionOptions(ftpPath, userName, password);

                using (var session = new Session())
                {
                    session.Open(sessionOptions);
                    session.PutFiles(localPath, remotePath).Check();
                }

                return "OK";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        private static SessionOptions GetSessionOptions(string ftpPath, string userName, string password)
        {
            return new SessionOptions
                       {
                           Protocol = Protocol.Sftp,
                           HostName = ftpPath,
                           GiveUpSecurityAndAcceptAnySshHostKey = true,
                           UserName = userName,
                           Password = password
                       };
        }
    }
}