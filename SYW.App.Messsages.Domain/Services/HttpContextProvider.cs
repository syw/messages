using System.IO;
using System.Web;

namespace SYW.App.Messsages.Domain.Services
{
	public interface IHttpContextProvider
	{
		HttpContext GetContext();
	}

	public class HttpContextProvider : IHttpContextProvider
	{
		public HttpContext GetContext()
		{
			return HttpContext.Current;
		}
	}
}