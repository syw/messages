﻿using System;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Entities;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Services.Platform;
using log4net;

namespace SYW.App.Messsages.Domain.Services.Installer
{
	public interface IAppInstaller
	{
		void Install();
		void SendWelcomeMessage(ObjectId toEntity);
	}

	public class AppInstaller : IAppInstaller
	{
		private readonly IAppLinksService _appLinksService;
		private readonly IAppService _appService;
		private readonly ILog _logger;
		private readonly IConversationRepository _conversationRepository;
		private readonly IMessageRepository _messageRepository;
		private readonly IAppInstallerSettings _appInstallerSettings;
		private readonly IEntitiesRepository _entitiesRepository;
		private readonly IRoutes _routes;

		public AppInstaller(IAppService appService, ILog log, IAppLinksService appLinksService, IConversationRepository conversationRepository, IMessageRepository messageRepository, IAppInstallerSettings appInstallerSettings, IEntitiesRepository entitiesRepository, IRoutes routes)
		{
			_appService = appService;
			_logger = log;
			_appLinksService = appLinksService;
			_conversationRepository = conversationRepository;
			_messageRepository = messageRepository;
			_appInstallerSettings = appInstallerSettings;
			_entitiesRepository = entitiesRepository;
			_routes = routes;
		}

		public void Install()
		{
			_logger.Debug("Entering Post-Login");

			_appService.Install();

			_logger.Debug("App installed");

			_appLinksService.Register("Messages", _routes.DefaultAppUrl);

			_logger.Debug("App registered");
		}

		// TODO : Not sure this should be here
		public void SendWelcomeMessage(ObjectId toEntity)
		{
			try
			{
				var supportEntity = GetSupportEntity();

				if (supportEntity == null)
					return;

				var converstation = _conversationRepository.Create(supportEntity._id, new[] { toEntity });

				_messageRepository.Create(converstation.Id, supportEntity._id, _appInstallerSettings.WelcomeMessage);
			}
			catch (Exception exception)
			{
				_logger.Warn("Welcome message wasn't sent to " + toEntity.ToString(), exception);
			}
		}

		private Entity GetSupportEntity()
		{
			if (string.IsNullOrEmpty(_appInstallerSettings.SupportEntityId))
			{
				_logger.Warn("Support entity id is not configured. Users will not recieve welcome message.");
				return null;
			}

			var supportEntityId = ObjectId.Parse(_appInstallerSettings.SupportEntityId);

			var supportEntity = _entitiesRepository.Get(supportEntityId);
			if (supportEntity == null)
			{
				_logger.Warn("Support entity id is configured BUT doesn't exist in the DB. Users will not recieve welcome message.");
				return null;
			}

			return supportEntity;
		}
	}
}