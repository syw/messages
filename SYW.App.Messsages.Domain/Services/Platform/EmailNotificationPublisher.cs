﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.Messages;
using SYW.App.Messsages.Domain.Services.Settings;
using log4net;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IEmailNotificationPublisher
	{
		void Publish(ObjectId conversationId, ObjectId authorId, string messageContent, IList<Attachment> attachments);
	}

	public class EmailNotificationPublisher : IEmailNotificationPublisher
	{
		private readonly IPlatformProxy _platformProxy;
		private readonly IApplicationSettings _applicationSettings;
		private readonly ILog _log;
		private readonly IConversationService _conversationService;
		private readonly IEmailNotificationSettings _emailNotificationSettings;

		public EmailNotificationPublisher(
			IPlatformProxy platformProxy, 
			IApplicationSettings applicationSettings, 
			ILog log, 
			IConversationService conversationService, 
			IEmailNotificationSettings emailNotificationSettings)
		{
			_platformProxy = platformProxy;
			_applicationSettings = applicationSettings;
			_log = log;
			_conversationService = conversationService;
			_emailNotificationSettings = emailNotificationSettings;
		}

		public void Publish(ObjectId conversationId, ObjectId authorId, string messageContent, IList<Attachment> attachments)
		{
			var emailHeader = attachments.IsNullOrEmpty()
				                    ? _emailNotificationSettings.RegularMessageHeader
				                    : _emailNotificationSettings.MessageWithAttachementHeader;

			var emailContent = GenerateContent(messageContent, attachments);

			var recipients = GetRecipients(conversationId, authorId);
			recipients.ToList().ForEach(userId =>
									{
										try
										{
											_platformProxy.Get<string>(_applicationSettings.EmailNotificationsPath + "/email/user-action", "POST", CreateParameters(userId, emailHeader, emailContent));
										}
										catch (Exception)
										{
											_log.Warn("failed sending mail to user " + userId);
										}
									});
		}

		private static string GenerateContent(string messageContent, IList<Attachment> attachments)
		{
			var sb = new StringBuilder();

			if (messageContent != null)
				sb.AppendLine(string.Format("\"{0}\": ",messageContent));

			if (!attachments.IsNullOrEmpty())
                attachments.ToList().ForEach(a => sb.AppendLine(string.Format("{0}: {1}", a.EntityName, a.EntityLink)));

			return sb.ToString();
		}

		private KeyValuePair<string, object>[] CreateParameters(long userId, string messageHeader, string messageContent)
		{
			return new[]
						{
							new KeyValuePair<string, object>("recipientId", userId),
							new KeyValuePair<string, object>("userAction", messageHeader),
							new KeyValuePair<string, object>("details", messageContent),
							new KeyValuePair<string, object>("linkText", "Reply")
						};
		}

		private IEnumerable<long> GetRecipients(ObjectId conversationId, ObjectId authorId)
		{
			var conversation = _conversationService.Get(conversationId.ToString(), authorId);

			return conversation.Participants
								.Where(p => p._id != authorId)
								.Select(x => x.OriginalId);
		}
	}
}