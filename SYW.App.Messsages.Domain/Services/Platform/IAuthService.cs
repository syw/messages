﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using CommonGround.Utilities;
using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IAuthService
	{
		string GetOfflineToken(long userId);
		string GetGlobalOfflineToken();
	}

	public class AuthService : IAuthService
	{
		private readonly IPlatformProxy _platformProxy;
		private readonly IApplicationSettings _applicationSettings;
		private readonly string _servicePath;

		public AuthService(IPlatformProxy platformProxy, IApplicationSettings applicationSettings)
		{
			_platformProxy = platformProxy;
			_applicationSettings = applicationSettings;
			_servicePath = _applicationSettings.AuthenticationPath;
		}

		public string GetOfflineToken(long userId)
		{
			var now = SystemTime.Now();

			var timeStamp = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, DateTimeKind.Utc);

			var signature = new SignatureBuilder().Append(userId).Append(_applicationSettings.AppId).Append(timeStamp).Append(_applicationSettings.AppSecret).Sign();

			var serviceParameters = new NameValueCollection(4)
										{
											{"userId", userId.ToString()},
											{"appId", _applicationSettings.AppId.ToString()},
											{"timestamp", timeStamp.ToString("yyyy-MM-ddTHH:mm:ss")},
											{"signature", signature}
										};

			return _platformProxy.GetOfflineToken(_servicePath + "/get-token", serviceParameters);
		}

		public string GetGlobalOfflineToken()
		{
			var now = SystemTime.Now();

			var timeStamp = new DateTime(now.Year, now.Month, now.Day, now.Hour, now.Minute, now.Second, DateTimeKind.Utc);

			var signature = CreateSignature(_applicationSettings.AppId, _applicationSettings.AppSecret, timeStamp);

			var serviceParameters = new NameValueCollection(3)
										{
											{"appId", _applicationSettings.AppId.ToString()},
											{"timestamp", timeStamp.ToString("yyyy-MM-ddTHH:mm:ss")},
											{"signature", signature}
										};

			return _platformProxy.GetOfflineToken(_servicePath + "/get-token", serviceParameters);
		}


		private string CreateSignature(long appId, string appSecret, DateTime timestamp)
		{
			var hashAlgorithm = SHA256.Create();
			var dateWithoutMilisecounds = new DateTime(timestamp.Year, timestamp.Month, timestamp.Day, timestamp.Hour, timestamp.Minute, timestamp.Second, DateTimeKind.Utc);
			var bytes = BitConverter.GetBytes(appId)
									.Concat(BitConverter.GetBytes((dateWithoutMilisecounds - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds))
									.Concat(Encoding.UTF8.GetBytes(appSecret))
									.ToArray();
			var computedHash = hashAlgorithm.ComputeHash(bytes);
			var signature = computedHash.Aggregate(new StringBuilder(computedHash.Length*2), (sb, i) => sb.Append(i.ToString("x2"))).ToString().ToLower();
			return signature;
		}
	}
}