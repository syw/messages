﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IEmailNotificationSettings
	{
		[Default("sent you a message")]
		string RegularMessageHeader { get; set; }

		[Default("shared something with you")]
		string MessageWithAttachementHeader { get; set; }
	}
}
