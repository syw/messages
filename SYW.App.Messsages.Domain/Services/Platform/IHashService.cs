﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using PlatformClient;
using log4net;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IHashService
	{
		string CreateSignature(string token, string appSecret);
	}

	public class HashService : IHashService
	{
		private readonly ILog _logger;

		public HashService(ILog logger)
		{
			_logger = logger;
		}

		public string CreateSignature(string token, string appSecret)
		{
			var hashAlgorithm = SHA256.Create();

			var tokenSecret = Encoding.UTF8.GetBytes(token + appSecret);

			var hashByteArray = hashAlgorithm.ComputeHash(tokenSecret);
			var hashHexa = hashByteArray.ToHexString();
			var loweredHashHexa = hashHexa.ToLower();

			_logger.Debug(string.Format("Hash creation: Token={0},TokenLength={1},SaltWithSecretString={2},SaltedWithSecretHexa={3}, SaltedWithSecretLength={4}, HashInBytes={5}, HashHexa={6}, ResultHash={7}, UserAgent={8}\n",
										token, token.Length, Encoding.UTF8.GetString(tokenSecret), tokenSecret.ToHexString(), tokenSecret.Length, Encoding.UTF8.GetString(hashByteArray), hashHexa, loweredHashHexa, GetUserAgent()));

			return loweredHashHexa;
		}

		private string GetUserAgent()
		{
			var context = HttpContext.Current;
			var userAgent = context != null ? context.Request.UserAgent : null;
			return userAgent;
		}
	}
}