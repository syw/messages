using System;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IPlatformConfiguration
	{
		Uri PlatformSecureApiBaseUrl { get; }
	}
}