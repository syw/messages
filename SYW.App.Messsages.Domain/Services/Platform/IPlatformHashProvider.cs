using System.Security.Cryptography;
using System.Text;
using System.Web;
using PlatformClient;
using SYW.App.Messsages.Domain.Services.Settings;
using log4net;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IPlatformHashProvider
	{
		string GetHash();
	}

	public class PlatformHashProvider : IPlatformHashProvider
	{
		private readonly IApplicationSettings _applicationSettings;
		private readonly IPlatformTokenProvider _platformTokenProvider;
		private readonly ILog _logger;

		public PlatformHashProvider(IApplicationSettings applicationSettings, IPlatformTokenProvider platformTokenProvider, ILog logger)
		{
			_applicationSettings = applicationSettings;
			_platformTokenProvider = platformTokenProvider;
			_logger = logger;
		}

		public string GetHash()
		{
			var hashAlgorithm = SHA256.Create();
			var token = _platformTokenProvider.GetToken();
			var saltedWithSecret = Encoding.UTF8.GetBytes(token + _applicationSettings.AppSecret);

			var hashByteArray = hashAlgorithm.ComputeHash(saltedWithSecret);
			var hashHexa = hashByteArray.ToHexString();
			var loweredHashHexa = hashHexa.ToLower();

			_logger.Debug(string.Format("Hash creation: Token={0},TokenLength={1},SaltWithSecretString={2},SaltedWithSecretHexa={3}, SaltedWithSecretLength={4}, HashInBytes={5}, HashHexa={6}, ResultHash={7}, UserAgent={8}\n",
										token, token.Length, Encoding.UTF8.GetString(saltedWithSecret), saltedWithSecret.ToHexString(), saltedWithSecret.Length, Encoding.UTF8.GetString(hashByteArray), hashHexa, loweredHashHexa, GetUserAgent()));

			return loweredHashHexa;
		}


		private string GetUserAgent()
		{
			var context = HttpContext.Current;
			var userAgent = context != null ? context.Request.UserAgent : null;
			return userAgent;
		}
	}
}