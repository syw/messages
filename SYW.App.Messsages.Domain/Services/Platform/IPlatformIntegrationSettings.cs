using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public interface IPlatformIntegrationSettings
	{
		[Default("https://sandboxplatform.shopyourway.com/")]
		string PlatformSecureApiBaseUrl { get; set; }
	}
}