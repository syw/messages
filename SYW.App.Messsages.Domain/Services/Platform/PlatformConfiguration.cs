﻿using System;

namespace SYW.App.Messsages.Domain.Services.Platform
{
	public class PlatformConfiguration : IPlatformConfiguration
	{
		private readonly IPlatformIntegrationSettings _platformIntegrationSettings;

		public PlatformConfiguration(IPlatformIntegrationSettings platformIntegrationSettings)
		{
			_platformIntegrationSettings = platformIntegrationSettings;
		}

		public Uri PlatformSecureApiBaseUrl
		{
			get { return new Uri(_platformIntegrationSettings.PlatformSecureApiBaseUrl); }
		}
	}
}