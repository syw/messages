using System.Collections.Generic;
using System.Linq;
using PlatformClient.Platform;
using SYW.App.Messsages.Domain.Services.Platform;
using SYW.App.Messsages.Domain.Services.Settings;
using SYW.App.Messsages.Domain.Settings;

namespace SYW.App.Messsages.Domain.Platform
{
	public interface IProductService
	{
		IList<SocialProductInfo> GetProducts(IList<long> ids);
	}

	public class ProductService : IProductService
	{
		private readonly IPlatformProxy _platformProxy;
		private readonly IApplicationSettings _applicationSettings;

		private readonly string _servicePath;

		public ProductService(IPlatformProxy platformProxy, IApplicationSettings applicationSettings)
		{
			_platformProxy = platformProxy;
			_applicationSettings = applicationSettings;

			_servicePath = _applicationSettings.ProductsPath;
		}


		public IList<SocialProductInfo> GetProducts(IList<long> ids)
		{
			var requestParams = ids.Select(val => new KeyValuePair<string, object>("ids", val)).ToArray();

			return _platformProxy.Get<List<SocialProductInfo>>(_servicePath + "/get", requestParams);
		}
	}
}