﻿using SYW.App.Messsages.Domain.Services.Settings;

namespace SYW.App.Messsages.Domain.Services
{
	public interface IRoutes
	{
		string DefaultAppUrl { get; }
	}

	public class Routes : IRoutes
	{
		private readonly IApplicationSettings _applicationSettings;

		public Routes(IApplicationSettings applicationSettings)
		{
			_applicationSettings = applicationSettings;
		}

		public string DefaultAppUrl
		{
			get { return string.Format(_applicationSettings.DefaultAppUrl, _applicationSettings.AppId); }
		}
	}
}