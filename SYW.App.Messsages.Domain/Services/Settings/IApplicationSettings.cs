﻿using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Services.Settings
{
	public interface IApplicationSettings
	{
		[Default("880875876a8e4b739babffbd7f7b1ff9")]
		string AppSecret { get; set; }

		[Default("1000")]
		long AppId { get; set; }

		[Default("applinks")]
		string AppLinkPath { get; set; }

		[Default("app")]
		string AppPath { get; set; }

		[Default("tags")]
		string TagsPath { get; set; }

		[Default("users")]
		string UsersPath { get; set; }

		[Default("notifications")]
		string EmailNotificationsPath { get; set; }

		[Default("wall")]
		string WallPath { get; set; }

		[Default("auth")]
		string AuthenticationPath { get; set; }

		[Default("messages")]
		string Domain { get; set; }

		[Default("8")]
		int ListPageSize { get; set; }

		[Default("4")]
		int GridPageSize { get; set; }

		[Default("24")]
		int PackagePageSize { get; set; }

		[Default("6")]
		int PackageRowSize { get; set; }

		[Default("http://messaging.shopyourway.com/Content")]
		string StaticContentLocation { get; set; }

		[Default("http://ohio.local")]
		string BaseSiteUrl { get; set; }

		[Default("/app/{0}/r")]
		string DefaultAppUrl { get; set; }

		[Default("http://ccmsg.sywcdn.net/content/email-attachments")]
		string EmailAttachmentsUrl { get; set; }

		[Default("https://www.shopyourway.com/secured/people/{0}/{1}/settings/invite")]
		string InvitationUrlFormat { get; set; }

        [Default("http://images.shopyourway.com/static/img/fusion/missing-images/user.png")]
        string MissingUserImage { get; set; }
	}
}