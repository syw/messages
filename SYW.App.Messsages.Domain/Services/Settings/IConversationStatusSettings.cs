﻿using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Services.Settings
{
	public interface IConversationStatusSettings
	{
		[Default(3)]
		int DaysToKeepConversationAsNew { get; set; }
	}
}