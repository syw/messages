﻿using CommonGround.Settings;

namespace SYW.App.Messsages.Domain.Services.Settings
{
	public interface ISystemMessagesSettings
	{
		[Default(6)]
		int ExpirationTimeInMonths { get; set; }

		[Default(4)]
		int CacheExpirationTimeInHours { get; set; }
	}
}
