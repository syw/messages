﻿using System.ComponentModel.DataAnnotations;
using Mongo;
using MongoDB.Bson;

namespace SYW.App.Messsages.Domain.Settings
{
	public class SettingValue : BaseEntity<ObjectId>
	{
		[Required]
		public string Section { get; set; }

		[Required]
		public string Key { get; set; }

		[Required]
		public string Value { get; set; }
	}
}