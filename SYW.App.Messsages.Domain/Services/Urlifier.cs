﻿using System;
using log4net;

namespace SYW.App.Messsages.Domain.Services
{
	public interface IUrlifier
	{
		string Urlify(string input);
	}

	public class Urlifier : IUrlifier
	{
		private const int MAX_LENGTH = 100;
		private readonly ILog _logger;

		public Urlifier(ILog logger)
		{
			_logger = logger;
		}

		public string Urlify(string input)
		{
			if (string.IsNullOrEmpty(input))
				return string.Empty;

			try
			{
				var maximumTargetLength = Math.Min(MAX_LENGTH, input.Length);

				var newString = new char[maximumTargetLength];
				var newLength = 0;
				for (var i = 0; i < input.Length && newLength < maximumTargetLength; ++i)
				{
					var ch = input[i];
					if (char.IsLetterOrDigit(ch))
					{
						newString[newLength++] = ch;
						continue;
					}
					if (newLength == 0)
						continue;
					if (ch == '\'')
						continue;
					if (newString[newLength - 1] == '-')
						continue;
					newString[newLength++] = '-';
				}

				input = new string(newString, 0, newLength);

				input = input.Trim('-');

				if (input == "") input = "_";
				input = input.ToLower();

				return input;
			}
			catch (Exception ex)
			{
				_logger.Error("Error Urlifying input string", ex);
				return string.Empty;
			}
		}
	}
}