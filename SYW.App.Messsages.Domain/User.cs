namespace SYW.App.Messsages.Domain
{
	public class User
	{
		public long Id { get; set; }
		public string Name { get; set; }
		public string Image { get; set; }
		public string Email { get; set; }
	}
}