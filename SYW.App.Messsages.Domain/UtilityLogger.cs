﻿using System;
using System.Configuration;
using System.IO;
using CommonGround.Utilities;

namespace SYW.App.Messsages.Domain
{
    public interface IUtilityLogger
    {
        bool IsEnabled { get; }

        bool Debug(string content);
        bool Error(string content);
        bool Error(Exception exception);
        bool Error(string content, Exception exception);
        bool Info(string content);
    }

    public class UtilityLogger : IUtilityLogger
    {
        private readonly IFileHelper _fileHelper;
        private readonly string _logPath;

        public UtilityLogger(IFileHelper fileHelper)
        {
            _fileHelper = fileHelper;

            var relativeConfigurablePath = ConfigurationManager.AppSettings["UtilitiesLogFilePath"];
            _logPath = Directory.GetCurrentDirectory() + relativeConfigurablePath + @"UtilityLogger.log";
        }

        public bool IsEnabled { get { return bool.Parse(ConfigurationManager.AppSettings["UtilitiesLogEnabled"]); } }

        public bool Debug(string content)
        {
            if (!IsEnabled) return false;

            try
            {
                var debugContent = string.Format("DEBUG | {0} " + Environment.NewLine + " {1}." + Environment.NewLine + "===================================",
                    SystemTime.Now().ToString("MM-dd-yyyy HH:mm"),
                    content);

                _fileHelper.WriteLine(_logPath, debugContent);
                Console.WriteLine(debugContent);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Error(string content)
        {
            if (!IsEnabled) return false;

            try
            {
                var errorContent = string.Format("ERROR | {0} " + Environment.NewLine + " {1}." + Environment.NewLine + "===================================",
					SystemTime.Now().ToString("MM-dd-yyyy HH:mm"),
                    content);

                _fileHelper.WriteLine(_logPath, errorContent);
                Console.WriteLine(errorContent);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Error(Exception exception)
        {
            if (!IsEnabled) return false;

            try
            {
                var errorContent = string.Format("ERROR | {0} " + Environment.NewLine + " {1}." + Environment.NewLine + "===================================",
					SystemTime.Now().ToString("MM-dd-yyyy HH:mm"),
                    exception);

                _fileHelper.WriteLine(_logPath, errorContent);
                Console.WriteLine(errorContent);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Error(string content, Exception exception)
        {
            if (!IsEnabled) return false;

            try
            {
                var errorContent = string.Format("ERROR | {0} " + Environment.NewLine + " {1}." + Environment.NewLine + "===================================",
					SystemTime.Now().ToString("MM-dd-yyyy HH:mm"),
                    exception);

                _fileHelper.WriteLine(_logPath, errorContent);
                Console.WriteLine(errorContent);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Info(string content)
        {
            if (!IsEnabled) return false;

            try
            {
                var infoContent = string.Format("INFO | {0} " + Environment.NewLine + " {1}." + Environment.NewLine + "===================================",
					SystemTime.Now().ToString("MM-dd-yyyy HH:mm"),
                    content);

                _fileHelper.WriteLine(_logPath, infoContent);
                Console.WriteLine(infoContent);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
