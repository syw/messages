﻿using System.IO;
using System.IO.Compression;
using Ionic.Zip;

namespace SYW.App.Messsages.Domain
{
	public interface IZipHelper
	{
		void Compress(FileInfo fileToCompress);
		string Decompress(FileInfo fileToDecompress);
		string CompressDirectory(DirectoryInfo directoryToCompress);
	}

	public class ZipHelper : IZipHelper
	{
		public void Compress(FileInfo fileToCompress)
		{
			using (var originalFileStream = fileToCompress.OpenRead())
			{
				if (!((File.GetAttributes(fileToCompress.FullName) &
				       FileAttributes.Hidden) != FileAttributes.Hidden &
				      fileToCompress.Extension != ".gz")) return;

				using (var compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
				{
					using (var compressionStream = new GZipStream(compressedFileStream, CompressionMode.Compress))
					{
						originalFileStream.CopyTo(compressionStream);
					}
				}
			}
		}

		public string Decompress(FileInfo fileToDecompress)
		{
			var newFileName = fileToDecompress.FullName;

			if (fileToDecompress.Extension != ".gz")
			{
				return newFileName;
			}

			using (var originalFileStream = fileToDecompress.OpenRead())
			{
				var currentFileName = newFileName;

				newFileName = currentFileName.Remove(currentFileName.Length - fileToDecompress.Extension.Length);

				using (var decompressedFileStream = File.Create(newFileName))
				{
					using (var decompressionStream = new GZipStream(originalFileStream, CompressionMode.Decompress))
					{
						decompressionStream.CopyTo(decompressedFileStream);
					}
				}
			}

			return newFileName;
		}

		public string CompressDirectory(DirectoryInfo directoryToCompress)
		{
			using (var zipFile = new ZipFile())
			{
				zipFile.AddDirectory(directoryToCompress.FullName);
				zipFile.Save(directoryToCompress.FullName + ".zip");

				return directoryToCompress.FullName + ".zip";
			}
		}
	}
}