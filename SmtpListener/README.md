﻿
C# 

• IApplicationSettings holds a new property named EmailAttachmentsUrl. This property holds the url of the CDN from which we set the displayed images source. 
To emulate the SYW-CDN I’ve created a site locally and I use it to save and retrieve the email attached images.
Need to change this property to SYW-CDN url.

• Integration Tests  - The NodeJs listener project has a configuration file that holds a list of allowed email prefixes (the received email participants’ addresses prefixes must be in that list in order to receive an email). If the node list has “test” as one of the allowed prefixes, test@somthing.com is valid.
In my integration tests I’ve used “tomer2rioi11dk3b2udo” as the recipients’ addresses prefix. 
Need to make sure to change Integration tests recipients’ addresses prefix ,if we change the allowed email prefixes list in the node config file, in order for the tests to work. 


# EmailListener

o	Install NodeJs.
o	Install ImageMagick Framework from http://www.imagemagick.org/  (necessary in order to work with node imagemagick package).
o	Copy files to specified folder.
o	Open command prompt in the folder where we copied the files and type "npm install".
o	The project is installed.

o	Enter the config.js file –
		Set the config.imageUploadFolder property to where we want to save the attached images.
		Set the config.attachmentMaxSize property to limit attachment size in bytes.

