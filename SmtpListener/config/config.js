﻿var config = {}

//smtp server listen port
config.listenPort = 25;

//do not check if the sender domain is valid
config.disableDNSValidation = true

//acceptable image types
config.acceptedAttachmentTypes = ['image/gif', 'image/jpeg', 'image/png'];

//where should I upload images
config.imageUploadUrl = 'http://www.shopyourway.com/accountdetails/uploadimage';

//where to save images
config.imageUploadFolder = "C:\\Projects\\Sears\\messages\\FileServer\\FileServer\\FileServer\\EmailImages";

config.maxImageWidth = 1100;

// max size in Bytes
config.attachmentMaxSize = 1000000;

// Debug

//who gets the email as json
//config.pushToUrls = [
//	{url : "http://messages.apphb.com/-/trusted-message-post" , allowedEmailPrefixes : [ "tomer2rioi11dk3b2udo", "kitty4kx14c3ogtthyt38", "statusz24c6js3ow42ie8k" ]}];

// Prod

config.pushToUrls = [
{url : "http://www.shopyourway.com/statusViaEmail/post", allowedEmailPrefixes : [ "tomer2rioi11dk3b2udo", "kitty4kx14c3ogtthyt38", "statusz24c6js3ow42ie8k" ] },
{url : "http://localhost/-/statusViaEmail/post" } ];



//don't toch this
module.exports = config;