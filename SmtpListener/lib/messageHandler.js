﻿var request = require( 'request' );
var sanitizeHtml = require( 'sanitize-html' );
var config = require( '../config/config.js' );
var MemoryStream = require( 'memorystream' );
var tempDir = config.imageUploadFolder;
var MailParser = require( "mailparser" ).MailParser;
var fs = require( "fs" );
var im = require( 'imagemagick' );

var messageHandler = function () {

    var mailparser = new MailParser( { streamAttachments: true });

    createDirectoryIfNeeded( config.imageUploadFolder );

    var didUploadAttachment = false;
    var didParsedMessage = false;

    var emailData = {};
    var attachmentsUrls = "";

    var handledAttachmentsNum = 0;

    this.parseStream = function ( messageStream ) {
        if ( messageStream == null ) return;
        console.log( "strting mime parser" );
        messageStream.pipe( mailparser );
    };

    function pushToApplications( guid ) {

        if ( !( ( didUploadAttachment || didHandledAllAttachments() ) && didParsedMessage ) ) return;

        printObject( emailData );
        printIfNotNull( guid, "guid" );

        for ( var i = 0; i < config.pushToUrls.length; i++ ) {
            pushToUrl( config.pushToUrls[i], guid );
        }
    }

    function pushToUrl( urlConfig, guid ) {
        var url = urlConfig.url;
        console.log( "pushing to " + url );

        var allowedAddresses = getAllowedPublishAdresses( emailData.to.split( "," ), urlConfig.allowedEmailPrefixes );
        if ( !allowedAddresses || allowedAddresses.length == 0 ) {
            console.log( "nothing to push to " + url );
            return;
        }

        var allowedAddressesAsString = allowedAddresses.join( "," );
        console.log( "allowed emails: " + allowedAddressesAsString );

        var r = request.post( url,
            function ( error, response, body ) {
                if ( !error && response.statusCode == 200 ) {
                    console.log( "message was pushed to " + url );
                }
                else {
                    console.log( "unable to push to url " + url + " error: " + error + ", status code:" + response.statusCode );
                }
            });

        var form = r.form();
        form.append( 'sender', emailData.from );

        form.append( 'particpantEmails', allowedAddressesAsString );
        if ( emailData.title ) form.append( 'title', emailData.title );
        if ( guid != null ) form.append( 'imageGuid', guid );

        var encodedContent = encodeURIComponent( emailData.fullContent );
        form.append( 'content', encodedContent );

        if ( attachmentsUrls ) {
            // Remove last semicolon
            emailData.attachmentsUrls = attachmentsUrls.slice( 0, -1 );
            form.append( 'attachmentsUrl', emailData.attachmentsUrls );
        }
        if ( emailData.senderName ) form.append( 'senderName', emailData.senderName );
    }

    //handle attachment
    mailparser.on( "attachment", function ( attachment ) {
        if ( !isAcceptableAttachment( attachment ) ) {
            handledAttachmentsNum++;
            return;
        }

        console.log( "starting attachment handling" );
        var guid = GUID();
        var fileParts = attachment.fileName.split( "." );
        if ( fileParts.length < 2 ) return;
        var filePath = config.imageUploadFolder + "/" + guid + "." + fileParts[1];
        var destFilePath = config.imageUploadFolder + "/" + guid + "_resized." + fileParts[1];
        var output = fs.createWriteStream( filePath );

        output.once( 'close',
            function () {
                im.identify( filePath, function ( err, features ) {
                    if ( err ) throw err;
                    if ( features.width >= config.maxImageWidth ) {
                        resizeImage(filePath,destFilePath,guid,fileParts[1]);
                    } else {
                        console.log( "finished reading attachment" );
                        var fileName = filePath.substring( filePath.lastIndexOf( '/' ) + 1 );
                        addAttachmentsUrl(fileName);
                    }
                });
            });

        attachment.stream.on( "end", function () {
            if ( this.length > config.attachmentMaxSize ) {
                fs.unlinkSync( filePath );
                handledAttachmentsNum++;
                output.removeAllListeners();
                attachment.stream.emit( "close" );
                pushToApplications();
            }
        });
        attachment.stream.pipe( output );
    });

    //handle email message
    mailparser.on( "end", function ( mailObject ) {
        console.log( "finished parsing the message." );

        didParsedMessage = true;
        if ( mailObject.attachments == null || mailObject.attachments.lenght == 0 ) {
            didUploadAttachment = true;
        }
        emailData = mapMailObject( mailObject );
        pushToApplications();

    });

    function mapMailObject( mailObject ) {
        var simpleContent = enforceNoHtml( mailObject.text );
        var title = enforceNoHtml( mailObject.subject );
        var fullContent = enforceBasicHtml( mailObject.html );
        
        if ( isNullOrEmpty( fullContent ) || fullContent == 'undefined' ) //fallback to plaintext.
            fullContent = simpleContent;

        if ( !isNullOrEmpty( title ) )		//add title to fullContent
            fullContent = fullContent ? "<div>" + title + ':</div>' + fullContent : title;
        if ( !fullContent )				// make sure fullContent is not empty.
            fullContent = " ";

        return {
            from: addressesToString( mailObject.from ),
            senderName: mailObject.from[0].name,
            to: addressesToString( mailObject.to ),
            title: mailObject.subject,
            simpleContent: simpleContent,
            fullContent: fullContent
        };
    }

     function resizeImage( filePath, destFilePath, guid, fileExtention ) {
        im.resize( {
            srcPath: filePath,
            dstPath: destFilePath,
            width: config.maxImageWidth
        }, function ( err, stdout, stderr ) {
                if ( err ) throw err;
                console.log( 'resized ' + guid + '.' + fileExtention + ' to fit width of ' + config.maxImageWidth + "px" );
                console.log( "finished reading attachment" );
                var fileNameResized = destFilePath.substring( filePath.lastIndexOf( '/' ) + 1 );
                addAttachmentsUrl(fileNameResized);
            });
    }

    function addAttachmentsUrl(fileName) {
        handledAttachmentsNum++;
        attachmentsUrls += fileName + ";";
        pushToApplications();
    }
    
    function didHandledAllAttachments() {
        return typeof ( mailparser.mailData.attachments ) != "undefined" && handledAttachmentsNum == mailparser.mailData.attachments.length;
    }


    function enforceNoHtml( content ) {
        return sanitizeHtml( content, {
            allowedTags: [],
            allowedAttributes: {},
        });
    }

    function enforceBasicHtml( content ) {
        return sanitizeHtml( content, {
            allowedTags: ['b', 'div', 'p', 'font', 'u', 'strong', 'br', 'span'],
            allowedAttributes: { font: ['color'] },
        });
    }

    function isNullOrEmpty( text ) {
        return !text || /^\s*$/.test( text );
    }

    function isAcceptableAttachment( attachment ) {
        return contains( config.acceptedAttachmentTypes, attachment.contentType );
    }

    function addressesToString( addressArray ) {
        var delimiter = ",";
        var s = "";
        for ( var i = 0; i < addressArray.length; ++i ) {
            s += addressArray[i].address + delimiter;
        }
        return s.substring( 0, s.length - delimiter.length );
    }

    function GUID() {
        var S4 = function () { return ( ( ( 1 + Math.random() ) * 0x10000 ) | 0 ).toString( 16 ).substring( 1 ); };
        return ( S4() + S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4() );
    }

    function createDirectoryIfNeeded( path ) {
        fs.exists( path,
            function ( exists ) {
                if ( !exists ) {
                    fs.mkdirSync( path, 0666 );
                }
            });
    }

    function contains( a, obj ) {
        for ( var i = 0; i < a.length; i++ ) {
            if ( a[i] === obj ) {
                return true;
            }
        }
        return false;
    }

    //these lines are written for pavel

    function getEmailPrefix( address ) {
        var addressParts = address.split( "@" );
        if ( addressParts == null || addressParts == undefined || addressParts.length < 2 ) return undefined;
        return addressParts[0];
    }

    function getAllowedPublishAdresses( adresses, emailPrefixes ) {
        if ( emailPrefixes == undefined ) {
            console.log( "no filter list found. allowing all!" );
            return adresses;
        }
        var allowed = new Array( 0 );
        for ( var i = 0; i < adresses.length; ++i ) {
            var address = adresses[i];
            console.log( "matching address: " + address );
            var addressPrefix = getEmailPrefix( address );
            if ( addressPrefix != undefined && contains( emailPrefixes, addressPrefix ) ) {
                console.log( "address approved: " + address );
                allowed.push( address );
            }
            else {
                console.log( "address rejected: " + address );
            }
        }
        return allowed;
    }

    function printObject( obj ) {

        for ( var prop in obj ) {
            printIfNotNull( obj[prop], prop );
        }
    }

    function printIfNotNull( param, caption ) {
        if ( param == null ) return;
        console.log( caption + ": " + param );
    }
};

module.exports = messageHandler;


