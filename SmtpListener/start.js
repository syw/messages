﻿
var config = require('./config/config.js');
var simplesmtp = require("simplesmtp");
var fs = require("fs");
var MemoryStream = require('memorystream');
var messageHandler = require("./lib/messageHandler.js");

var smtp = simplesmtp.createServer({disableDNSValidation : config.disableDNSValidation });
smtp.listen(config.listenPort, function (error) {
	if (error == null) return;
	console.error( "unable to start smtp server on port " + config.listenPort + " " + error);
	process.exit();
});

process.stdin.resume();
process.stdin.setEncoding('utf8');

process.on( 'SIGINT', function() {
  console.log( "\nshutting down")
  smtp.end()
  console.log( "\nbye.")
  process.exit()
})

console.log("type CTRL+C in order to stop the server")


smtp.on("startData", function(envelope){ 
    console.log("Message from:", envelope.from);  
    console.log("Message to:", envelope.to);
	var memStream = new MemoryStream();
	parser = new messageHandler();
	try {
		parser.parseStream(memStream);
	}
	catch(e) {
		console.log(e);
	}
   envelope.saveStream = memStream;
});

smtp.on("data", function(envelope, chunk){
    envelope.saveStream.write(chunk);
});

smtp.on("dataReady", function(envelope, callback){
    console.log(envelope.data);
	envelope.saveStream.end();
    callback(null, "ABC1"); // ABC1 is the queue id to be advertised to the client
    callback(new Error("That was clearly a spam!"));
});