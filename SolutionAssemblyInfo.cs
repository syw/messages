﻿using System.Reflection;

[assembly: AssemblyCopyright("Copyright Sears Israel")]
[assembly: AssemblyCompany("Sears Israel")]
[assembly: AssemblyTrademark("SYW Messages App")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: AssemblyInformationalVersion("$GIT_HASH$ from $GIT_BRANCH$ built on $BUILD_DATE$")]