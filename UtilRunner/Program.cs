﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using CommonGround;
using SYW.App.Messages.Web.Config;
using SYW.App.Messsages.Domain;
using UtilRunner.Utils;
using log4net;

namespace UtilRunner
{
	public class Program
	{
		private static ICommonContainer _container;
		private static IUtilityLogger _utilityLogger;
		private static ILog _logger;

		public static void Main(string[] args)
		{
			Bootstrap();

			if (InvalidArgs(args))
				PrintHelpMessage();

			RunUtility(args);
		}

		private static void RunUtility(string[] args)
		{
			var utilName = args[0];

			var utilTypeToRun = Assembly.GetExecutingAssembly().GetExportedTypes()
				.Where(x => typeof (IUtil).IsAssignableFrom(x) && !x.IsAbstract)
				.FirstOrDefault(x => x.Name.ToLower() == utilName.ToLower());

			if (utilTypeToRun == null)
			{
				_utilityLogger.Error("Couldn't find util to run '" + utilName + "'");
				return;
			}

			var utilInterface = Assembly.GetExecutingAssembly().GetExportedTypes()
				.Where(x => typeof (IUtil).IsAssignableFrom(x) && x.IsAbstract)
				.FirstOrDefault(x => x.Name.ToLower().Substring(1) == utilName.ToLower());

			if (utilInterface == null)
			{
				_utilityLogger.Error("Couldn't find matching interface for util '" + utilName + "'");
				_utilityLogger.Error("Util interface must inherit from IUtil");
				return;
			}

			var util = (IUtil)_container.Resolve(utilInterface);

			try
			{
				util.Run(args);
			}
			catch (Exception ex)
			{
				_utilityLogger.Error("Error while running the utility '" + utilInterface.Name + "'");
				_utilityLogger.Error(ex);
				_logger.Error("Error while running the utility '" + utilInterface.Name + "'", ex);
			}

			Environment.Exit(0);
		}

		private static void PrintHelpMessage()
		{
			Console.WriteLine("Invalid arguments!");
			Console.WriteLine("Add the name of the utility as the only argument.");
			Console.WriteLine("e.g.: UtilRunner.exe BauEmailUtil");
		}

		private static bool InvalidArgs(IEnumerable<string> args)
		{
			return args.Count() != 1;
		}

		private static void Bootstrap()
		{
			_container = new CommonContainer();
			var bootstrapper = new Bootstrapper(_container);

			var currentAssembly = Assembly.GetExecutingAssembly();

			bootstrapper.AddAssemblies(currentAssembly);
			bootstrapper.RegisterMongoDb();
			bootstrapper.InitializeLogger();
			bootstrapper.RegisterDefaultInterfaces();
			bootstrapper.RegisterSettingsManager();
			bootstrapper.RegisterEvents();
			bootstrapper.InitializeMappers();

			_logger = _container.Resolve<ILog>();
			_utilityLogger = _container.Resolve<IUtilityLogger>();
		}
	}
}