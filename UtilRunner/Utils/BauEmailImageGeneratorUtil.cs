using SYW.App.Messsages.Domain.Feeds;

namespace UtilRunner.Utils
{
	public interface IBauEmailImageGeneratorUtil : IUtil
	{
	}
	public class BauEmailImageGeneratorUtil : IBauEmailImageGeneratorUtil
	{

		private readonly IBauEmailImageGenerator _bauEmailImageGenerator;

		public BauEmailImageGeneratorUtil(IBauEmailImageGenerator bauEmailImageGenerator)
		{
			_bauEmailImageGenerator = bauEmailImageGenerator;
		}


        public void Run(string[] args)
		{
			_bauEmailImageGenerator.GenerateImage();
		}
	}
}