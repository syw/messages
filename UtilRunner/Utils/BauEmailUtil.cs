﻿using System;
using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Feeds;

namespace UtilRunner.Utils
{
	public interface IBauEmailUtil : IUtil {}

	public class BauEmailUtil : IBauEmailUtil
	{
		private readonly IBauEmailFeedProvider _bauEmailFeedProvider;
		private readonly IMongoStorage<BauEmailFeedObjectDto,ObjectId> _bauEmailFeedObjectDtoStorage;
		private readonly IMongoStorage<MessageDto,ObjectId> _messageDtoStorage;
		private readonly IBauEmailFeedProviderSettings _bauEmailFeedProviderSettings;

		public BauEmailUtil(IBauEmailFeedProvider bauEmailFeedProvider, IMongoStorage<BauEmailFeedObjectDto,ObjectId> bauEmailFeedObjectDtoStorage, IMongoStorage<MessageDto,ObjectId> messageDtoStorage, IBauEmailFeedProviderSettings bauEmailFeedProviderSettings)
		{
			_bauEmailFeedProvider = bauEmailFeedProvider;
			_bauEmailFeedObjectDtoStorage = bauEmailFeedObjectDtoStorage;
			_messageDtoStorage = messageDtoStorage;
			_bauEmailFeedProviderSettings = bauEmailFeedProviderSettings;
		}

        public void Run(string[] args)
		{
			Console.WriteLine("Starting to read feed...");
			var bauFeed = _bauEmailFeedProvider.GetFeed();
			Console.WriteLine("feed read...");

			foreach (var bauEmail in bauFeed)
			{
				Console.WriteLine("Starting to create system message...");
				if (MessageExists(bauEmail))
				{
					Console.WriteLine(string.Format("\t Message {0} already exist", bauEmail.EmailId));
					continue;
				}

				if (bauEmail.Published >= SystemTime.Now() || bauEmail.Published.AddDays(_bauEmailFeedProviderSettings.PublishTimespanInDays) <= SystemTime.Now())
				{
					Console.WriteLine(string.Format("\t Message {0} published date {1} is greater than now or {2} days older", bauEmail.EmailId, bauEmail.Published.ToShortDateString(), _bauEmailFeedProviderSettings.PublishTimespanInDays));
					continue;
				}

				_bauEmailFeedObjectDtoStorage.Insert(new BauEmailFeedObjectDto
														{
															BrandName = bauEmail.BrandName,
															Content = bauEmail.Content,
															EmailId = bauEmail.EmailId,
															EndDate = bauEmail.EndDate,
															Logo = bauEmail.Logo,
															Published = bauEmail.Published,
															SubjectLine = bauEmail.SubjectLine
														});
			}
			Console.WriteLine("Done :)");
		}

		private bool MessageExists(BauEmailFeedObject bauEmail)
		{
			var filterBauEmailFeedObjectDtoStorag = Builders<BauEmailFeedObjectDto>.Filter.Eq(x => x.EmailId, bauEmail.EmailId);
			var filterMessageDtoStorage = Builders<MessageDto>.Filter.Eq(x => x.FeedMessageId, bauEmail.EmailId);

			return _bauEmailFeedObjectDtoStorage.GetFirstOrDefault(filterBauEmailFeedObjectDtoStorag)!=null || _messageDtoStorage.GetFirstOrDefault(filterMessageDtoStorage)!=null;
		}
	}
}