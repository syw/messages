﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualBasic.FileIO;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Entities;

namespace UtilRunner.Utils
{
    public interface IConversationCounterIndicatiorUpdater : IUtil
    {
    }

    public class ConversationCounterIndicatiorUpdater : IConversationCounterIndicatiorUpdater
    {
        private readonly IConversationCounterUpdater _conversationCounterUpdater;
        private readonly IEntitiesRepository _entitiesRepository;
        private readonly IConversationCounterService _conversationCounterService;


        public ConversationCounterIndicatiorUpdater(IConversationCounterUpdater conversationCounterUpdater,
            IEntitiesRepository entitiesRepository, IConversationCounterService conversationCounterService)
        {
            _conversationCounterUpdater = conversationCounterUpdater;
            _entitiesRepository = entitiesRepository;
            _conversationCounterService = conversationCounterService;
        }

        public void Run(string[] args)
        {
            Console.WriteLine("Reading users from MongoDB");
            var userIds = _entitiesRepository.GetAll();
            Console.WriteLine("Read " + userIds.Count + " users from MongoDB");

            Console.WriteLine("Reading users from CSV file - " + args[1]);
            var dbIds = new List<long>();

            using (TextFieldParser parser = new TextFieldParser(args[1]))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    string[] fields = parser.ReadFields();
                    foreach (string field in fields)
                    {
                        dbIds.Add(Int64.Parse(field));
                    }
                }
            }
            Console.WriteLine("Read " + dbIds.Count + " users from CSV file");

            Console.WriteLine("Updating users");
            for(var i = 0 ; i < userIds.Count; i++)
            {
                if (i % 10000 == 0)
                    Console.WriteLine("Updated " + i + " Users of " + userIds.Count);

                if(dbIds.Contains(userIds[i].OriginalId))
                {
                    _conversationCounterUpdater.UpdateUnreadNumber(userIds[i]._id);
                }

            } 
        }
    }
}