namespace UtilRunner.Utils
{
	public class DailyChangesCollectionInfo : IDailyChangesCollectionInfo
	{
		public string CreationDateFieldName { get; set; }
		public string PluralEntityName { get; set; }
		public string CollectionName { get; set; }
		public string SortingFieldName { get; set; }
	}
}