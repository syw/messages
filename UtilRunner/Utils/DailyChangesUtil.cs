﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Mongo;
using MongoDB.Bson;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using SYW.App.Messsages.Domain;
using SYW.App.Messsages.Domain.Services.Ftp;

namespace UtilRunner.Utils
{
	public interface IDailyChangesUtil: IUtil {
	}

	public class DailyChangesUtil : IDailyChangesUtil
	{
		private readonly IFtpUploader _ftpUploader;
		private readonly IUtilityLogger _utilityLogger;
		private readonly IZipHelper _zipHelper;
		private readonly IMongoDatabaseProvider _databaseProvider;
		private readonly IDailyChangesUtilSettings _settings;

		private Process _currentProcess;

		private readonly string _rootDirectory;

		public DailyChangesUtil(IFtpUploader ftpUploader, IZipHelper zipHelper, IUtilityLogger utilityLogger, IMongoDatabaseProvider databaseProvider, IDailyChangesUtilSettings settings)
		{
			_ftpUploader = ftpUploader;
			_zipHelper = zipHelper;
			_utilityLogger = utilityLogger;
			_databaseProvider = databaseProvider;
			_settings = settings;

			_rootDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		}

		public void Run(string[] args)
		{
			ExecuteDailyExport();
		}

		public void ExecuteDailyExport()
		{
			_utilityLogger.Debug("Begining DailyChangesUtil - Run");

			_currentProcess = Process.GetCurrentProcess();

			_utilityLogger.Debug(string.Format("Checking for newly added documents from {0} days ago...", _settings.DaysBack));

			var currentDate = DateTime.Today;
			var fromDate = currentDate.AddDays(-_settings.DaysBack);

			var newOutputDirectory = new DirectoryInfo(Path.Combine(_rootDirectory,
			                                                        _settings.OutputPath,
			                                                        string.Format("delta_{0}", currentDate.ToString("MM-dd-yyyy"))));
			if (!newOutputDirectory.Exists)
				newOutputDirectory.Create();

			List<DailyChangesCollectionInfo> collectionInfos = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DailyChangesCollectionInfo>>(_settings.CollectionInfos);

			Parallel.ForEach(collectionInfos, collectionInfo => ExportFile(collectionInfo, newOutputDirectory.FullName, fromDate));

			_utilityLogger.Debug("Completed writing to files, compressing and uploading..." + Environment.NewLine);

			var compressedFilePath = _zipHelper.CompressDirectory(newOutputDirectory);

			var uploadStatus = _ftpUploader.UploadFiles(
				_settings.DailyChangesFtpIp,
				_settings.DailyChangesFtpOutputPath,
				_settings.DailyChangesFtpUser,
				_settings.DailyChangesFtpPassword,
				compressedFilePath);

			if (uploadStatus == "OK")
			{
				_utilityLogger.Debug("Attempting to clean Scripts directory" + Environment.NewLine);
				newOutputDirectory.Delete(true);
				_utilityLogger.Debug("Cleaning successfuly finished" + Environment.NewLine);
			}
			else
			{
				_utilityLogger.Error("Upload failed, files are kept untouched at " + newOutputDirectory.FullName);
				_utilityLogger.Error(uploadStatus);
			}
			_currentProcess.Kill();
		}

		private void ExportFile(IDailyChangesCollectionInfo collectionInfo, string path, DateTime fromDate)
		{
			_utilityLogger.Debug(string.Format("Looking for new records of {0}", collectionInfo.PluralEntityName));

			var collection = _databaseProvider.Database.GetCollection<BsonDocument>(collectionInfo.CollectionName);
			var fileName = GetFileName(path, collectionInfo.CollectionName);

			string fullPath = Path.Combine(path, fileName);
			if (File.Exists(fullPath))
				File.Delete(fullPath);

			int batchNum = 0;
			var sort = Builders<BsonDocument>.Sort.Ascending(collectionInfo.SortingFieldName);
			var filter = Builders<BsonDocument>.Filter.Gte(collectionInfo.CreationDateFieldName, fromDate);

			while (true)
			{
				var newRecords = collection
					.Find(filter, new FindOptions {BatchSize = int.MaxValue /* As for Mongo driver 2.2 exists an issue https://jira.mongodb.org/browse/CSHARP-1510 . Update to 2.2.1 + is needed, the workaround is specifying BatchSize.*/})
					.Sort(sort)
					.Skip(batchNum*_settings.BatchSize)
					.Limit(_settings.BatchSize)
					.ToList();


				if (!newRecords.Any())
				{
					if (batchNum == 0)
					{
						_utilityLogger.Debug(string.Format("No new records of {0} found.", collectionInfo.PluralEntityName));
					}
					return;
				}

				using (var streamWriter = new StreamWriter(fileName, true))
				{
					_utilityLogger.Debug(string.Format("Exporting {3} batch, {2} new records of {0} to {1}", collectionInfo.PluralEntityName, fileName, newRecords.Count, batchNum + 1));

					foreach (var doc in newRecords)
					{
						using (var stringWriter = new StringWriter())
						using (var jsonWriter = new JsonWriter(stringWriter, new JsonWriterSettings()))
						{
							var context = BsonSerializationContext.CreateRoot(jsonWriter);
							collection.DocumentSerializer.Serialize(context, doc);
							var line = stringWriter.ToString();
							streamWriter.WriteLine(line);
						}
					}
				}
				batchNum++;
			}
		}

		private string GetFileName(string path, string collectionName)
		{
			return string.Format("{0}\\{1}_{2}.json", path, collectionName, DateTime.Now.ToString("MM-dd-yyyy"));
		}
	}
}