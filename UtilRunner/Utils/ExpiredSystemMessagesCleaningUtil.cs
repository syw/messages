﻿using System;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Services.Settings;

namespace UtilRunner.Utils
{
	public interface IExpiredSystemMessagesCleaningUtil : IUtil {}

	public class ExpiredSystemMessagesCleaningUtil : IExpiredSystemMessagesCleaningUtil
	{
		private readonly IMongoStorage<MessageDto, ObjectId> _messagesStorage;
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private readonly IMongoStorage<UserConversationStatus, ObjectId> _userConversationStatusStorage;
		private readonly IMongoStorage<UserMessageStatus, ObjectId> _userMessageStatusStorage;
		private readonly ISystemMessagesSettings _systemMessagesSettings;

		public ExpiredSystemMessagesCleaningUtil(
			IMongoStorage<ConversationDto, ObjectId> conversationStorage,
			IMongoStorage<MessageDto, ObjectId> messagesStorage,
			IMongoStorage<UserConversationStatus, ObjectId> userConversationStatusStorage,
			IMongoStorage<UserMessageStatus, ObjectId> userMessageStatusStorage,
			ISystemMessagesSettings systemMessagesSettings)
		{
			_conversationStorage = conversationStorage;
			_messagesStorage = messagesStorage;
			_userConversationStatusStorage = userConversationStatusStorage;
			_userMessageStatusStorage = userMessageStatusStorage;
			_systemMessagesSettings = systemMessagesSettings;
		}

        public void Run(string[] args)
		{
			var filterSytemConversationsIds = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var projectionSystemConversationsIds = Builders<ConversationDto>.Projection.Include(x => x._id);
			var sytemConversationsIds = _conversationStorage.SelectAs(filterSytemConversationsIds, projectionSystemConversationsIds).Select(y => y["_id"].AsObjectId).ToList();

			var filterMessagesStorage = Builders<MessageDto>.Filter.In("ConversationId", sytemConversationsIds);
			var systemMessages = _messagesStorage.GetList(filterMessagesStorage);

			int expirationPeriod = _systemMessagesSettings.ExpirationTimeInMonths;

			var expirationDate = DateTime.UtcNow.AddMonths(- expirationPeriod);
			var expiredMessagesIds = systemMessages.Where(x => x.Date < expirationDate).Select(m => m._id).ToList();

			var filterRemoveMessageStatusStorage = Builders<UserMessageStatus>.Filter.In("MessageId", expiredMessagesIds);
			_userMessageStatusStorage.Remove(filterRemoveMessageStatusStorage);

			var filterRemoveMessagesStorage = Builders<MessageDto>.Filter.In("_id", expiredMessagesIds);
			_messagesStorage.Remove(filterRemoveMessagesStorage);

			var emptyConversationsIds = sytemConversationsIds.Where(x => !(systemMessages.Any(s => !expiredMessagesIds.Contains(s._id) && s.ConversationId == x))).ToList();

			if (!emptyConversationsIds.Any()) return;

			var filterRemoveConversationStatusStorage = Builders<UserConversationStatus>.Filter.In("ConversationId", emptyConversationsIds);
			_userConversationStatusStorage.Remove(filterRemoveConversationStatusStorage);

			var filterRemoveConversationStorage = Builders<ConversationDto>.Filter.In("_id", emptyConversationsIds);
			_conversationStorage.Remove(filterRemoveConversationStorage);
		}
	}
}