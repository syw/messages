namespace UtilRunner.Utils
{
	public interface IDailyChangesCollectionInfo {
		string CreationDateFieldName { get; set; }
		string PluralEntityName { get; set; }
		string CollectionName { get; set; }
		string SortingFieldName { get; set; }
	}
}