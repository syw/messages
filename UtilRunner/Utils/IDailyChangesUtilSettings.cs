﻿using System.Collections.Generic;
using CommonGround.Settings;

namespace UtilRunner.Utils
{
	public interface IDailyChangesUtilSettings
	{
		[Default("")]
		string OutputPath { get; set; }

		[Default(1)]
		int DaysBack { get; set; }

		[Default(10000)]
		int BatchSize { get; set; }

		[Default("[{ 'CreationDateFieldName' : 'Date', 'PluralEntityName' : 'Messages', 'CollectionName' : 'messagedto', 'SortingFieldName' : 'Date' }, { 'CreationDateFieldName' : 'StartDate', 'PluralEntityName' : 'Conversations', 'CollectionName' : 'conversationdto', 'SortingFieldName' : 'StartDate' }, { 'CreationDateFieldName' : 'MemberSince', 'PluralEntityName' : 'Entities', 'CollectionName' : 'entities', 'SortingFieldName' : 'MemberSince' }, { 'CreationDateFieldName' : 'CreatedDateTime', 'PluralEntityName' : 'Bau Entities', 'CollectionName' : 'bauentity', 'SortingFieldName' : 'CreatedDateTime' }, { 'CreationDateFieldName' : 'CreatedDateTime', 'PluralEntityName' : 'User Conversation Statuses', 'CollectionName' : 'userconversationstatus', 'SortingFieldName' : 'CreatedDateTime' }, { 'CreationDateFieldName' : 'CreatedDateTime', 'PluralEntityName' : 'Message Statuses', 'CollectionName' : 'usermessagestatus', 'SortingFieldName' : 'CreatedDateTime' }, { 'CreationDateFieldName' : 'CreatedDateTime', 'PluralEntityName' : 'Entity Networks', 'CollectionName' : 'entitynetwork', 'SortingFieldName' : 'CreatedDateTime' }]")]
		string CollectionInfos { get; set; }

		[Default("10.0.0.148")]
	    string DailyChangesFtpIp { get; set; }

        [Default("Snap")]
	    string DailyChangesFtpUser { get; set; }

        [Default("1234#qwer")]
        string DailyChangesFtpPassword { get; set; }

		[Default("\\")]
		string DailyChangesFtpOutputPath { get; set; }
	}
}