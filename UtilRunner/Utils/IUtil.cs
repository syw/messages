namespace UtilRunner.Utils
{
	public interface IUtil
	{
		void Run(string[] args);
	}
}