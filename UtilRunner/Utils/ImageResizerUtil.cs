﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;

namespace UtilRunner.Utils
{
	public interface IImageResizerUtil : IUtil {}

	public class ImageResizerUtil : IImageResizerUtil
	{
        public void Run(string[] args)
		{
			Console.WriteLine("Please Enter folder path");

			var folderName = Console.ReadLine();
			Console.Clear();
			if (folderName != null && Directory.Exists(folderName))
			{
				var resizedImagesCounter = 0;
				int requestedWidth;
				var outputText = "Please enter required width";
				string userInput;
				do
				{
					Console.WriteLine(outputText);
					userInput = Console.ReadLine();
					Console.Clear();
					outputText = "Width must be a number , Please enter required width or enter 'x' to exit";
					if (userInput != null && userInput.ToLower() == "x")
						return;
				}
				while (!Int32.TryParse(userInput, out requestedWidth));

				var imagesFolder = new DirectoryInfo(folderName);
				var allImages = imagesFolder.GetFiles("*.gif").Concat(imagesFolder.GetFiles("*.jpg")).Concat(imagesFolder.GetFiles("*.bmp")).Concat(imagesFolder.GetFiles("*.png"));

				foreach (var imageFile in allImages)
				{
					var imgOriginal = Image.FromFile(imageFile.FullName);
					var percentWidth = ((float)imgOriginal.Width/(float)requestedWidth);

					var resizedBitmap = new Bitmap(requestedWidth, (int)(imgOriginal.Height/percentWidth));
					var resizedImage = Graphics.FromImage(resizedBitmap);
					resizedImage.InterpolationMode = InterpolationMode.HighQualityBicubic;
					resizedImage.CompositingQuality = CompositingQuality.HighQuality;
					resizedImage.SmoothingMode = SmoothingMode.HighQuality;
					resizedImage.DrawImage(imgOriginal, 0, 0, resizedBitmap.Width, resizedBitmap.Height);
					imgOriginal.Dispose();
					resizedImage.Dispose();
					imageFile.Delete();
					resizedBitmap.Save(folderName + "\\" + imageFile.Name);
					resizedBitmap.Dispose();
					resizedImagesCounter++;
				}
				Console.WriteLine("{0} Images have been resized ", resizedImagesCounter);
			}
			else
			{
				Console.WriteLine("Cannot find the specified directory - {0}", folderName);
			}
		}
	}
}