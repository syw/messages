﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using CommonGround.Utilities;
using Mongo;
using MongoDB.Bson;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Entities;
using SYW.App.Messsages.Domain.Feeds;
using SYW.App.Messsages.Domain.Messages;

namespace UtilRunner.Utils
{
	public interface IMissingBauFixes : IUtil {}

	public class MissingBauFixes : IMissingBauFixes
	{
		private readonly IMongoStorage<BauEmailFeedObjectDto,ObjectId> _bauEmailFeedObjectDtoStorage;

		public MissingBauFixes(IMongoStorage<BauEmailFeedObjectDto, ObjectId> bauEmailFeedObjectDtoStorage)
		{
			_bauEmailFeedObjectDtoStorage = bauEmailFeedObjectDtoStorage;
		}

        public void Run(string[] args)
		{
			Console.WriteLine("Enter folder name");

			var foldername = Console.ReadLine();

			foreach (var filename in  Directory.EnumerateFiles(foldername))
			{
				using (var sr = new StreamReader(filename))
				{
					var shortFileName = new FileInfo(filename).Name;

					var shortDate = shortFileName.Replace(foldername, "").Replace(".html", "");
					var regularDate = new DateTime(2014, int.Parse(shortDate.Substring(0, 2)), int.Parse(shortDate.Substring(2, 2)));
					
					if (!_sujbectLines[foldername].ContainsKey(shortDate))
					{
						Console.WriteLine("Missing file for " + foldername + " and " + shortDate + " in subject line dictionary.");
						continue;
					}

					var bauEmailFeedObjectDto = new BauEmailFeedObjectDto
					                            	{
					                            		EmailId = Guid.NewGuid().ToString(),
					                            		HasScreenshot = false,
					                            		Content = sr.ReadToEnd(),
														SubjectLine = _sujbectLines[foldername][shortDate],
					                            		BrandName = _brands[foldername],
														Logo = _logos[foldername],
					                            		Published = regularDate,
					                            		EndDate = regularDate.AddDays(7)
					                            	};

					_bauEmailFeedObjectDtoStorage.Insert(bauEmailFeedObjectDto);
				}
			}
		}
		//. searshometownstore
		private readonly IDictionary<string, string> _brands = new Dictionary<string, string>
		                                                       	{
		                                                       		{"landsend", "Lands' End"},
																	{"craftsman", "Craftsman"},
																	{"mygofer", "MyGofer"},
																	{"searshometownstore", "Sears Hometown Store"},
																	{"shopyourway", "Shop Your Way"},
																	{"sywvip", "Shop Your Way Rewards VIP"},
																	{"kmart", "Kmart"},
																	{"sears", "Sears"}

																};
		private readonly IDictionary<string, string> _logos = new Dictionary<string, string>
		                                                      	{
		                                                      		{"landsend", "http://s2.sywcdn.net/getImage?url=%2f%2fs5.sywcdn.net%2fuser%2fea94_4563835.jpg&t=Brand&w=130&h=130&qlt=75&mrg=1&s=5a1c53f3cf9b47c7c1fe131a796bf504"},
		                                                      		{"craftsman", "http://s5.sywcdn.net/getImage?url=%2f%2fs4.sywcdn.net%2fuser%2fa63c_4784089.jpg&t=Brand&w=130&h=130&qlt=75&mrg=1&s=b0178873fc59d395e3ee769e5078a4b6"},
																	{"mygofer", "http://s3.sywcdn.net/getImage?url=%2f%2fs1.sywcdn.net%2fuser%2f417b_101469.jpg&t=Site&w=130&h=130&qlt=75&mrg=1&s=0094a893b8359c4b5ea5235d473483bf"},
																	{"searshometownstore", "http://s1.sywcdn.net/getImage?url=%2f%2fs4.sywcdn.net%2fuser%2f37b3_124318.jpg&t=Brand&w=130&h=130&qlt=75&mrg=1&s=0025b83597a62a0e46c6ffe446e84861"},
																	{"shopyourway", "http://s5.sywcdn.net/getImage?url=%2f%2fs1.sywcdn.net%2fuser%2f3748_269837.jpg&t=Site&w=130&h=130&qlt=75&mrg=1&s=2d5eef7f6b17d4f1708971b2653b9858"},
																	{"sywvip", "http://s3.sywcdn.net/getImage?url=%2f%2fs4.sywcdn.net%2fuser%2f89ca_660475.jpg&t=Brand&w=130&h=130&qlt=75&s=95724af33ba4a7c03d4c183dc1937594"},
																	{"kmart", "http://s5.sywcdn.net/getImage?url=%2f%2fs4.sywcdn.net%2fuser%2f435e_12369724.jpg&t=Brand&w=130&h=130&qlt=75&mrg=1&s=8f6113ba66bdff60e8179fb51deb01ef"},
																	{"sears", "http://s1.sywcdn.net/getImage?url=%2f%2fs5.sywcdn.net%2fuser%2f3670_11936417.jpg&t=Brand&w=130&h=130&qlt=75&mrg=1&s=bdb9b510512a85ffaf93f0dcbd00a018"}
																};

		private readonly IDictionary<string, IDictionary<string, string>> _sujbectLines = new Dictionary<string, IDictionary<string, string>>
		{
		    {"landsend", new Dictionary<string, string>
		        {
		            {"0317","Lucky you - still time to save up to 30%"},
					{"0318","Today! Save up to 25% off Shape & Enhance"},
					{"0319","One tote holds a full weekend of style"},
					{"0320","Welcome spring... and 20% off Swim!"},
					{"0321","Limited time: 20% off Swim"},
					{"0322","20% off Swim, Shorts, Tees & more"},
					{"0323","Ends today: 20% off spring must-haves"},
					{"0324","Swim in confidence with free shipping both ways"},
					{"0325","3-day Spring Sale!"},
					{"0326","Ends tomorrow: spring savings"},
					{"0327","Save up to 30% - ends today!"},
					{"0328","Save 25% on your order | limited time"},
					{"0329","Swim right in with 25% off your order"},
					{"0330","Your entire order is 25% off!"},
					{"0331","Last day to save 25% on your order!"},
					{"0401","Extended! Extra day to save 25%"},
					{"0402","Your April Agenda"},
					{"0403","Spring's best dresses, 20% off!"},
					{"0404","The color fix + 20% off dresses"},
					{"0405","Ends today: 20% off dresses"}
		        }
		    }, 
			{"craftsman", new Dictionary<string, string>
				{
					{"0323","Saver Days thru 3/29 + Up to 50% off tools"},
					{"0327","Only 3 days left! Save up to 50% off"}
				}
			}, 
			{"mygofer", new Dictionary<string, string>
				{
					{"0316","Check out this week's top deals"},
					{"0320","Spring Has Sprung! Up to 25% off Cleaning Supplies"},
					{"0323","Check out this week's top deals"},
					{"0403","Spring Is Sprouting Savings - up to 50% off General Grocery"}
				}
			},
			{"searshometownstore", new Dictionary<string, string>
				{
					{"0328","Grand opening: Extra 10% in coupon savings"}
				}
			},
			{"shopyourway", new Dictionary<string, string>
				{
					{"0327","Save up to 20% with your exclusive coupons"},
					{"0321","Sunday & Monday only: Extra 10-15% off almost everything!"},
					{"0329","How is your vacuum?"},
					{"0331","Ends 4/2: Up to 70% off mattresses & more"},
					{"0402","FREE shipping for 3 months, just for you!"},
					{"0404","Up to $80 in gift cards + a free gift from Kmart Pharmacy"}
				}
			},
			{"sywvip", new Dictionary<string, string>
				{
					{"0329","Extra 30% off all shoes"}
				}
			},
			{"kmart", new Dictionary<string, string>
				{
					{"0316","Enter to win 100,000 points + your LocalAd"},
					{"0317","Survive the madness: Everything you need for tournament time"},
					{"0319","Hurry! ? The Clearance Countdown ends soon!"},
					{"0321","Not feeling in your “Prime”? 2-day shipping just $39/year"},
					{"0324","Save now, pay later"},
					{"0330","Warm up to spring with shorts and tees + LocalAd"},
					{"0401","Cash your next check for FREE at Kmart!"},
					{"0404","See how Brenda does Layaway"},
					{"0405","All about baby: $5 back in points on baby purchases"}
				}
			},
			{"sears", new Dictionary<string, string>
				{
					{"0316","Extra 5%-10% off: Keep your green!"},
					{"0321","Family & Friends – NOW online + in store, too!"},
					{"0322","Online tonight! Family & Friends: Extra 5%–15% off"},
					{"0330","Extra 5%-10% off ? Midnight Madness"},
					{"0401","Today only! A trip in time = Dime deals & more!"},
					{"0402","Spring into fashion this season with 10% back in points!"},
					{"0404","Get TOP brands @ LOWEST prices of the season!"},
					{"0405","Double feature! Lowest prices of the season + Super Buys"}
				}
			},
		};
	}
}