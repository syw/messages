﻿using System;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain.Feeds;

namespace UtilRunner.Utils
{
	public interface ISanityUtil : IUtil { }

	public class SanityUtil : ISanityUtil
	{
		private readonly IBauEmailFeedProvider _bauEmailFeedProvider;
		private readonly IMongoStorage<BauEmailFeedObjectDto,ObjectId> _bauEmailFeedObjectDtoStorage;
		private readonly IMongoStorage<MessageDto, ObjectId> _messageDtoStorage;
		private readonly IBauEmailFeedProviderSettings _bauEmailFeedProviderSettings;

		public SanityUtil(IBauEmailFeedProvider bauEmailFeedProvider, IMongoStorage<BauEmailFeedObjectDto, ObjectId> bauEmailFeedObjectDtoStorage, IMongoStorage<MessageDto, ObjectId> messageDtoStorage, IBauEmailFeedProviderSettings bauEmailFeedProviderSettings)
		{
			_bauEmailFeedProvider = bauEmailFeedProvider;
			_bauEmailFeedObjectDtoStorage = bauEmailFeedObjectDtoStorage;
			_messageDtoStorage = messageDtoStorage;
			_bauEmailFeedProviderSettings = bauEmailFeedProviderSettings;
		}

        public void Run(string[] args)
		{
			Console.WriteLine("UtilRunner works!");
			Console.Write("Trying to read from Mongo... ");
			var filter = Builders<MessageDto>.Filter.Empty;
			var dto = _messageDtoStorage.GetFirstOrDefault(filter);
			Console.WriteLine("Success. Some message from the mongo: \"{0}\"", dto.Content);

			Console.ReadLine();
		}
	}
}