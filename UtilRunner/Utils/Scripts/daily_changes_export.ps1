# =========================
# Created by Ilan Ezersky
# Creation on 2015-12-28
# Last updated on 2016-02-24
# Version 1.0.3
# ==========================
param (
	[string]$days_back = 1, 
	[string]$mongo_path = "C:\Program Files\MongoDB\Server\3.2\bin",
    [string]$output_path = ""
    )

Write-Host "Checking for newly added documents from" $days_back "days ago..."

# Notes:
# You might need to change host's ip & address, depending on where's the database located.

$from_date = (Get-Date).AddDays(-$days_back);
$current_date = Get-Date -format yyyy_M_d;
# Initial UNIX date.
$default_date = [datetime]"1/1/1970 02:00";
# Total number of milliseconds that passed from the initial UNIX date and to yesterday.
$total_ms = [long]($from_date - $default_date).TotalMilliseconds;

$invocation = (Get-Variable MyInvocation).Value;
# current directory path -- to return to after the script is run.
$initial_directoy_path = Split-Path $invocation.MyCommand.Path;

# File names
# Send a value to $output_path in order to change where the files will be created.
$message_dto_file_name = $initial_directoy_path + $output_path + "\messagedto_" + $current_date + ".json";
$conversation_dto_file_name = $initial_directoy_path + $output_path + "\conversationdto_" + $current_date + ".json";
$entities_file_name = $initial_directoy_path + $output_path + "\entity_" + $current_date + ".json";
$bau_entity_file_name = $initial_directoy_path + $output_path + "\bauentity_" + $current_date + ".json";
$user_conversation_status_file_name = $initial_directoy_path + $output_path + "\userconversationstatus_" + $current_date + ".json";
$user_message_status_file_name = $initial_directoy_path + $output_path + "\usermessagestatus_" + $current_date + ".json";
$entity_networks_file_name = $initial_directoy_path + $output_path + "\entitynetwork_" + $current_date + ".json";

# MongoDB greater than or equal
$gte = '$gte';
# the executable to use in order to begin the exporting process.
$exe = ".\mongoexport.exe";

Set-Location -Path $mongo_path;

$invocation = (Get-Variable MyInvocation).Value;

Write-Host "Switched location to"  $mongo_path;

Write-Host "Exporting new messages...";
Write-Host '.\mongoexport -h appsmongo1:27017 -d messages -c messagedto -q "{Date:{$gte: new Date(' $total_ms ') }}" -o ' $message_dto_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c messagedto -q "{Date:{$gte: new Date(' + $total_ms + ') }}" -o ' + $message_dto_file_name;

# Begin exporting new messages.
Start-Process -FilePath $exe -ArgumentList $arguements;

Write-Host "Exporting new conversations...";
Write-Host '.\mongoexport -h appsmongo1:27017 -d messages -c conversationdto -q "{StartDate:{$gte: new Date('  $total_ms  ') }}" -o ' $conversation_dto_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c conversationdto -q "{StartDate:{$gte: new Date(' + $total_ms + ') }}" -o ' + $conversation_dto_file_name;

# Begin exporting new conversations.
Start-Process -FilePath $exe -ArgumentList $arguements;

Write-Host "Exporting new entities...";
Write-Host '.\mongoexport -h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c entities -q "{MemberSince:{$gte: new Date('  $total_ms  ') }}" -o ' $entities_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c entities -q "{MemberSince:{$gte: new Date(' + $total_ms + ') }}" -o ' + $entities_file_name;

# Begin exporting new entities.
Start-Process -FilePath $exe -ArgumentList $arguements;

Write-Host "Exporting new bau entities...";
Write-Host '.\mongoexport -h appsmongo1:27017 -d messages -c bauentity -q "{CreatedDateTime:{$gte: new Date('  $total_ms  ') }}" -o ' $bau_entity_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -d messages -c bauentity -q "{CreatedDateTime:{$gte: new Date(' + $total_ms + ') }}" -o ' + $bau_entity_file_name;
# Begin exporting new bau entities.
Start-Process -FilePath $exe -ArgumentList $arguements;

Write-Host "Exporting new user conversation statuses...";
Write-Host '.\mongoexport -h appsmongo1:27017 -d messages -c messagedto -q "{CreatedDateTime:{$gte: new Date('  $total_ms  ') }}" -o ' $user_conversation_status_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c messagedto -q "{CreatedDateTime:{$gte: new Date(' + $total_ms + ') }}" -o ' + $user_conversation_status_file_name;

# Begin exporting new user conversation statuses.
Start-Process -FilePath $exe -ArgumentList $arguements;

Write-Host "Exporting new user message statuses...";
Write-Host '.\mongoexport -h appsmongo1:27017 -d messages -c usermessagestatus -q "{CreatedDateTime:{$gte: new Date(' + $total_ms + ') }}" -o ' $user_message_status_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c usermessagestatus -q "{CreatedDateTime:{$gte: new Date(' + $total_ms + ') }}" -o ' + $user_message_status_file_name;

# Begin exporting new user message statuses.
Start-Process -FilePath $exe -ArgumentList $arguements;

Write-Host "Exporting new entity networks...";
Write-Host '.\mongoexport -h appsmongo1:27017 -d messages -c entitynetwork -q "{CreatedDateTime:{$gte: new Date('  $total_ms  ') }}" -o ' $entity_networks_file_name;
Write-Host "================================";
$arguements = '-h appsmongo1:27017 -u messagesapp -p fancy1 -d messages -c entitynetwork -q "{CreatedDateTime:{$gte: new Date(' + $total_ms + ') }}" -o ' + $entity_networks_file_name;

# Begin exporting new entity networks.
Start-Process -FilePath $exe -ArgumentList $arguements;

Set-Location -Path $initial_directoy_path
