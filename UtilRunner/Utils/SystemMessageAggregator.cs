﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Emails;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain;

namespace UtilRunner.Utils
{
	public interface ISystemMessageAggregator : IUtil {}

	public class SystemMessageAggregator : ISystemMessageAggregator
	{
		private readonly IMongoStorage<UserMessageStatus, ObjectId> _userMessageStatusStorage;
		private readonly IMongoStorage<UserConversationStatus, ObjectId> _userConversationStatusStorage;
		private readonly IMongoStorage<MessageDto, ObjectId> _messagesStorage;
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private readonly IUserConversationStatusRepository _userConversationStatusRepository;
		private readonly IBauEntitiesRepository _bauEntitiesRepository;

		public SystemMessageAggregator(IMongoStorage<ConversationDto, ObjectId> conversationStorage, IMongoStorage<MessageDto, ObjectId> messagesStorage, IUserConversationStatusRepository userConversationStatusRepository, IUserMessageStatusRepository userMessageStatusRepository, IMongoStorage<UserMessageStatus, ObjectId> userMessageStatusStorage, IMongoStorage<UserConversationStatus, ObjectId> userConversationStatusStorage, IBauEntitiesRepository bauEntitiesRepository)
		{
			_conversationStorage = conversationStorage;
			_messagesStorage = messagesStorage;
			_userConversationStatusRepository = userConversationStatusRepository;
			_userMessageStatusStorage = userMessageStatusStorage;
			_userConversationStatusStorage = userConversationStatusStorage;
			_bauEntitiesRepository = bauEntitiesRepository;
		}

        public void Run(string[] args)
		{
			Console.WriteLine("Getting all conversations");
			var filter = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var orderBy = Builders<ConversationDto>.Sort.Descending(x=>x.StartDate);

			var allSystemConversations =_conversationStorage.GetList(filter,orderBy);

			//ExecuteGroupByCreator(allSystemConversations);

	        ExecuteGroupByPublisher(allSystemConversations);
		}

		private void ExecuteGroupByPublisher(List<ConversationDto> allSystemConversations)
		{
			var allPublishers = _bauEntitiesRepository.GetAll();
			var groupedPublishers = GroupPublishers(allPublishers).Where(x=> x.Value.Count > 1).ToList();
			Console.WriteLine("Finished grouping publishers, following publishers found: ");

			var systemConversationGrouped = new Dictionary<ObjectId, IEnumerable<ConversationDto>>();

			foreach (var publisher in groupedPublishers)
			{
				var p = publisher;
				var entityId = p.Value.OrderBy(x => x.EntityId).First().EntityId;

				Console.WriteLine("{0} Started grouping conversations for \"{1}\", found {2} duplicates.\n Main ID: {3}", DateTime.Now.ToString("HH:mm:ss"), publisher.Key, publisher.Value.Count, entityId);

				if (systemConversationGrouped.ContainsKey(entityId))
				{
					systemConversationGrouped[entityId] = systemConversationGrouped[entityId].Union(allSystemConversations.Where(x => p.Value.Any(y => y.EntityId == x.CreatorId)));
					continue;
				}
				var gc = allSystemConversations.Where(x => p.Value.Any(y => y.EntityId == x.CreatorId)).ToList();
				systemConversationGrouped.Add(entityId, gc);

				Console.WriteLine("{0} Found {1} conversations for \"{2}\"", DateTime.Now.ToString("HH:mm:ss"), gc.Count, publisher.Key);
			}

			foreach (var systemConversations in systemConversationGrouped)
			{
				var first = systemConversations.Value.First();

				var masterConversationId = first._id;

				Console.WriteLine("Master conversation id {0}, aggregating {1} conversations", masterConversationId, systemConversations.Value.Count());

				Parallel.ForEach(
					systemConversations.Value.Where(x=> x._id != masterConversationId), 
					conversation => AggregateConversationToMaster(conversation, masterConversationId));
			}

			foreach (var publisher in groupedPublishers)
			{
				var p = publisher;
				var entityId = p.Value.OrderBy(x => x.EntityId).First().EntityId;

				foreach (var duplicate in p.Value)
				{
					if (duplicate.EntityId == entityId)
						continue;

					var publisherFilter = Builders<BauEntity>.Filter.Eq("_id", duplicate._id);
					Console.WriteLine("Will delete {0}, name: {1}", duplicate._id, duplicate.BauType);
					_bauEntitiesRepository.Remove(publisherFilter);
				}

			}
		}

		private static Dictionary<string, List<BauEntity>> GroupPublishers(IEnumerable<BauEntity> allPublishers)
		{
			var groupedPublishers = new Dictionary<string, List<BauEntity>>();

			foreach (var publisher in allPublishers)
			{
				if (groupedPublishers.ContainsKey(publisher.BauType))
				{
					groupedPublishers[publisher.BauType].Add(publisher);
					continue;
				}
				groupedPublishers.Add(publisher.BauType, new List<BauEntity> {publisher});
			}
			return groupedPublishers;
		}

		private void ExecuteGroupByCreator(IEnumerable<ConversationDto> allSystemConversations)
		{
			var systemConversationGrouped = allSystemConversations
				.Where(x => x.CreatorId != ObjectId.Parse("5045fc09c2cdbcc25a6f3df3") && x.CreatorId != ObjectId.Parse("50796ab72cc26505b8f49c97"))
				.GroupBy(x => x.CreatorId);

			foreach (var systemConversations in systemConversationGrouped)
			{
				var first = systemConversations.First();

				var masterConversationId = first._id;

				Console.WriteLine("Master conversation id " + masterConversationId + " and aggregating " + systemConversations.Count());

				systemConversations.AsParallel().ForAll(
					conversation => AggregateConversationToMaster(conversation, masterConversationId));
			}
		}

		private void AggregateConversationToMaster(ConversationDto conversation, ObjectId masterConversationId)
		{
			Console.WriteLine("Get all messages for " + conversation._id);

			var filter = Builders<MessageDto>.Filter.Eq(x => x.ConversationId, conversation._id);
			var messages = _messagesStorage.GetList(filter);

			var updateModels = messages
				.Select(message => new UpdateOneModel<MessageDto>(
					Builders<MessageDto>.Filter.Eq("_id", message._id), 
					Builders<MessageDto>.Update.Set("ConversationId", masterConversationId))).ToList();

			_messagesStorage.UpdateBulk(updateModels, new BulkWriteOptions { IsOrdered = false });


			var conversationStatusFilter = Builders<UserConversationStatus>.Filter.Eq("ConversationId", conversation._id);
			_userConversationStatusStorage.Remove(conversationStatusFilter);

			var conversationFilter = Builders<ConversationDto>.Filter.Eq("_id", conversation._id);
			Console.WriteLine("Will delete {0}, master: {1}", conversation._id, masterConversationId);
			_conversationStorage.Remove(conversationFilter);
		}

		private UserMessageStatus CreateNewMessageStatus(ObjectId userId, ObjectId conversationId, ObjectId messageId, ConversationStatus status, DateTime date)
		{
			return new UserMessageStatus
						{
							_id = ObjectId.GenerateNewId(),
							UserId = userId,
							ConversationId = conversationId,
							MessageId = messageId,
							Status = (int)status,
                            CreatedDateTime = date,
							Date = date
						};
		}
	}
}