﻿using System;
using System.Drawing;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain;
using log4net;
using SYW.App.Messsages.Domain.Feeds;

namespace UtilRunner.Utils
{
	public interface ISystemMessageDimensionGenerator : IUtil {}

	public class SystemMessageDimensionGenerator : ISystemMessageDimensionGenerator
	{
		private readonly IMongoStorage<MessageDto,ObjectId> _messagesStorage;
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private readonly IBauScreenshotsFilesService _bauScreenshotsFilesService;
		private readonly ILog _log;

		public SystemMessageDimensionGenerator(IMongoStorage<MessageDto, ObjectId> messagesStorage,
												IBauScreenshotsFilesService bauScreenshotsFilesService,
												IMongoStorage<ConversationDto, ObjectId> conversationStorage, ILog log)
		{
			_messagesStorage = messagesStorage;
			_bauScreenshotsFilesService = bauScreenshotsFilesService;
			_conversationStorage = conversationStorage;
			_log = log;
		}

        public void Run(string[] args)
		{
			var filter = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var orderBy = Builders<ConversationDto>.Sort.Descending(x => x.StartDate);
			var projection = Builders<ConversationDto>.Projection.Include(x => x._id);
			
			var systemConversationsBulks = _conversationStorage.SelectAs(filter, projection, orderBy).Select(y=>y["_id"].AsObjectId).ToList();

			foreach (var systemConversations in systemConversationsBulks.Bulks(5))
			{
				var filter2 = Builders<MessageDto>.Filter.In("ConversationId", systemConversations);
				var messages = _messagesStorage.GetList(filter2);

				foreach (var message in messages)
				{
					try
					{
						var image = new Bitmap(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(message._id.ToString()));
						message.ContentHeight = image.Height;
						message.ContentWidth = image.Width;
						_messagesStorage.Upsert(message);
					}
					catch (Exception exception)
					{
						Console.WriteLine("Something went wrong when migrating the dimensions to the static content \r\n" + exception);
						_log.Error("Something went wrong when migrating the screenshot to the static content", exception);
					}
				}
			}
		}
	}
}