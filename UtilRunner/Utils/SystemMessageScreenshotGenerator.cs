﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;
using SYW.App.Messsages.Domain;
using log4net;
using MongoDB.Driver.Linq;
using SYW.App.Messsages.Domain.Feeds;

namespace UtilRunner.Utils
{
	public interface ISystemMessageScreenshotGenerator : IUtil {}

	public class SystemMessageScreenshotGenerator : ISystemMessageScreenshotGenerator
	{
		private readonly IMongoStorage<MessageDto,ObjectId> _messagesStorage;
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;
		private readonly IBauScreenshotsFilesService _bauScreenshotsFilesService;
		private readonly IBauEmailImageGeneratorSettings _bauEmailImageGeneratorSettings;
		private readonly ILog _log;
		private readonly ITumbnailCreatorService _tumbnailCreatorService;

		public SystemMessageScreenshotGenerator(IMongoStorage<MessageDto, ObjectId> messagesStorage, IBauScreenshotsFilesService bauScreenshotsFilesService, IMongoStorage<ConversationDto, ObjectId> conversationStorage, ILog log, IBauEmailImageGeneratorSettings bauEmailImageGeneratorSettings, ITumbnailCreatorService tumbnailCreatorService)
		{
			_messagesStorage = messagesStorage;
			_bauScreenshotsFilesService = bauScreenshotsFilesService;
			_conversationStorage = conversationStorage;
			_log = log;
			_bauEmailImageGeneratorSettings = bauEmailImageGeneratorSettings;
			_tumbnailCreatorService = tumbnailCreatorService;
		}

        public void Run(string[] args)
		{
			var filter = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, true);
			var orderBy = Builders<ConversationDto>.Sort.Descending(x=>x.StartDate);
			var projection = Builders<ConversationDto>.Projection.Include(x => x._id);
			var systemConversationsBulks = _conversationStorage.SelectAs(filter,projection, orderBy).Select(x=>x["_id"].AsObjectId);

			foreach (var systemConversations in systemConversationsBulks.Bulks(5))
			{

				var filter2 = Builders<MessageDto>.Filter.In("ConversationId", systemConversations);
				var allMessages = _messagesStorage.GetList(filter2);

				foreach (var messages in allMessages.Bulks(5))
				{
					var unexistingScreenshots = GetUnexistingScreenshots(messages);

					if (unexistingScreenshots.IsNullOrEmpty())
						continue;

					var phantomJsParameters = string.Join(" ", unexistingScreenshots.ToArray());

					try
					{
						var errors = RenderMessagesEmailUsingPhantomJs(phantomJsParameters);

						if (!string.IsNullOrEmpty(errors))
						{
							Console.WriteLine("Something went wrong when calling phantomjs with " + phantomJsParameters + "\r\n" + errors);
							_log.Error("Something went wrong when calling phantomjs with " + phantomJsParameters + "\r\n" + errors);
							continue;
						}

						foreach (var message in messages)
						{
							try
							{
								MigrateScreenshotToStaticContent(message);
							}
							catch (Exception exception)
							{
								Console.WriteLine("Something went wrong when migrating the screenshot to the static content \r\n" + exception);
								_log.Error("Something went wrong when migrating the screenshot to the static content", exception);
							}
						}
					}
					catch (Exception exception)
					{
						_log.Error("Something went wrong when calling phantomjs with " + phantomJsParameters, exception);
					}
				}
			}
		}

		private string RenderMessagesEmailUsingPhantomJs(string phantomJsParameters)
		{
		    var bla = @"C:\developments\projects\messages\UtilRunner\PhantomJs";

            var info = new ProcessStartInfo(Path.Combine(_bauEmailImageGeneratorSettings.PhatomJsPath, "phantomjs.exe"), "--ssl-protocol=any render_bauemails.js " + _bauEmailImageGeneratorSettings.SystemMessageImageRenderingUrl + " " + phantomJsParameters)
							{
								RedirectStandardInput = true,
								RedirectStandardOutput = true,
								RedirectStandardError = true,
								UseShellExecute = false,
								CreateNoWindow = true,
								WorkingDirectory = _bauEmailImageGeneratorSettings.PhatomJsPath
							};

			var p = Process.Start(info);
			p.Start();
			p.WaitForExit();

			var errors = p.StandardError.ReadToEnd();
			return errors;
		}

		private string[] GetUnexistingScreenshots(IList<MessageDto> messages)
		{
			var unexistingScreenshots = messages.Where(x => !_bauScreenshotsFilesService.MessageScreenShotExists(x._id.ToString()))
												.Select(b => b._id.ToString())
												.ToArray();

			return unexistingScreenshots;
		}

		private void MigrateScreenshotToStaticContent(MessageDto message)
		{
			var screenshot = new FileInfo(_bauScreenshotsFilesService.GetBauScreenshotPath(message._id.ToString()));

			if (!screenshot.Exists)
			{
				Console.WriteLine("File {0} doesn't exist", screenshot.FullName);
				// Write to log and continue
				return;
			}
			
			var resizedThumbnail = _tumbnailCreatorService.CreateThumbnail(screenshot.FullName, _bauEmailImageGeneratorSettings.ThumbnailWidth);
			if (resizedThumbnail != null)
			{
				try
				{
					Console.WriteLine("Copying resized image at {0} to {1}", screenshot.FullName, _bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(message._id.ToString()));
					resizedThumbnail.Save(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(message._id.ToString()));
					resizedThumbnail.Dispose();
				}
				catch 
				{
					Console.WriteLine("Error while trying to save resized image - {0}", screenshot.FullName);
					resizedThumbnail.Dispose();
				}
			}
			else
			{
				Console.WriteLine("Error while trying to resize {0}", screenshot.FullName);
				Console.WriteLine("Copying Original {0} to {1}", screenshot.FullName, _bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(message._id.ToString()));
				screenshot.CopyTo(_bauScreenshotsFilesService.GetBauStaticContentScreenshotPath(message._id.ToString()), true);
			}
		}
	}
}