﻿using SYW.App.Messsages.Domain.Feeds;

namespace UtilRunner.Utils
{
	public interface ISystemMessagesPublisher : IUtil
	{
	}

	public class SystemMessagesPublisher : ISystemMessagesPublisher
	{
		private readonly SYW.App.Messsages.Domain.Feeds.ISystemMessagesPublisher _systemMessagesPublisher;

		public SystemMessagesPublisher(SYW.App.Messsages.Domain.Feeds.ISystemMessagesPublisher systemMessagesPublisher)
		{
			_systemMessagesPublisher = systemMessagesPublisher;
		}

        public void Run(string[] args)
		{
			_systemMessagesPublisher.Publish();
		}
	}
}