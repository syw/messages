﻿using System;
using System.Collections.Generic;
using System.Linq;
using Mongo;
using MongoDB.Bson;
using MongoDB.Driver;
using SYW.App.Messsages.Domain.DataAccess.Conversations;
using SYW.App.Messsages.Domain.DataAccess.Messages;

namespace UtilRunner.Utils
{
	public interface IUserMessageAggregator : IUtil {}

	public class UserMessageAggregator : IUserMessageAggregator
	{
		private readonly IMongoStorage<UserConversationStatus,ObjectId> _userConversationStatusStorage;
		private readonly IMongoStorage<MessageDto, ObjectId> _messagesStorage;
		private readonly IMongoStorage<ConversationDto, ObjectId> _conversationStorage;

		public List<ConversationDto> AllUserConversations { get; set; }

		public UserMessageAggregator(
			IMongoStorage<ConversationDto, ObjectId> conversationStorage,
			IMongoStorage<MessageDto, ObjectId> messagesStorage,
			IMongoStorage<UserConversationStatus, ObjectId> userConversationStatusStorage)
		{
			_conversationStorage = conversationStorage;
			_messagesStorage = messagesStorage;
			_userConversationStatusStorage = userConversationStatusStorage;
		}

        public void Run(string[] args)
		{
			Console.Clear();
			Console.WriteLine("Start Migrating User Conversations");
			var filter = Builders<ConversationDto>.Filter.Eq(x => x.IsSystemMessage, false);

			AllUserConversations = _conversationStorage.GetList(filter);

			Console.WriteLine("Total of " + AllUserConversations.Count + " Conversations");

			while (AllUserConversations.Count > 1)
			{
				var allConversationsIterator = AllUserConversations.Count - 1;

				var conversation = AllUserConversations[allConversationsIterator];

				Console.WriteLine("Migrating Conversation #" + conversation._id);

				var conversationsToMerge = new HashSet<ConversationDto>(AllUserConversations.Where(x => x.ParticipantsIds.OrderBy(i => i.Pid).SequenceEqual(conversation.ParticipantsIds.OrderBy(n => n.Pid))));

				if (conversationsToMerge.Count <= 1)
				{
					AllUserConversations.Remove(conversation);
					continue;
				}

				Console.WriteLine("Conversations to merge: " + conversationsToMerge.Count);

				var mainConversation = conversationsToMerge.OrderBy(x => x.StartDate).Last();

				foreach (var conversationDto in conversationsToMerge.Where(conversationDto => conversationDto._id != mainConversation._id))
				{
					MoveMessagesToMainConversation(conversationDto._id, mainConversation._id);
					DeleteConversation(conversationDto);
				}
			}
			Console.WriteLine("Finished Migrating User Conversations!");
		}

		private void DeleteConversation(ConversationDto conversation)
		{
			var filter = Builders<UserConversationStatus>.Filter.Eq(x => x.ConversationId, conversation._id);
			var projection = Builders<UserConversationStatus>.Projection.Include(x => x.ConversationId);
			var conversationStatuses = _userConversationStatusStorage.SelectAs(filter,projection).Select(x=>x["_id"].AsObjectId);

			var userConversationStatusStorageDelete = Builders<UserConversationStatus>.Filter.In("_id",conversationStatuses);
			_userConversationStatusStorage.Remove(userConversationStatusStorageDelete);

			var conversationStorageRemove = Builders<ConversationDto>.Filter.Eq(x => x._id, conversation._id);
			_conversationStorage.Remove(conversationStorageRemove);
			AllUserConversations.Remove(conversation);
		}

		private void MoveMessagesToMainConversation(ObjectId sourceConversationId, ObjectId targetConversationId)
		{
			var filter = Builders<MessageDto>.Filter.Eq(x => x.ConversationId, sourceConversationId);
			var messages = _messagesStorage.GetList(filter);

			foreach (var message in messages)
			{
				message.ConversationId = targetConversationId;
				_messagesStorage.Upsert(message);
			}
		}
	}
}