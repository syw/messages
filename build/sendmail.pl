%mailparams = (
	Smtp => shift(@ARGV),
	To => shift(@ARGV),
	From => shift(@ARGV),
	Cc => shift(@ARGV),
	Subject => shift(@ARGV),
	Message => "@ARGV"
);

$mailparams{'Message'} =~ s|([^\\])\\n|$1\n|g;

%mailcfg = (
    'smtp'    => [ qw( localhost ) ],
    'from'    => '', # default sender e-mail, used when no From header in mail
    'mime'    => 1, # use MIME encoding by default
    'retries' => 1, # number of retries on smtp connect failure
    'delay'   => 1, # delay in seconds between retries
    'tz'      => '', # only to override automatic detection
    'port'    => 25, # change it if you always use a non-standard port
    'debug'   => 0 # prints stuff to STDERR
);

# *******************************************************************

use Socket;
use Time::Local; # for automatic time zone detection
use Sys::Hostname; # for use of hostname in HELO

# use MIME::QuotedPrint if available and configured in %mailcfg
eval("use MIME::QuotedPrint");
$mailcfg{'mime'} &&= (!$@);

# regex for e-mail addresses where full=$1, user=$2, domain=$3
# see pod documentation about this regex

my $word_rx = '[\x21\x23-\x27\x2A-\x2B\x2D\x2F\w\x3D\x3F]+';
my $user_rx = $word_rx         # valid chars
             .'(?:\.' . $word_rx . ')*' # possibly more words preceded by a dot
             ;
my $dom_rx = '\w[-\w]*(?:\.\w[-\w]*)*'; # less valid chars in domain names
my $ip_rx = '\[\d{1,3}(?:\.\d{1,3}){3}\]';

$address_rx = '((' . $user_rx . ')\@(' . $dom_rx . '|' . $ip_rx . '))';
; # v. 0.61

sendmail(%mailparams) or die $error;

exit 0;

sub time_to_date {
    # convert a time() value to a date-time string according to RFC 822

    my $time = $_[0] || time(); # default to now if no argument

    my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
    my @wdays  = qw(Sun Mon Tue Wed Thu Fri Sat);

    my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst)
        = localtime($time);

    my $TZ = $mailcfg{'tz'};
    if ( $TZ eq "" ) {
        # offset in hours
        my $offset  = sprintf "%.1f", (timegm(localtime) - time) / 3600;
        my $minutes = sprintf "%02d", abs( $offset - int($offset) ) * 60;
        $TZ  = sprintf("%+03d", int($offset)) . $minutes;
    }
    return join(" ",
                    ($wdays[$wday] . ','),
                     $mday,
                     $months[$mon],
                     $year+1900,
                     sprintf("%02d:%02d:%02d", $hour, $min, $sec),
                     $TZ
               );
} # end sub time_to_date

sub sendmail {

    $error = '';
    $log = "Mail::Sendmail v. $VERSION - "    . scalar(localtime()) . "\n";

    my $CRLF = "\015\012";
    local $/ = $CRLF;
    local $\ = ''; # to protect us from outside settings
    local $_;

    my (%mail, $k,
        $smtp, $server, $port, $connected, $localhost,
        $fromaddr, $recip, @recipients, $to, $header,
       );

    # -------- a few internal subs ----------
    sub fail {
        # things to do before returning a sendmail failure
        print STDERR @_ if $^W;
        $error .= join(" ", @_) . "\n";
        close S;
        return 0;
    }

    sub socket_write {
        my $i;
        for $i (0..$#_) {
            # accept references, so we don't copy potentially big data
            my $data = ref($_[$i]) ? $_[$i] : \$_[$i];
            if ($mailcfg{'debug'} > 5) {
                if (length($$data) < 500) {
                    print ">", $$data;
                }
                else {
                    print "> [...", length($$data), " bytes sent ...]\n";
                }
            }
            print(S $$data) || return 0;
        }
        1;
    }

    sub socket_read {
        my $response; # for multi-line server responses
        do {
            chomp($_ = <S>);
            print "<$_\n" if $mailcfg{'debug'} > 5;
            if (/^[45]/ or !$_) {
                return; # return false
            }
            $response .= $_;
         } while (/^[\d]+-/);
         return $response;
    }
    # -------- end of internal subs ----------

    # all config keys to lowercase, to prevent typo errors
    foreach $k (keys %mailcfg) {
        if ($k =~ /[A-Z]/) {
            $mailcfg{lc($k)} = $mailcfg{$k};
        }
    }

    # redo mail hash, arranging keys case etc...
    while (@_) {
        $k = shift @_;
        if (!$k and $^W) {
            warn "Received false mail hash key: \'$k\'. Did you forget to put it in quotes?\n";
        }

        # arrange keys case
        $k = ucfirst lc($k);

        $k =~ s/\s*:\s*$//o; # kill colon (and possible spaces) at end, we add it later.
        # uppercase also after "-", so people don't complain that headers case is different
        # than in Outlook.
        $k =~ s/-(.)/"-" . uc($1)/ge;
        $mail{$k} = shift @_;
    }

    $smtp = $mail{'Smtp'} || $mail{'Server'};
    unshift @{$mailcfg{'smtp'}}, $smtp if ($smtp and $mailcfg{'smtp'}->[0] ne $smtp);

    # delete non-header keys, so we don't send them later as mail headers
    # I like this syntax, but it doesn't seem to work with AS port 5.003_07:
    # delete @mail{'Smtp', 'Server'};
    # so instead:
    delete $mail{'Smtp'}; delete $mail{'Server'};

    $mailcfg{'port'} = $mail{'Port'} || $mailcfg{'port'} || 25;
    delete $mail{'Port'};

    {    # don't warn for undefined values below
        local $^W = 0;
        $mail{'Message'} = join("", $mail{'Message'}, $mail{'Body'}, $mail{'Text'});
    }

    # delete @mail{'Body', 'Text'};
    delete $mail{'Body'}; delete $mail{'Text'};

    # Extract 'From:' e-mail address to use as envelope sender

    $fromaddr = $mail{'Sender'} || $mail{'From'} || $mailcfg{'from'};
    delete $mail{'Sender'};
    unless ($fromaddr =~ /$address_rx/) {
        return fail("Bad or missing From address: \'$fromaddr\'");
    }
    $fromaddr = $1;

    # add Date header if needed
    $mail{Date} ||= time_to_date() ;
    $log .= "Date: $mail{Date}\n";

    # cleanup message, and encode if needed
    $mail{'Message'} =~ s/\r\n/\n/go;     # normalize line endings, step 1 of 2 (next step after MIME encoding)

    $mail{'Mime-Version'} ||= '1.0';
    $mail{'Content-Type'} ||= 'text/plain; charset="iso-8859-1"';

    unless ( $mail{'Content-Transfer-Encoding'}
          || $mail{'Content-Type'} =~ /multipart/io )
    {
        if ($mailcfg{'mime'}) {
            $mail{'Content-Transfer-Encoding'} = 'quoted-printable';
            $mail{'Message'} = encode_qp($mail{'Message'});
        }
        else {
            $mail{'Content-Transfer-Encoding'} = '8bit';
            if ($mail{'Message'} =~ /[\x80-\xFF]/o) {
                $error .= "MIME::QuotedPrint not present!\nSending 8bit characters, hoping it will come across OK.\n";
                warn "MIME::QuotedPrint not present!\n",
                     "Sending 8bit characters without encoding, hoping it will come across OK.\n"
                     if $^W;
            }
        }
    }

    $mail{'Message'} =~ s/^\./\.\./gom;     # handle . as first character
    $mail{'Message'} =~ s/\n/$CRLF/go; # normalize line endings, step 2.

    # Get recipients
    {    # don't warn for undefined values below
        local $^W = 0;
        $recip = join(", ", $mail{To}, $mail{Cc}, $mail{Bcc});
    }

    delete $mail{'Bcc'};

    @recipients = ();
    while ($recip =~ /$address_rx/go) {
        push @recipients, $1;
    }
    unless (@recipients) {
        return fail("No recipient!")
    }

    # get local hostname for polite HELO
    $localhost = hostname() || 'localhost';

    foreach $server ( @{$mailcfg{'smtp'}} ) {
        # open socket needs to be inside this foreach loop on Linux,
        # otherwise all servers fail if 1st one fails !??! why?
        unless ( socket S, AF_INET, SOCK_STREAM, scalar(getprotobyname 'tcp') ) {
            return fail("socket failed ($!)")
        }

        print "- trying $server\n" if $mailcfg{'debug'} > 1;

        $server =~ s/\s+//go; # remove spaces just in case of a typo
        # extract port if server name like "mail.domain.com:2525"
        $port = ($server =~ s/:(\d+)$//o) ? $1 : $mailcfg{'port'};
        $smtp = $server; # save $server for use outside foreach loop

        my $smtpaddr = inet_aton $server;
        unless ($smtpaddr) {
            $error .= "$server not found\n";
            next; # next server
        }

        my $retried = 0; # reset retries for each server
        while ( ( not $connected = connect S, pack_sockaddr_in($port, $smtpaddr) )
            and ( $retried < $mailcfg{'retries'} )
              ) {
            $retried++;
            $error .= "connect to $server failed ($!)\n";
            print "- connect to $server failed ($!)\n" if $mailcfg{'debug'} > 1;
            print "retrying in $mailcfg{'delay'} seconds...\n" if $mailcfg{'debug'} > 1;
            sleep $mailcfg{'delay'};
        }

        if ( $connected ) {
            print "- connected to $server\n" if $mailcfg{'debug'} > 3;
            last;
        }
        else {
            $error .= "connect to $server failed\n";
            print "- connect to $server failed, next server...\n" if $mailcfg{'debug'} > 1;
            next; # next server
        }
    }

    unless ( $connected ) {
        return fail("connect to $smtp failed ($!) no (more) retries!")
    };

    {
        local $^W = 0; # don't warn on undefined variables
        # Add info to log variable
        $log .= "Server: $smtp Port: $port\n"
              . "From: $fromaddr\n"
              . "Subject: $mail{Subject}\n"
              . "To: ";
    }

    my($oldfh) = select(S); $| = 1; select($oldfh);

    socket_read()
        || return fail("Connection error from $smtp on port $port ($_)");
    socket_write("HELO $localhost$CRLF")
        || return fail("send HELO error");
    socket_read()
        || return fail("HELO error ($_)");
    socket_write("MAIL FROM: <$fromaddr>$CRLF")
        || return fail("send MAIL FROM: error");
    socket_read()
        || return fail("MAIL FROM: error ($_)");

    foreach $to (@recipients) {
        socket_write("RCPT TO: <$to>$CRLF")
            || return fail("send RCPT TO: error");
        socket_read()
            || return fail("RCPT TO: error ($_)");
        $log .= "$to\n    ";
    }

    # start data part

    socket_write("DATA$CRLF")
        || return fail("send DATA error");
    socket_read()
        || return fail("DATA error ($_)");

    # print headers
    foreach $header (keys %mail) {
        next if $header eq "Message";
        $mail{$header} =~ s/\s+$//o; # kill possible trailing garbage
        socket_write("$header: $mail{$header}$CRLF")
            || return fail("send $header: error");
    };

    #- test diconnecting from network here, to see what happens
    #- print STDERR "DISCONNECT NOW!\n";
    #- sleep 4;
    #- print STDERR "trying to continue, expecting an error... \n";

    # send message body (passed as a reference, in case it's big)
    socket_write($CRLF, \$mail{'Message'}, "$CRLF.$CRLF")
           || return fail("send message error");
    socket_read()
        || return fail("message transmission error ($_)");
    $log .= "\nResult: $_";

    # finish
    socket_write("QUIT$CRLF")
           || return fail("send QUIT error");
    socket_read();
    close S;

    return 1;
} # end sub sendmail
