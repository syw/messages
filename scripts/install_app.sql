# main installation
INSERT INTO apps (`Id`, `Name`, `ContentBaseUrl`, `Secret`, `AppLinkImageUrl`, `FullCanvasMinWidth`)
VALUES (144, 'Messages', 'helloapps.apphb.com/mailbox', '0734E51010E94f5d8260366743FBCA9C', 'http://helloapps.apphb.com/content/appIcon.png', 800);

# required permissions
INSERT INTO app_permissions (`appId`, `permission`)
VALUES (144, 1);
INSERT INTO app_permissions (`appId`, `permission`)
VALUES (144, 5);

# install for specific user (this is for user #25 - change accordingly)
INSERT INTO apps_users (`appid`, `userid`, `joindate`)
VALUES (144, 25, SYSDATE());
INSERT INTO app_links (`appid`, `entityid`, `title`, `url`)
VALUES (144, 25, 'Messages', '/app/144/r/');

# after this, you need to go to '.../app/144/post-login' to complete installation
