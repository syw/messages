@echo off

set SCRIPT_PATH=%~dp0

set PACKAGE_ROOT=%SCRIPT_PATH%..
set APPCMD_TOOL=%SystemRoot%\system32\inetsrv\APPCMD
set WWWROOT=c:\Shavrir\Websites\MessagesApp

echo %TIME% - Deploying %PACKAGE_ROOT% ...

:copyWeb
%APPCMD_TOOL% stop site /site.name:MessagesApp
%APPCMD_TOOL% stop apppool /apppool.name:MessagesApp

echo %TIME% - Update Web application MessagesApp ...
if exist %WWWROOT% rmdir /q /s %WWWROOT%
mkdir %WWWROOT%
xcopy /e/y %PACKAGE_ROOT%\web\* %WWWROOT%

%APPCMD_TOOL% start apppool /apppool.name:MessagesApp
%APPCMD_TOOL% start site /site.name:MessagesApp

net start MongoDB 2> nul

:done
echo %TIME% - Done

